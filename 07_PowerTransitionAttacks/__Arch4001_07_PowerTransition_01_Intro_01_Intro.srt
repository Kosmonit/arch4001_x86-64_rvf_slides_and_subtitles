1
00:00:00,560 --> 00:00:04,880
so at last we arrive at the

2
00:00:02,399 --> 00:00:07,040
long-promised s3 vulnerabilities known

3
00:00:04,880 --> 00:00:08,559
for being a possible opportunity to

4
00:00:07,040 --> 00:00:11,599
compromise some of these other things

5
00:00:08,559 --> 00:00:13,840
like the flash protection and the smm

6
00:00:11,599 --> 00:00:17,039
protection to be discussed later is to

7
00:00:13,840 --> 00:00:18,960
be discussed now so back in 2013 at

8
00:00:17,039 --> 00:00:21,279
mitre while we were doing security

9
00:00:18,960 --> 00:00:22,800
research on bios vulnerabilities we were

10
00:00:21,279 --> 00:00:25,519
running a tool we had developed called

11
00:00:22,800 --> 00:00:27,840
copernicus which both did a dump of the

12
00:00:25,519 --> 00:00:29,199
bios and also checked the security

13
00:00:27,840 --> 00:00:31,840
relevant bits

14
00:00:29,199 --> 00:00:34,000
and sam had noticed that there was

15
00:00:31,840 --> 00:00:36,079
actually a situation where sometimes

16
00:00:34,000 --> 00:00:38,320
some models according to the initial

17
00:00:36,079 --> 00:00:40,000
copernicus checks those models were just

18
00:00:38,320 --> 00:00:42,800
you know fine and fully protected and

19
00:00:40,000 --> 00:00:44,719
there was no problem but sometimes they

20
00:00:42,800 --> 00:00:46,480
would actually show up in our full data

21
00:00:44,719 --> 00:00:48,640
from you know analyzing all the internal

22
00:00:46,480 --> 00:00:50,320
systems sometimes the same model would

23
00:00:48,640 --> 00:00:52,480
show up as vulnerable for no particular

24
00:00:50,320 --> 00:00:54,480
reason eventually we tracked it down and

25
00:00:52,480 --> 00:00:56,559
found that it was caused by going to

26
00:00:54,480 --> 00:00:58,320
sleep and then waking up so if someone

27
00:00:56,559 --> 00:01:00,399
closed their laptop lid and then opened

28
00:00:58,320 --> 00:01:02,079
their laptop lid it would show up as

29
00:01:00,399 --> 00:01:03,760
vulnerable so we didn't really know what

30
00:01:02,079 --> 00:01:05,600
was up with this we filed for a certain

31
00:01:03,760 --> 00:01:08,560
vulnerability number we reported it to

32
00:01:05,600 --> 00:01:10,479
dell tell said thanks they fixed it

33
00:01:08,560 --> 00:01:12,320
but they never exactly told us how they

34
00:01:10,479 --> 00:01:13,600
fixed it or where it all applied and

35
00:01:12,320 --> 00:01:15,600
things like that

36
00:01:13,600 --> 00:01:17,119
so we didn't really dig into this

37
00:01:15,600 --> 00:01:19,040
further we had a whole bunch of other

38
00:01:17,119 --> 00:01:21,040
vulnerabilities that we fully understood

39
00:01:19,040 --> 00:01:22,880
what was happening so this one kind of

40
00:01:21,040 --> 00:01:25,759
got lost for a bit

41
00:01:22,880 --> 00:01:27,680
now in 2015 after the main sleep wake

42
00:01:25,759 --> 00:01:30,079
vulnerability had been found pedro

43
00:01:27,680 --> 00:01:32,079
velaka noticed something very similar to

44
00:01:30,079 --> 00:01:33,520
the previous snorlax vulnerability

45
00:01:32,079 --> 00:01:34,560
occurring on

46
00:01:33,520 --> 00:01:36,720
max

47
00:01:34,560 --> 00:01:38,560
so this was a situation where again if

48
00:01:36,720 --> 00:01:39,920
the mac was put to sleep and woken up

49
00:01:38,560 --> 00:01:42,079
all of a sudden a bunch of the security

50
00:01:39,920 --> 00:01:44,320
bits seem to be unlocked

51
00:01:42,079 --> 00:01:45,759
now kitty moe named this prince harming

52
00:01:44,320 --> 00:01:48,320
instead of prince charming because of

53
00:01:45,759 --> 00:01:50,720
the poison kiss waking you up from sleep

54
00:01:48,320 --> 00:01:52,960
and this was a thing where pedro

55
00:01:50,720 --> 00:01:55,920
unfortunately decided to

56
00:01:52,960 --> 00:01:57,600
accidentally zero day apple because he

57
00:01:55,920 --> 00:01:59,520
had seen that we were going to be

58
00:01:57,600 --> 00:02:01,600
talking about apple systems applicable

59
00:01:59,520 --> 00:02:03,920
to this sleep attack that had been

60
00:02:01,600 --> 00:02:05,840
presented the previous december

61
00:02:03,920 --> 00:02:07,920
december 2014

62
00:02:05,840 --> 00:02:09,200
and he just assumed well you know they

63
00:02:07,920 --> 00:02:11,360
must be presenting the same thing

64
00:02:09,200 --> 00:02:13,599
happens on max and hey look at this i

65
00:02:11,360 --> 00:02:16,319
detected it too i found it too

66
00:02:13,599 --> 00:02:17,840
but uh of course if he found it and he

67
00:02:16,319 --> 00:02:19,440
knew that this you know he mentioned

68
00:02:17,840 --> 00:02:20,879
explicitly in his blog post that he knew

69
00:02:19,440 --> 00:02:23,040
we were going to be talking about this

70
00:02:20,879 --> 00:02:24,160
later on at def con well then there's

71
00:02:23,040 --> 00:02:26,560
kind of

72
00:02:24,160 --> 00:02:28,480
no real justification for just trying to

73
00:02:26,560 --> 00:02:30,000
jump ahead and say i found this too but

74
00:02:28,480 --> 00:02:32,720
i know that there's no patch available

75
00:02:30,000 --> 00:02:34,000
yet so anyways he assumed that what we

76
00:02:32,720 --> 00:02:36,000
were going to be talking about at the

77
00:02:34,000 --> 00:02:39,200
conference was what he found but it was

78
00:02:36,000 --> 00:02:40,720
not he had found a new vulnerability and

79
00:02:39,200 --> 00:02:42,879
so basically this particular

80
00:02:40,720 --> 00:02:44,720
vulnerability didn't occur on the models

81
00:02:42,879 --> 00:02:47,519
that i had in my personal possession

82
00:02:44,720 --> 00:02:49,280
which is why i hadn't found it either

83
00:02:47,519 --> 00:02:51,440
anyways what does this all mean how can

84
00:02:49,280 --> 00:02:52,879
it be the case that systems just go to

85
00:02:51,440 --> 00:02:55,120
sleep and then wake up in a vulnerable

86
00:02:52,879 --> 00:02:56,720
state well that's what we need to

87
00:02:55,120 --> 00:02:58,640
investigate how sleeping works to

88
00:02:56,720 --> 00:03:00,959
understand so we're going to start out

89
00:02:58,640 --> 00:03:03,280
our s3 attack tree and the attacker

90
00:03:00,959 --> 00:03:06,720
wants to write malware to sm ram or spy

91
00:03:03,280 --> 00:03:09,040
flash and to do that they can just see

92
00:03:06,720 --> 00:03:11,360
if they go to sleep and wake up and if

93
00:03:09,040 --> 00:03:13,440
it's magically unlocked so that's the

94
00:03:11,360 --> 00:03:15,680
data that you could derive from those

95
00:03:13,440 --> 00:03:17,920
two examples but once we understand more

96
00:03:15,680 --> 00:03:20,400
about how the locks work and we can

97
00:03:17,920 --> 00:03:21,680
understand what exactly is going on

98
00:03:20,400 --> 00:03:23,599
so to understand that we have to

99
00:03:21,680 --> 00:03:25,120
understand acpi

100
00:03:23,599 --> 00:03:28,080
advanced configuration and power

101
00:03:25,120 --> 00:03:30,720
interface this was the 1996 era

102
00:03:28,080 --> 00:03:32,799
successor to the apm advanced power

103
00:03:30,720 --> 00:03:35,760
management which was the technology that

104
00:03:32,799 --> 00:03:38,319
was in use for that smm port b2 stuff

105
00:03:35,760 --> 00:03:40,879
that we saw before eventually the spec

106
00:03:38,319 --> 00:03:43,040
came under the maintenance of the uefi

107
00:03:40,879 --> 00:03:45,280
forum around 2013

108
00:03:43,040 --> 00:03:47,519
and that's because this spec has to do

109
00:03:45,280 --> 00:03:49,280
with you know how to put the system into

110
00:03:47,519 --> 00:03:51,680
lower power states and to bring it back

111
00:03:49,280 --> 00:03:53,840
up into full power states and always

112
00:03:51,680 --> 00:03:55,920
this basically involves the

113
00:03:53,840 --> 00:03:59,120
coordination between the operating

114
00:03:55,920 --> 00:04:00,959
system and firmware so acpi defines a

115
00:03:59,120 --> 00:04:03,599
few different power states

116
00:04:00,959 --> 00:04:05,439
s0 is the main active one that you

117
00:04:03,599 --> 00:04:06,400
normally think of as your computer just

118
00:04:05,439 --> 00:04:09,360
working

119
00:04:06,400 --> 00:04:11,680
s3 is sleep so that's typically if you

120
00:04:09,360 --> 00:04:13,920
just you know say literally sleep on

121
00:04:11,680 --> 00:04:15,280
some particular system or you know says

122
00:04:13,920 --> 00:04:17,280
low power mode

123
00:04:15,280 --> 00:04:21,759
close the lid that's typically going to

124
00:04:17,280 --> 00:04:24,080
go into s3 sleep s4 is then hibernate or

125
00:04:21,759 --> 00:04:26,080
deep sleep that is the situation in

126
00:04:24,080 --> 00:04:28,400
which you basically store all of your

127
00:04:26,080 --> 00:04:30,880
ram contents to the hard drive and then

128
00:04:28,400 --> 00:04:33,759
you restore the ram contents after you

129
00:04:30,880 --> 00:04:36,400
wake up so that's much slower than s3

130
00:04:33,759 --> 00:04:38,000
sleep s3 sleep the most of the stuff is

131
00:04:36,400 --> 00:04:41,280
powered down but the ram is still

132
00:04:38,000 --> 00:04:43,360
refreshed so ram is capacitors needs

133
00:04:41,280 --> 00:04:45,759
power and so you just keep powering it

134
00:04:43,360 --> 00:04:48,800
and then you can have a quick refresh

135
00:04:45,759 --> 00:04:51,680
and then s5 is the notional powered off

136
00:04:48,800 --> 00:04:53,120
although it may not be 100 100 powered

137
00:04:51,680 --> 00:04:55,280
off

138
00:04:53,120 --> 00:04:58,000
they make a distinction in acpi from the

139
00:04:55,280 --> 00:05:00,800
little literal mechanical disconnect of

140
00:04:58,000 --> 00:05:02,880
power versus the f5 power state and then

141
00:05:00,800 --> 00:05:05,360
there's some additional states which are

142
00:05:02,880 --> 00:05:07,840
sort of uh in between states not quite

143
00:05:05,360 --> 00:05:09,840
as low power as s3 and consequently will

144
00:05:07,840 --> 00:05:12,160
allow you to resume from this lower

145
00:05:09,840 --> 00:05:14,240
power state much faster so for instance

146
00:05:12,160 --> 00:05:17,199
modern macs don't actually use s3 they

147
00:05:14,240 --> 00:05:20,160
use s0i3

148
00:05:17,199 --> 00:05:22,960
so let's dig into this sleep power state

149
00:05:20,160 --> 00:05:25,280
oh what was that spooky

150
00:05:22,960 --> 00:05:28,080
okay so modern hardware is a bunch of

151
00:05:25,280 --> 00:05:29,840
different little chunks of hardware and

152
00:05:28,080 --> 00:05:30,960
if you dig in even deeper this is what

153
00:05:29,840 --> 00:05:32,720
it looks like on the silicon it's a

154
00:05:30,960 --> 00:05:33,520
whole bunch of different transistors

155
00:05:32,720 --> 00:05:36,080
doing a bunch of different

156
00:05:33,520 --> 00:05:38,000
functionalities so it would be nice if

157
00:05:36,080 --> 00:05:39,680
you could power off the transistors that

158
00:05:38,000 --> 00:05:42,160
are not actually in use that would give

159
00:05:39,680 --> 00:05:43,919
you much longer battery life your cpu

160
00:05:42,160 --> 00:05:45,759
would not be you know consuming so much

161
00:05:43,919 --> 00:05:47,919
power all the time and indeed in one of

162
00:05:45,759 --> 00:05:50,240
intel's books about active management

163
00:05:47,919 --> 00:05:52,080
technology what we now know as the

164
00:05:50,240 --> 00:05:53,919
management engine or converged security

165
00:05:52,080 --> 00:05:56,479
and management engine they talked about

166
00:05:53,919 --> 00:05:58,240
the benefit of amt being able to

167
00:05:56,479 --> 00:05:59,199
continue to execute in lower power

168
00:05:58,240 --> 00:06:00,800
states

169
00:05:59,199 --> 00:06:03,039
so back then it was a northbridge

170
00:06:00,800 --> 00:06:06,000
southbridge architecture

171
00:06:03,039 --> 00:06:08,240
gmch global memory controller hub and io

172
00:06:06,000 --> 00:06:10,080
controller hub so consider everything

173
00:06:08,240 --> 00:06:13,039
powered on it's obviously using the most

174
00:06:10,080 --> 00:06:14,960
power possible now if it goes into s3

175
00:06:13,039 --> 00:06:16,160
sleep so you close the lid and the cpu

176
00:06:14,960 --> 00:06:18,479
powers down

177
00:06:16,160 --> 00:06:21,199
the cpu can power down but they can

178
00:06:18,479 --> 00:06:23,600
still selectively keep some chunks of

179
00:06:21,199 --> 00:06:25,680
hardware still powered on so most of the

180
00:06:23,600 --> 00:06:27,919
ram can go into just self refresh mode

181
00:06:25,680 --> 00:06:30,000
so it's using less power and then just a

182
00:06:27,919 --> 00:06:31,919
little bit of the ram can be used by the

183
00:06:30,000 --> 00:06:35,919
intel management engine it can stay

184
00:06:31,919 --> 00:06:37,520
awake and the whole point of intel amt

185
00:06:35,919 --> 00:06:39,600
is to allow for essentially remote

186
00:06:37,520 --> 00:06:41,440
management remote waking things up patch

187
00:06:39,600 --> 00:06:43,600
them let them go back to sleep

188
00:06:41,440 --> 00:06:45,360
so there can be chunks of hardware that

189
00:06:43,600 --> 00:06:47,039
are still powered on in lower power

190
00:06:45,360 --> 00:06:51,360
states so they make a distinction

191
00:06:47,039 --> 00:06:54,639
between s3 m1 so management engine is on

192
00:06:51,360 --> 00:06:56,560
and s3 m off where you do go all the way

193
00:06:54,639 --> 00:06:58,960
down into s3 sleep the management engine

194
00:06:56,560 --> 00:07:01,360
is also powered off and only the ram is

195
00:06:58,960 --> 00:07:04,479
in self-refresh mode so that's you know

196
00:07:01,360 --> 00:07:05,840
a very low amount of power usage

197
00:07:04,479 --> 00:07:08,160
not of low

198
00:07:05,840 --> 00:07:10,160
not as low of course as s4 where you

199
00:07:08,160 --> 00:07:11,919
don't even self refresh these you just

200
00:07:10,160 --> 00:07:13,680
store all the contents out to a hard

201
00:07:11,919 --> 00:07:16,560
drive and then you don't have to keep

202
00:07:13,680 --> 00:07:16,560
anything powered on there


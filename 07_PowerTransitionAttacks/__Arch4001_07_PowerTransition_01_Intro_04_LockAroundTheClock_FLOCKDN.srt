1
00:00:00,14 --> 00:00:00,5
All right

2
00:00:00,5 --> 00:00:02,86
Here's another register we saw before that is used to

3
00:00:02,86 --> 00:00:04,78
lock down the system flock down

4
00:00:05,12 --> 00:00:08,5
That is used to protect the protected range registers from

5
00:00:08,5 --> 00:00:09,42
being rewritten

6
00:00:09,43 --> 00:00:11,22
So when does that get reset?

7
00:00:11,33 --> 00:00:14,56
While according to the documentation on the five series

8
00:00:14,74 --> 00:00:17,22
This bit can only be cleared by a hardware reset

9
00:00:17,22 --> 00:00:20,39
due to a global reset or a host partition reset

10
00:00:20,4 --> 00:00:22,36
in an intel Emmy enabled system

11
00:00:22,74 --> 00:00:24,28
And on the eight series i

12
00:00:24,28 --> 00:00:25,05
O controller hub,

13
00:00:25,05 --> 00:00:27,49
it says it a little bit better in my opinion

14
00:00:27,69 --> 00:00:31,57
says it's reset following a global reset when the host

15
00:00:31,57 --> 00:00:34,63
and any partitions are both reset or a host partition

16
00:00:34,63 --> 00:00:35,02
reset,

17
00:00:35,02 --> 00:00:38,48
which is any time platform reset is asserted either from

18
00:00:38,48 --> 00:00:40,05
a cold or a warm reset

19
00:00:40,44 --> 00:00:43,83
So what exactly are these host partition resets and global

20
00:00:43,83 --> 00:00:45,15
resets and stuff like that?

21
00:00:46,14 --> 00:00:51,81
I gotta rT FM This documentation says that a reset

22
00:00:51,81 --> 00:00:55,22
in which the host and the intel Emmy partitions of

23
00:00:55,22 --> 00:00:57,82
the platform or reset is called a global reset

24
00:00:57,99 --> 00:01:00,43
So let's go ahead and put a little note here

25
00:01:00,54 --> 00:01:04,36
Global reset is a host reset plus an Emmy reset

26
00:01:04,84 --> 00:01:07,98
or communicative lee a global reset minus

27
00:01:07,98 --> 00:01:09,69
And me reset is a host reset

28
00:01:09,69 --> 00:01:12,71
So host reset is where you're not resetting the M

29
00:01:12,71 --> 00:01:13,05
E

30
00:01:13,44 --> 00:01:17,14
All right then this documentation up here says a reset

31
00:01:17,15 --> 00:01:20,88
in which the host platform is reset and platform reset

32
00:01:20,89 --> 00:01:23,93
is asserted is called a host reset or a host

33
00:01:23,93 --> 00:01:25,05
partition reset?

34
00:01:25,74 --> 00:01:28,89
So a host reset is the host platform is reset

35
00:01:28,89 --> 00:01:30,74
and platform reset is asserted

36
00:01:30,75 --> 00:01:32,5
How many times do you think that?

37
00:01:32,5 --> 00:01:34,61
I just said reset on this slide?

38
00:01:34,62 --> 00:01:35,16
Right,

39
00:01:35,54 --> 00:01:37,26
someone counted up and put it in the,

40
00:01:37,27 --> 00:01:37,68
you know,

41
00:01:37,7 --> 00:01:40,56
put it in the forums anyways

42
00:01:40,94 --> 00:01:42,37
So we're trying to understand,

43
00:01:42,37 --> 00:01:44,47
we know that flock down comes unlocked

44
00:01:44,48 --> 00:01:47,29
Either it said basically either a host reset or a

45
00:01:47,29 --> 00:01:49,55
global reset will unlock flock down

46
00:01:49,94 --> 00:01:53,17
And so what you're really talking about here is that

47
00:01:53,18 --> 00:01:56,91
a global reset means you've got a management engine system

48
00:01:56,92 --> 00:01:59,75
and it's resetting both the host and the management agent

49
00:02:00,44 --> 00:02:00,8
No

50
00:02:00,81 --> 00:02:05,11
So the documentation doesn't say what I host partition is

51
00:02:05,13 --> 00:02:08,97
but just really took me a long time searching around

52
00:02:08,97 --> 00:02:12,25
to try to find something authoritative back in the four

53
00:02:12,25 --> 00:02:16,24
series Mosch data sheet section 1.1 terminology,

54
00:02:16,24 --> 00:02:21,15
it says host this term is used anonymously with processor

55
00:02:21,64 --> 00:02:22,16
Okay,

56
00:02:22,17 --> 00:02:25,93
so from that we can take that host bridges and

57
00:02:25,93 --> 00:02:28,11
host partitions and stuff like that

58
00:02:28,12 --> 00:02:31,99
We're going to assume that intel is talking about host

59
00:02:31,99 --> 00:02:34,18
reset is the process of reset

60
00:02:34,19 --> 00:02:36,53
And the reason why they would make this distinction is

61
00:02:36,53 --> 00:02:40,56
because the management engine can actually stay alive again in

62
00:02:40,56 --> 00:02:41,45
lower power states

63
00:02:41,45 --> 00:02:44,22
Like we saw before with that diagram out of one

64
00:02:44,22 --> 00:02:44,96
of the intel books,

65
00:02:44,96 --> 00:02:47,4
it showed that the M E was alive and running

66
00:02:47,4 --> 00:02:51,7
when S three was had powered down the CPU So

67
00:02:51,7 --> 00:02:54,27
basically there is a capability,

68
00:02:54,28 --> 00:02:54,49
you know,

69
00:02:54,49 --> 00:02:56,97
even beyond just like low power states and stuff like

70
00:02:56,97 --> 00:02:57,43
that,

71
00:02:57,45 --> 00:03:00,05
there is a capability for the CSM me to just

72
00:03:00,05 --> 00:03:00,52
reset?

73
00:03:00,52 --> 00:03:02,87
The CPU while leaving itself up and running

74
00:03:02,97 --> 00:03:05,87
And that would be basically a host reset

75
00:03:05,94 --> 00:03:07,45
That is not a global reset

76
00:03:07,84 --> 00:03:09,83
If you're like just literally restarting,

77
00:03:09,83 --> 00:03:11,72
restarting the machine that's going to be global reset,

78
00:03:11,72 --> 00:03:14,65
that will repower power off and power on this sme


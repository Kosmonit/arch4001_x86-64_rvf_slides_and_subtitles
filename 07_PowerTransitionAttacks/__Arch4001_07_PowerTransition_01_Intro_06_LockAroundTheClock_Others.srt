1
00:00:00,080 --> 00:00:02,960
all right what about these other

2
00:00:01,280 --> 00:00:05,759
registers that we've seen before things

3
00:00:02,960 --> 00:00:07,759
like tsegmb which has a lock bit but the

4
00:00:05,759 --> 00:00:10,639
lock bit doesn't say when it becomes

5
00:00:07,759 --> 00:00:12,240
unlocked neither does this msr smm

6
00:00:10,639 --> 00:00:14,240
feature control which was used for

7
00:00:12,240 --> 00:00:16,480
stopping smm from executing outside of

8
00:00:14,240 --> 00:00:18,080
smm it had a lock bit to lock it down so

9
00:00:16,480 --> 00:00:19,039
that you could say that you know it no

10
00:00:18,080 --> 00:00:21,039
one can

11
00:00:19,039 --> 00:00:23,680
turn this feature back off but it

12
00:00:21,039 --> 00:00:25,359
doesn't say when it becomes unlocked so

13
00:00:23,680 --> 00:00:27,039
how do you find out whether or not these

14
00:00:25,359 --> 00:00:29,119
things become unlocked when and where

15
00:00:27,039 --> 00:00:30,400
and everything else well you just gotta

16
00:00:29,119 --> 00:00:32,800
do experimentation

17
00:00:30,400 --> 00:00:34,960
you would do something like making a

18
00:00:32,800 --> 00:00:36,640
custom bios that you know you can infect

19
00:00:34,960 --> 00:00:38,960
or that you've you know built from

20
00:00:36,640 --> 00:00:40,320
scratch from source maybe using core

21
00:00:38,960 --> 00:00:42,320
boot like you learn about in the other

22
00:00:40,320 --> 00:00:44,399
classes and then you could you know

23
00:00:42,320 --> 00:00:47,200
power the system down bring it back up

24
00:00:44,399 --> 00:00:50,719
and then execute code on the wake from

25
00:00:47,200 --> 00:00:53,680
s3 wake from s4 awake from s5 execute

26
00:00:50,719 --> 00:00:55,760
code on those wake paths and find out

27
00:00:53,680 --> 00:00:58,640
whether or not something that was locked

28
00:00:55,760 --> 00:00:58,640
came unlocked


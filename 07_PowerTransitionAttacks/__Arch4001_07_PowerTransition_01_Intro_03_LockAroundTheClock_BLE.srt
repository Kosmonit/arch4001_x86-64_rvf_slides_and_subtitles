1
00:00:00,320 --> 00:00:04,640
now i want to talk about how some of the

2
00:00:02,480 --> 00:00:07,200
lock bits that we've seen previously in

3
00:00:04,640 --> 00:00:09,120
the class get reset at reboot time i

4
00:00:07,200 --> 00:00:11,120
want to see what the documentation says

5
00:00:09,120 --> 00:00:13,519
and what it doesn't say about how that

6
00:00:11,120 --> 00:00:15,839
happens so we're going to start with ble

7
00:00:13,519 --> 00:00:17,600
bios lock enable so we said that it you

8
00:00:15,839 --> 00:00:19,039
know if you reboot the system it'll get

9
00:00:17,600 --> 00:00:21,199
so if it's locked down it's locked down

10
00:00:19,039 --> 00:00:23,359
until a reboot but you know how exactly

11
00:00:21,199 --> 00:00:25,199
does that happen what does that mean

12
00:00:23,359 --> 00:00:26,480
so it says that you know setting this to

13
00:00:25,199 --> 00:00:29,519
one enables setting the bios right

14
00:00:26,480 --> 00:00:32,160
enable bit to cause an smi and once set

15
00:00:29,519 --> 00:00:33,680
this bit can only be cleared by a

16
00:00:32,160 --> 00:00:36,800
plittlest

17
00:00:33,680 --> 00:00:39,920
a platform reset so what is a platform

18
00:00:36,800 --> 00:00:42,960
reset well on this documentation

19
00:00:39,920 --> 00:00:46,320
which talks about platform reset it says

20
00:00:42,960 --> 00:00:49,680
the pch asserts platform reset during

21
00:00:46,320 --> 00:00:51,840
power up and when software initiates a

22
00:00:49,680 --> 00:00:55,840
hard reset sequence through the reset

23
00:00:51,840 --> 00:00:58,399
control register i o register cf9

24
00:00:55,840 --> 00:01:01,039
so let's look at cf9 reset control

25
00:00:58,399 --> 00:01:04,320
register it's just a hard-coded always

26
00:01:01,039 --> 00:01:06,400
that port so we look at that and bit2

27
00:01:04,320 --> 00:01:08,080
says that this resets the cpu but when

28
00:01:06,400 --> 00:01:09,680
we read it says this bit transitions

29
00:01:08,080 --> 00:01:12,640
from zero to one it initializes a hard

30
00:01:09,680 --> 00:01:15,119
or soft reset determined by the sys

31
00:01:12,640 --> 00:01:16,880
reset bit bit one of this register okay

32
00:01:15,119 --> 00:01:17,680
now we need to go look at assist reset

33
00:01:16,880 --> 00:01:19,360
bit

34
00:01:17,680 --> 00:01:22,159
this bit is used to determine whether

35
00:01:19,360 --> 00:01:25,360
it's a hard or soft reset if it's zero

36
00:01:22,159 --> 00:01:27,200
it's soft and if it's one it's hard so

37
00:01:25,360 --> 00:01:28,880
we're going to this soft one says it

38
00:01:27,200 --> 00:01:31,040
activates a nit we haven't seen anything

39
00:01:28,880 --> 00:01:32,159
about init and it talks about pci clocks

40
00:01:31,040 --> 00:01:33,759
so we're not going to care about that

41
00:01:32,159 --> 00:01:35,759
one we're going to say we only care

42
00:01:33,759 --> 00:01:39,360
about this hard reset

43
00:01:35,759 --> 00:01:40,400
so it says when reset cpu goes from zero

44
00:01:39,360 --> 00:01:41,920
to one

45
00:01:40,400 --> 00:01:43,840
which is right there if that goes to

46
00:01:41,920 --> 00:01:46,159
zero to one the pch performs a hard

47
00:01:43,840 --> 00:01:48,960
reset by activating platform reset and

48
00:01:46,159 --> 00:01:51,840
so stat active for a minimum of one

49
00:01:48,960 --> 00:01:54,479
millisecond in this case sleep as three

50
00:01:51,840 --> 00:01:56,719
sleep as four sleep as five state

51
00:01:54,479 --> 00:01:58,640
actually depend on the full reset bit

52
00:01:56,719 --> 00:02:00,000
okay now we need to go up here full

53
00:01:58,640 --> 00:02:02,719
reset bit

54
00:02:00,000 --> 00:02:05,280
so if this is zero it says that you know

55
00:02:02,719 --> 00:02:08,239
when this hard reset happens if it's

56
00:02:05,280 --> 00:02:10,319
zero the pch will keep those high so

57
00:02:08,239 --> 00:02:12,239
those are only active when they're low

58
00:02:10,319 --> 00:02:14,080
because it has that hash at the end so

59
00:02:12,239 --> 00:02:17,280
keeping them high means basically it's

60
00:02:14,080 --> 00:02:21,040
not asserting those whereas if it's one

61
00:02:17,280 --> 00:02:22,400
a full reset so a hard full reset will

62
00:02:21,040 --> 00:02:24,640
drive these low

63
00:02:22,400 --> 00:02:27,200
activating them for three to five

64
00:02:24,640 --> 00:02:29,360
seconds so basically it's going to force

65
00:02:27,200 --> 00:02:30,879
all of those to turn off the power to

66
00:02:29,360 --> 00:02:32,959
all of those devices for three to five

67
00:02:30,879 --> 00:02:34,959
seconds and that should really power

68
00:02:32,959 --> 00:02:37,599
down all of those things and then it

69
00:02:34,959 --> 00:02:39,519
mentions additionally if this bit is set

70
00:02:37,599 --> 00:02:42,319
so if that's set then there's some other

71
00:02:39,519 --> 00:02:44,720
resets the assist reset power okay

72
00:02:42,319 --> 00:02:46,480
watchdog timer resets which will also

73
00:02:44,720 --> 00:02:47,840
push down all of these things to kill

74
00:02:46,480 --> 00:02:49,360
the power to them so there's one more

75
00:02:47,840 --> 00:02:51,360
lock that we saw where it actually said

76
00:02:49,360 --> 00:02:52,879
explicitly in the documentation once set

77
00:02:51,360 --> 00:02:56,080
this bit can only be cleared by a

78
00:02:52,879 --> 00:02:58,800
platform reset and that is smi lock that

79
00:02:56,080 --> 00:03:01,440
was the one that has to do with locking

80
00:02:58,800 --> 00:03:03,760
in the fact that you may not disable

81
00:03:01,440 --> 00:03:06,879
smis on the system so you want to keep

82
00:03:03,760 --> 00:03:09,360
the global smi enable set to one so that

83
00:03:06,879 --> 00:03:10,239
sms are not suppressed so you set smi

84
00:03:09,360 --> 00:03:13,120
lock

85
00:03:10,239 --> 00:03:15,519
but the smi lock will go away if the

86
00:03:13,120 --> 00:03:17,360
system goes through platform reset so at

87
00:03:15,519 --> 00:03:19,599
this point basically we know that

88
00:03:17,360 --> 00:03:22,319
platform reset is some signal that is

89
00:03:19,599 --> 00:03:24,319
going to be asserted upon reset

90
00:03:22,319 --> 00:03:26,799
but that particular reset could be

91
00:03:24,319 --> 00:03:28,879
something like a reset with a hard reset

92
00:03:26,799 --> 00:03:31,280
which is going to assert platform reset

93
00:03:28,879 --> 00:03:34,959
which will also assert the sleep s3

94
00:03:31,280 --> 00:03:34,959
interest 4 and s5


1
00:00:00,320 --> 00:00:04,480
so now we're going to go down the rabbit

2
00:00:02,000 --> 00:00:07,359
hole by going down the power well

3
00:00:04,480 --> 00:00:09,360
as you may or may not know modern cpus

4
00:00:07,359 --> 00:00:12,000
are built up layer by layer of

5
00:00:09,360 --> 00:00:14,480
successive layers of silicon and oxide

6
00:00:12,000 --> 00:00:16,960
and metal and things like that and so a

7
00:00:14,480 --> 00:00:18,080
cross section of a chip these days looks

8
00:00:16,960 --> 00:00:20,560
like this

9
00:00:18,080 --> 00:00:22,960
now i am not a hardware person but there

10
00:00:20,560 --> 00:00:25,359
is a terminology that's frequently used

11
00:00:22,960 --> 00:00:27,519
in this low power states it's about

12
00:00:25,359 --> 00:00:29,119
power planes and power wells

13
00:00:27,519 --> 00:00:31,439
and like i say i'm not a hardware person

14
00:00:29,119 --> 00:00:34,239
but what i imagine is that those terms

15
00:00:31,439 --> 00:00:36,000
came into existence because effectively

16
00:00:34,239 --> 00:00:38,800
in a cross-section of a chip you can

17
00:00:36,000 --> 00:00:41,760
have a power plane being effectively a

18
00:00:38,800 --> 00:00:43,920
metal layer that links power to a bunch

19
00:00:41,760 --> 00:00:46,000
of different devices and those devices

20
00:00:43,920 --> 00:00:48,960
just have to kind of drill down

21
00:00:46,000 --> 00:00:50,960
via a well to the relevant plane and

22
00:00:48,960 --> 00:00:52,160
then they will derive power from it so i

23
00:00:50,960 --> 00:00:54,079
don't know if this is where the terms

24
00:00:52,160 --> 00:00:56,239
came from but this makes it easy for me

25
00:00:54,079 --> 00:00:58,800
as a software firmware kind of person to

26
00:00:56,239 --> 00:01:00,399
understand it so the wells are what dig

27
00:00:58,800 --> 00:01:01,520
down into the chip and connect to the

28
00:01:00,399 --> 00:01:04,400
planes

29
00:01:01,520 --> 00:01:08,080
so if for instance this power plane went

30
00:01:04,400 --> 00:01:10,080
out of usage if it was powered down then

31
00:01:08,080 --> 00:01:12,080
the hardware chunks that were connected

32
00:01:10,080 --> 00:01:14,320
via wells to that plane would turn off

33
00:01:12,080 --> 00:01:16,080
and the rest of them would stay on

34
00:01:14,320 --> 00:01:18,640
and so this starts to

35
00:01:16,080 --> 00:01:21,040
exemplify how you can think about these

36
00:01:18,640 --> 00:01:24,000
low power states these acpi low power

37
00:01:21,040 --> 00:01:26,560
states if this was for instance you know

38
00:01:24,000 --> 00:01:28,159
all the stuff that gets turned off by s3

39
00:01:26,560 --> 00:01:30,720
power state and this is all the stuff

40
00:01:28,159 --> 00:01:32,159
that gets turned off by s4 and s5 so

41
00:01:30,720 --> 00:01:34,079
like only the stuff that's connected

42
00:01:32,159 --> 00:01:36,159
here would be alive in s5 only the stuff

43
00:01:34,079 --> 00:01:38,159
here for s4 and so forth

44
00:01:36,159 --> 00:01:40,560
and so as you move into progressively

45
00:01:38,159 --> 00:01:42,399
lower power states there is going to be

46
00:01:40,560 --> 00:01:44,159
less energy usage because more hardware

47
00:01:42,399 --> 00:01:46,880
chunks are powered down so in the

48
00:01:44,159 --> 00:01:49,119
documentation it talks about both power

49
00:01:46,880 --> 00:01:50,799
planes and power wells so here it's

50
00:01:49,119 --> 00:01:53,040
talking about planes and it lists a few

51
00:01:50,799 --> 00:01:55,600
of them the cpu plane the main plane the

52
00:01:53,040 --> 00:01:57,759
memory plane the me plane lan and a

53
00:01:55,600 --> 00:01:59,520
bunch of device specific and for each of

54
00:01:57,759 --> 00:02:02,479
these planes it talks about different

55
00:01:59,520 --> 00:02:05,280
signals that control them the sleep s3

56
00:02:02,479 --> 00:02:08,239
signal sleep s4 and s5 and some other

57
00:02:05,280 --> 00:02:11,200
things sleep m lan and so forth now as a

58
00:02:08,239 --> 00:02:13,440
reminder back from before like with smis

59
00:02:11,200 --> 00:02:15,840
the hash sign at the end of a signal

60
00:02:13,440 --> 00:02:19,040
name means that it's active low so the

61
00:02:15,840 --> 00:02:20,480
s3 signal is asserted if that signal

62
00:02:19,040 --> 00:02:23,280
goes low

63
00:02:20,480 --> 00:02:26,160
so another view of these acpi power

64
00:02:23,280 --> 00:02:28,160
states from the acpi spec this time

65
00:02:26,160 --> 00:02:30,000
specifically we're going to want to look

66
00:02:28,160 --> 00:02:31,680
at this sleep state and we want to

67
00:02:30,000 --> 00:02:34,800
understand how you get into and out of

68
00:02:31,680 --> 00:02:38,160
sleep state so you start in the s0 state

69
00:02:34,800 --> 00:02:40,400
and then there's this slp sleep type and

70
00:02:38,160 --> 00:02:42,720
if you set the sleep type to s3 and then

71
00:02:40,400 --> 00:02:45,120
you set sleep enable then it goes down

72
00:02:42,720 --> 00:02:47,200
into the s3 state

73
00:02:45,120 --> 00:02:49,280
so let's go find what that sleep type

74
00:02:47,200 --> 00:02:50,800
and sleep enable is so the intel

75
00:02:49,280 --> 00:02:52,879
documentation

76
00:02:50,800 --> 00:02:55,599
in talking about its implementation of

77
00:02:52,879 --> 00:02:58,480
the acpi standard says that setting the

78
00:02:55,599 --> 00:03:00,319
desired type in the sleep type field and

79
00:02:58,480 --> 00:03:01,680
then setting the sleep enable bit is

80
00:03:00,319 --> 00:03:03,360
what brings you down into the sleep

81
00:03:01,680 --> 00:03:05,280
state great so that's pretty much a

82
00:03:03,360 --> 00:03:07,360
one-to-one mapping and then it tells you

83
00:03:05,280 --> 00:03:10,159
about these different sleep types s1 s3

84
00:03:07,360 --> 00:03:11,680
s4 s5 we're only going to care about the

85
00:03:10,159 --> 00:03:13,760
s3 type

86
00:03:11,680 --> 00:03:16,319
so sleep type if we look for that in the

87
00:03:13,760 --> 00:03:19,840
manuals we find this we find

88
00:03:16,319 --> 00:03:22,080
pm1 control so this is a register at pm

89
00:03:19,840 --> 00:03:24,159
base plus 4. as a reminder power

90
00:03:22,080 --> 00:03:27,920
management base was a

91
00:03:24,159 --> 00:03:30,239
variable port io that had

92
00:03:27,920 --> 00:03:32,720
128 bytes had to wait there for a second

93
00:03:30,239 --> 00:03:35,360
i couldn't remember 128 bytes worth of

94
00:03:32,720 --> 00:03:37,519
port i o after it and we saw a bunch of

95
00:03:35,360 --> 00:03:39,680
other things that you know offset 60

96
00:03:37,519 --> 00:03:42,080
offset 30 a bunch of other registers

97
00:03:39,680 --> 00:03:44,400
dealing with you know smm stuff because

98
00:03:42,080 --> 00:03:46,560
smm was originally for power management

99
00:03:44,400 --> 00:03:50,400
amongst other things and now here we see

100
00:03:46,560 --> 00:03:53,680
this more direct acpi usage of pmbase

101
00:03:50,400 --> 00:03:56,959
plus 4 is pm1 control and inside of that

102
00:03:53,680 --> 00:04:00,000
we see sleep type and sleep enable

103
00:03:56,959 --> 00:04:03,280
amongst these types we've got 1 0 one is

104
00:04:00,000 --> 00:04:06,879
the suspend to ram or sleep s3 one one

105
00:04:03,280 --> 00:04:08,959
zero sleep s4 and so it asserts sleep s3

106
00:04:06,879 --> 00:04:10,959
and sleep s4 so that corresponds to the

107
00:04:08,959 --> 00:04:13,360
s4 state which means you're powering

108
00:04:10,959 --> 00:04:14,640
down all the s3 stuff and the s4 stuff

109
00:04:13,360 --> 00:04:18,079
and then the

110
00:04:14,640 --> 00:04:20,079
s5 is going to power down s3 s4 and s5

111
00:04:18,079 --> 00:04:22,079
so it's worth noting that this document

112
00:04:20,079 --> 00:04:25,120
says these bits are only reset by the

113
00:04:22,079 --> 00:04:27,520
rtc rst so then we got to go find out

114
00:04:25,120 --> 00:04:30,720
what that means well it's the return of

115
00:04:27,520 --> 00:04:33,120
the rtc the real-time clock slash cmos

116
00:04:30,720 --> 00:04:35,360
that we saw in architecture 2001. so

117
00:04:33,120 --> 00:04:37,840
there we had said that that clock is

118
00:04:35,360 --> 00:04:40,639
able to keep ticking forward thanks to

119
00:04:37,840 --> 00:04:42,639
usually a battery it's battery-backed

120
00:04:40,639 --> 00:04:44,240
memory for the cmos the non-volatile

121
00:04:42,639 --> 00:04:46,160
memory stays alive as long as this

122
00:04:44,240 --> 00:04:48,639
battery is powered and the clock keeps

123
00:04:46,160 --> 00:04:50,479
sticking forward as long as this clock

124
00:04:48,639 --> 00:04:53,440
as long as this battery is powered so

125
00:04:50,479 --> 00:04:56,000
basically this rtc reset only happens

126
00:04:53,440 --> 00:04:58,639
when you lose power to this battery and

127
00:04:56,000 --> 00:05:01,600
so this effectively is the rtc well and

128
00:04:58,639 --> 00:05:04,479
by the previous slide saying that those

129
00:05:01,600 --> 00:05:06,720
sleep bits are only reset upon a rtc

130
00:05:04,479 --> 00:05:07,919
reset what they're essentially saying is

131
00:05:06,720 --> 00:05:10,880
basically these are never going to be

132
00:05:07,919 --> 00:05:12,960
reset unless this thing dies so most of

133
00:05:10,880 --> 00:05:15,360
the time when you go down to sleep and

134
00:05:12,960 --> 00:05:18,720
you come back up from sleep the values

135
00:05:15,360 --> 00:05:20,880
in pm1 c and t are still going to be set

136
00:05:18,720 --> 00:05:23,440
so that means while an operating system

137
00:05:20,880 --> 00:05:26,800
may set a sleep type and then send it

138
00:05:23,440 --> 00:05:28,720
down to sleep by hitting sleep enable

139
00:05:26,800 --> 00:05:31,199
when it comes back up when the you know

140
00:05:28,720 --> 00:05:33,280
bios is resuming from asleep it can

141
00:05:31,199 --> 00:05:36,400
actually check this sleep type register

142
00:05:33,280 --> 00:05:38,000
and as long as the rtc is not dead as

143
00:05:36,400 --> 00:05:40,400
long as that battery is not dead then it

144
00:05:38,000 --> 00:05:42,400
will effectively see why it went to

145
00:05:40,400 --> 00:05:44,160
sleep and so it can change its behavior

146
00:05:42,400 --> 00:05:46,080
upon resume

147
00:05:44,160 --> 00:05:48,960
so we said s3 is what we're going to

148
00:05:46,080 --> 00:05:50,160
care about the most but there's also s4

149
00:05:48,960 --> 00:05:52,720
and s5

150
00:05:50,160 --> 00:05:55,759
and so just one more time uh multiple

151
00:05:52,720 --> 00:05:59,840
places say it over and over again the

152
00:05:55,759 --> 00:06:03,440
slp s3 is the signal which controls the

153
00:05:59,840 --> 00:06:05,919
s3 power plane and the signal shuts off

154
00:06:03,440 --> 00:06:07,039
power to all non-critical systems win in

155
00:06:05,919 --> 00:06:09,520
s3

156
00:06:07,039 --> 00:06:11,600
and we've also got the s4 and the s5 in

157
00:06:09,520 --> 00:06:12,800
the same documentation here we don't

158
00:06:11,600 --> 00:06:14,560
care as much about those for the

159
00:06:12,800 --> 00:06:17,199
purposes of the attack that we're making

160
00:06:14,560 --> 00:06:17,199
our way towards


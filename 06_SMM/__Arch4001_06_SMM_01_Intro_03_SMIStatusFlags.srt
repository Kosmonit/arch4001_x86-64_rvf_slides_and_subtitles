1
00:00:00,160 --> 00:00:04,560
okay well it is a interesting question

2
00:00:02,639 --> 00:00:06,560
of how does the system management

3
00:00:04,560 --> 00:00:08,080
interrupt handler know what the cause of

4
00:00:06,560 --> 00:00:10,960
an smi was

5
00:00:08,080 --> 00:00:12,880
and that is reported via these various

6
00:00:10,960 --> 00:00:15,759
status bits that were in that big long

7
00:00:12,880 --> 00:00:18,720
table so specifically the bios right

8
00:00:15,759 --> 00:00:21,119
status flag is going to tell it that one

9
00:00:18,720 --> 00:00:22,640
of these two events occurred

10
00:00:21,119 --> 00:00:25,039
so again this we're going to be in a

11
00:00:22,640 --> 00:00:26,880
very type oe section of the manual

12
00:00:25,039 --> 00:00:28,480
apparently here or at least according to

13
00:00:26,880 --> 00:00:30,320
my interpretation i might be wrong and

14
00:00:28,480 --> 00:00:32,399
some intel person can go tell me i'm

15
00:00:30,320 --> 00:00:34,480
wrong but i believe this should be bios

16
00:00:32,399 --> 00:00:35,840
write enable equal to zero so attempting

17
00:00:34,480 --> 00:00:38,160
to write to the bios while right is

18
00:00:35,840 --> 00:00:41,520
equal to zero so when we go search for

19
00:00:38,160 --> 00:00:44,480
this bios write status flag we find this

20
00:00:41,520 --> 00:00:47,120
there's some tico status one register

21
00:00:44,480 --> 00:00:49,680
it has bios write status inside of it

22
00:00:47,120 --> 00:00:51,920
and if it's zero the software clears

23
00:00:49,680 --> 00:00:53,600
this bit by setting it to one so after

24
00:00:51,920 --> 00:00:55,520
the system management interrupt happens

25
00:00:53,600 --> 00:00:57,360
if this bit was set then software would

26
00:00:55,520 --> 00:00:59,840
clear it by setting it to one which

27
00:00:57,360 --> 00:01:01,840
would put this back to zero but normally

28
00:00:59,840 --> 00:01:04,239
the way that it gets set to one is the

29
00:01:01,840 --> 00:01:06,240
pch sets this bit and generates an smi

30
00:01:04,239 --> 00:01:08,240
to indicate an invalid attempt to write

31
00:01:06,240 --> 00:01:10,240
to bios this occurred when either you

32
00:01:08,240 --> 00:01:11,520
know either of these two things but

33
00:01:10,240 --> 00:01:14,000
unfortunately this seems to be a bunch

34
00:01:11,520 --> 00:01:16,560
of typos here it says that bios wp bit

35
00:01:14,000 --> 00:01:18,080
is changed from zero to one and the bld

36
00:01:16,560 --> 00:01:21,280
bit is also set

37
00:01:18,080 --> 00:01:23,920
well there's no hits in the manuals for

38
00:01:21,280 --> 00:01:27,280
bios wp so i think that's supposed to be

39
00:01:23,920 --> 00:01:29,920
bios w e that's right enable

40
00:01:27,280 --> 00:01:32,560
furthermore i think that bld is probably

41
00:01:29,920 --> 00:01:34,640
supposed to be ble here

42
00:01:32,560 --> 00:01:37,040
and so it either is the bios for write

43
00:01:34,640 --> 00:01:40,159
enable bit is changed from zero to one

44
00:01:37,040 --> 00:01:42,479
and the bios lock enable bit is set or

45
00:01:40,159 --> 00:01:45,360
any right is attempted to the bios and

46
00:01:42,479 --> 00:01:46,960
the bios write protect or bios right

47
00:01:45,360 --> 00:01:47,759
enable bit rather

48
00:01:46,960 --> 00:01:49,360
is

49
00:01:47,759 --> 00:01:51,360
also set

50
00:01:49,360 --> 00:01:53,119
i think that should be clear so i mean

51
00:01:51,360 --> 00:01:55,759
that's probably just this typo

52
00:01:53,119 --> 00:01:57,520
transposing itself into here so if you

53
00:01:55,759 --> 00:02:00,079
attempt to write to the bios while the

54
00:01:57,520 --> 00:02:01,600
bios write enable bit is clear well it's

55
00:02:00,079 --> 00:02:02,960
saying write is not enabled so you're

56
00:02:01,600 --> 00:02:05,439
not allowed to do that

57
00:02:02,960 --> 00:02:08,239
anyways there's some status bit and the

58
00:02:05,439 --> 00:02:10,800
software is going to check it and then

59
00:02:08,239 --> 00:02:12,480
it's going to act accordingly so where

60
00:02:10,800 --> 00:02:15,200
is this status bit found where is this

61
00:02:12,480 --> 00:02:18,480
tyco 1 status register it's at tico base

62
00:02:15,200 --> 00:02:20,319
plus 4. well where's tico base found

63
00:02:18,480 --> 00:02:22,560
so you search around fortico base and

64
00:02:20,319 --> 00:02:26,720
you'll find this line that says tcobe's

65
00:02:22,560 --> 00:02:29,440
value which is pm base plus 60.

66
00:02:26,720 --> 00:02:33,440
so what's pm base

67
00:02:29,440 --> 00:02:36,239
well pm base is the acpi base address

68
00:02:33,440 --> 00:02:41,200
register which is that the lpc device

69
00:02:36,239 --> 00:02:44,160
bus 0 device 31 function 0 offset 40.

70
00:02:41,200 --> 00:02:47,519
so it's so simple you just go to pm base

71
00:02:44,160 --> 00:02:50,640
which is at bdf031040

72
00:02:47,519 --> 00:02:53,440
take bits 15 to 7 that'll give you the

73
00:02:50,640 --> 00:02:54,879
pm base add 60 to it that'll give you a

74
00:02:53,440 --> 00:02:56,319
tico base

75
00:02:54,879 --> 00:02:58,800
and at this point i should probably say

76
00:02:56,319 --> 00:03:01,360
actually pm base is an io

77
00:02:58,800 --> 00:03:03,599
address it's an address in the i o space

78
00:03:01,360 --> 00:03:04,959
not a memory address so pull this out

79
00:03:03,599 --> 00:03:07,760
you get an i o

80
00:03:04,959 --> 00:03:09,760
port io address add 60 to that address

81
00:03:07,760 --> 00:03:12,640
you get tco base which is still a port

82
00:03:09,760 --> 00:03:15,840
io address add four to that you get tico

83
00:03:12,640 --> 00:03:18,720
status tico 1 status and then check bit

84
00:03:15,840 --> 00:03:20,640
8 of that and you'll find the bios write

85
00:03:18,720 --> 00:03:22,800
status and now i need to let my

86
00:03:20,640 --> 00:03:24,720
animation catch up to everything that i

87
00:03:22,800 --> 00:03:27,920
just said

88
00:03:24,720 --> 00:03:31,360
it's so simple right

89
00:03:27,920 --> 00:03:32,959
so one manner of port is tico status one

90
00:03:31,360 --> 00:03:35,599
well it's something that's primarily

91
00:03:32,959 --> 00:03:37,680
poked by hardware so hardware sets the

92
00:03:35,599 --> 00:03:39,920
bits saying why a system management

93
00:03:37,680 --> 00:03:42,720
interrupt happened and then it's peaked

94
00:03:39,920 --> 00:03:45,040
by the smi handler although the smi

95
00:03:42,720 --> 00:03:47,840
handler has to poke it also to clear the

96
00:03:45,040 --> 00:03:48,879
bits so we saw back with that bios write

97
00:03:47,840 --> 00:03:50,879
status

98
00:03:48,879 --> 00:03:53,040
it needs to write a one back over the

99
00:03:50,879 --> 00:03:56,000
existing one in order to set it back to

100
00:03:53,040 --> 00:03:56,000
a zero


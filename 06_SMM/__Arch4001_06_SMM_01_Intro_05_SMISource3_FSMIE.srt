1
00:00:00,240 --> 00:00:04,160
now our next two smi sources are just

2
00:00:02,480 --> 00:00:06,560
being covered because of their

3
00:00:04,160 --> 00:00:09,040
interestingness for if someone had

4
00:00:06,560 --> 00:00:11,440
malware running in smm they would give

5
00:00:09,040 --> 00:00:14,639
them interesting capabilities so the

6
00:00:11,440 --> 00:00:17,680
next one is the fsmie bit which fires

7
00:00:14,639 --> 00:00:20,480
whenever a flash cycle is done

8
00:00:17,680 --> 00:00:23,119
so on a big list of smi's

9
00:00:20,480 --> 00:00:25,920
scroll on down

10
00:00:23,119 --> 00:00:28,000
and you find the spy command completed

11
00:00:25,920 --> 00:00:30,880
and there's no additional enables and

12
00:00:28,000 --> 00:00:32,480
it's recorded in the spy status

13
00:00:30,880 --> 00:00:35,200
now i should say at this point that i'm

14
00:00:32,480 --> 00:00:37,760
not actually super 100 sure that this is

15
00:00:35,200 --> 00:00:39,600
what actually corresponds to fsmi is

16
00:00:37,760 --> 00:00:42,079
just the closest thing i could find i

17
00:00:39,600 --> 00:00:44,719
think if it does correspond to fsmi then

18
00:00:42,079 --> 00:00:45,680
this should probably say that the fsmi

19
00:00:44,719 --> 00:00:48,079
bit

20
00:00:45,680 --> 00:00:49,360
is an additional enable but you know

21
00:00:48,079 --> 00:00:50,800
whatever

22
00:00:49,360 --> 00:00:52,559
so when i say that this is kind of you

23
00:00:50,800 --> 00:00:54,399
know the closest i can find to something

24
00:00:52,559 --> 00:00:57,440
that i think is the right status for the

25
00:00:54,399 --> 00:00:59,680
fsmi bit that's because this spy status

26
00:00:57,440 --> 00:01:02,000
thing says this bit will be set if the

27
00:00:59,680 --> 00:01:05,840
spy logic is generating an smi which

28
00:01:02,000 --> 00:01:10,880
will be the case for ms fsmie

29
00:01:05,840 --> 00:01:13,520
so it is bit 26 of the spy status or

30
00:01:10,880 --> 00:01:16,479
sorry smi status register which is found

31
00:01:13,520 --> 00:01:18,560
at pm base plus 34 so once again power

32
00:01:16,479 --> 00:01:21,680
management base it's going to be some

33
00:01:18,560 --> 00:01:24,840
port io base of a variable port i o

34
00:01:21,680 --> 00:01:28,320
address range and offset 34 will be this

35
00:01:24,840 --> 00:01:30,479
register now what is fsmi

36
00:01:28,320 --> 00:01:33,119
well this is a bit that if you were

37
00:01:30,479 --> 00:01:34,320
looking at the you know full definitions

38
00:01:33,119 --> 00:01:36,720
of those

39
00:01:34,320 --> 00:01:38,560
spy registers before you might have seen

40
00:01:36,720 --> 00:01:40,560
it it is the hardware sequencing of

41
00:01:38,560 --> 00:01:42,159
flash control register

42
00:01:40,560 --> 00:01:44,399
bit 15

43
00:01:42,159 --> 00:01:47,840
and it's read write and it says when set

44
00:01:44,399 --> 00:01:50,799
to one the spy asserts an smi whenever

45
00:01:47,840 --> 00:01:52,640
the flash cycle done bit is one

46
00:01:50,799 --> 00:01:54,560
so this essentially means

47
00:01:52,640 --> 00:01:56,960
that you know you cause a flash

48
00:01:54,560 --> 00:01:58,399
transaction like we saw before we we you

49
00:01:56,960 --> 00:01:59,920
know showed you how you could do a

50
00:01:58,399 --> 00:02:02,560
sequence of events that would lead to a

51
00:01:59,920 --> 00:02:04,320
valid read or write flash transaction

52
00:02:02,560 --> 00:02:06,399
and then the hardware once it's done

53
00:02:04,320 --> 00:02:09,360
getting the data or writing the data it

54
00:02:06,399 --> 00:02:11,680
sets f done equal to 1 to say you know

55
00:02:09,360 --> 00:02:13,680
i'm done now but what this says is if

56
00:02:11,680 --> 00:02:16,160
this bit was set when the flash cycle

57
00:02:13,680 --> 00:02:18,720
starts then when the flash cycle is done

58
00:02:16,160 --> 00:02:21,920
it's going to actually fire off an smi

59
00:02:18,720 --> 00:02:24,959
and so before the code executing on the

60
00:02:21,920 --> 00:02:27,520
operating system on the main cpu

61
00:02:24,959 --> 00:02:29,280
is able to actually go and check you

62
00:02:27,520 --> 00:02:31,680
know it may be pulling on the done bit

63
00:02:29,280 --> 00:02:33,280
but before it even has an opportunity to

64
00:02:31,680 --> 00:02:35,599
successfully see that the done bit is

65
00:02:33,280 --> 00:02:38,080
set an smi is going to fire off and that

66
00:02:35,599 --> 00:02:40,239
means code running in smm which could be

67
00:02:38,080 --> 00:02:41,840
malware will have the first opportunity

68
00:02:40,239 --> 00:02:43,599
to look at the contents of the flash

69
00:02:41,840 --> 00:02:45,440
registers and fix them up if there's

70
00:02:43,599 --> 00:02:48,000
something they don't like there so let's

71
00:02:45,440 --> 00:02:50,239
go back to our animation from before of

72
00:02:48,000 --> 00:02:53,280
writing to the flash chip but now we'll

73
00:02:50,239 --> 00:02:55,040
assume that fsmi is set and that malware

74
00:02:53,280 --> 00:02:57,280
exists in smm

75
00:02:55,040 --> 00:03:00,239
so the cpu says go on a flash

76
00:02:57,280 --> 00:03:02,959
transaction and the magic happens and

77
00:03:00,239 --> 00:03:05,519
the cpu continues to read the hardware

78
00:03:02,959 --> 00:03:07,120
sequencing flash status register to see

79
00:03:05,519 --> 00:03:08,239
if it's done yet is it done yet is it

80
00:03:07,120 --> 00:03:11,120
done yet

81
00:03:08,239 --> 00:03:12,480
now enter in the happy little elves down

82
00:03:11,120 --> 00:03:15,120
in the pch

83
00:03:12,480 --> 00:03:17,200
and if the hardware sequencing flash

84
00:03:15,120 --> 00:03:19,599
done is set and if

85
00:03:17,200 --> 00:03:22,560
the hsfc

86
00:03:19,599 --> 00:03:26,400
control fsmie bit

87
00:03:22,560 --> 00:03:28,319
is set then when the done happens it's

88
00:03:26,400 --> 00:03:30,720
going to fire a system management

89
00:03:28,319 --> 00:03:35,200
interrupt the cpu will go into system

90
00:03:30,720 --> 00:03:36,720
management mode and now if there's a smm

91
00:03:35,200 --> 00:03:38,319
monster in the middle

92
00:03:36,720 --> 00:03:41,280
smitam

93
00:03:38,319 --> 00:03:43,200
who's residing in smram

94
00:03:41,280 --> 00:03:45,280
they get the first opportunity to see

95
00:03:43,200 --> 00:03:47,360
the contents of the flash data registers

96
00:03:45,280 --> 00:03:49,360
and they can for instance say if i see

97
00:03:47,360 --> 00:03:51,440
that you're reading from the flash

98
00:03:49,360 --> 00:03:54,799
address where i know that i'm keeping my

99
00:03:51,440 --> 00:03:56,959
evil data to persist in smm across

100
00:03:54,799 --> 00:03:58,959
reboots then i can just clean it up and

101
00:03:56,959 --> 00:04:01,680
put any sort of innocuous data and

102
00:03:58,959 --> 00:04:03,599
you'll be none the wiser i'll resume you

103
00:04:01,680 --> 00:04:05,280
will get back code execution but you

104
00:04:03,599 --> 00:04:07,680
will be seeing

105
00:04:05,280 --> 00:04:10,480
manipulated data instead of the actual

106
00:04:07,680 --> 00:04:12,959
data coming from the flash chip

107
00:04:10,480 --> 00:04:15,519
so here be dragons you can learn more

108
00:04:12,959 --> 00:04:16,880
about smitam and the txt based

109
00:04:15,519 --> 00:04:19,680
countermeasure

110
00:04:16,880 --> 00:04:21,519
at this talk so i had to cite this

111
00:04:19,680 --> 00:04:23,440
there's a different one so this is

112
00:04:21,519 --> 00:04:24,800
actually a later talk where first we

113
00:04:23,440 --> 00:04:26,400
talked about hey there's this attack

114
00:04:24,800 --> 00:04:29,360
some item working you know men in the

115
00:04:26,400 --> 00:04:31,199
middle your access to the spy flash chip

116
00:04:29,360 --> 00:04:33,120
and then we talked about txt to defend

117
00:04:31,199 --> 00:04:35,440
against it and later on i talked about

118
00:04:33,120 --> 00:04:37,199
how txt could actually be used for smi

119
00:04:35,440 --> 00:04:40,240
suppression as you'll be learning about

120
00:04:37,199 --> 00:04:42,560
in a little bit but because the original

121
00:04:40,240 --> 00:04:44,479
talk doesn't have the nice proper smitem

122
00:04:42,560 --> 00:04:47,040
diagrams and stuff in there this one is

123
00:04:44,479 --> 00:04:48,479
the better one to look at and just fyi

124
00:04:47,040 --> 00:04:50,080
since i'm bringing it up

125
00:04:48,479 --> 00:04:52,560
there was a censored chunk of

126
00:04:50,080 --> 00:04:55,040
documentation from intel confidential

127
00:04:52,560 --> 00:04:56,479
documentation about txt and how it

128
00:04:55,040 --> 00:04:58,639
worked that i had to censor in this

129
00:04:56,479 --> 00:05:00,320
diagram but later on that documentation

130
00:04:58,639 --> 00:05:02,479
got released so

131
00:05:00,320 --> 00:05:06,560
you can check this tweet where i show

132
00:05:02,479 --> 00:05:06,560
how the slides should have really looked


1
00:00:00,399 --> 00:00:04,799
at long last we're over to the other

2
00:00:02,639 --> 00:00:05,680
path to learn about system management

3
00:00:04,799 --> 00:00:07,279
mode

4
00:00:05,680 --> 00:00:09,200
so at the very beginning of class we

5
00:00:07,279 --> 00:00:12,320
talked about real mode in the reset

6
00:00:09,200 --> 00:00:13,920
vector so check on that and now we can

7
00:00:12,320 --> 00:00:16,720
finally learn about system management

8
00:00:13,920 --> 00:00:19,039
mode which is set up by firmware so if

9
00:00:16,720 --> 00:00:20,800
real mode and the bios starts out at the

10
00:00:19,039 --> 00:00:22,480
very beginning of the reset setting a

11
00:00:20,800 --> 00:00:23,840
whole bunch of stuff up

12
00:00:22,480 --> 00:00:25,519
it has to you know do a bunch of

13
00:00:23,840 --> 00:00:27,599
configuration of the hardware one of the

14
00:00:25,519 --> 00:00:29,519
things that it configures is system

15
00:00:27,599 --> 00:00:31,439
measurement mode code what's going to

16
00:00:29,519 --> 00:00:33,920
run in this special mode

17
00:00:31,439 --> 00:00:36,559
so what is smm well for now it's the

18
00:00:33,920 --> 00:00:39,280
most privileged execution mode on an x86

19
00:00:36,559 --> 00:00:41,600
processor and i say for now because

20
00:00:39,280 --> 00:00:44,399
intel is actively trying to add hardware

21
00:00:41,600 --> 00:00:46,559
mechanisms to make it deprivable so

22
00:00:44,399 --> 00:00:48,559
basically a bios could set up smm so

23
00:00:46,559 --> 00:00:50,879
that it wasn't so privileged so that it

24
00:00:48,559 --> 00:00:53,199
can't immediately and guarantee

25
00:00:50,879 --> 00:00:56,079
compromise of a virtualization system or

26
00:00:53,199 --> 00:00:57,920
hypervisor but but that's all in the

27
00:00:56,079 --> 00:01:00,800
newest hardware so most people don't

28
00:00:57,920 --> 00:01:03,120
have that yet and even if people had it

29
00:01:00,800 --> 00:01:05,199
other bios vendors and operating system

30
00:01:03,120 --> 00:01:07,119
makers have to come to agreements about

31
00:01:05,199 --> 00:01:10,400
how it will be used in order to

32
00:01:07,119 --> 00:01:12,080
de-privilege smm so for now smm is the

33
00:01:10,400 --> 00:01:14,479
most privileged execution mode and why

34
00:01:12,080 --> 00:01:16,640
do we say that well because it is able

35
00:01:14,479 --> 00:01:18,960
to read and write from everyone else's

36
00:01:16,640 --> 00:01:20,720
ram but no one's allowed to read and

37
00:01:18,960 --> 00:01:22,960
write from its ram

38
00:01:20,720 --> 00:01:25,439
so it can go ahead and scribble all over

39
00:01:22,960 --> 00:01:27,119
a kernel and inject code that you know

40
00:01:25,439 --> 00:01:28,799
steals your keystrokes or something like

41
00:01:27,119 --> 00:01:30,880
that it can scribble all over a

42
00:01:28,799 --> 00:01:32,960
hypervisor and break the isolation

43
00:01:30,880 --> 00:01:34,799
between different virtual machines so

44
00:01:32,960 --> 00:01:38,159
obviously you know if someone escalated

45
00:01:34,799 --> 00:01:40,000
from within a vm to smm on some cloud

46
00:01:38,159 --> 00:01:41,920
infrastructure that's going to be a bad

47
00:01:40,000 --> 00:01:43,520
time because then they can you know go

48
00:01:41,920 --> 00:01:45,759
get access to all the other data and all

49
00:01:43,520 --> 00:01:47,759
the other vms of the cloud provider on

50
00:01:45,759 --> 00:01:50,240
that particular system and also as we

51
00:01:47,759 --> 00:01:52,399
saw obliquely in the flash protection

52
00:01:50,240 --> 00:01:54,159
section i didn't want to cover it you

53
00:01:52,399 --> 00:01:56,479
know in too much detail we'll cover that

54
00:01:54,159 --> 00:01:59,040
now here but basically

55
00:01:56,479 --> 00:02:01,600
smm is responsible for dealing with

56
00:01:59,040 --> 00:02:03,600
rewriting the bios write enable to zero

57
00:02:01,600 --> 00:02:05,280
if someone sets it and you know the

58
00:02:03,600 --> 00:02:06,799
particular system doesn't want to allow

59
00:02:05,280 --> 00:02:09,119
writing to the flash

60
00:02:06,799 --> 00:02:11,120
smm is interesting because it basically

61
00:02:09,119 --> 00:02:13,200
executes in the background without the

62
00:02:11,120 --> 00:02:15,599
need for involvement by something like

63
00:02:13,200 --> 00:02:17,280
an operating system a system management

64
00:02:15,599 --> 00:02:19,360
interrupt happens it transitions to this

65
00:02:17,280 --> 00:02:20,959
mode run some code and then hands back

66
00:02:19,360 --> 00:02:23,599
to the operating system which may be

67
00:02:20,959 --> 00:02:25,680
none the wiser when it enters into smm

68
00:02:23,599 --> 00:02:27,599
all the processors are placed into

69
00:02:25,680 --> 00:02:30,000
system management mode so basically

70
00:02:27,599 --> 00:02:31,920
everything on the system stops it goes

71
00:02:30,000 --> 00:02:34,000
off and runs the code and then

72
00:02:31,920 --> 00:02:35,200
everything on the system resumes so one

73
00:02:34,000 --> 00:02:37,599
of the first things that typically

74
00:02:35,200 --> 00:02:40,239
happens upon entry to smm is that the

75
00:02:37,599 --> 00:02:42,959
code is going to set up a typical 32 or

76
00:02:40,239 --> 00:02:45,920
64-bit paging it's making it much more

77
00:02:42,959 --> 00:02:48,319
like protected mode instead of real mode

78
00:02:45,920 --> 00:02:50,160
now smm is only entered through system

79
00:02:48,319 --> 00:02:52,400
management interrupts and it's only

80
00:02:50,160 --> 00:02:54,879
exited through a special resume

81
00:02:52,400 --> 00:02:56,959
instruction rsm that resume instruction

82
00:02:54,879 --> 00:02:59,440
may only be executed from within a

83
00:02:56,959 --> 00:03:01,200
processor that is in smm any other place

84
00:02:59,440 --> 00:03:03,840
it's going to cause a fault

85
00:03:01,200 --> 00:03:06,080
so returning back to this diagram we did

86
00:03:03,840 --> 00:03:08,800
actually see all of these smi's and

87
00:03:06,080 --> 00:03:11,440
resumes there's some smi's and resumes

88
00:03:08,800 --> 00:03:13,760
smi resume here we go should have just

89
00:03:11,440 --> 00:03:16,159
made this auto animate to keep going a

90
00:03:13,760 --> 00:03:17,360
whole bunch of smis and resumes and so

91
00:03:16,159 --> 00:03:19,280
you know what are those they're

92
00:03:17,360 --> 00:03:20,879
basically saying smi is the way out of

93
00:03:19,280 --> 00:03:22,879
long mode and then it magically

94
00:03:20,879 --> 00:03:25,200
transitions over to here teleporting

95
00:03:22,879 --> 00:03:27,440
over into smm mode and the resume

96
00:03:25,200 --> 00:03:29,840
assembly instruction gets you out of smm

97
00:03:27,440 --> 00:03:30,879
mode and back to whatever mode you just

98
00:03:29,840 --> 00:03:32,640
came from

99
00:03:30,879 --> 00:03:34,959
and likewise on the actual intel

100
00:03:32,640 --> 00:03:37,680
documentation of the different finite

101
00:03:34,959 --> 00:03:40,000
states of the cpu there are all these

102
00:03:37,680 --> 00:03:42,480
smis and resumes now you may have

103
00:03:40,000 --> 00:03:44,640
noticed that those smi ended with a hash

104
00:03:42,480 --> 00:03:46,159
symbol so from the documentation it says

105
00:03:44,640 --> 00:03:48,560
that the hash symbol at the end of the

106
00:03:46,159 --> 00:03:50,319
signal name indicates that the active or

107
00:03:48,560 --> 00:03:52,560
asserted state occurs when the signal is

108
00:03:50,319 --> 00:03:54,400
at a low voltage level when someone when

109
00:03:52,560 --> 00:03:57,040
the hash is not present the signal is

110
00:03:54,400 --> 00:03:58,400
asserted at a high voltage level so uh

111
00:03:57,040 --> 00:04:00,319
in electrical engineering they'll

112
00:03:58,400 --> 00:04:02,480
oftentimes put a little sort of bar

113
00:04:00,319 --> 00:04:05,120
above a signal to indicate that it's

114
00:04:02,480 --> 00:04:06,720
active low instead of active high and

115
00:04:05,120 --> 00:04:09,120
basically i think just to make it easier

116
00:04:06,720 --> 00:04:11,200
for typing intel is just went with a

117
00:04:09,120 --> 00:04:13,599
hash at the end of the thing to indicate

118
00:04:11,200 --> 00:04:16,079
that smi is an active low signal so

119
00:04:13,599 --> 00:04:18,160
basically if there were a physical wire

120
00:04:16,079 --> 00:04:19,840
running to the processor that wire

121
00:04:18,160 --> 00:04:21,359
should be pulled down to a low voltage

122
00:04:19,840 --> 00:04:24,479
rather than up to a high voltage in

123
00:04:21,359 --> 00:04:26,400
order to say it's smi time

124
00:04:24,479 --> 00:04:28,400
the overall point of system management

125
00:04:26,400 --> 00:04:31,120
mode as the name implies is for

126
00:04:28,400 --> 00:04:33,040
management of the system now that can be

127
00:04:31,120 --> 00:04:34,720
management of things like power that

128
00:04:33,040 --> 00:04:37,440
could be management of things like

129
00:04:34,720 --> 00:04:39,360
security on earlier systems but it's

130
00:04:37,440 --> 00:04:42,160
really for just sort of any code that

131
00:04:39,360 --> 00:04:44,560
the oem wants to run in an isolated

132
00:04:42,160 --> 00:04:46,560
environment that can be independent of

133
00:04:44,560 --> 00:04:47,520
the operating system so that code is

134
00:04:46,560 --> 00:04:49,759
going to be running whether you're

135
00:04:47,520 --> 00:04:51,199
running linux or windows so that code

136
00:04:49,759 --> 00:04:53,440
needs to not have any particular

137
00:04:51,199 --> 00:04:55,520
operating system dependencies because

138
00:04:53,440 --> 00:04:57,199
the code should really be more about the

139
00:04:55,520 --> 00:04:59,600
hardware the particulars of the hardware

140
00:04:57,199 --> 00:05:02,000
and the power management thereof or the

141
00:04:59,600 --> 00:05:04,240
particulars of the oem special secret

142
00:05:02,000 --> 00:05:06,160
sauce that they want to hide there

143
00:05:04,240 --> 00:05:08,960
rather than being anything specific to

144
00:05:06,160 --> 00:05:11,440
an operating system but although a oem

145
00:05:08,960 --> 00:05:13,600
may place some special sauce there and

146
00:05:11,440 --> 00:05:15,360
although we can't directly inspect

147
00:05:13,600 --> 00:05:16,800
system management mode from an operating

148
00:05:15,360 --> 00:05:18,960
system so we can't just dump the

149
00:05:16,800 --> 00:05:20,639
contents and reverse engineer it and see

150
00:05:18,960 --> 00:05:22,080
you know what the vendor put in there

151
00:05:20,639 --> 00:05:23,199
and what particular vulnerabilities it

152
00:05:22,080 --> 00:05:25,440
may have

153
00:05:23,199 --> 00:05:28,000
the system management mode code is set

154
00:05:25,440 --> 00:05:29,520
up by the bios so it is incumbent upon

155
00:05:28,000 --> 00:05:31,840
the bios to

156
00:05:29,520 --> 00:05:33,440
write some content some code into some

157
00:05:31,840 --> 00:05:35,440
particular memory region lock down that

158
00:05:33,440 --> 00:05:37,039
memory region and then that code will be

159
00:05:35,440 --> 00:05:38,479
available whenever the system management

160
00:05:37,039 --> 00:05:40,080
interrupt fires

161
00:05:38,479 --> 00:05:42,240
so that means at the end of the day it

162
00:05:40,080 --> 00:05:44,639
is ultimately possible to find this smi

163
00:05:42,240 --> 00:05:47,199
handler code by digging around in the

164
00:05:44,639 --> 00:05:49,680
bios spy flash image

165
00:05:47,199 --> 00:05:52,479
that also means that protecting smm is a

166
00:05:49,680 --> 00:05:55,520
matter of both protecting the sm ram

167
00:05:52,479 --> 00:05:57,360
code the smm code at run time and also

168
00:05:55,520 --> 00:05:59,840
protecting the spy flash because an

169
00:05:57,360 --> 00:06:01,360
attacker can either run time break into

170
00:05:59,840 --> 00:06:04,400
system management mode through whatever

171
00:06:01,360 --> 00:06:06,720
attack surfaces it exposes or they could

172
00:06:04,400 --> 00:06:08,800
break into it via infecting the bios

173
00:06:06,720 --> 00:06:10,800
flash chip and just getting loaded up by

174
00:06:08,800 --> 00:06:12,560
the bios at boot time

175
00:06:10,800 --> 00:06:14,479
and of course you do indeed want to

176
00:06:12,560 --> 00:06:16,400
protect the smm at runtime because you

177
00:06:14,479 --> 00:06:18,960
don't want you know an attacker in one

178
00:06:16,400 --> 00:06:21,120
vm bouncing their way up to smm and

179
00:06:18,960 --> 00:06:22,800
bouncing their way over and down to some

180
00:06:21,120 --> 00:06:24,319
other vm

181
00:06:22,800 --> 00:06:26,400
and so then there's the interesting

182
00:06:24,319 --> 00:06:28,400
property that you know infecting the

183
00:06:26,400 --> 00:06:31,039
bios definitely implies that you will be

184
00:06:28,400 --> 00:06:32,720
able to infect smm infecting smm does

185
00:06:31,039 --> 00:06:34,880
not necessarily mean that you'll be able

186
00:06:32,720 --> 00:06:37,199
to infect the bios but sometimes it does

187
00:06:34,880 --> 00:06:40,080
right if it's not using protected range

188
00:06:37,199 --> 00:06:42,880
registers then smm is sort of the only

189
00:06:40,080 --> 00:06:45,840
other protection mechanism in play those

190
00:06:42,880 --> 00:06:48,000
optional material like the flash master

191
00:06:45,840 --> 00:06:50,080
regions and stuff like that well we said

192
00:06:48,000 --> 00:06:52,160
the bios is always able to access the

193
00:06:50,080 --> 00:06:54,639
bios region just that's how the hardware

194
00:06:52,160 --> 00:06:56,960
works regardless of whatever privilege

195
00:06:54,639 --> 00:06:58,639
bits get set so that's part of why that

196
00:06:56,960 --> 00:07:00,160
was optional material is because you

197
00:06:58,639 --> 00:07:02,639
know that doesn't really come into play

198
00:07:00,160 --> 00:07:05,039
when it comes to protection on the x86

199
00:07:02,639 --> 00:07:06,800
from the x86 from an attacker trying to

200
00:07:05,039 --> 00:07:08,720
privilege escalate that's more about

201
00:07:06,800 --> 00:07:10,960
protection from the other devices like

202
00:07:08,720 --> 00:07:13,280
the gigabit ethernet or the management

203
00:07:10,960 --> 00:07:14,800
engine so as mentioned before smm is

204
00:07:13,280 --> 00:07:17,199
only entered through system management

205
00:07:14,800 --> 00:07:20,639
interrupts and that can be caused by the

206
00:07:17,199 --> 00:07:22,080
invocation of the smi pin or the

207
00:07:20,639 --> 00:07:23,280
advanced programmable interrupt

208
00:07:22,080 --> 00:07:25,919
controller

209
00:07:23,280 --> 00:07:27,680
now in reality these days there is no

210
00:07:25,919 --> 00:07:29,599
smi so

211
00:07:27,680 --> 00:07:31,199
intel you know of course they want to

212
00:07:29,599 --> 00:07:32,960
reduce the number of pins otherwise they

213
00:07:31,199 --> 00:07:35,840
keep growing forever and so they have

214
00:07:32,960 --> 00:07:37,520
this notion of virtual legacy wires

215
00:07:35,840 --> 00:07:39,599
which are basically just a message

216
00:07:37,520 --> 00:07:42,400
that's communicated between pch and the

217
00:07:39,599 --> 00:07:44,240
cpu over the dmi interface and that

218
00:07:42,400 --> 00:07:46,400
basically says yeah hey you know by the

219
00:07:44,240 --> 00:07:48,879
way you should start system management

220
00:07:46,400 --> 00:07:50,639
mode here's an smi for you and so

221
00:07:48,879 --> 00:07:52,400
there's not actually a physical wire

222
00:07:50,639 --> 00:07:54,080
necessarily anymore

223
00:07:52,400 --> 00:07:55,759
the system management interrupts need to

224
00:07:54,080 --> 00:07:58,000
be considered their own completely

225
00:07:55,759 --> 00:08:00,639
different type of interrupts they can't

226
00:07:58,000 --> 00:08:02,639
be masked with the interrupt flag or

227
00:08:00,639 --> 00:08:04,800
with the you know clear interrupt flag

228
00:08:02,639 --> 00:08:06,160
assembly instruction they don't have

229
00:08:04,800 --> 00:08:08,000
anything to do with the interrupt

230
00:08:06,160 --> 00:08:09,759
descriptor table that was discussed in

231
00:08:08,000 --> 00:08:12,319
architecture 2001.

232
00:08:09,759 --> 00:08:14,479
things like non-maskable interrupts they

233
00:08:12,319 --> 00:08:17,360
actually sort of lose the race when it

234
00:08:14,479 --> 00:08:19,120
comes to if a smi and a non-maskable

235
00:08:17,360 --> 00:08:22,000
interrupt occur at the exact same time

236
00:08:19,120 --> 00:08:24,319
the smi gets precedence so the smi

237
00:08:22,000 --> 00:08:26,400
really is sort of the most powerful

238
00:08:24,319 --> 00:08:27,599
interrupt and if it happens the system

239
00:08:26,400 --> 00:08:30,240
is going to go run some system

240
00:08:27,599 --> 00:08:32,479
management interrupt code so you know

241
00:08:30,240 --> 00:08:35,200
abstractly smi's happen but what are

242
00:08:32,479 --> 00:08:37,519
some of the causes of sms well here's

243
00:08:35,200 --> 00:08:39,599
some of the causes of smi's it's a whole

244
00:08:37,519 --> 00:08:41,279
bunch of them and i'm going to now

245
00:08:39,599 --> 00:08:46,080
highlight a few of them that are

246
00:08:41,279 --> 00:08:46,080
security relevant in the coming videos


1
00:00:00,480 --> 00:00:03,600
now for the remainder of the attacks i'm

2
00:00:02,080 --> 00:00:06,080
really going to just blow through them

3
00:00:03,600 --> 00:00:08,080
very quickly the expectation is that i

4
00:00:06,080 --> 00:00:10,160
the tour guide have shown you where the

5
00:00:08,080 --> 00:00:12,320
village is and it's up to you the

6
00:00:10,160 --> 00:00:14,240
student to go back and visit it

7
00:00:12,320 --> 00:00:15,920
read the papers read the white papers

8
00:00:14,240 --> 00:00:18,000
look at the slides watch the videos

9
00:00:15,920 --> 00:00:19,760
multiple times for each if you really

10
00:00:18,000 --> 00:00:23,519
care about this material you have to

11
00:00:19,760 --> 00:00:26,320
read a lot of stuff so let's move our

12
00:00:23,519 --> 00:00:29,279
threat tree off to the side and next i'm

13
00:00:26,320 --> 00:00:30,960
going to bulk multiple attacks under a

14
00:00:29,279 --> 00:00:33,680
umbrella category that i'm going to call

15
00:00:30,960 --> 00:00:36,000
remap attacks these have to do with when

16
00:00:33,680 --> 00:00:39,200
someone finds a clever way to

17
00:00:36,000 --> 00:00:42,160
map something else over smm and then

18
00:00:39,200 --> 00:00:44,160
subsequently right into smm

19
00:00:42,160 --> 00:00:46,399
so the first one is the most literal

20
00:00:44,160 --> 00:00:48,559
remap attack because there's a couple of

21
00:00:46,399 --> 00:00:50,079
registers that are called remap limit

22
00:00:48,559 --> 00:00:51,840
and rebat base

23
00:00:50,079 --> 00:00:53,920
and there's a defense of locking those

24
00:00:51,840 --> 00:00:55,039
registers as well as locking some other

25
00:00:53,920 --> 00:00:57,280
registers

26
00:00:55,039 --> 00:00:59,920
so let's see about that now i told you

27
00:00:57,280 --> 00:01:02,640
before that because the memory mapped i

28
00:00:59,920 --> 00:01:04,559
o range steals a chunk of the physical

29
00:01:02,640 --> 00:01:06,320
memory address space

30
00:01:04,559 --> 00:01:08,880
it is you know undesirable you've just

31
00:01:06,320 --> 00:01:10,400
lost megabytes and megabytes of ram that

32
00:01:08,880 --> 00:01:12,880
you could otherwise be using for your

33
00:01:10,400 --> 00:01:16,479
operating system so there is this

34
00:01:12,880 --> 00:01:18,080
capability to remap a chunk of higher

35
00:01:16,479 --> 00:01:20,880
physical address space above four

36
00:01:18,080 --> 00:01:23,520
gigabytes and have it be that that ram

37
00:01:20,880 --> 00:01:25,680
that was lost to mmi oh gets reused and

38
00:01:23,520 --> 00:01:27,280
remapped up to high address range

39
00:01:25,680 --> 00:01:29,600
so this is the picture i showed you

40
00:01:27,280 --> 00:01:31,520
before from the invisible things labs

41
00:01:29,600 --> 00:01:34,320
research in 2009

42
00:01:31,520 --> 00:01:36,400
and like i said there is just literally

43
00:01:34,320 --> 00:01:38,640
a register called remap limit and a

44
00:01:36,400 --> 00:01:41,200
register called remap base and they say

45
00:01:38,640 --> 00:01:44,240
to take memory from below four gigabyte

46
00:01:41,200 --> 00:01:46,880
and that is used at uh for mmio right

47
00:01:44,240 --> 00:01:49,200
here and map it up to there so what they

48
00:01:46,880 --> 00:01:52,240
had found was that they could take this

49
00:01:49,200 --> 00:01:54,880
range and overlap it with smram and

50
00:01:52,240 --> 00:01:56,960
consequently writing to this ram up here

51
00:01:54,880 --> 00:01:59,119
would allow them to write into smram and

52
00:01:56,960 --> 00:02:01,040
similarly read from it so first they

53
00:01:59,119 --> 00:02:02,880
read from it then they audited the code

54
00:02:01,040 --> 00:02:04,719
then they found vulnerabilities

55
00:02:02,880 --> 00:02:06,399
so the fix for that was you know they

56
00:02:04,719 --> 00:02:08,560
just had a one-liner here that said

57
00:02:06,399 --> 00:02:10,160
intel patched the bug in 2008 this was

58
00:02:08,560 --> 00:02:11,760
done by patching the bios code to

59
00:02:10,160 --> 00:02:15,840
properly lock the memory configuration

60
00:02:11,760 --> 00:02:18,080
registers now this attack was against a

61
00:02:15,840 --> 00:02:20,239
actual intel system an intel reference

62
00:02:18,080 --> 00:02:22,239
developer board but it's always very

63
00:02:20,239 --> 00:02:24,319
much the case that just because you know

64
00:02:22,239 --> 00:02:26,080
intel patches their particular bios

65
00:02:24,319 --> 00:02:27,599
doesn't mean that this doesn't exist on

66
00:02:26,080 --> 00:02:29,760
a whole bunch of other biases from a

67
00:02:27,599 --> 00:02:30,879
whole bunch of other vendors so you know

68
00:02:29,760 --> 00:02:32,959
this was a

69
00:02:30,879 --> 00:02:35,680
interesting little you know quirk and i

70
00:02:32,959 --> 00:02:37,440
think at you know circa 2009 uh

71
00:02:35,680 --> 00:02:39,040
researchers were not as good about you

72
00:02:37,440 --> 00:02:40,400
know going around to every vendor and

73
00:02:39,040 --> 00:02:42,640
saying does this apply to you does this

74
00:02:40,400 --> 00:02:44,640
apply to you that's uh something that

75
00:02:42,640 --> 00:02:47,760
you know a lot of us learned in circa

76
00:02:44,640 --> 00:02:49,120
2000 you know 13 14 15 about the

77
00:02:47,760 --> 00:02:51,200
coordination between different

78
00:02:49,120 --> 00:02:52,480
biospenders okay well that was see

79
00:02:51,200 --> 00:02:54,480
that's as much as i'm going to be going

80
00:02:52,480 --> 00:02:55,440
into these things that was the remap

81
00:02:54,480 --> 00:02:58,000
attack

82
00:02:55,440 --> 00:03:00,159
next is overlapping the guard graphics

83
00:02:58,000 --> 00:03:03,120
aperture remap table

84
00:03:00,159 --> 00:03:04,959
with the smm so there's a graphics

85
00:03:03,120 --> 00:03:08,800
address remapping table

86
00:03:04,959 --> 00:03:11,440
and deflo in 2007 described the guard in

87
00:03:08,800 --> 00:03:14,159
the context of virtualization attacks

88
00:03:11,440 --> 00:03:16,879
and then later on in 2010 after the

89
00:03:14,159 --> 00:03:18,159
research on smm had started to pick up

90
00:03:16,879 --> 00:03:20,400
he had you know mentioned that

91
00:03:18,159 --> 00:03:22,959
specifically in the context of attacking

92
00:03:20,400 --> 00:03:25,440
smm while simultaneously saying that you

93
00:03:22,959 --> 00:03:27,680
know the guard has gone away from newer

94
00:03:25,440 --> 00:03:29,519
hardware and so wasn't applicable but

95
00:03:27,680 --> 00:03:31,519
there's a picture that i like a little

96
00:03:29,519 --> 00:03:33,360
bit better available from the intel

97
00:03:31,519 --> 00:03:35,280
folks when they redescribed it

98
00:03:33,360 --> 00:03:37,680
anyways the basics of it as described in

99
00:03:35,280 --> 00:03:39,440
the original paper was that there's this

100
00:03:37,680 --> 00:03:41,599
graphics aperture

101
00:03:39,440 --> 00:03:44,560
and if you overlapped the graphics

102
00:03:41,599 --> 00:03:47,200
aperture with sm ram it would mean that

103
00:03:44,560 --> 00:03:48,799
as it says calls to the smram will first

104
00:03:47,200 --> 00:03:51,440
reach the graphics aperture and be

105
00:03:48,799 --> 00:03:54,319
redirected to b so an attacker could put

106
00:03:51,440 --> 00:03:55,920
some code over here and consequently

107
00:03:54,319 --> 00:03:57,519
overlap this thing and it would redirect

108
00:03:55,920 --> 00:03:59,599
to the attacker's code instead of the

109
00:03:57,519 --> 00:04:02,000
real smm code so a slightly more

110
00:03:59,599 --> 00:04:04,080
detailed picture sort of relevant in the

111
00:04:02,000 --> 00:04:06,000
context of you know we saw stuff about

112
00:04:04,080 --> 00:04:08,159
stolen graphics memory and things like

113
00:04:06,000 --> 00:04:11,439
that in the context of tseg so there's

114
00:04:08,159 --> 00:04:13,519
this gtt mmi oh there's the graphics

115
00:04:11,439 --> 00:04:15,680
memory there's a graphics aperture and

116
00:04:13,519 --> 00:04:17,840
whereas normally it's supposed to be the

117
00:04:15,680 --> 00:04:19,440
case that you know graphics aperture is

118
00:04:17,840 --> 00:04:21,840
you know redirecting into graphics

119
00:04:19,440 --> 00:04:25,199
memory instead it can be made to

120
00:04:21,840 --> 00:04:26,400
redirect into sm ram and consequently

121
00:04:25,199 --> 00:04:29,840
these

122
00:04:26,400 --> 00:04:32,960
gtt ptes as in page table entries can

123
00:04:29,840 --> 00:04:35,040
allow for redirection into smram

124
00:04:32,960 --> 00:04:36,400
net result is that an attacker can read

125
00:04:35,040 --> 00:04:38,160
and write smram when they're not

126
00:04:36,400 --> 00:04:41,199
supposed to be able to

127
00:04:38,160 --> 00:04:42,560
the next attack is overlapping the apic

128
00:04:41,199 --> 00:04:45,040
advanced programmable interrupt

129
00:04:42,560 --> 00:04:49,280
controller with smm all right so in the

130
00:04:45,040 --> 00:04:51,440
memory sinkhole attack by demos at 2015

131
00:04:49,280 --> 00:04:52,960
he basically took in advance overlapped

132
00:04:51,440 --> 00:04:55,360
the advanced programmable interrupt

133
00:04:52,960 --> 00:04:58,320
controller which is a memory mapped io

134
00:04:55,360 --> 00:05:00,080
range with smm now we said at the time

135
00:04:58,320 --> 00:05:02,080
that there was actually a hardware fix

136
00:05:00,080 --> 00:05:03,680
that was added into second generation

137
00:05:02,080 --> 00:05:06,479
cores and newer

138
00:05:03,680 --> 00:05:08,800
and so those were about circa 2013 and

139
00:05:06,479 --> 00:05:11,039
this was time found in 2015 so that

140
00:05:08,800 --> 00:05:13,759
means you know intel had closed the

141
00:05:11,039 --> 00:05:15,759
fundamental hole in hardware uh

142
00:05:13,759 --> 00:05:18,639
by the time this was actually found

143
00:05:15,759 --> 00:05:20,160
so the basic idea was that you know

144
00:05:18,639 --> 00:05:22,880
again it was kind of exploiting this

145
00:05:20,160 --> 00:05:24,960
fact of sort of like the cash attack of

146
00:05:22,880 --> 00:05:27,919
if the processor

147
00:05:24,960 --> 00:05:30,479
sees that the apic range is over

148
00:05:27,919 --> 00:05:33,440
overlapped with the smm range

149
00:05:30,479 --> 00:05:35,440
then its access is going to effectively

150
00:05:33,440 --> 00:05:37,680
bypass the mch it's going to be you know

151
00:05:35,440 --> 00:05:40,560
routing here but it's actually routing

152
00:05:37,680 --> 00:05:43,280
to you know ram which is sm ram so it's

153
00:05:40,560 --> 00:05:44,720
bypassing the access controls of the mch

154
00:05:43,280 --> 00:05:46,560
at the time or it would be you know

155
00:05:44,720 --> 00:05:48,479
nominally the pch now had it not been

156
00:05:46,560 --> 00:05:50,080
fixed in other hardware the sort of

157
00:05:48,479 --> 00:05:52,479
interesting thing about it was that it

158
00:05:50,080 --> 00:05:54,639
was an extremely extremely constrained

159
00:05:52,479 --> 00:05:56,639
attack there was just you know certain

160
00:05:54,639 --> 00:05:59,680
bytes that were actually writable within

161
00:05:56,639 --> 00:06:01,759
the apic range but he still found a way

162
00:05:59,680 --> 00:06:03,759
to you know pull it off on some systems

163
00:06:01,759 --> 00:06:05,600
to make it so that he could

164
00:06:03,759 --> 00:06:07,199
still effectively corrupt the smram in a

165
00:06:05,600 --> 00:06:08,720
way that allowed him to get arbitrary

166
00:06:07,199 --> 00:06:10,400
code execution

167
00:06:08,720 --> 00:06:12,240
now the thing that i disagreed with

168
00:06:10,400 --> 00:06:13,600
about this research is that you know he

169
00:06:12,240 --> 00:06:15,520
claimed at the time it was an

170
00:06:13,600 --> 00:06:18,080
unpatchable vulnerability

171
00:06:15,520 --> 00:06:20,319
and i don't think that's really true so

172
00:06:18,080 --> 00:06:22,639
basically a system management interrupt

173
00:06:20,319 --> 00:06:25,440
handler could check immediately upon

174
00:06:22,639 --> 00:06:28,160
entry into smm you know is the apic has

175
00:06:25,440 --> 00:06:30,400
that been overlapped with my smram range

176
00:06:28,160 --> 00:06:32,080
now it is true that if it does that and

177
00:06:30,400 --> 00:06:35,680
if it you know for instance either

178
00:06:32,080 --> 00:06:37,919
crashes out or if it uh relocates the

179
00:06:35,680 --> 00:06:40,160
apic back to a sane memory range instead

180
00:06:37,919 --> 00:06:41,759
of sm ram memory range it would be

181
00:06:40,160 --> 00:06:44,880
breaking the quote-unquote feature of

182
00:06:41,759 --> 00:06:46,479
being able to relocate the apic but i

183
00:06:44,880 --> 00:06:48,240
personally don't think that many people

184
00:06:46,479 --> 00:06:50,639
were actually using that as it was

185
00:06:48,240 --> 00:06:52,800
intended and you could make an argument

186
00:06:50,639 --> 00:06:54,080
that even if they were they really

187
00:06:52,800 --> 00:06:56,319
should have been you know checking

188
00:06:54,080 --> 00:06:58,160
whether or not it was overlapping

189
00:06:56,319 --> 00:07:00,960
so regardless this is just another

190
00:06:58,160 --> 00:07:02,960
example of a clever use of remapping in

191
00:07:00,960 --> 00:07:04,960
order to access smram when you're not

192
00:07:02,960 --> 00:07:06,880
supposed to be able to

193
00:07:04,960 --> 00:07:09,360
and finally the last

194
00:07:06,880 --> 00:07:11,919
remapping attack is overlapping pcie

195
00:07:09,360 --> 00:07:14,800
mmio bars with smm

196
00:07:11,919 --> 00:07:16,880
so you learned all about the mmio bars

197
00:07:14,800 --> 00:07:18,880
in the pcie section

198
00:07:16,880 --> 00:07:21,840
and the interesting thing here is

199
00:07:18,880 --> 00:07:24,800
basically what if an attacker overlapped

200
00:07:21,840 --> 00:07:26,880
a bar with smm and then specifically it

201
00:07:24,800 --> 00:07:30,160
can't just be you know any bar it has to

202
00:07:26,880 --> 00:07:33,120
be essentially a bar that smm the smi

203
00:07:30,160 --> 00:07:34,720
handler itself actually accesses so in

204
00:07:33,120 --> 00:07:36,240
the research they showed you know a

205
00:07:34,720 --> 00:07:38,319
bunch of different

206
00:07:36,240 --> 00:07:41,680
base address registers that are accessed

207
00:07:38,319 --> 00:07:43,759
via smi handlers so the idea is step one

208
00:07:41,680 --> 00:07:47,280
from outside of the os you overlap the

209
00:07:43,759 --> 00:07:49,680
mmio range with smm step two you wait

210
00:07:47,280 --> 00:07:52,879
for the smi handler to write to what

211
00:07:49,680 --> 00:07:55,280
they think is a pci memory mapped i o

212
00:07:52,879 --> 00:07:58,400
range but it will actually be redirected

213
00:07:55,280 --> 00:08:00,479
in in the context of smm and actually

214
00:07:58,400 --> 00:08:04,160
overwrite themselves somewhere inside of

215
00:08:00,479 --> 00:08:06,319
this range so another remapping attack

216
00:08:04,160 --> 00:08:08,400
so the question is are there other ways

217
00:08:06,319 --> 00:08:10,160
to remap right so each of these was kind

218
00:08:08,400 --> 00:08:11,840
of you know found independently they

219
00:08:10,160 --> 00:08:13,360
weren't you know specifically saying i

220
00:08:11,840 --> 00:08:14,639
want to find another way to remap

221
00:08:13,360 --> 00:08:16,720
they're just trying to say like i want

222
00:08:14,639 --> 00:08:19,039
to get into smram somehow

223
00:08:16,720 --> 00:08:22,000
over time one comes to recognize that

224
00:08:19,039 --> 00:08:23,919
this is a general category and so you

225
00:08:22,000 --> 00:08:24,960
know why why can't there be more things

226
00:08:23,919 --> 00:08:26,800
right so

227
00:08:24,960 --> 00:08:29,120
really it comes down to you know someone

228
00:08:26,800 --> 00:08:31,520
needs to read all sorts of chunks of the

229
00:08:29,120 --> 00:08:33,440
intel documentation play around with it

230
00:08:31,520 --> 00:08:36,320
and maybe they will find another

231
00:08:33,440 --> 00:08:39,120
location which can allow for remapping

232
00:08:36,320 --> 00:08:43,839
over sm ram and allowing to write and

233
00:08:39,120 --> 00:08:43,839
bypass the access controls like these


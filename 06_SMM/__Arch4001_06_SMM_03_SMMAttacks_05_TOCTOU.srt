1
00:00:00,160 --> 00:00:03,679
okay now we're going to introduce

2
00:00:01,680 --> 00:00:06,160
another general class of smi

3
00:00:03,679 --> 00:00:09,040
implementation bugs talk to attacks time

4
00:00:06,160 --> 00:00:10,960
of check time of use and you can use the

5
00:00:09,040 --> 00:00:13,200
standard sort of defensive mechanisms

6
00:00:10,960 --> 00:00:15,120
and also as with most talk to attacks

7
00:00:13,200 --> 00:00:17,920
there is a defensive mechanism having to

8
00:00:15,120 --> 00:00:20,080
deal with not fetching data outside

9
00:00:17,920 --> 00:00:22,240
multiple times so time of check time of

10
00:00:20,080 --> 00:00:24,800
use vulnerabilities they're a type of

11
00:00:22,240 --> 00:00:26,560
race condition because essentially you

12
00:00:24,800 --> 00:00:29,199
may check something and say oh yeah this

13
00:00:26,560 --> 00:00:31,920
looks good but then some time passes and

14
00:00:29,199 --> 00:00:34,000
then later on you use it but it could be

15
00:00:31,920 --> 00:00:36,239
changed by then it could be changed

16
00:00:34,000 --> 00:00:37,920
either because the data location that

17
00:00:36,239 --> 00:00:39,280
you're checking is sort of naturally

18
00:00:37,920 --> 00:00:41,120
changing there can be a race condition

19
00:00:39,280 --> 00:00:43,600
where an adversary changes it out from

20
00:00:41,120 --> 00:00:45,920
underneath you or for other reasons so

21
00:00:43,600 --> 00:00:47,920
even though smm is supposed to

22
00:00:45,920 --> 00:00:49,840
synchronize all the processor cores and

23
00:00:47,920 --> 00:00:52,800
it's only supposed to start running smi

24
00:00:49,840 --> 00:00:54,320
handlers once everything is in smm

25
00:00:52,800 --> 00:00:56,239
that means you know theoretically there

26
00:00:54,320 --> 00:00:58,320
should be no attacker controlled code

27
00:00:56,239 --> 00:01:00,879
running out on you know any of the cpus

28
00:00:58,320 --> 00:01:02,640
right and while that may be true the

29
00:01:00,879 --> 00:01:05,280
thing is there are these peripheral

30
00:01:02,640 --> 00:01:07,439
processors which have the capability to

31
00:01:05,280 --> 00:01:09,600
do direct memory access and so that

32
00:01:07,439 --> 00:01:11,439
means even if the attacker is not

33
00:01:09,600 --> 00:01:13,119
capable of running on any of the cpu

34
00:01:11,439 --> 00:01:15,520
cores they could be running on a

35
00:01:13,119 --> 00:01:18,240
peripheral processor which could time a

36
00:01:15,520 --> 00:01:20,799
write to memory in such a way that smm

37
00:01:18,240 --> 00:01:22,560
read some memory and then the dma

38
00:01:20,799 --> 00:01:24,320
happened and changed the memory and then

39
00:01:22,560 --> 00:01:26,000
smm reads the memory again and it's

40
00:01:24,320 --> 00:01:27,520
different the second time now these

41
00:01:26,000 --> 00:01:30,560
peripheral processors can't just

42
00:01:27,520 --> 00:01:33,040
directly dma into the smm range into

43
00:01:30,560 --> 00:01:35,600
smram because that's protected against

44
00:01:33,040 --> 00:01:37,600
dma so here's a vulnerability that the

45
00:01:35,600 --> 00:01:41,280
intel researchers found in the open

46
00:01:37,600 --> 00:01:43,759
source uefi source code and so it has to

47
00:01:41,280 --> 00:01:46,560
do with there's a data size that's added

48
00:01:43,759 --> 00:01:48,399
to a name size you get an info size and

49
00:01:46,560 --> 00:01:50,560
then if the info size is greater than

50
00:01:48,399 --> 00:01:53,119
some size you'll continue and get a

51
00:01:50,560 --> 00:01:55,600
variable and then later on it comes down

52
00:01:53,119 --> 00:01:58,479
here and it once again it checks this

53
00:01:55,600 --> 00:02:00,159
data size from up here and it checks if

54
00:01:58,479 --> 00:02:02,240
it's greater than a

55
00:02:00,159 --> 00:02:03,680
var data size and if so then it does a

56
00:02:02,240 --> 00:02:07,040
mem copy

57
00:02:03,680 --> 00:02:10,160
well this spatial separation here of the

58
00:02:07,040 --> 00:02:11,760
checkup here proceeding down here and a

59
00:02:10,160 --> 00:02:13,840
use down here

60
00:02:11,760 --> 00:02:16,160
means that you could have a attacker

61
00:02:13,840 --> 00:02:17,840
come in and dma and change the contents

62
00:02:16,160 --> 00:02:20,319
so it's coming from a com buffer which

63
00:02:17,840 --> 00:02:22,160
is outside of smm so this particular

64
00:02:20,319 --> 00:02:24,000
type of talk to is what's called a

65
00:02:22,160 --> 00:02:26,239
double fetch vulnerability because it's

66
00:02:24,000 --> 00:02:28,400
reading something outside of its control

67
00:02:26,239 --> 00:02:30,560
and then it's reading it again later on

68
00:02:28,400 --> 00:02:32,959
so dealing with double fetches the

69
00:02:30,560 --> 00:02:35,840
standard mitigation is to basically not

70
00:02:32,959 --> 00:02:38,959
double fetch just fetch once so get a

71
00:02:35,840 --> 00:02:41,120
copy pull it into smm and use only that

72
00:02:38,959 --> 00:02:43,280
copy in smm so that it is subsequently

73
00:02:41,120 --> 00:02:45,840
and not vulnerable to manipulation by a

74
00:02:43,280 --> 00:02:47,599
dma attacker outside of smm

75
00:02:45,840 --> 00:02:49,120
so those are the top two vulnerabilities

76
00:02:47,599 --> 00:02:50,879
standard defensive mechanisms would

77
00:02:49,120 --> 00:02:52,480
apply if the attacker you know breaks

78
00:02:50,879 --> 00:02:54,480
into smm you want them to be as

79
00:02:52,480 --> 00:02:56,319
restricted as possible and the defense

80
00:02:54,480 --> 00:02:57,440
again is to not double fetch in this

81
00:02:56,319 --> 00:02:59,120
case

82
00:02:57,440 --> 00:03:01,040
and of course there can be other

83
00:02:59,120 --> 00:03:02,879
vulnerabilities just generically so this

84
00:03:01,040 --> 00:03:05,840
is the end of our

85
00:03:02,879 --> 00:03:08,080
threat tree for now this is just other

86
00:03:05,840 --> 00:03:10,080
vulnerabilities as a general catch-all

87
00:03:08,080 --> 00:03:12,080
there are other types of vulnerabilities

88
00:03:10,080 --> 00:03:14,159
and you have to read the fun research in

89
00:03:12,080 --> 00:03:16,239
order to go find them

90
00:03:14,159 --> 00:03:17,360
so with that we have covered all of the

91
00:03:16,239 --> 00:03:18,959
attacks that we're going to cover in

92
00:03:17,360 --> 00:03:23,200
this class and it looks like we just

93
00:03:18,959 --> 00:03:23,200
have sleep vulnerabilities left


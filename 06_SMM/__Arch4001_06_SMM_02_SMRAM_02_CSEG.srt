1
00:00:00,040 --> 00:00:02,200
So let's talk about the different locations where

2
00:00:02,200 --> 00:00:03,460
SMRAM
could reside

3
00:00:03,940 --> 00:00:04,830
So we saw that

4
00:00:04,830 --> 00:00:08,320
SMBASE
can be overwritten by the SMI handler via rewriting

5
00:00:08,320 --> 00:00:11,310
the safe state and then it is a 32 bit

6
00:00:11,310 --> 00:00:12,350
value only

7
00:00:12,350 --> 00:00:14,930
So it can only exist somewhere inside of the four

8
00:00:14,930 --> 00:00:16,750
gigabyte physical address range

9
00:00:17,240 --> 00:00:19,390
Now,
typically on modern systems S

10
00:00:19,390 --> 00:00:21,430
Mm is relocated at least once

11
00:00:21,500 --> 00:00:24,500
So it'll start out at S and base of hex

12
00:00:24,500 --> 00:00:25,370
30,000

13
00:00:25,500 --> 00:00:27,560
But then it'll move around for reasons,

14
00:00:27,560 --> 00:00:28,600
we'll see a little bit later

15
00:00:29,140 --> 00:00:30,570
That would look something like this

16
00:00:30,680 --> 00:00:33,350
Let's say that we have the bios here and it's

17
00:00:33,360 --> 00:00:35,250
executing some code from RAM

18
00:00:35,270 --> 00:00:37,720
So the bios may start up in the spi flash

19
00:00:37,720 --> 00:00:40,140
chip but at some point it's going to copy the

20
00:00:40,140 --> 00:00:44,440
code off to RAM so it takes and it copies

21
00:00:44,440 --> 00:00:47,120
the code that it would like to have be the

22
00:00:47,120 --> 00:00:50,960
initial sm RAM code down to physical address hex 30,000

23
00:00:51,540 --> 00:00:52,750
And if you caught it,

24
00:00:53,010 --> 00:00:54,500
that was dark sonic

25
00:00:54,510 --> 00:00:58,080
Following along to be the code that's running in S

26
00:00:58,080 --> 00:00:58,860
mm
Later on,

27
00:00:59,190 --> 00:01:02,510
the bios will also create a copy of the actual

28
00:01:02,510 --> 00:01:04,780
sm room where they're going to want it later somewhere

29
00:01:04,780 --> 00:01:05,280
else

30
00:01:05,390 --> 00:01:09,020
Then they're going to cause a system management interrupt which

31
00:01:09,020 --> 00:01:12,060
will vector to hear this code right here

32
00:01:12,940 --> 00:01:15,690
And then now it's in S mm Now I didn't

33
00:01:15,690 --> 00:01:18,850
actually create this particular picture when I was just you

34
00:01:18,850 --> 00:01:22,450
know,
searching through my MS frizzle pictures before someone else created

35
00:01:22,450 --> 00:01:23,820
this erased face thing

36
00:01:23,820 --> 00:01:26,650
And I just thought it looked interesting and I actually

37
00:01:26,650 --> 00:01:29,490
found this before I found the non erased faced version

38
00:01:29,580 --> 00:01:33,660
But this is a perfect representation of the code executing

39
00:01:33,660 --> 00:01:34,010
in S

40
00:01:34,010 --> 00:01:34,510
Mm

41
00:01:34,700 --> 00:01:35,870
It's in a dark place

42
00:01:35,870 --> 00:01:38,390
It's faceless because you can't see it for reasons we'll

43
00:01:38,390 --> 00:01:39,060
see later

44
00:01:39,440 --> 00:01:40,520
So yeah that's S mm

45
00:01:40,530 --> 00:01:41,750
What does S mm do,

46
00:01:41,750 --> 00:01:45,660
what does this first initial relocating Sm RAM do?

47
00:01:45,790 --> 00:01:48,540
Well it starts at 3800

48
00:01:48,540 --> 00:01:49,620
So the I

49
00:01:49,620 --> 00:01:49,960
P

50
00:01:49,960 --> 00:01:50,990
Is 8000

51
00:01:50,990 --> 00:01:53,120
The bases 30,000

52
00:01:53,130 --> 00:01:56,950
So the entry point is there at 38,000 I think

53
00:01:56,950 --> 00:01:58,750
I said 30 838,000

54
00:01:59,340 --> 00:02:01,540
And what it's going to do is it's going to

55
00:02:01,540 --> 00:02:05,260
take this saved area where the safe state from before

56
00:02:05,270 --> 00:02:06,420
S
Mm was running

57
00:02:06,440 --> 00:02:09,710
And it's going to rewrite that with a new sm

58
00:02:09,710 --> 00:02:13,390
base for the location of the actual later on SmR

59
00:02:13,600 --> 00:02:15,810
So you can see this code can be pretty simple

60
00:02:15,810 --> 00:02:17,820
This just has to be a pretty simple stub that

61
00:02:17,820 --> 00:02:20,740
just kind of smashes S and base with some correct

62
00:02:20,740 --> 00:02:23,560
value that the bios actually wants the sm room to

63
00:02:23,560 --> 00:02:24,060
live at

64
00:02:24,190 --> 00:02:27,700
And then after that it can just resume and boom

65
00:02:27,710 --> 00:02:29,450
like that you're back in bios

66
00:02:29,940 --> 00:02:32,370
So now that you have resumed from S

67
00:02:32,370 --> 00:02:32,700
M
M

68
00:02:32,700 --> 00:02:36,040
S
And base has been updated with whatever value was saved

69
00:02:36,040 --> 00:02:37,410
there before X

70
00:02:37,410 --> 00:02:39,260
123000

71
00:02:40,440 --> 00:02:42,560
And the actual dark sonic

72
00:02:42,560 --> 00:02:45,850
The actual code that's going to run in Sm RAM

73
00:02:45,860 --> 00:02:46,850
is going to be here

74
00:02:46,890 --> 00:02:49,680
So the next time there's a system management interrupt as

75
00:02:49,680 --> 00:02:52,170
a base is going to be 123000

76
00:02:52,180 --> 00:02:54,470
And this is the code that's actually going to run

77
00:02:54,770 --> 00:02:55,130
All right

78
00:02:55,130 --> 00:02:57,740
Intel defines a few locations for S

79
00:02:57,740 --> 00:03:00,850
M RAM and this guidance is basically trying to accommodate

80
00:03:00,850 --> 00:03:01,520
a few things

81
00:03:01,640 --> 00:03:04,130
One is trying to make it easier for bios developers

82
00:03:04,130 --> 00:03:04,880
to just know,

83
00:03:04,890 --> 00:03:07,010
hey,
you should probably put it there too

84
00:03:07,010 --> 00:03:10,130
It's trying to avoid SM RAM overlapping with other stuff

85
00:03:10,140 --> 00:03:10,950
And three,

86
00:03:10,950 --> 00:03:14,090
there are hardware mechanisms put in place to protect SM

87
00:03:14,090 --> 00:03:17,260
RAM and if you want to use those mechanisms,

88
00:03:17,440 --> 00:03:20,560
if you're trying to do something actually that requires restriction

89
00:03:20,560 --> 00:03:23,150
then you should abide by intel's recommendations in order to

90
00:03:23,150 --> 00:03:24,020
get that protection

91
00:03:24,640 --> 00:03:27,290
And so this is all just another part of the

92
00:03:27,290 --> 00:03:29,350
biases job of building up a memory map

93
00:03:29,840 --> 00:03:34,740
Now,
older documentation listed these three locations as possible

94
00:03:34,750 --> 00:03:38,860
S MM locations but on modern PCH systems there's only

95
00:03:38,860 --> 00:03:39,500
two options

96
00:03:39,500 --> 00:03:40,550
So we'll focus on that

97
00:03:41,040 --> 00:03:45,270
There's the compatible or C SEG range and there's just

98
00:03:45,270 --> 00:03:46,520
the thing called T Segway,

99
00:03:46,520 --> 00:03:48,810
they never exactly tell you what T stands for

100
00:03:48,920 --> 00:03:51,540
So we're going to concern ourselves primarily with these,

101
00:03:51,550 --> 00:03:53,930
starting with C SEG and then moving on to T

102
00:03:53,930 --> 00:03:54,430
Segue

103
00:03:55,140 --> 00:03:55,470
Well,

104
00:03:55,470 --> 00:03:58,770
this was our memory map Tetris from before and we

105
00:03:58,770 --> 00:04:00,470
saw within that there was an S

106
00:04:00,470 --> 00:04:04,050
M RAM range that was listed as being at T

107
00:04:04,050 --> 00:04:07,380
sing well with that we'll get to that,

108
00:04:07,380 --> 00:04:08,780
that's going to be T segue for later

109
00:04:08,780 --> 00:04:11,810
But the C SEG is actually down here in this

110
00:04:11,980 --> 00:04:14,120
DOS compatible memory range

111
00:04:14,130 --> 00:04:17,220
So let's drill in on that and see what's stored

112
00:04:17,220 --> 00:04:20,060
there from the fourth generation CPU data sheets

113
00:04:20,060 --> 00:04:23,920
We see something like this and it's basically saying that

114
00:04:23,930 --> 00:04:27,100
S mm will live at a fixed area starting at

115
00:04:27,100 --> 00:04:30,510
physical address A and four zeros and ending at physical

116
00:04:30,510 --> 00:04:32,090
address B and four chefs

117
00:04:32,440 --> 00:04:35,450
There is also referred to as the legacy video area

118
00:04:35,600 --> 00:04:38,860
So this memory ranges actually reused

119
00:04:39,240 --> 00:04:41,840
If the processor is in S mm then it can

120
00:04:41,840 --> 00:04:44,760
be used for system management mode code

121
00:04:45,140 --> 00:04:47,840
But if the processor is not an S mm then

122
00:04:47,840 --> 00:04:50,620
it will be used as the sort of legacy or

123
00:04:50,620 --> 00:04:52,410
DOS video memory area

124
00:04:52,720 --> 00:04:54,840
And as I was googling around because I was just

125
00:04:54,850 --> 00:04:56,830
curious to learn a little bit more about that

126
00:04:56,830 --> 00:04:59,310
I found this good website down here where if you

127
00:04:59,310 --> 00:05:01,520
want to go and learn a little bit more about

128
00:05:01,520 --> 00:05:03,350
how dos used that you can

129
00:05:03,640 --> 00:05:05,710
But for our purposes we're going to think of this

130
00:05:05,720 --> 00:05:09,430
A two will just say A to C as S

131
00:05:09,430 --> 00:05:09,930
M RAM

132
00:05:10,440 --> 00:05:12,830
So just as an fyi while we have this picture

133
00:05:12,830 --> 00:05:15,340
up,
I would just point out that this range right here

134
00:05:15,350 --> 00:05:18,260
F and 000 up to F and F

135
00:05:18,260 --> 00:05:18,950
F
F

136
00:05:18,960 --> 00:05:22,110
This would have been the original 80 86 reset vector

137
00:05:22,120 --> 00:05:25,290
And even on later systems like 80 to 86 which

138
00:05:25,290 --> 00:05:27,450
have 32 bit support still,

139
00:05:27,450 --> 00:05:29,580
if they would have changed the code segment,

140
00:05:29,580 --> 00:05:31,620
they would be sort of restricted to that range right

141
00:05:31,620 --> 00:05:32,120
there

142
00:05:32,140 --> 00:05:35,860
And note this area right here is the expansion area

143
00:05:35,870 --> 00:05:38,790
and that was used for option RAMs back on old

144
00:05:38,790 --> 00:05:41,380
bios is they would copy it into this physical address

145
00:05:41,380 --> 00:05:43,660
range and execute the option rooms from there

146
00:05:44,140 --> 00:05:45,350
So again,

147
00:05:45,360 --> 00:05:48,740
this is going to be the compatible or C Seg

148
00:05:48,750 --> 00:05:51,670
memory range for S mm where dark sonic is hiding

149
00:05:51,670 --> 00:05:52,560
behind the scenes

150
00:05:53,540 --> 00:05:56,000
How do you enable that compatible SMRAM range

151
00:05:56,000 --> 00:05:58,080
We said it could be video memory or it could

152
00:05:58,080 --> 00:05:59,260
be SMRAM

153
00:05:59,740 --> 00:06:00,070
Well,

154
00:06:00,070 --> 00:06:03,940
there is a register called SMRAMC System management RAM

155
00:06:03,940 --> 00:06:08,600
control and this has a G_SMRAME

156
00:06:08,610 --> 00:06:12,660
global or whatever SMRAM enable

157
00:06:13,040 --> 00:06:14,590
And it says that if there's a set to one

158
00:06:14,590 --> 00:06:16,650
compatible SMRAM functions are enabled,

159
00:06:16,650 --> 00:06:19,660
providing 1 28 K of diagram accessible at a

160
00:06:20,040 --> 00:06:20,700
you know,

161
00:06:20,700 --> 00:06:22,840
starting at a zero and so going up to B

162
00:06:22,840 --> 00:06:25,130
f F F F and then it says once D

163
00:06:25,130 --> 00:06:25,670
lock is set,

164
00:06:25,670 --> 00:06:26,990
this bit becomes our oh,

165
00:06:27,000 --> 00:06:29,150
so we're going to learn about deadlock in a second

166
00:06:29,840 --> 00:06:30,250
Now,

167
00:06:30,250 --> 00:06:32,370
I would just make the point that this sm room

168
00:06:32,370 --> 00:06:35,920
register was located in a different location back on MCHes

169
00:06:36,320 --> 00:06:38,600
0:0:0, 0x9D

170
00:06:38,650 --> 00:06:41,940
And then it disappeared in the documentation for the first

171
00:06:41,940 --> 00:06:44,880
through third generation cores And then it reappeared magically in

172
00:06:44,880 --> 00:06:46,190
the fourth generation,

173
00:06:46,190 --> 00:06:47,560
at a slightly different address,

174
00:06:47,570 --> 00:06:49,460
0:0:0 0x88

175
00:06:49,740 --> 00:06:52,060
And then it has now disappeared again on the 10th

176
00:06:52,060 --> 00:06:53,310
generation systems

177
00:06:53,780 --> 00:06:56,353
And actually some of the beta testers pointer out

178
00:06:56,353 --> 00:06:57,838
that it actually does appear

179
00:06:57,838 --> 00:07:00,949
on the 10th generation Comet Lake systems  

180
00:07:00,949 --> 00:07:04,715
but then it disappears in the 10th generation Ice Lake systems

181
00:07:04,715 --> 00:07:07,044
So, pretty sure at this point that it...

182
00:07:07,044 --> 00:07:09,537
compatible CSEG is actually

183
00:07:09,537 --> 00:07:11,493
being unsupported starting from the

184
00:07:11,493 --> 00:07:14,534
10th generation Ice Lake systems


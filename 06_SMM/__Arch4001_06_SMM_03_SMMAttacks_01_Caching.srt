1
00:00:00,160 --> 00:00:04,720
all right you are here the first on a

2
00:00:02,399 --> 00:00:06,319
few sections on attacks on system

3
00:00:04,720 --> 00:00:08,400
management mode

4
00:00:06,319 --> 00:00:11,040
so this particular attack is going to be

5
00:00:08,400 --> 00:00:14,000
cache misconfiguration and the defense

6
00:00:11,040 --> 00:00:16,400
setting smrrs let's see what that means

7
00:00:14,000 --> 00:00:19,039
well we need to refresh you a little bit

8
00:00:16,400 --> 00:00:21,840
on some stuff about caching and how it's

9
00:00:19,039 --> 00:00:23,920
managed on intel systems so caching is

10
00:00:21,840 --> 00:00:26,080
available in all operating system modes

11
00:00:23,920 --> 00:00:28,320
including system management mode

12
00:00:26,080 --> 00:00:31,439
and typically what are used to configure

13
00:00:28,320 --> 00:00:34,559
caching are some msrs called memory type

14
00:00:31,439 --> 00:00:37,600
range registers mtrrs so it's a special

15
00:00:34,559 --> 00:00:40,079
type of mt of msr that has a form like

16
00:00:37,600 --> 00:00:42,399
this you've got a physical base and

17
00:00:40,079 --> 00:00:43,920
you've got a physical mask the base is

18
00:00:42,399 --> 00:00:45,280
going to say what type it is and then

19
00:00:43,920 --> 00:00:47,280
it's going to have an address and then

20
00:00:45,280 --> 00:00:48,559
the mask is used to define the range

21
00:00:47,280 --> 00:00:50,160
that it covers

22
00:00:48,559 --> 00:00:53,039
now typically these are going to be

23
00:00:50,160 --> 00:00:55,199
configured by the bios to allow for a

24
00:00:53,039 --> 00:00:57,280
fast startup but the operating system

25
00:00:55,199 --> 00:00:59,280
can configure them as well

26
00:00:57,280 --> 00:01:02,640
now while there's a bunch of different

27
00:00:59,280 --> 00:01:04,320
types that can be used in those mtrrs

28
00:01:02,640 --> 00:01:06,479
we're going to focus only on the one

29
00:01:04,320 --> 00:01:09,600
that was discussed in the attacks in

30
00:01:06,479 --> 00:01:11,600
2009 it was first reported by some

31
00:01:09,600 --> 00:01:13,840
french government researchers and later

32
00:01:11,600 --> 00:01:15,920
by the invisible things labs folks who

33
00:01:13,840 --> 00:01:17,520
discovered it independently and found

34
00:01:15,920 --> 00:01:19,920
out from intel that someone had already

35
00:01:17,520 --> 00:01:22,560
beat them to it so how does this write

36
00:01:19,920 --> 00:01:24,720
back the wb work

37
00:01:22,560 --> 00:01:26,479
well right back caching is basically

38
00:01:24,720 --> 00:01:28,720
trying to reduce the amount of traffic

39
00:01:26,479 --> 00:01:31,360
between the processor and the memory and

40
00:01:28,720 --> 00:01:34,079
so reads come from the cache lines on

41
00:01:31,360 --> 00:01:36,079
cache hits writes are performed back

42
00:01:34,079 --> 00:01:38,640
into the cache and they are not

43
00:01:36,079 --> 00:01:40,640
immediately flushed or written to memory

44
00:01:38,640 --> 00:01:43,040
if there's a read or a right miss

45
00:01:40,640 --> 00:01:45,439
that'll cause a cash fill and if there's

46
00:01:43,040 --> 00:01:47,280
any sort of modified line caused by

47
00:01:45,439 --> 00:01:49,200
writes and things like that it is going

48
00:01:47,280 --> 00:01:50,560
to eventually be written back to memory

49
00:01:49,200 --> 00:01:53,439
obviously you have to have memory

50
00:01:50,560 --> 00:01:54,479
coherency but it's going to be at some

51
00:01:53,439 --> 00:01:56,560
later time

52
00:01:54,479 --> 00:01:58,880
simply put for our purposes we can just

53
00:01:56,560 --> 00:02:01,920
think of it like if a memory range has

54
00:01:58,880 --> 00:02:05,680
been defined as right-back caching the

55
00:02:01,920 --> 00:02:07,680
cpu will do its best to always use cache

56
00:02:05,680 --> 00:02:09,599
version of things it's going to read

57
00:02:07,680 --> 00:02:12,160
from cash it's going to write to cash

58
00:02:09,599 --> 00:02:14,560
and it's mostly not going to write that

59
00:02:12,160 --> 00:02:16,640
cash value back to ram except at some

60
00:02:14,560 --> 00:02:19,280
particular times we don't care about

61
00:02:16,640 --> 00:02:22,959
so deflow had pointed out that there is

62
00:02:19,280 --> 00:02:26,000
a problem with the coherency because the

63
00:02:22,959 --> 00:02:28,319
cpu is accessing and operating on the

64
00:02:26,000 --> 00:02:30,879
cache values whereas the protection

65
00:02:28,319 --> 00:02:32,640
mechanism for smram they were mostly

66
00:02:30,879 --> 00:02:35,120
dealing with compatible smram in the

67
00:02:32,640 --> 00:02:36,959
context of this 2009 talk so think of

68
00:02:35,120 --> 00:02:38,160
the protection mechanism meaning the

69
00:02:36,959 --> 00:02:40,560
d-lock and

70
00:02:38,160 --> 00:02:42,640
well just the d-lock bit i guess so that

71
00:02:40,560 --> 00:02:46,160
protection mechanism the d-lock is in

72
00:02:42,640 --> 00:02:48,800
the mch at the time pch now so here's

73
00:02:46,160 --> 00:02:51,440
what things look like you had your cpu

74
00:02:48,800 --> 00:02:53,440
and its cache and you had smram with the

75
00:02:51,440 --> 00:02:55,280
d lock which is basically saying this is

76
00:02:53,440 --> 00:02:56,879
locked down it's going to translate to

77
00:02:55,280 --> 00:02:59,120
video memory it's not going to translate

78
00:02:56,879 --> 00:03:02,000
ram system management interrupt is

79
00:02:59,120 --> 00:03:03,920
triggered and then sm ram becomes

80
00:03:02,000 --> 00:03:06,080
available so you're in system management

81
00:03:03,920 --> 00:03:08,239
mode now and therefore this decodes to

82
00:03:06,080 --> 00:03:10,959
smram instead of video memory

83
00:03:08,239 --> 00:03:13,280
that access that invocation of the

84
00:03:10,959 --> 00:03:16,239
initial code is going to lead to that

85
00:03:13,280 --> 00:03:19,519
code being cached in the cache

86
00:03:16,239 --> 00:03:21,840
when you resume out of smm that code can

87
00:03:19,519 --> 00:03:24,080
still be in the cache and you know even

88
00:03:21,840 --> 00:03:25,360
though the d-lock has been locked back

89
00:03:24,080 --> 00:03:28,720
down

90
00:03:25,360 --> 00:03:31,360
so now if an attacker writes to you know

91
00:03:28,720 --> 00:03:34,239
nominally smram or whatever it takes to

92
00:03:31,360 --> 00:03:38,239
hit the same cache line then the next

93
00:03:34,239 --> 00:03:40,879
time that the cpu enters into smm it's

94
00:03:38,239 --> 00:03:42,799
going to prefer to use the cached copy

95
00:03:40,879 --> 00:03:44,879
because the attacker has marked that

96
00:03:42,799 --> 00:03:46,400
particular range as right back and the

97
00:03:44,879 --> 00:03:48,400
cpu is going to say oh that's right back

98
00:03:46,400 --> 00:03:49,920
memory i'm going to use i've got a hit

99
00:03:48,400 --> 00:03:51,519
on the cache so i'm going to use the

100
00:03:49,920 --> 00:03:53,519
copy from the cache

101
00:03:51,519 --> 00:03:55,360
but that can mean that it is the

102
00:03:53,519 --> 00:03:57,439
attacker's code that is being executed

103
00:03:55,360 --> 00:03:59,200
from cache rather than the original

104
00:03:57,439 --> 00:04:01,439
system management interrupt handler and

105
00:03:59,200 --> 00:04:03,680
you can see how it wouldn't take much to

106
00:04:01,439 --> 00:04:05,760
redirect control flow that attacker's

107
00:04:03,680 --> 00:04:08,159
code really just needs to be a jump

108
00:04:05,760 --> 00:04:10,560
somewhere outside of smram where they've

109
00:04:08,159 --> 00:04:11,920
already staged some attacker's code and

110
00:04:10,560 --> 00:04:13,599
then they don't have to worry about this

111
00:04:11,920 --> 00:04:15,840
protection whatsoever

112
00:04:13,599 --> 00:04:18,560
so the fix for this was that intel

113
00:04:15,840 --> 00:04:21,120
created some other msrs called system

114
00:04:18,560 --> 00:04:22,880
management range registers smrrs they

115
00:04:21,120 --> 00:04:24,400
have a fizz base and a fizz mask just

116
00:04:22,880 --> 00:04:27,199
like mtrrs

117
00:04:24,400 --> 00:04:29,280
and basically they are going to override

118
00:04:27,199 --> 00:04:32,080
whatever the mtrs say they're going to

119
00:04:29,280 --> 00:04:34,639
take preference and precedence and so

120
00:04:32,080 --> 00:04:36,320
you know the bios and smm can set what

121
00:04:34,639 --> 00:04:39,520
they want the caching

122
00:04:36,320 --> 00:04:41,840
strategy to be for smm and then these

123
00:04:39,520 --> 00:04:43,759
smrr's will take precedence

124
00:04:41,840 --> 00:04:46,560
additionally the smrr's can only be

125
00:04:43,759 --> 00:04:48,560
written to when the processor is in smm

126
00:04:46,560 --> 00:04:50,400
so that makes it so that there's not any

127
00:04:48,560 --> 00:04:52,880
you know trivial opportunities to

128
00:04:50,400 --> 00:04:54,880
rewrite them so the smrs look like this

129
00:04:52,880 --> 00:04:56,240
basically the same as mtrrs you've got a

130
00:04:54,880 --> 00:04:58,160
type you've got a base and you've got a

131
00:04:56,240 --> 00:05:00,560
mask which essentially sets out the

132
00:04:58,160 --> 00:05:02,560
range so whenever the processor as long

133
00:05:00,560 --> 00:05:04,560
as these are you know valid and enabled

134
00:05:02,560 --> 00:05:06,000
then whenever the processor is in smm

135
00:05:04,560 --> 00:05:08,400
they will use whatever type was

136
00:05:06,000 --> 00:05:10,400
specified in the smrrs and there

137
00:05:08,400 --> 00:05:12,639
therefore only needs to be one smrr

138
00:05:10,400 --> 00:05:14,400
whereas mtrrs can cover a bunch of

139
00:05:12,639 --> 00:05:16,240
different ranges these are basically

140
00:05:14,400 --> 00:05:19,360
just like this is what's used if you're

141
00:05:16,240 --> 00:05:21,600
in smm as a cpu execution mode

142
00:05:19,360 --> 00:05:24,000
and so whenever the system is not in smm

143
00:05:21,600 --> 00:05:25,759
if you try to read from the ranges

144
00:05:24,000 --> 00:05:27,919
specified by smrs you're just going to

145
00:05:25,759 --> 00:05:30,240
get you know invalid data like f's

146
00:05:27,919 --> 00:05:32,479
writes to it or straight up ignored and

147
00:05:30,240 --> 00:05:34,960
when you're outside of smm the memory

148
00:05:32,479 --> 00:05:36,080
type effectively is uncachable so really

149
00:05:34,960 --> 00:05:38,400
doesn't matter because the rights are

150
00:05:36,080 --> 00:05:40,080
ignored but that's how it behaves now

151
00:05:38,400 --> 00:05:42,240
some older systems don't actually

152
00:05:40,080 --> 00:05:45,280
support smrs so you do have to check an

153
00:05:42,240 --> 00:05:49,120
msr to find out if the msrs are there so

154
00:05:45,280 --> 00:05:50,479
the ia32 mtrr capabilities register

155
00:05:49,120 --> 00:05:52,639
tells you about you know other

156
00:05:50,479 --> 00:05:55,039
capabilities but specifically bit 11 is

157
00:05:52,639 --> 00:05:57,759
what's going to tell you if smrs are

158
00:05:55,039 --> 00:06:00,160
supported on this particular system now

159
00:05:57,759 --> 00:06:02,080
smrr started as a non-architectural

160
00:06:00,160 --> 00:06:04,000
register on the earliest systems and

161
00:06:02,080 --> 00:06:07,120
then eventually became an architectural

162
00:06:04,000 --> 00:06:09,039
ia32 smrr register so of course you

163
00:06:07,120 --> 00:06:12,000
always need to double check for your

164
00:06:09,039 --> 00:06:14,560
data sheets what the actual msr number

165
00:06:12,000 --> 00:06:16,639
is in particular because read write

166
00:06:14,560 --> 00:06:18,479
everything if you try to read some msr

167
00:06:16,639 --> 00:06:20,800
and it's not valid it may crash your

168
00:06:18,479 --> 00:06:23,120
system right so that was our next attack

169
00:06:20,800 --> 00:06:25,280
which was applicable to both c-seg and

170
00:06:23,120 --> 00:06:28,400
t-seg cache misconfiguration

171
00:06:25,280 --> 00:06:30,639
specifically marking the smm range as

172
00:06:28,400 --> 00:06:34,160
right-back and then the defense for that

173
00:06:30,639 --> 00:06:35,600
is smrr's well you're done now with smm

174
00:06:34,160 --> 00:06:37,680
wright protection actually because that

175
00:06:35,600 --> 00:06:40,560
was the last sort of fundamental

176
00:06:37,680 --> 00:06:42,560
mechanism for write protecting smm and

177
00:06:40,560 --> 00:06:44,479
you've seen a little bit of the attacks

178
00:06:42,560 --> 00:06:47,360
but let's keep trucking and see some

179
00:06:44,479 --> 00:06:47,360
more attacks


1
00:00:00,399 --> 00:00:03,760
all right now we're going to need a

2
00:00:01,680 --> 00:00:06,160
little space again let's keep on

3
00:00:03,760 --> 00:00:08,559
expanding and in this section we're

4
00:00:06,160 --> 00:00:10,320
going to see smi handler implementation

5
00:00:08,559 --> 00:00:11,679
bugs there's going to be multiple

6
00:00:10,320 --> 00:00:14,080
categories and there's going to be a

7
00:00:11,679 --> 00:00:15,920
whole bunch of defensive mechanisms i'm

8
00:00:14,080 --> 00:00:17,760
going to be adding this bar here for

9
00:00:15,920 --> 00:00:20,080
defensive mechanisms that are common to

10
00:00:17,760 --> 00:00:22,240
the various implementation bugs and a

11
00:00:20,080 --> 00:00:24,640
bar here for a bunch of privilege

12
00:00:22,240 --> 00:00:26,800
reduction options that are available for

13
00:00:24,640 --> 00:00:28,720
everyone but yeah this is going to be

14
00:00:26,800 --> 00:00:31,039
you know a lot of defensive options but

15
00:00:28,720 --> 00:00:32,880
that also just means a lot of work for

16
00:00:31,039 --> 00:00:34,239
defenders if they want to actually stop

17
00:00:32,880 --> 00:00:36,000
this kind of thing

18
00:00:34,239 --> 00:00:38,320
so the key point here is that there can

19
00:00:36,000 --> 00:00:40,480
be all sorts of different implementation

20
00:00:38,320 --> 00:00:42,000
bugs but again through the you know

21
00:00:40,480 --> 00:00:44,239
looking at the different research that's

22
00:00:42,000 --> 00:00:46,399
come out over time we can categorize

23
00:00:44,239 --> 00:00:48,079
some of them into a couple of buckets so

24
00:00:46,399 --> 00:00:49,360
this is the first bucket callout

25
00:00:48,079 --> 00:00:51,760
vulnerabilities

26
00:00:49,360 --> 00:00:54,000
so the idea is that smram is supposed to

27
00:00:51,760 --> 00:00:56,239
be this isolated area that you know no

28
00:00:54,000 --> 00:00:58,640
attacker can breach and get into

29
00:00:56,239 --> 00:01:00,480
but if you have something like a call

30
00:00:58,640 --> 00:01:02,960
assembly instruction in there that is

31
00:01:00,480 --> 00:01:05,680
taking a memory address and that memory

32
00:01:02,960 --> 00:01:07,439
address is outside of smram well then

33
00:01:05,680 --> 00:01:09,040
that means an attacker can just you know

34
00:01:07,439 --> 00:01:11,280
rewrite the memory address outside of

35
00:01:09,040 --> 00:01:13,200
smram and point it wherever they want to

36
00:01:11,280 --> 00:01:15,439
attack your shell code that code will

37
00:01:13,200 --> 00:01:18,799
run in the context of a cpu that is in

38
00:01:15,439 --> 00:01:20,479
sm ram or smm and consequently that code

39
00:01:18,799 --> 00:01:22,240
will be effectively the most privileged

40
00:01:20,479 --> 00:01:25,360
cpu execution code

41
00:01:22,240 --> 00:01:27,920
so the itl folks found this back in 2009

42
00:01:25,360 --> 00:01:30,560
there was also a random bug track post

43
00:01:27,920 --> 00:01:33,040
in 2009 by a person who pointed this out

44
00:01:30,560 --> 00:01:36,240
in some asus systems

45
00:01:33,040 --> 00:01:38,560
in 2010 the french researchers started

46
00:01:36,240 --> 00:01:41,040
pointing out the same kind of problems

47
00:01:38,560 --> 00:01:42,720
of you know jumping or calling outside

48
00:01:41,040 --> 00:01:44,320
of sm ram

49
00:01:42,720 --> 00:01:46,399
and they actually made a distinction

50
00:01:44,320 --> 00:01:48,640
between you know just calling directly

51
00:01:46,399 --> 00:01:50,320
outside or fetching a

52
00:01:48,640 --> 00:01:51,680
function pointer from outside i don't

53
00:01:50,320 --> 00:01:55,200
make the distinction i'll just say

54
00:01:51,680 --> 00:01:57,119
either way it's calling out of smram

55
00:01:55,200 --> 00:01:59,439
now it was interesting as i was digging

56
00:01:57,119 --> 00:02:01,200
back through the research to see this

57
00:01:59,439 --> 00:02:02,799
they were covering a summary of the

58
00:02:01,200 --> 00:02:06,000
various attacks that were known at the

59
00:02:02,799 --> 00:02:08,800
time circa 2010 and they had outlined a

60
00:02:06,000 --> 00:02:10,319
interrupt descriptor table register

61
00:02:08,800 --> 00:02:11,840
based attack

62
00:02:10,319 --> 00:02:14,560
and they said they hadn't actually

63
00:02:11,840 --> 00:02:16,800
proved it exploitable so there you go go

64
00:02:14,560 --> 00:02:18,959
read that research and see if you can

65
00:02:16,800 --> 00:02:20,480
find a way to make it exploitable you

66
00:02:18,959 --> 00:02:23,599
there's a free little research

67
00:02:20,480 --> 00:02:25,440
opportunity for you then circa 2015

68
00:02:23,599 --> 00:02:27,200
corey and me had started finding these

69
00:02:25,440 --> 00:02:28,720
kind of vulnerabilities well i wanted to

70
00:02:27,200 --> 00:02:30,480
call them incursion vulnerabilities

71
00:02:28,720 --> 00:02:32,720
because i like naming stuff after marvel

72
00:02:30,480 --> 00:02:34,560
things and this is what it's like when

73
00:02:32,720 --> 00:02:37,599
worlds collide

74
00:02:34,560 --> 00:02:39,200
smm and not smm coming into conflict

75
00:02:37,599 --> 00:02:40,800
basically it was a situation where you

76
00:02:39,200 --> 00:02:42,879
know corey was off finding

77
00:02:40,800 --> 00:02:44,720
vulnerabilities in smm

78
00:02:42,879 --> 00:02:46,319
and unfortunately i had to break it to

79
00:02:44,720 --> 00:02:48,080
him after he found them i was like i'm

80
00:02:46,319 --> 00:02:49,680
pretty sure this is the same class of

81
00:02:48,080 --> 00:02:51,760
bug that you know invisible things lab

82
00:02:49,680 --> 00:02:52,800
founded back in 2009

83
00:02:51,760 --> 00:02:55,760
anyways

84
00:02:52,800 --> 00:02:57,760
by the way this is black panther not uh

85
00:02:55,760 --> 00:02:58,959
batman hopefully you know folks

86
00:02:57,760 --> 00:03:00,720
recognize that now i know you don't

87
00:02:58,959 --> 00:03:02,959
normally see him with a cape in the mcu

88
00:03:00,720 --> 00:03:05,440
but that's black panther

89
00:03:02,959 --> 00:03:09,040
anyways so exactly the same thing just

90
00:03:05,440 --> 00:03:10,560
typical smm reaching outside of smm and

91
00:03:09,040 --> 00:03:12,640
that's a vulnerability you can write

92
00:03:10,560 --> 00:03:14,239
either python you know scripts in order

93
00:03:12,640 --> 00:03:16,640
to find a whole bunch of these kind of

94
00:03:14,239 --> 00:03:18,480
vulnerabilities so then after we publish

95
00:03:16,640 --> 00:03:20,400
that research another researcher went

96
00:03:18,480 --> 00:03:21,440
off and you know decided to reverse

97
00:03:20,400 --> 00:03:23,360
engineer

98
00:03:21,440 --> 00:03:25,920
the lenovo systems that had listed

99
00:03:23,360 --> 00:03:28,319
themselves as patched and so he wrote a

100
00:03:25,920 --> 00:03:29,680
blog post specifically on

101
00:03:28,319 --> 00:03:31,680
reverse engineering and finding the

102
00:03:29,680 --> 00:03:33,680
vulnerability and then exploiting it and

103
00:03:31,680 --> 00:03:36,000
then later on he went off to find his

104
00:03:33,680 --> 00:03:38,879
own vulnerability and found a novel one

105
00:03:36,000 --> 00:03:39,760
and posted a zero day for how to exploit

106
00:03:38,879 --> 00:03:42,000
it

107
00:03:39,760 --> 00:03:44,400
okay so now i'm going to introduce a

108
00:03:42,000 --> 00:03:46,480
common bus here for different defensive

109
00:03:44,400 --> 00:03:48,879
capabilities that are going to apply to

110
00:03:46,480 --> 00:03:51,360
this to different smi handler

111
00:03:48,879 --> 00:03:53,519
implementation bugs so the first one is

112
00:03:51,360 --> 00:03:55,120
just audit the code of course auditing

113
00:03:53,519 --> 00:03:57,280
the code should be able to find these

114
00:03:55,120 --> 00:04:00,080
kind of vulnerabilities

115
00:03:57,280 --> 00:04:03,200
so what else can you do well one of the

116
00:04:00,080 --> 00:04:06,159
oldest capabilities to try to deal with

117
00:04:03,200 --> 00:04:08,560
smm exploits was a thing called smi

118
00:04:06,159 --> 00:04:10,959
transfer monitor it's basically jailing

119
00:04:08,560 --> 00:04:13,439
a smm inside of a vmm

120
00:04:10,959 --> 00:04:15,760
now this was first brought to light when

121
00:04:13,439 --> 00:04:17,840
invisible things lab started attacking

122
00:04:15,760 --> 00:04:19,600
intel trusted execution technology and

123
00:04:17,840 --> 00:04:22,400
they basically found that it seemed to

124
00:04:19,600 --> 00:04:24,320
be the case that if they broke into smm

125
00:04:22,400 --> 00:04:26,400
then they could just automatically and

126
00:04:24,320 --> 00:04:28,400
guaranteed defeat the security

127
00:04:26,400 --> 00:04:30,560
guarantees of txt

128
00:04:28,400 --> 00:04:32,960
and so intel told them that the solution

129
00:04:30,560 --> 00:04:35,199
for that was a thing called the stm smm

130
00:04:32,960 --> 00:04:37,280
transfer monitor or smi transfer monitor

131
00:04:35,199 --> 00:04:39,680
it's referred to as both things in the

132
00:04:37,280 --> 00:04:42,240
intel documentation and the basic idea

133
00:04:39,680 --> 00:04:44,160
was again you just take a virtual

134
00:04:42,240 --> 00:04:47,199
machine virtual machine management

135
00:04:44,160 --> 00:04:50,800
system and you wrap smm in it and then

136
00:04:47,199 --> 00:04:52,800
the idea was if the operating system is

137
00:04:50,800 --> 00:04:54,639
virtualized as well then there needs to

138
00:04:52,800 --> 00:04:58,160
be some sort of communication between

139
00:04:54,639 --> 00:05:00,400
the stm and the vmm to you know agree on

140
00:04:58,160 --> 00:05:02,479
what areas of memory are restricted and

141
00:05:00,400 --> 00:05:04,639
reserved and so forth what is smm

142
00:05:02,479 --> 00:05:06,560
allowed to reach out into for instance

143
00:05:04,639 --> 00:05:09,680
maybe there's a communications buffer

144
00:05:06,560 --> 00:05:12,000
that is directly uh needed to transfer

145
00:05:09,680 --> 00:05:14,560
information from an operating system

146
00:05:12,000 --> 00:05:16,160
into smm and it shouldn't be able to get

147
00:05:14,560 --> 00:05:17,680
access to anything else

148
00:05:16,160 --> 00:05:21,360
and then here's my little color

149
00:05:17,680 --> 00:05:24,160
commentary from my book on intel safer

150
00:05:21,360 --> 00:05:27,360
computing initiative 2006 i was rating

151
00:05:24,160 --> 00:05:29,360
this probably around 2008 2009 something

152
00:05:27,360 --> 00:05:31,520
like that must have been 2009 because it

153
00:05:29,360 --> 00:05:33,520
would have been after uh after invisible

154
00:05:31,520 --> 00:05:35,199
things labs and it was funny because

155
00:05:33,520 --> 00:05:37,600
basically they're talking about you know

156
00:05:35,199 --> 00:05:40,000
how the protections work and they said

157
00:05:37,600 --> 00:05:42,240
you know stm is one of the things that

158
00:05:40,000 --> 00:05:44,960
helps protect the physical pages but

159
00:05:42,240 --> 00:05:48,560
that's a gap because nobody actually had

160
00:05:44,960 --> 00:05:51,280
stms nobody was using stms at the time

161
00:05:48,560 --> 00:05:54,240
so it's obviously very complex to wrap

162
00:05:51,280 --> 00:05:56,479
all of system management mode in a vmm

163
00:05:54,240 --> 00:05:58,400
and furthermore it's not exactly clear

164
00:05:56,479 --> 00:06:01,280
you know what the communication and

165
00:05:58,400 --> 00:06:03,360
negotiation should be between vmms and

166
00:06:01,280 --> 00:06:05,199
stms about you know who gets access to

167
00:06:03,360 --> 00:06:07,680
what memory and so forth

168
00:06:05,199 --> 00:06:09,039
so the reality is that basically no one

169
00:06:07,680 --> 00:06:12,240
was using

170
00:06:09,039 --> 00:06:14,160
stm there was an initiative by some nsa

171
00:06:12,240 --> 00:06:17,199
researchers on the defensive side of the

172
00:06:14,160 --> 00:06:19,840
house who understood that stm was fully

173
00:06:17,199 --> 00:06:22,479
required to have secure systems and so

174
00:06:19,840 --> 00:06:25,600
they tried to get stms in use and things

175
00:06:22,479 --> 00:06:28,240
like that but to no avail effectively

176
00:06:25,600 --> 00:06:31,680
eventually intel released a reference

177
00:06:28,240 --> 00:06:34,000
implementation in 2015 and

178
00:06:31,680 --> 00:06:36,000
opened up the documentation that was

179
00:06:34,000 --> 00:06:38,319
previously confidential

180
00:06:36,000 --> 00:06:41,039
so that's one approach which is the

181
00:06:38,319 --> 00:06:42,800
mitigation of privilege reduction of

182
00:06:41,039 --> 00:06:45,600
system management mode basically if you

183
00:06:42,800 --> 00:06:48,160
wrap it in a vmm then that means the vmm

184
00:06:45,600 --> 00:06:49,840
the stm is going to be able to restrict

185
00:06:48,160 --> 00:06:51,919
all of the physical memory that it has

186
00:06:49,840 --> 00:06:54,240
access to and that would be a useful

187
00:06:51,919 --> 00:06:56,319
privilege reduction what else can you do

188
00:06:54,240 --> 00:06:58,639
to try to make it so that a system

189
00:06:56,319 --> 00:07:00,800
management mode compromise is not as bad

190
00:06:58,639 --> 00:07:03,199
well starting around the

191
00:07:00,800 --> 00:07:05,680
fourth generation core series or so

192
00:07:03,199 --> 00:07:08,720
intel added in this msr

193
00:07:05,680 --> 00:07:11,360
smsr smm feature control and it has a

194
00:07:08,720 --> 00:07:14,880
bit inside of it that basically acts

195
00:07:11,360 --> 00:07:17,360
like smap for smm meaning that if smm

196
00:07:14,880 --> 00:07:19,919
tries to execute code outside of the

197
00:07:17,360 --> 00:07:21,759
range that is covered by smrrs which

198
00:07:19,919 --> 00:07:24,400
will effectively be considered the de

199
00:07:21,759 --> 00:07:26,080
facto smm range if it tries to execute

200
00:07:24,400 --> 00:07:29,199
outside of there that'll cause a machine

201
00:07:26,080 --> 00:07:31,599
check error and so basically smm calling

202
00:07:29,199 --> 00:07:33,919
out of its you know little constrained

203
00:07:31,599 --> 00:07:36,319
area of memory will cause an error so

204
00:07:33,919 --> 00:07:38,400
that would be good as well

205
00:07:36,319 --> 00:07:40,560
so what's another thing that we can do

206
00:07:38,400 --> 00:07:41,759
to try to deal with smm call out

207
00:07:40,560 --> 00:07:44,000
vulnerabilities

208
00:07:41,759 --> 00:07:46,400
well we can use some software only

209
00:07:44,000 --> 00:07:48,720
mechanisms such as installing page

210
00:07:46,400 --> 00:07:50,879
tables that are read-only

211
00:07:48,720 --> 00:07:52,560
so we said that when you get into smm to

212
00:07:50,879 --> 00:07:54,240
start with it looks very much like

213
00:07:52,560 --> 00:07:56,560
16-bit real mode

214
00:07:54,240 --> 00:07:59,599
and then typically the software is going

215
00:07:56,560 --> 00:08:00,960
to transition itself into 32 or 64-bit

216
00:07:59,599 --> 00:08:02,400
protective mode now within that

217
00:08:00,960 --> 00:08:05,199
protected mode they're going to want to

218
00:08:02,400 --> 00:08:07,680
use some sort of page table and if the

219
00:08:05,199 --> 00:08:09,840
bios when it set up the initial code

220
00:08:07,680 --> 00:08:12,080
makes it so that those page tables are

221
00:08:09,840 --> 00:08:14,319
self-referential and they indicate that

222
00:08:12,080 --> 00:08:16,639
the page table itself may not be written

223
00:08:14,319 --> 00:08:18,720
to then that's going to make it that

224
00:08:16,639 --> 00:08:21,599
much harder for an attacker to change

225
00:08:18,720 --> 00:08:23,840
the memory ranges which the page tables

226
00:08:21,599 --> 00:08:26,000
have mapped into smm

227
00:08:23,840 --> 00:08:27,919
also you can use some of the typical

228
00:08:26,000 --> 00:08:30,800
exploit mitigation things like setting

229
00:08:27,919 --> 00:08:33,200
the execute disable bit on data pages

230
00:08:30,800 --> 00:08:34,719
for any sort of you know stack or heap

231
00:08:33,200 --> 00:08:36,959
anything like that that's used inside of

232
00:08:34,719 --> 00:08:39,279
it any global data should also be marked

233
00:08:36,959 --> 00:08:41,360
read-only and the code should also be

234
00:08:39,279 --> 00:08:42,800
you know marked as non-writable it says

235
00:08:41,360 --> 00:08:44,880
read only here but of course you're

236
00:08:42,800 --> 00:08:48,800
going to want to have it executable so

237
00:08:44,880 --> 00:08:50,720
executable xor writeable so non-writable

238
00:08:48,800 --> 00:08:53,440
and so taken together these sort of

239
00:08:50,720 --> 00:08:55,600
things help restrict the physical memory

240
00:08:53,440 --> 00:08:57,760
that smm is able to access and that

241
00:08:55,600 --> 00:08:59,680
helps deal with this notion that if smm

242
00:08:57,760 --> 00:09:01,600
was broken into that it could just

243
00:08:59,680 --> 00:09:04,080
scribble all over operating systems and

244
00:09:01,600 --> 00:09:06,720
vmms and so these sort of changes are

245
00:09:04,080 --> 00:09:09,519
contributed to edk-2 the efi development

246
00:09:06,720 --> 00:09:12,720
kit 2 which is sort of the reference

247
00:09:09,519 --> 00:09:14,480
skeleton for building uefi systems so

248
00:09:12,720 --> 00:09:16,880
that means that you know third-party

249
00:09:14,480 --> 00:09:18,880
vendors ibv's oems

250
00:09:16,880 --> 00:09:21,200
they could be using these to help

251
00:09:18,880 --> 00:09:23,360
restrict smm significantly now intel

252
00:09:21,200 --> 00:09:24,800
recognized that just making page tables

253
00:09:23,360 --> 00:09:26,160
read-only and things like that was

254
00:09:24,800 --> 00:09:28,880
insufficient there were still

255
00:09:26,160 --> 00:09:30,720
opportunities for privilege escalation

256
00:09:28,880 --> 00:09:33,680
and so they started introducing new

257
00:09:30,720 --> 00:09:35,920
hardware changes around 2018

258
00:09:33,680 --> 00:09:36,880
coincidentally enough about three years

259
00:09:35,920 --> 00:09:38,880
after

260
00:09:36,880 --> 00:09:41,519
when you know researchers had started

261
00:09:38,880 --> 00:09:44,800
finding all sorts of smm vulnerabilities

262
00:09:41,519 --> 00:09:47,600
so these new hardware mechanisms have

263
00:09:44,800 --> 00:09:49,120
cr0 cr3 smbs these are things that if an

264
00:09:47,600 --> 00:09:50,880
attacker could manipulate that would

265
00:09:49,120 --> 00:09:53,279
allow them to gain arbitrary code

266
00:09:50,880 --> 00:09:55,120
execution in the context of smm it makes

267
00:09:53,279 --> 00:09:57,760
it so that these can be locked down so

268
00:09:55,120 --> 00:10:00,240
that even smm itself is not able to

269
00:09:57,760 --> 00:10:01,600
change these so cr3 for instance that

270
00:10:00,240 --> 00:10:03,519
points at the page tables right and we

271
00:10:01,600 --> 00:10:05,519
learned that in architecture 2001. if an

272
00:10:03,519 --> 00:10:07,440
attacker can find you know some sequence

273
00:10:05,519 --> 00:10:10,480
of instructions would allow them to

274
00:10:07,440 --> 00:10:12,480
write to cr3 then that whole software

275
00:10:10,480 --> 00:10:15,440
only page table protection mechanism

276
00:10:12,480 --> 00:10:17,519
wouldn't be useful but if the code has

277
00:10:15,440 --> 00:10:19,920
locked down cr3 so that even if they

278
00:10:17,519 --> 00:10:22,320
find such a you know sequence it won't

279
00:10:19,920 --> 00:10:24,240
actually write to cr3 then that'll make

280
00:10:22,320 --> 00:10:26,000
it that much harder for them so that

281
00:10:24,240 --> 00:10:28,800
would be another defensive mechanism

282
00:10:26,000 --> 00:10:30,640
again reducing the privilege of

283
00:10:28,800 --> 00:10:32,240
smm by basically saying these things

284
00:10:30,640 --> 00:10:34,640
that it should otherwise be able to do

285
00:10:32,240 --> 00:10:36,880
like changing these control registers it

286
00:10:34,640 --> 00:10:38,959
is not able to do anymore

287
00:10:36,880 --> 00:10:42,720
yet another thing starting in 2020

288
00:10:38,959 --> 00:10:44,560
hardware is that they will effectively

289
00:10:42,720 --> 00:10:47,839
restrict the privileges of most of the

290
00:10:44,560 --> 00:10:50,640
smm code to be ring three so the entry

291
00:10:47,839 --> 00:10:52,399
point may start at ring zero and the

292
00:10:50,640 --> 00:10:54,800
entry point is actually going to be

293
00:10:52,399 --> 00:10:57,600
provided by intel so intel now provides

294
00:10:54,800 --> 00:10:59,519
a sort of entry binary that deal which

295
00:10:57,600 --> 00:11:01,360
utilizes these

296
00:10:59,519 --> 00:11:03,279
hardware protection mechanisms and

297
00:11:01,360 --> 00:11:04,800
importantly utilizes a measurement

298
00:11:03,279 --> 00:11:07,600
technology that we'll talk about in a

299
00:11:04,800 --> 00:11:09,760
second so intel provides a basic binary

300
00:11:07,600 --> 00:11:11,920
blob to say this is the entry point to

301
00:11:09,760 --> 00:11:14,959
smm and then the expectation is that

302
00:11:11,920 --> 00:11:18,320
blob will you know transition privileges

303
00:11:14,959 --> 00:11:21,440
so that the oem smi handler so you know

304
00:11:18,320 --> 00:11:23,600
del lenovo hp whoever has an smi handler

305
00:11:21,440 --> 00:11:26,800
that code is going to run as ring three

306
00:11:23,600 --> 00:11:28,560
so now if that thing gets compromised

307
00:11:26,800 --> 00:11:30,480
it will not be able to any other things

308
00:11:28,560 --> 00:11:33,040
that you know control registers any

309
00:11:30,480 --> 00:11:35,200
other port io any other stuff that it

310
00:11:33,040 --> 00:11:37,680
shouldn't otherwise have access to

311
00:11:35,200 --> 00:11:39,200
uh will now be restricted away from that

312
00:11:37,680 --> 00:11:40,720
now you know this is pretty new

313
00:11:39,200 --> 00:11:43,120
technology and you know the real

314
00:11:40,720 --> 00:11:45,360
question is who if anyone is using this

315
00:11:43,120 --> 00:11:47,040
at this point but but that's an

316
00:11:45,360 --> 00:11:48,160
interesting thing to go investigate

317
00:11:47,040 --> 00:11:50,399
right

318
00:11:48,160 --> 00:11:54,079
so that's the other defensive mechanism

319
00:11:50,399 --> 00:11:56,079
deprivage the main body of the smm code

320
00:11:54,079 --> 00:11:58,399
by making it ring three instead of ring

321
00:11:56,079 --> 00:12:00,560
zero what else can we do

322
00:11:58,399 --> 00:12:03,040
well we talked about the problems with

323
00:12:00,560 --> 00:12:05,360
stm and we said you know stm

324
00:12:03,040 --> 00:12:06,320
uh wasn't really in use you know even

325
00:12:05,360 --> 00:12:08,000
once there was a reference

326
00:12:06,320 --> 00:12:09,760
implementation there just wasn't you

327
00:12:08,000 --> 00:12:12,079
know enough demand from people other

328
00:12:09,760 --> 00:12:13,760
than nsa saying we need this

329
00:12:12,079 --> 00:12:14,720
and so consequently it's not really in

330
00:12:13,760 --> 00:12:15,519
use

331
00:12:14,720 --> 00:12:18,560
now

332
00:12:15,519 --> 00:12:20,720
the fact that the lack of stm would

333
00:12:18,560 --> 00:12:22,800
leave txt vulnerable meant that this

334
00:12:20,720 --> 00:12:25,200
important technology txt

335
00:12:22,800 --> 00:12:27,600
which is now subsequently being used for

336
00:12:25,200 --> 00:12:30,240
things like windows secure launch

337
00:12:27,600 --> 00:12:31,920
it had this architectural hole and so

338
00:12:30,240 --> 00:12:33,440
you have you know a couple of options

339
00:12:31,920 --> 00:12:36,320
either a everyone in the world has to

340
00:12:33,440 --> 00:12:39,600
adopt stms which clearly isn't happening

341
00:12:36,320 --> 00:12:44,079
or b intel had to introduce new hardware

342
00:12:39,600 --> 00:12:46,959
capabilities to plug the smm hole in txt

343
00:12:44,079 --> 00:12:50,000
while not requiring a full-fledged stm

344
00:12:46,959 --> 00:12:53,120
and so this ppam platform properties

345
00:12:50,000 --> 00:12:54,240
assessment module is effectively a stm

346
00:12:53,120 --> 00:12:56,000
light

347
00:12:54,240 --> 00:12:58,240
it's not trying to virtualize the system

348
00:12:56,000 --> 00:13:00,959
it's not trying to deal with the

349
00:12:58,240 --> 00:13:03,200
constraints to you know de-privilege the

350
00:13:00,959 --> 00:13:04,880
thing but it is trying to make it so

351
00:13:03,200 --> 00:13:08,000
that the measurement capability that

352
00:13:04,880 --> 00:13:09,120
already existed in txt is effective and

353
00:13:08,000 --> 00:13:10,800
so

354
00:13:09,120 --> 00:13:12,720
you know system makers can have some

355
00:13:10,800 --> 00:13:14,720
trust that this launched environment

356
00:13:12,720 --> 00:13:16,240
that txt launches that's supposed to be

357
00:13:14,720 --> 00:13:18,240
dynamic route of trust which means you

358
00:13:16,240 --> 00:13:20,480
just pop a little trusted environment

359
00:13:18,240 --> 00:13:22,639
into existence and you have to have some

360
00:13:20,480 --> 00:13:25,040
belief that you know the system behaves

361
00:13:22,639 --> 00:13:27,360
in a way that you can trust it

362
00:13:25,040 --> 00:13:29,600
well ppam gives you the ability to

363
00:13:27,360 --> 00:13:31,680
provide a more trustworthy measurement

364
00:13:29,600 --> 00:13:35,360
capability that was otherwise subverted

365
00:13:31,680 --> 00:13:37,120
by smm before so overall you know this

366
00:13:35,360 --> 00:13:38,399
is described in the same document as

367
00:13:37,120 --> 00:13:40,720
those other you know hardening

368
00:13:38,399 --> 00:13:42,720
mechanisms it seems to me to be you know

369
00:13:40,720 --> 00:13:44,959
a lot more tractable in that it's just

370
00:13:42,720 --> 00:13:47,440
doing measurement of the code it's not

371
00:13:44,959 --> 00:13:49,440
trying to jail everything and because

372
00:13:47,440 --> 00:13:51,519
intel provides this stuff it should be

373
00:13:49,440 --> 00:13:54,000
easier for vendors to pretty much just

374
00:13:51,519 --> 00:13:56,560
drop it in also microsoft is pushing

375
00:13:54,000 --> 00:13:58,480
this as a thing that they really want

376
00:13:56,560 --> 00:14:00,639
because basically at this point they've

377
00:13:58,480 --> 00:14:02,639
kind of given up on securing the entire

378
00:14:00,639 --> 00:14:04,560
firmware ecosystem and they just say i

379
00:14:02,639 --> 00:14:07,040
want to be able to use txt i want to be

380
00:14:04,560 --> 00:14:08,880
able to securely launch my vmm for you

381
00:14:07,040 --> 00:14:11,680
know windows virtualization based

382
00:14:08,880 --> 00:14:13,040
security and everything besides that you

383
00:14:11,680 --> 00:14:16,160
know they don't care

384
00:14:13,040 --> 00:14:17,760
so this is a interesting capability

385
00:14:16,160 --> 00:14:20,320
and you know the fact that intel is

386
00:14:17,760 --> 00:14:23,040
providing this hardened smi entry point

387
00:14:20,320 --> 00:14:25,040
uh means that you know ipvs hopefully

388
00:14:23,040 --> 00:14:27,839
can't mess it up

389
00:14:25,040 --> 00:14:29,839
so that is a detection mechanism so this

390
00:14:27,839 --> 00:14:31,920
would not deal directly with the

391
00:14:29,839 --> 00:14:33,600
prevention of callout vulnerabilities it

392
00:14:31,920 --> 00:14:35,680
would only deal with if someone

393
00:14:33,600 --> 00:14:37,680
exploited vulnerability if they got

394
00:14:35,680 --> 00:14:40,240
themselves into smm

395
00:14:37,680 --> 00:14:42,399
then this ppam mechanism should provide

396
00:14:40,240 --> 00:14:44,560
a way to trust that the overall

397
00:14:42,399 --> 00:14:47,279
architecture of txt is capable of

398
00:14:44,560 --> 00:14:49,279
measuring smm and if there's a bad guy

399
00:14:47,279 --> 00:14:51,279
there then it should

400
00:14:49,279 --> 00:14:52,800
you know change the measurements and if

401
00:14:51,279 --> 00:14:54,800
you have anything

402
00:14:52,800 --> 00:14:56,240
in those measurements that sorry if you

403
00:14:54,800 --> 00:14:57,839
have anything restricted behind those

404
00:14:56,240 --> 00:14:59,279
measurements such as a you know full

405
00:14:57,839 --> 00:15:02,240
disk encryption

406
00:14:59,279 --> 00:15:04,639
decryption key then you wouldn't decrypt

407
00:15:02,240 --> 00:15:07,680
once the smm is infected and you would

408
00:15:04,639 --> 00:15:09,360
only decrypt if it was uninfected

409
00:15:07,680 --> 00:15:11,440
so all in all you know having read

410
00:15:09,360 --> 00:15:13,279
through that paper recently it made me

411
00:15:11,440 --> 00:15:15,440
think very much you know i should buy a

412
00:15:13,279 --> 00:15:17,040
comet lake to attack but then i thought

413
00:15:15,440 --> 00:15:19,120
to myself no

414
00:15:17,040 --> 00:15:21,360
you should buy a comet lake to attack

415
00:15:19,120 --> 00:15:23,680
why should i hug all the glory this is a

416
00:15:21,360 --> 00:15:26,000
research opportunity for you

417
00:15:23,680 --> 00:15:28,639
basically go out read how this works you

418
00:15:26,000 --> 00:15:30,959
know go find you know sign the nda get

419
00:15:28,639 --> 00:15:33,920
access to the proprietary documentation

420
00:15:30,959 --> 00:15:36,959
or don't and then go out and see how

421
00:15:33,920 --> 00:15:38,959
this works analyze any uh bios vendors

422
00:15:36,959 --> 00:15:41,199
who say that they're using microsoft

423
00:15:38,959 --> 00:15:42,959
secure launch and you know see whether

424
00:15:41,199 --> 00:15:45,040
they're using this what if any of these

425
00:15:42,959 --> 00:15:47,199
pieces are they using what if any

426
00:15:45,040 --> 00:15:50,000
vulnerabilities like smm callouts do

427
00:15:47,199 --> 00:15:50,000
they still have


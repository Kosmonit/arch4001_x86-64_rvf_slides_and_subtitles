1
00:00:00,480 --> 00:00:04,799
now let's learn about tseg toled and the

2
00:00:02,639 --> 00:00:06,560
case of the stolen graphics memory which

3
00:00:04,799 --> 00:00:08,880
is to say tseg we're going to be

4
00:00:06,560 --> 00:00:10,719
learning about tseg now an interesting

5
00:00:08,880 --> 00:00:12,719
thing is that while the older data

6
00:00:10,719 --> 00:00:15,679
sheets have this and they specify both

7
00:00:12,719 --> 00:00:18,880
the compatible and the tseg ranges

8
00:00:15,679 --> 00:00:21,520
the 10th generation ice lake data sheets

9
00:00:18,880 --> 00:00:24,080
lose this definition and they just list

10
00:00:21,520 --> 00:00:26,720
t-seg whereas the 10th generation comet

11
00:00:24,080 --> 00:00:29,199
lake had both c-seg and t-sec so it

12
00:00:26,720 --> 00:00:31,439
looks like that is the cut line between

13
00:00:29,199 --> 00:00:33,760
when c-seg starts to be deprecated

14
00:00:31,439 --> 00:00:36,000
apparently 10th gen comet lake has it

15
00:00:33,760 --> 00:00:38,000
10th gen ice lake and everything beyond

16
00:00:36,000 --> 00:00:39,920
that doesn't have it so that means we

17
00:00:38,000 --> 00:00:42,719
now need to learn about t-seg this seems

18
00:00:39,920 --> 00:00:45,280
to be the de facto and preferred way of

19
00:00:42,719 --> 00:00:48,079
dealing with sm ram going forward so

20
00:00:45,280 --> 00:00:50,640
t-seg is defined as toled minus stolen

21
00:00:48,079 --> 00:00:52,640
minus t sag to toledo minus stolen but

22
00:00:50,640 --> 00:00:55,520
what does that mean what is toled what

23
00:00:52,640 --> 00:00:58,719
is t seg what is stolen in our memory

24
00:00:55,520 --> 00:01:02,320
map tetris tolud was listed there

25
00:00:58,719 --> 00:01:05,439
and above toled is this pci memory

26
00:01:02,320 --> 00:01:08,000
that's the mmio space that is used for

27
00:01:05,439 --> 00:01:09,280
things like the extended configuration

28
00:01:08,000 --> 00:01:12,159
address space

29
00:01:09,280 --> 00:01:14,080
below toled is the graphics memory or

30
00:01:12,159 --> 00:01:16,720
graphic stolen memory as you'll see it's

31
00:01:14,080 --> 00:01:20,159
called by the documentation

32
00:01:16,720 --> 00:01:22,560
and then beyond that is tseg so that's

33
00:01:20,159 --> 00:01:25,680
what we're going to care about tsegmb

34
00:01:22,560 --> 00:01:28,400
and you can see it's listed as smram so

35
00:01:25,680 --> 00:01:30,400
between tsegmb and toilet is this

36
00:01:28,400 --> 00:01:32,479
graphics memory which is called the

37
00:01:30,400 --> 00:01:35,200
graphics stolen memory in the graphics

38
00:01:32,479 --> 00:01:37,040
gtt stolen memory so we got to

39
00:01:35,200 --> 00:01:39,759
understand what's going on in this

40
00:01:37,040 --> 00:01:41,280
little snippet of the memory space well

41
00:01:39,759 --> 00:01:43,920
zooming in on it from the 11th

42
00:01:41,280 --> 00:01:46,159
generation data sheet you've got the pci

43
00:01:43,920 --> 00:01:48,799
memory range and then you've got to load

44
00:01:46,159 --> 00:01:51,360
toled is defined as bus zero device zero

45
00:01:48,799 --> 00:01:54,320
function zero offset bc

46
00:01:51,360 --> 00:01:56,799
the graphics stolen memory starts at bus

47
00:01:54,320 --> 00:01:58,560
zero device zero offset zero

48
00:01:56,799 --> 00:02:03,119
plus zero device zero function zero

49
00:01:58,560 --> 00:02:04,560
offset b0 this one's b4 and tseg is b8

50
00:02:03,119 --> 00:02:07,280
so you can see a whole bunch of stuff in

51
00:02:04,560 --> 00:02:09,759
the memory controller space plus zero

52
00:02:07,280 --> 00:02:13,120
bus zero device zero function zero and

53
00:02:09,759 --> 00:02:15,360
so the sm ram here is bounded by t seg

54
00:02:13,120 --> 00:02:16,879
on the bottom and graphics stolen memory

55
00:02:15,360 --> 00:02:19,040
on the top so let's look at the

56
00:02:16,879 --> 00:02:23,360
definition of those registers

57
00:02:19,040 --> 00:02:24,879
all right toled top of lower usable dram

58
00:02:23,360 --> 00:02:28,319
offset bc

59
00:02:24,879 --> 00:02:30,720
so this register has bits 20 to 31 are

60
00:02:28,319 --> 00:02:32,800
used for a address which is the top of

61
00:02:30,720 --> 00:02:35,280
lower usable dram then there's some

62
00:02:32,800 --> 00:02:38,160
reserve space then there's a lock

63
00:02:35,280 --> 00:02:40,800
all right the top of lower usable dram

64
00:02:38,160 --> 00:02:42,640
is basically saying in the context of

65
00:02:40,800 --> 00:02:44,560
you know this space

66
00:02:42,640 --> 00:02:46,319
toilet is here and it's saying this is

67
00:02:44,560 --> 00:02:48,319
ram stuff below this like there's the

68
00:02:46,319 --> 00:02:50,640
normal main memory there's a dma

69
00:02:48,319 --> 00:02:52,959
protected region as it's called some

70
00:02:50,640 --> 00:02:56,080
smram some stolen memory for the

71
00:02:52,959 --> 00:02:58,959
graphics but then above tolin is where

72
00:02:56,080 --> 00:03:01,519
you start seeing pcie memory mapped i o

73
00:02:58,959 --> 00:03:03,519
and things like that so this above it is

74
00:03:01,519 --> 00:03:06,400
like space that is used for memory

75
00:03:03,519 --> 00:03:08,640
mapped io and this down here is special

76
00:03:06,400 --> 00:03:10,800
ram usage effectively

77
00:03:08,640 --> 00:03:13,519
so toilet is the top of lower usable

78
00:03:10,800 --> 00:03:15,280
dram and we had talked very briefly

79
00:03:13,519 --> 00:03:16,959
before i think we'll see it again yeah

80
00:03:15,280 --> 00:03:19,120
we will see it again

81
00:03:16,959 --> 00:03:21,040
that there is a way to sort of reclaim

82
00:03:19,120 --> 00:03:23,920
this physical memory space that's stolen

83
00:03:21,040 --> 00:03:25,680
by memory mapped i o so basically toled

84
00:03:23,920 --> 00:03:30,000
is where does the

85
00:03:25,680 --> 00:03:32,879
thievery for memory mapped io start

86
00:03:30,000 --> 00:03:36,000
and there's sonic again all right so

87
00:03:32,879 --> 00:03:38,480
toled top of lower usable dram then

88
00:03:36,000 --> 00:03:40,879
there's the base of data of stolen

89
00:03:38,480 --> 00:03:42,480
memory and so this is graphics data

90
00:03:40,879 --> 00:03:46,480
stolen dram

91
00:03:42,480 --> 00:03:49,040
and it has this again 28 to 31 is the

92
00:03:46,480 --> 00:03:51,760
base and there's a lock bit

93
00:03:49,040 --> 00:03:54,239
then there's the gtt stolen memory this

94
00:03:51,760 --> 00:03:57,280
stands for graphics translation table

95
00:03:54,239 --> 00:03:58,319
and that again has bits 20 to 31 with a

96
00:03:57,280 --> 00:04:01,920
lock bit

97
00:03:58,319 --> 00:04:03,519
and finally tseg bits 20 to 31 and a

98
00:04:01,920 --> 00:04:05,200
lock bit

99
00:04:03,519 --> 00:04:06,959
and so the question is why do all these

100
00:04:05,200 --> 00:04:09,920
things have a lock bit

101
00:04:06,959 --> 00:04:11,599
well so let's think of it this way if

102
00:04:09,920 --> 00:04:13,439
toilet is the start of sort of the

103
00:04:11,599 --> 00:04:15,200
memory mapped i o and it's you know

104
00:04:13,439 --> 00:04:17,680
stuff above there is you know not going

105
00:04:15,200 --> 00:04:19,759
to ever be used as ram ram until you use

106
00:04:17,680 --> 00:04:22,000
the remapping mechanism

107
00:04:19,759 --> 00:04:24,320
the stuff down here is basically an

108
00:04:22,000 --> 00:04:25,759
adjustable range of how much memory

109
00:04:24,320 --> 00:04:27,440
needs to be stolen for this how much

110
00:04:25,759 --> 00:04:29,840
memory needs to be stolen for that how

111
00:04:27,440 --> 00:04:32,960
much memory needs to be used for sm ram

112
00:04:29,840 --> 00:04:34,479
and so forth so by placing a t-segment

113
00:04:32,960 --> 00:04:36,000
you're going to want to place it at some

114
00:04:34,479 --> 00:04:37,840
location and then you're going to want

115
00:04:36,000 --> 00:04:40,000
to lock it in place and you're going to

116
00:04:37,840 --> 00:04:41,120
want to place the base of the gtt and

117
00:04:40,000 --> 00:04:43,199
then you're going to want to lock it in

118
00:04:41,120 --> 00:04:45,919
place because if you don't lock these

119
00:04:43,199 --> 00:04:47,600
things in place and if sm ram is going

120
00:04:45,919 --> 00:04:49,680
to be this area that has this highly

121
00:04:47,600 --> 00:04:52,080
privileged code that can scribble all

122
00:04:49,680 --> 00:04:53,919
over everyone's memory everywhere

123
00:04:52,080 --> 00:04:55,440
then you have these potential attacks

124
00:04:53,919 --> 00:04:57,520
which i'm just calling titch attacks

125
00:04:55,440 --> 00:04:58,960
here because we're going to move things

126
00:04:57,520 --> 00:05:01,680
just a titch

127
00:04:58,960 --> 00:05:04,720
so if you didn't lock down this base of

128
00:05:01,680 --> 00:05:07,440
gtt stolen memory then an attacker could

129
00:05:04,720 --> 00:05:09,440
stitch it down this way and scribble all

130
00:05:07,440 --> 00:05:10,560
over this stuff and then move it back up

131
00:05:09,440 --> 00:05:12,479
and they would have effectively

132
00:05:10,560 --> 00:05:15,680
scribbled over sm ram

133
00:05:12,479 --> 00:05:17,759
same thing if the tcgmb was not locked

134
00:05:15,680 --> 00:05:20,720
down they could just move it a little

135
00:05:17,759 --> 00:05:22,800
titch and move it up and scribble over

136
00:05:20,720 --> 00:05:25,440
sm ram and then you know get arbitrary

137
00:05:22,800 --> 00:05:29,280
code in smm or for instance they could

138
00:05:25,440 --> 00:05:30,880
slide a tseg mb down into main memory so

139
00:05:29,280 --> 00:05:32,080
they can put their attacker controlled

140
00:05:30,880 --> 00:05:34,400
code there

141
00:05:32,080 --> 00:05:37,600
push the sm base down here and then as

142
00:05:34,400 --> 00:05:39,840
long as their code is at sm base plus

143
00:05:37,600 --> 00:05:41,199
x8000 then their attacker controlled

144
00:05:39,840 --> 00:05:43,840
code would run there

145
00:05:41,199 --> 00:05:45,600
so these things right here basically you

146
00:05:43,840 --> 00:05:47,759
know the thing we really care about is

147
00:05:45,600 --> 00:05:49,759
between t segment and the graphics

148
00:05:47,759 --> 00:05:52,320
stolen memory that is going to be our

149
00:05:49,759 --> 00:05:54,080
tseg that is going to be our sm ram

150
00:05:52,320 --> 00:05:55,840
that's the place that you know intel

151
00:05:54,080 --> 00:05:58,160
generally recommends you move from

152
00:05:55,840 --> 00:06:01,520
compatible memory region up into this

153
00:05:58,160 --> 00:06:03,600
t-seg because t-seg is granted

154
00:06:01,520 --> 00:06:05,840
extra protections it's granted

155
00:06:03,600 --> 00:06:08,639
protection against dma attacks just

156
00:06:05,840 --> 00:06:11,039
automatically whatever is in this range

157
00:06:08,639 --> 00:06:13,199
is not accessible via dma translate

158
00:06:11,039 --> 00:06:15,440
transactions and it's also protected

159
00:06:13,199 --> 00:06:17,600
against someone just directly scribbling

160
00:06:15,440 --> 00:06:18,720
into it so building up our attack tree a

161
00:06:17,600 --> 00:06:22,080
little bit more

162
00:06:18,720 --> 00:06:25,919
the t-seg defense is to lock the

163
00:06:22,080 --> 00:06:27,199
t-segment and the bgsm the base of gtt

164
00:06:25,919 --> 00:06:29,680
stolen memory

165
00:06:27,199 --> 00:06:33,120
and that will afford protection for the

166
00:06:29,680 --> 00:06:35,600
t-seg range against both dma attacks and

167
00:06:33,120 --> 00:06:37,520
attacks originating from the cpu now

168
00:06:35,600 --> 00:06:40,000
just like protected range registers it

169
00:06:37,520 --> 00:06:42,000
is possible that even if the defender

170
00:06:40,000 --> 00:06:44,240
locked down these things they could have

171
00:06:42,000 --> 00:06:46,000
misconfigured it so an attacker could

172
00:06:44,240 --> 00:06:48,880
try to exploit a misconfiguration of

173
00:06:46,000 --> 00:06:51,120
tseg where the you know defender for

174
00:06:48,880 --> 00:06:53,440
instance set t-seg too high but they're

175
00:06:51,120 --> 00:06:54,800
actually using sm ram below t-seg or

176
00:06:53,440 --> 00:06:56,960
above t-seg

177
00:06:54,800 --> 00:06:59,440
and so consequently

178
00:06:56,960 --> 00:07:01,440
that would be an exploitable scenario so

179
00:06:59,440 --> 00:07:04,000
the defense against that is to carefully

180
00:07:01,440 --> 00:07:05,919
analyze the tseg range versus where the

181
00:07:04,000 --> 00:07:08,319
smm code is actually placed all right

182
00:07:05,919 --> 00:07:10,560
now i'm going to introduce a crossbar

183
00:07:08,319 --> 00:07:11,840
here because most of the rest of these

184
00:07:10,560 --> 00:07:13,199
defenses that we're going to be talking

185
00:07:11,840 --> 00:07:16,319
about in this threat tree apply

186
00:07:13,199 --> 00:07:19,120
equivalently well to c-seg and t-seg so

187
00:07:16,319 --> 00:07:22,080
for instance there's an attack of sleep

188
00:07:19,120 --> 00:07:23,759
wake attacks which as with the

189
00:07:22,080 --> 00:07:25,120
protecting the spy flash chip we said

190
00:07:23,759 --> 00:07:26,479
we're going to cover later so that's

191
00:07:25,120 --> 00:07:28,479
going to be the next major section in

192
00:07:26,479 --> 00:07:30,240
the class so we'll skip that for now

193
00:07:28,479 --> 00:07:32,400
okay well you're done and you're not

194
00:07:30,240 --> 00:07:35,360
done so you're done with learning about

195
00:07:32,400 --> 00:07:37,039
sm ram for now and you've seen a little

196
00:07:35,360 --> 00:07:38,960
bit of write protection but there's more

197
00:07:37,039 --> 00:07:42,319
to see for right protection as we make

198
00:07:38,960 --> 00:07:42,319
our way to the attacks


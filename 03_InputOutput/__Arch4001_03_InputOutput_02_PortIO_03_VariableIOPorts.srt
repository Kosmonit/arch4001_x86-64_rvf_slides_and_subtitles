1
00:00:00,160 --> 00:00:04,880
in contrast to fixed i o ports variable

2
00:00:02,639 --> 00:00:07,680
i o ports of course can be moved around

3
00:00:04,880 --> 00:00:08,559
the i o address space to any particular

4
00:00:07,680 --> 00:00:11,280
port

5
00:00:08,559 --> 00:00:13,519
number that is not actually in use

6
00:00:11,280 --> 00:00:15,360
there are some types of variable port

7
00:00:13,519 --> 00:00:18,160
ranges which are actually just built

8
00:00:15,360 --> 00:00:19,840
into intel hardware which are required

9
00:00:18,160 --> 00:00:21,840
by certain specifications and things

10
00:00:19,840 --> 00:00:24,720
like that but there's also the entire

11
00:00:21,840 --> 00:00:26,960
class of variable i o ports that are

12
00:00:24,720 --> 00:00:29,519
used by pcie devices

13
00:00:26,960 --> 00:00:31,599
so a particular peripheral such as a

14
00:00:29,519 --> 00:00:33,680
network card graphics card anything like

15
00:00:31,599 --> 00:00:35,840
that the software that interacts with

16
00:00:33,680 --> 00:00:38,079
that card could choose to use a

17
00:00:35,840 --> 00:00:40,879
mechanism called base address registers

18
00:00:38,079 --> 00:00:43,600
and pcie in order to set up the access

19
00:00:40,879 --> 00:00:45,200
ports for that particular device by

20
00:00:43,600 --> 00:00:47,120
locating them somewhere in the i o

21
00:00:45,200 --> 00:00:49,440
address space we'll see an example of

22
00:00:47,120 --> 00:00:51,360
this later on so the bios or other

23
00:00:49,440 --> 00:00:54,399
privileged software such as an operating

24
00:00:51,360 --> 00:00:56,800
system kernel drivers or the like can go

25
00:00:54,399 --> 00:01:00,000
ahead and move around these variable

26
00:00:56,800 --> 00:01:01,840
port i o ports to make it so that a

27
00:01:00,000 --> 00:01:04,000
particular peripheral is accessible at

28
00:01:01,840 --> 00:01:06,960
some particular port just like with

29
00:01:04,000 --> 00:01:09,200
fixed ports the other side of a port

30
00:01:06,960 --> 00:01:10,880
access is just some black box particular

31
00:01:09,200 --> 00:01:14,000
peripheral that behaves however it

32
00:01:10,880 --> 00:01:16,799
behaves and again determining what it is

33
00:01:14,000 --> 00:01:19,040
and how it behaves is a tricky situation

34
00:01:16,799 --> 00:01:21,040
but in the case of variable i o ports

35
00:01:19,040 --> 00:01:22,960
there's an extra consideration which is

36
00:01:21,040 --> 00:01:25,200
the fact that you can potentially have

37
00:01:22,960 --> 00:01:27,439
these things overlapped the hardware

38
00:01:25,200 --> 00:01:29,759
will not actually check for overlapped

39
00:01:27,439 --> 00:01:31,840
ranges and this has been used for

40
00:01:29,759 --> 00:01:33,759
attacks in the context of things like

41
00:01:31,840 --> 00:01:36,000
virtualization systems

42
00:01:33,759 --> 00:01:38,640
which peripheral gets the data when two

43
00:01:36,000 --> 00:01:41,520
ports are actually overlapped in their

44
00:01:38,640 --> 00:01:43,680
port i o range is typically undefined

45
00:01:41,520 --> 00:01:45,600
and that's what can lead to unexpected

46
00:01:43,680 --> 00:01:48,159
behaviors that can give an attacker an

47
00:01:45,600 --> 00:01:50,799
advantage so here's an example of a

48
00:01:48,159 --> 00:01:52,960
table from one of the pch data sheets

49
00:01:50,799 --> 00:01:55,280
specifically the seven series that talks

50
00:01:52,960 --> 00:01:57,439
about a bunch of different ranges that

51
00:01:55,280 --> 00:01:58,479
can be you know anywhere in the 64

52
00:01:57,439 --> 00:02:00,240
kilobyte

53
00:01:58,479 --> 00:02:02,399
uh i o space

54
00:02:00,240 --> 00:02:03,680
now out of these ranges the first one is

55
00:02:02,399 --> 00:02:07,119
the one that we're actually going to see

56
00:02:03,680 --> 00:02:08,720
the most the acpi range so acpi is

57
00:02:07,119 --> 00:02:10,640
advanced configuration and power

58
00:02:08,720 --> 00:02:13,120
interface and it has to do with power

59
00:02:10,640 --> 00:02:16,400
management it's a separate specification

60
00:02:13,120 --> 00:02:18,160
which intel conforms to and in order to

61
00:02:16,400 --> 00:02:21,440
support that specification they

62
00:02:18,160 --> 00:02:24,160
basically have some range of port i o

63
00:02:21,440 --> 00:02:26,160
which is going to provide access to some

64
00:02:24,160 --> 00:02:28,319
particular registers that have to do

65
00:02:26,160 --> 00:02:30,560
with you know going into low power

66
00:02:28,319 --> 00:02:33,440
states and things like that so this is

67
00:02:30,560 --> 00:02:35,760
going to come up later as a reference to

68
00:02:33,440 --> 00:02:38,560
a pm base register power management base

69
00:02:35,760 --> 00:02:40,239
register especially when we get into the

70
00:02:38,560 --> 00:02:42,239
area about sleep wake attacks towards

71
00:02:40,239 --> 00:02:44,800
the end of the class and so what this is

72
00:02:42,239 --> 00:02:48,080
saying here is that there are 64 bytes

73
00:02:44,800 --> 00:02:50,160
on the 7 series pch that are used to

74
00:02:48,080 --> 00:02:51,360
access some amount of power management

75
00:02:50,160 --> 00:02:53,200
hardware

76
00:02:51,360 --> 00:02:55,840
another one that we're going to see a

77
00:02:53,200 --> 00:02:59,040
bit in this class is the tico or total

78
00:02:55,840 --> 00:03:01,519
cost of ownership hardware and this has

79
00:02:59,040 --> 00:03:05,120
this on the 7th series it says that this

80
00:03:01,519 --> 00:03:08,000
is located 96 bytes above the acpi base

81
00:03:05,120 --> 00:03:10,480
now the acpi base is only 64 bytes and

82
00:03:08,000 --> 00:03:11,760
this is 96 bytes above and so some

83
00:03:10,480 --> 00:03:14,879
question about you know what's in

84
00:03:11,760 --> 00:03:16,800
between that but anyways it's 32 bytes

85
00:03:14,879 --> 00:03:19,360
has to do with total cost of ownership

86
00:03:16,800 --> 00:03:21,360
which is kind of a grab bag of

87
00:03:19,360 --> 00:03:23,120
miscellaneous functionality

88
00:03:21,360 --> 00:03:25,680
having to do with things like case

89
00:03:23,120 --> 00:03:27,040
intrusion etc but we're going to care

90
00:03:25,680 --> 00:03:28,640
about it because this particular

91
00:03:27,040 --> 00:03:30,400
hardware is

92
00:03:28,640 --> 00:03:32,560
intrinsically linked with things like

93
00:03:30,400 --> 00:03:34,400
system management interrupts

94
00:03:32,560 --> 00:03:36,879
so if we looked at a newer piece of

95
00:03:34,400 --> 00:03:39,760
hardware the 100 series pch we would see

96
00:03:36,879 --> 00:03:42,319
that the acpi range was increased in

97
00:03:39,760 --> 00:03:45,360
size to 96 bytes so now it's saying you

98
00:03:42,319 --> 00:03:47,920
know there's the full 96 bytes and tco

99
00:03:45,360 --> 00:03:49,360
is placed anywhere within the 64

100
00:03:47,920 --> 00:03:51,519
kilobyte range

101
00:03:49,360 --> 00:03:54,000
and if we looked at an even newer 500

102
00:03:51,519 --> 00:03:57,360
series pch we would see that actually

103
00:03:54,000 --> 00:04:00,400
the acpi power management port i o range

104
00:03:57,360 --> 00:04:03,280
is increased to be 256 bytes anywhere in

105
00:04:00,400 --> 00:04:06,159
the 64 kilobyte range and tco is still

106
00:04:03,280 --> 00:04:07,760
32 bytes in the range somewhere and you

107
00:04:06,159 --> 00:04:09,920
can see that the sort of number of

108
00:04:07,760 --> 00:04:12,080
peripherals that are accessible via port

109
00:04:09,920 --> 00:04:13,760
i o is actually decreasing with each

110
00:04:12,080 --> 00:04:15,599
subsequent generation

111
00:04:13,760 --> 00:04:17,919
but there's still plenty of you know

112
00:04:15,599 --> 00:04:20,000
legacy type things like parallel ports

113
00:04:17,919 --> 00:04:21,440
and serial ports which are always useful

114
00:04:20,000 --> 00:04:23,520
for getting you know debug output and

115
00:04:21,440 --> 00:04:25,440
things like that but also built-in

116
00:04:23,520 --> 00:04:27,759
hardware that intel might have like the

117
00:04:25,440 --> 00:04:30,160
sata host controller so the takeaway for

118
00:04:27,759 --> 00:04:32,720
variable i o ports is that again because

119
00:04:30,160 --> 00:04:35,360
there's a very limited range of i o

120
00:04:32,720 --> 00:04:37,520
ports total available intel doesn't

121
00:04:35,360 --> 00:04:40,080
recommend that people actually use port

122
00:04:37,520 --> 00:04:41,520
io they can if they want but it's not

123
00:04:40,080 --> 00:04:43,199
recommended

124
00:04:41,520 --> 00:04:45,600
some of the things like what we saw in

125
00:04:43,199 --> 00:04:48,000
the previous table are actually required

126
00:04:45,600 --> 00:04:50,160
and they're just typically implemented

127
00:04:48,000 --> 00:04:52,240
as registers somewhere inside of the

128
00:04:50,160 --> 00:04:53,840
intel hardware that something like the

129
00:04:52,240 --> 00:04:56,000
bios or the operating system would

130
00:04:53,840 --> 00:04:58,400
configure in order to set a particular

131
00:04:56,000 --> 00:05:00,639
port in order to access some particular

132
00:04:58,400 --> 00:05:03,039
configuration registers and because

133
00:05:00,639 --> 00:05:05,120
hardware and very frequently software

134
00:05:03,039 --> 00:05:07,680
such as virtualization software doesn't

135
00:05:05,120 --> 00:05:10,080
actually check for port overlap due to

136
00:05:07,680 --> 00:05:12,240
misconfiguration this can lead to very

137
00:05:10,080 --> 00:05:15,520
strange behavior which sometimes is to

138
00:05:12,240 --> 00:05:15,520
an attacker's advantage


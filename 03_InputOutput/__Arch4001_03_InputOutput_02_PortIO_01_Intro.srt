1
00:00:00,560 --> 00:00:04,640
so now we're going to do a brief recap

2
00:00:02,639 --> 00:00:07,440
of port io something you learned about

3
00:00:04,640 --> 00:00:09,360
in architecture 2001 but which is of

4
00:00:07,440 --> 00:00:12,559
particular interest to us here because

5
00:00:09,360 --> 00:00:14,880
you will often see bios is using port io

6
00:00:12,559 --> 00:00:16,800
to access the pci configuration address

7
00:00:14,880 --> 00:00:19,520
space as well as various other

8
00:00:16,800 --> 00:00:21,039
peripherals at early boot time so we're

9
00:00:19,520 --> 00:00:23,600
going to want to understand that so we

10
00:00:21,039 --> 00:00:25,519
can understand this

11
00:00:23,600 --> 00:00:27,680
now as a reminder the port io address

12
00:00:25,519 --> 00:00:29,439
space is completely separate from the

13
00:00:27,680 --> 00:00:31,279
memory address space it's not like

14
00:00:29,439 --> 00:00:33,280
memory mapped i o you can't just use a

15
00:00:31,279 --> 00:00:35,920
move assembly instruction to access it

16
00:00:33,280 --> 00:00:38,239
you have to use dedicated in and out

17
00:00:35,920 --> 00:00:41,120
assembly instructions and as a reminder

18
00:00:38,239 --> 00:00:42,000
you can think of it as if there were 65

19
00:00:41,120 --> 00:00:45,200
000

20
00:00:42,000 --> 00:00:48,079
ports each of which is one byte big or

21
00:00:45,200 --> 00:00:51,199
you can take and combine two adjacent

22
00:00:48,079 --> 00:00:54,399
bite-size ports to create a 16-bit port

23
00:00:51,199 --> 00:00:56,239
or four of them to create a 32-bit port

24
00:00:54,399 --> 00:00:58,399
each of these ports is ultimately going

25
00:00:56,239 --> 00:01:00,320
to be mapped to some sort of peripheral

26
00:00:58,399 --> 00:01:02,320
device some sort of other piece of

27
00:01:00,320 --> 00:01:03,840
hardware so an interesting and relevant

28
00:01:02,320 --> 00:01:05,920
question is how does the hardware

29
00:01:03,840 --> 00:01:08,400
actually distinguish between port i o

30
00:01:05,920 --> 00:01:11,600
and memory access and the answer is that

31
00:01:08,400 --> 00:01:14,080
way back on the intel 808 chip there was

32
00:01:11,600 --> 00:01:15,840
this i o pin and this particular pin

33
00:01:14,080 --> 00:01:17,280
would allow the processor to understand

34
00:01:15,840 --> 00:01:20,080
whether a particular access was

35
00:01:17,280 --> 00:01:22,799
ultimately destined for memory or for

36
00:01:20,080 --> 00:01:22,799
port i o


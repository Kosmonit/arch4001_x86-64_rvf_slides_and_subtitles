1
00:00:00,480 --> 00:00:04,240
now let's learn about memory mapped io

2
00:00:02,320 --> 00:00:06,319
and a little bit about how the bios sets

3
00:00:04,240 --> 00:00:07,680
up the configuration for the memory map

4
00:00:06,319 --> 00:00:10,240
in general

5
00:00:07,680 --> 00:00:12,480
so in architecture 2001 we talked about

6
00:00:10,240 --> 00:00:14,719
physical address spaces linear address

7
00:00:12,480 --> 00:00:16,480
spaces virtual address spaces

8
00:00:14,719 --> 00:00:18,560
and originally we talked about how if

9
00:00:16,480 --> 00:00:20,800
paging isn't enabled linear addresses

10
00:00:18,560 --> 00:00:22,960
map directly to physical addresses and

11
00:00:20,800 --> 00:00:24,320
then we talked about how page tables

12
00:00:22,960 --> 00:00:26,400
meant that you could translate a

13
00:00:24,320 --> 00:00:27,760
particular linear address to a different

14
00:00:26,400 --> 00:00:29,840
physical address

15
00:00:27,760 --> 00:00:32,239
now in this description of how someone

16
00:00:29,840 --> 00:00:34,160
would access physical memory we talked

17
00:00:32,239 --> 00:00:38,480
about how you know someone could say

18
00:00:34,160 --> 00:00:40,079
well give me linear address ffff0000

19
00:00:38,480 --> 00:00:41,600
and it would translate through page

20
00:00:40,079 --> 00:00:44,239
tables but let's say that it was

21
00:00:41,600 --> 00:00:45,840
identity mapped and it would go through

22
00:00:44,239 --> 00:00:47,320
and then we would expect this to access

23
00:00:45,840 --> 00:00:49,440
ram at

24
00:00:47,320 --> 00:00:53,600
ffff0000 right

25
00:00:49,440 --> 00:00:56,000
well rtfm has determined that was a lie

26
00:00:53,600 --> 00:00:59,520
so the reality is we've already seen

27
00:00:56,000 --> 00:01:01,520
that that fff zero range actually maps

28
00:00:59,520 --> 00:01:03,760
to the spy flash chip

29
00:01:01,520 --> 00:01:06,640
so that goes through goes through a page

30
00:01:03,760 --> 00:01:08,799
table and instead of accessing ram what

31
00:01:06,640 --> 00:01:10,240
you're doing is you're doing memory

32
00:01:08,799 --> 00:01:12,000
mapped io

33
00:01:10,240 --> 00:01:13,520
and in this particular case this

34
00:01:12,000 --> 00:01:15,520
particular address will translate

35
00:01:13,520 --> 00:01:17,600
through to accesses to the spy flash

36
00:01:15,520 --> 00:01:19,759
chip but also in the same range you

37
00:01:17,600 --> 00:01:22,000
could have other peripherals such as

38
00:01:19,759 --> 00:01:24,400
network cards graphic cards other

39
00:01:22,000 --> 00:01:27,360
devices on pcie or other internal

40
00:01:24,400 --> 00:01:29,920
devices so memory map i o is one of the

41
00:01:27,360 --> 00:01:31,840
two types of i o port i o which we saw

42
00:01:29,920 --> 00:01:32,799
in architecture 2001 and memory map

43
00:01:31,840 --> 00:01:34,320
diode

44
00:01:32,799 --> 00:01:35,840
and so fundamentally what you're doing

45
00:01:34,320 --> 00:01:37,680
when you're doing either of these types

46
00:01:35,840 --> 00:01:39,759
of i o is you're accessing something

47
00:01:37,680 --> 00:01:41,759
other than the cpu you're accessing

48
00:01:39,759 --> 00:01:44,159
something other than ram at least your

49
00:01:41,759 --> 00:01:46,000
normal dram that you think of

50
00:01:44,159 --> 00:01:48,159
in the context of a bios you're using

51
00:01:46,000 --> 00:01:51,119
assembly instructions to access these

52
00:01:48,159 --> 00:01:52,880
spaces and while things like port io or

53
00:01:51,119 --> 00:01:54,720
the particular memory ranges that are

54
00:01:52,880 --> 00:01:57,360
mapped to peripherals would usually be

55
00:01:54,720 --> 00:01:59,280
restricted to ring zero only in a normal

56
00:01:57,360 --> 00:02:00,880
operating system obviously when the

57
00:01:59,280 --> 00:02:03,360
system is just first starting out

58
00:02:00,880 --> 00:02:06,079
there's no ring privilege separation and

59
00:02:03,360 --> 00:02:07,759
so real mode can just go right through

60
00:02:06,079 --> 00:02:10,399
so let's talk a little bit about the

61
00:02:07,759 --> 00:02:13,040
memory map this is the idea that it

62
00:02:10,399 --> 00:02:15,599
turns out the physical address range is

63
00:02:13,040 --> 00:02:17,680
not exclusively mapped to

64
00:02:15,599 --> 00:02:19,599
physical ram it can be mapped to these

65
00:02:17,680 --> 00:02:21,280
other peripherals and so when the system

66
00:02:19,599 --> 00:02:23,520
first starts up the only thing it

67
00:02:21,280 --> 00:02:26,640
actually knows about is this upper range

68
00:02:23,520 --> 00:02:28,720
that is always by hardware defaulted to

69
00:02:26,640 --> 00:02:31,120
map to the spy flash chip on modern

70
00:02:28,720 --> 00:02:33,680
systems this is sometimes called the

71
00:02:31,120 --> 00:02:36,879
boot block and it's going to be a 512

72
00:02:33,680 --> 00:02:38,800
byte rank kilobyte range of memory that

73
00:02:36,879 --> 00:02:41,760
is going to map to the very end of the

74
00:02:38,800 --> 00:02:45,040
spy flash chip if you watch the optional

75
00:02:41,760 --> 00:02:46,720
material on the bios secret decoder ring

76
00:02:45,040 --> 00:02:48,560
you saw a little bit about how the

77
00:02:46,720 --> 00:02:50,560
hardware mapped this and it further

78
00:02:48,560 --> 00:02:53,760
mapped some additional space down below

79
00:02:50,560 --> 00:02:56,080
it to other chunks of the spy flash

80
00:02:53,760 --> 00:02:58,080
so if this is all the bios knows about

81
00:02:56,080 --> 00:02:59,680
when it's first getting started then

82
00:02:58,080 --> 00:03:01,680
that means that the bios needs to

83
00:02:59,680 --> 00:03:03,760
configure a whole bunch of other stuff

84
00:03:01,680 --> 00:03:05,280
in order to understand what's where in

85
00:03:03,760 --> 00:03:07,920
memory

86
00:03:05,280 --> 00:03:10,480
it effectively has to play tetris with a

87
00:03:07,920 --> 00:03:12,239
number of different areas of memory all

88
00:03:10,480 --> 00:03:14,800
of which have their own various rules

89
00:03:12,239 --> 00:03:19,800
and usages it's essentially doing tetris

90
00:03:14,800 --> 00:03:19,800
to kind of fill in this area of memory

91
00:03:30,239 --> 00:03:34,640
yeah so that's the bios's job to

92
00:03:32,879 --> 00:03:36,879
basically fill in this memory and

93
00:03:34,640 --> 00:03:38,400
understand what's mapped where make sure

94
00:03:36,879 --> 00:03:40,400
things don't overlap because that can

95
00:03:38,400 --> 00:03:42,560
cause very strange and potentially

96
00:03:40,400 --> 00:03:44,959
security relevant behavior

97
00:03:42,560 --> 00:03:47,360
so then poor gilgamesh who paid many

98
00:03:44,959 --> 00:03:49,840
bushels of wheat for his 8 gigabytes of

99
00:03:47,360 --> 00:03:51,760
ram back in the day well he wants to

100
00:03:49,840 --> 00:03:54,080
access that ram right he doesn't want to

101
00:03:51,760 --> 00:03:56,879
actually lose that memory to this memory

102
00:03:54,080 --> 00:03:58,959
mapped i o region and so intel has a

103
00:03:56,879 --> 00:04:01,760
solution for that which is this very

104
00:03:58,959 --> 00:04:03,200
complicated looking diagram but in order

105
00:04:01,760 --> 00:04:05,920
to make it simpler let's use this

106
00:04:03,200 --> 00:04:08,319
diagram from an invisible things labstop

107
00:04:05,920 --> 00:04:10,239
they actually used this to attack it

108
00:04:08,319 --> 00:04:12,560
turns out that there are a couple of

109
00:04:10,239 --> 00:04:15,519
registers that can be used to map a

110
00:04:12,560 --> 00:04:17,519
chunk of ram that is otherwise stolen

111
00:04:15,519 --> 00:04:19,759
from the physical address space by

112
00:04:17,519 --> 00:04:21,680
memory mapped i o you take this chunk of

113
00:04:19,759 --> 00:04:24,240
ram and you map it where these memory

114
00:04:21,680 --> 00:04:26,160
registers specify now they used it to

115
00:04:24,240 --> 00:04:28,639
overlap with system management mode in

116
00:04:26,160 --> 00:04:30,240
order to break past some access controls

117
00:04:28,639 --> 00:04:32,960
but in this context let's just think

118
00:04:30,240 --> 00:04:35,040
about its intended usage which is taking

119
00:04:32,960 --> 00:04:36,800
this chunk of physical ram mapping it

120
00:04:35,040 --> 00:04:39,759
somewhere else that an operating system

121
00:04:36,800 --> 00:04:39,759
can use it


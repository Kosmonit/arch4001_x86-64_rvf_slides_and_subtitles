1
00:00:00,240 --> 00:00:05,520
and one last time where does one learn

2
00:00:02,240 --> 00:00:07,520
about which ports are which devices well

3
00:00:05,520 --> 00:00:09,519
one does not simply read the intel

4
00:00:07,520 --> 00:00:11,440
software development manuals promise

5
00:00:09,519 --> 00:00:14,480
back in architecture 2001 that we would

6
00:00:11,440 --> 00:00:15,920
eventually in architecture 4001 get to

7
00:00:14,480 --> 00:00:17,440
the point where we would understand

8
00:00:15,920 --> 00:00:19,680
these data sheets a little bit better

9
00:00:17,440 --> 00:00:20,880
specifically the chipset documentation

10
00:00:19,680 --> 00:00:22,160
the first thing that you need to know is

11
00:00:20,880 --> 00:00:25,359
that there's actually two different

12
00:00:22,160 --> 00:00:27,680
types of port i o address spaces there's

13
00:00:25,359 --> 00:00:30,320
fixed ports which are always going to be

14
00:00:27,680 --> 00:00:32,239
at a very specific port and they can't

15
00:00:30,320 --> 00:00:34,480
be relocated and sometimes they can be

16
00:00:32,239 --> 00:00:37,200
disabled but not always and then there

17
00:00:34,480 --> 00:00:39,120
are variable ports these are ports that

18
00:00:37,200 --> 00:00:41,360
can actually be assigned a particular

19
00:00:39,120 --> 00:00:43,280
port by the software such as the bios or

20
00:00:41,360 --> 00:00:45,360
the operating system and they can be

21
00:00:43,280 --> 00:00:47,440
moved around and no matter where they're

22
00:00:45,360 --> 00:00:49,760
moved in the port io address space they

23
00:00:47,440 --> 00:00:51,680
will still map to the same particular

24
00:00:49,760 --> 00:00:54,399
piece of hardware so first let's

25
00:00:51,680 --> 00:00:56,800
consider fixed i o ports and while all

26
00:00:54,399 --> 00:00:59,280
port io is generally speaking a gateway

27
00:00:56,800 --> 00:01:02,480
to a black box in the case of fixed i o

28
00:00:59,280 --> 00:01:04,400
ports the pch on your system may say

29
00:01:02,480 --> 00:01:06,159
something about which port has which

30
00:01:04,400 --> 00:01:07,680
device behind it

31
00:01:06,159 --> 00:01:10,080
of course the other side is some

32
00:01:07,680 --> 00:01:11,680
particular piece of hardware and every

33
00:01:10,080 --> 00:01:13,439
piece of hardware can behave slightly

34
00:01:11,680 --> 00:01:15,759
differently it's ultimately up to the

35
00:01:13,439 --> 00:01:17,040
hardware maker how it behaves and that's

36
00:01:15,759 --> 00:01:19,280
why it can be very difficult to

37
00:01:17,040 --> 00:01:21,119
understand what's going on if you don't

38
00:01:19,280 --> 00:01:23,439
have a particular fixed port mapped to

39
00:01:21,119 --> 00:01:25,200
some particular piece of hardware

40
00:01:23,439 --> 00:01:28,400
and so we had seen things like this

41
00:01:25,200 --> 00:01:31,520
before in 2001 this is an example from a

42
00:01:28,400 --> 00:01:33,360
7 series pch and they have tables inside

43
00:01:31,520 --> 00:01:34,720
the data sheets which you should have

44
00:01:33,360 --> 00:01:36,320
found the datasheet by now and so you

45
00:01:34,720 --> 00:01:37,439
should be able to find this kind of

46
00:01:36,320 --> 00:01:39,840
table

47
00:01:37,439 --> 00:01:41,759
and the tables list both fixed and

48
00:01:39,840 --> 00:01:43,360
variable address ranges

49
00:01:41,759 --> 00:01:45,439
so starting with the fixed ranges you

50
00:01:43,360 --> 00:01:48,079
can think see things like dma

51
00:01:45,439 --> 00:01:49,439
controllers interrupt controllers and so

52
00:01:48,079 --> 00:01:52,159
forth

53
00:01:49,439 --> 00:01:55,280
now in particular you know the things

54
00:01:52,159 --> 00:01:57,119
can be not particularly illustrative in

55
00:01:55,280 --> 00:01:58,399
talking about what this particular

56
00:01:57,119 --> 00:02:01,119
hardware is

57
00:01:58,399 --> 00:02:04,159
for example port 60 has historically

58
00:02:01,119 --> 00:02:05,200
been used for the intel 8042 keyboard

59
00:02:04,159 --> 00:02:06,880
controller

60
00:02:05,200 --> 00:02:08,879
given the chip number you can expect

61
00:02:06,880 --> 00:02:11,200
that this has been used for a very long

62
00:02:08,879 --> 00:02:14,319
time since around the time of the first

63
00:02:11,200 --> 00:02:16,800
ibm pcs and this is a type of device

64
00:02:14,319 --> 00:02:18,480
that would have port 60 being used for

65
00:02:16,800 --> 00:02:21,200
an index into the keyboard controller

66
00:02:18,480 --> 00:02:23,280
and port 64 used as a data port

67
00:02:21,200 --> 00:02:24,959
but you don't get any of that detail by

68
00:02:23,280 --> 00:02:26,959
just looking at the data sheet it just

69
00:02:24,959 --> 00:02:28,640
says it's a microcontroller it doesn't

70
00:02:26,959 --> 00:02:30,800
tell you what it is doesn't tell you how

71
00:02:28,640 --> 00:02:33,360
you access it what the commands are or

72
00:02:30,800 --> 00:02:35,040
anything like that

73
00:02:33,360 --> 00:02:38,640
another thing that we actually did see

74
00:02:35,040 --> 00:02:42,560
before was port 70 and that was used for

75
00:02:38,640 --> 00:02:44,959
the cmos and real-time clock it also had

76
00:02:42,560 --> 00:02:46,319
an element of use for non-maskable

77
00:02:44,959 --> 00:02:48,720
interrupts but we didn't cover that

78
00:02:46,319 --> 00:02:53,200
before another thing we're going to see

79
00:02:48,720 --> 00:02:55,440
quite a bit later is the port b2 and b3

80
00:02:53,200 --> 00:02:56,239
these are listed as power management and

81
00:02:55,440 --> 00:02:58,239
so

82
00:02:56,239 --> 00:02:59,440
that doesn't really tell you a whole lot

83
00:02:58,239 --> 00:03:01,280
of anything

84
00:02:59,440 --> 00:03:03,440
but specifically this is going to be

85
00:03:01,280 --> 00:03:05,440
used for power management causing system

86
00:03:03,440 --> 00:03:07,120
management interrupts which system

87
00:03:05,440 --> 00:03:09,440
management mode being for system

88
00:03:07,120 --> 00:03:12,239
management power management is one of

89
00:03:09,440 --> 00:03:14,640
its jobs and so this is basically a way

90
00:03:12,239 --> 00:03:16,400
to invoke system management mode to have

91
00:03:14,640 --> 00:03:17,519
it go off and do some power management

92
00:03:16,400 --> 00:03:19,120
things

93
00:03:17,519 --> 00:03:20,560
so the key takeaway if you look at the

94
00:03:19,120 --> 00:03:22,480
data sheets is that there's a lot of

95
00:03:20,560 --> 00:03:25,440
different devices that are mapped to

96
00:03:22,480 --> 00:03:27,040
fixed i o address spaces and the space

97
00:03:25,440 --> 00:03:28,400
is actually very fragmented there's a

98
00:03:27,040 --> 00:03:30,480
whole bunch of

99
00:03:28,400 --> 00:03:32,959
unused spaces in there and so a

100
00:03:30,480 --> 00:03:35,440
particular hardware vendor could use any

101
00:03:32,959 --> 00:03:37,360
of those fixed port i o ranges and map

102
00:03:35,440 --> 00:03:39,120
them to some particular custom

103
00:03:37,360 --> 00:03:40,879
peripheral that they have and that can

104
00:03:39,120 --> 00:03:43,280
make it extremely difficult to figure

105
00:03:40,879 --> 00:03:45,920
out what is actually behind the curtain

106
00:03:43,280 --> 00:03:48,239
because of this very fragmented space

107
00:03:45,920 --> 00:03:50,560
intel these days doesn't recommend

108
00:03:48,239 --> 00:03:51,440
vendors actually use the port i o range

109
00:03:50,560 --> 00:03:53,360
for

110
00:03:51,440 --> 00:03:57,040
accessing peripherals they recommend

111
00:03:53,360 --> 00:03:57,040
that you use memory mapped i o


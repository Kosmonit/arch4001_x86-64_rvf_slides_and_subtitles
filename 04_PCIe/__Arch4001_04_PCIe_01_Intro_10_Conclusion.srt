1
00:00:00,160 --> 00:00:04,799
all right well we learned a lot of stuff

2
00:00:01,920 --> 00:00:07,200
in this section we learned about how pci

3
00:00:04,799 --> 00:00:10,000
has three address spaces configuration

4
00:00:07,200 --> 00:00:12,559
address space which is 256 bytes and is

5
00:00:10,000 --> 00:00:14,480
accessible via port i o before memory

6
00:00:12,559 --> 00:00:17,119
mapped io is set up and once memory

7
00:00:14,480 --> 00:00:18,960
mapped i o is set up via the pci export

8
00:00:17,119 --> 00:00:19,920
then it is accessible via memory map

9
00:00:18,960 --> 00:00:22,240
dial

10
00:00:19,920 --> 00:00:25,279
there is the configuration address space

11
00:00:22,240 --> 00:00:27,359
the second is memory mapped io spaces

12
00:00:25,279 --> 00:00:30,640
made available via the bars the base

13
00:00:27,359 --> 00:00:32,960
address registers and also the port io

14
00:00:30,640 --> 00:00:34,960
spaces where you configure a bar for

15
00:00:32,960 --> 00:00:37,200
port i o instead of memory map dial but

16
00:00:34,960 --> 00:00:39,920
that's not the recommended mechanism

17
00:00:37,200 --> 00:00:42,079
we also learned about pci e and how it

18
00:00:39,920 --> 00:00:44,320
has four address spaces three of which

19
00:00:42,079 --> 00:00:46,480
are the same and one the message space

20
00:00:44,320 --> 00:00:48,160
that we're not covering in this class

21
00:00:46,480 --> 00:00:50,239
and so we saw the only real difference

22
00:00:48,160 --> 00:00:52,879
that we care about is the fact that pci

23
00:00:50,239 --> 00:00:54,800
e has a four kilobyte extended

24
00:00:52,879 --> 00:00:57,280
configuration address space which is

25
00:00:54,800 --> 00:00:59,199
only available via memory mapped i o

26
00:00:57,280 --> 00:01:02,160
and in this section we understood a

27
00:00:59,199 --> 00:01:05,760
little bit better how intel uses pcie

28
00:01:02,160 --> 00:01:07,840
internally on its cpus and pchs as sort

29
00:01:05,760 --> 00:01:09,920
of the internal fabric to expose

30
00:01:07,840 --> 00:01:12,320
information about configuration

31
00:01:09,920 --> 00:01:14,720
registers for internal hardware pieces

32
00:01:12,320 --> 00:01:16,320
like the dram controller or the lpc

33
00:01:14,720 --> 00:01:17,600
device which are the two things that

34
00:01:16,320 --> 00:01:19,280
we're going to care about most in this

35
00:01:17,600 --> 00:01:20,560
class now obviously you could see

36
00:01:19,280 --> 00:01:22,479
there's a whole bunch of other stuff in

37
00:01:20,560 --> 00:01:25,040
the data sheets things like sata

38
00:01:22,479 --> 00:01:26,720
controllers and gigabit ethernet and so

39
00:01:25,040 --> 00:01:29,600
you can of course go off and check out

40
00:01:26,720 --> 00:01:31,439
those more on your own after the class

41
00:01:29,600 --> 00:01:33,680
so returning to this picture of miss

42
00:01:31,439 --> 00:01:35,920
frizzle being the driver interacting

43
00:01:33,680 --> 00:01:38,880
with a nic card we said that there was

44
00:01:35,920 --> 00:01:41,920
the 256 bytes of configuration address

45
00:01:38,880 --> 00:01:44,240
space and if she for instance you know

46
00:01:41,920 --> 00:01:46,079
sent out a transaction to interact with

47
00:01:44,240 --> 00:01:48,079
one of those registers in there it would

48
00:01:46,079 --> 00:01:50,560
go to the instruction decoder memory

49
00:01:48,079 --> 00:01:52,640
controller pcie hardware and then once

50
00:01:50,560 --> 00:01:54,799
it reaches the device the device would

51
00:01:52,640 --> 00:01:56,560
you know decide how to interpret it if

52
00:01:54,799 --> 00:01:59,119
it was something at the very beginning

53
00:01:56,560 --> 00:02:01,200
in the standard pci header like the base

54
00:01:59,119 --> 00:02:03,920
address registers then you know it would

55
00:02:01,200 --> 00:02:06,079
hit a register and the device would

56
00:02:03,920 --> 00:02:08,879
ultimately use that to expose some

57
00:02:06,079 --> 00:02:11,039
information via memory mapped i o or it

58
00:02:08,879 --> 00:02:13,840
could be setting a base address register

59
00:02:11,039 --> 00:02:15,360
for port i o and we saw for instance how

60
00:02:13,840 --> 00:02:18,239
you know a little snippet of assembly

61
00:02:15,360 --> 00:02:20,480
writing to the cf-8 register can take

62
00:02:18,239 --> 00:02:22,400
that particular value that's written

63
00:02:20,480 --> 00:02:25,120
parse it according to bus device

64
00:02:22,400 --> 00:02:26,959
function offset and use that to select

65
00:02:25,120 --> 00:02:29,200
some location inside the pci

66
00:02:26,959 --> 00:02:32,400
configuration address space so that when

67
00:02:29,200 --> 00:02:34,480
writing to the cfc register it would be

68
00:02:32,400 --> 00:02:37,040
writing to that config address space or

69
00:02:34,480 --> 00:02:39,840
reading from it so with that we're back

70
00:02:37,040 --> 00:02:42,080
to our map and we are done with the pci

71
00:02:39,840 --> 00:02:43,760
configuration address space and we are

72
00:02:42,080 --> 00:02:45,200
continuing on towards flashlight

73
00:02:43,760 --> 00:02:48,319
protection but we're going to take a

74
00:02:45,200 --> 00:02:49,760
quick pit stop at pcie optionram attacks

75
00:02:48,319 --> 00:02:53,840
because now we know everything we need

76
00:02:49,760 --> 00:02:53,840
to know to understand how those work


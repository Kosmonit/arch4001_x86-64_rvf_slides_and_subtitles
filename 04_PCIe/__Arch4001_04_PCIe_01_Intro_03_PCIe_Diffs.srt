1
00:00:00,640 --> 00:00:06,480
so now let's move on to see how pcie is

2
00:00:03,760 --> 00:00:08,160
different from legacy pci and what

3
00:00:06,480 --> 00:00:09,920
particular changes we care about for the

4
00:00:08,160 --> 00:00:11,120
purposes of this class

5
00:00:09,920 --> 00:00:12,960
now as mentioned before it's

6
00:00:11,120 --> 00:00:15,280
significantly different at the physical

7
00:00:12,960 --> 00:00:16,800
hardware level but they kept backwards

8
00:00:15,280 --> 00:00:18,720
compatibility for things like the

9
00:00:16,800 --> 00:00:20,720
configuration address space so it's

10
00:00:18,720 --> 00:00:22,720
actually mostly the same at the software

11
00:00:20,720 --> 00:00:25,279
level the one big difference that we're

12
00:00:22,720 --> 00:00:28,640
going to care about is that it adds

13
00:00:25,279 --> 00:00:31,119
a four kilobyte extended address space

14
00:00:28,640 --> 00:00:33,600
so the pci configuration address space

15
00:00:31,119 --> 00:00:35,600
is extended from 256 bytes to 4

16
00:00:33,600 --> 00:00:37,920
kilobytes

17
00:00:35,600 --> 00:00:41,040
so that would be something like this you

18
00:00:37,920 --> 00:00:44,719
extend that up to being 4 kilobytes

19
00:00:41,040 --> 00:00:47,360
instead of just 256 bytes

20
00:00:44,719 --> 00:00:48,879
so pcie does actually have four address

21
00:00:47,360 --> 00:00:50,640
spaces instead of three but we're

22
00:00:48,879 --> 00:00:53,120
actually not going to care about or get

23
00:00:50,640 --> 00:00:56,239
into the fourth one so it's basically

24
00:00:53,120 --> 00:00:58,480
the same as pcie in pci in that it has

25
00:00:56,239 --> 00:01:00,399
the configuration address space but now

26
00:00:58,480 --> 00:01:03,440
it's been extended to 4 kilobytes

27
00:01:00,399 --> 00:01:05,760
instead of 256 bytes and that's unique

28
00:01:03,440 --> 00:01:07,119
in that you can only access it now the

29
00:01:05,760 --> 00:01:10,159
extra space

30
00:01:07,119 --> 00:01:11,600
via memory mapped i o the first 256

31
00:01:10,159 --> 00:01:14,479
bytes you can use either memory mapped

32
00:01:11,600 --> 00:01:16,799
diode or port io but the last bytes you

33
00:01:14,479 --> 00:01:18,479
can only access through memory map dial

34
00:01:16,799 --> 00:01:20,880
it also once again has the optional

35
00:01:18,479 --> 00:01:22,799
memory mapped i o space and a port i o

36
00:01:20,880 --> 00:01:24,720
space and then this fourth space is the

37
00:01:22,799 --> 00:01:25,920
pci message space

38
00:01:24,720 --> 00:01:27,680
but we're not actually going to cover

39
00:01:25,920 --> 00:01:28,720
that all so let's pretend that doesn't

40
00:01:27,680 --> 00:01:30,880
exist

41
00:01:28,720 --> 00:01:32,640
in terms of the topology differences it

42
00:01:30,880 --> 00:01:36,079
looks basically the same in that you

43
00:01:32,640 --> 00:01:39,360
still have 256 buses 32 devices eight

44
00:01:36,079 --> 00:01:42,079
functions but whereas in pci the buses

45
00:01:39,360 --> 00:01:47,520
were connected by bridges you can still

46
00:01:42,079 --> 00:01:49,680
in pcie have a pcie 2 legacy pci bridge

47
00:01:47,520 --> 00:01:51,360
so that'll still connect those up but

48
00:01:49,680 --> 00:01:53,840
when we're talking about sort of native

49
00:01:51,360 --> 00:01:56,320
pcie interconnects are instead talking

50
00:01:53,840 --> 00:01:57,840
about switches which again you know as

51
00:01:56,320 --> 00:01:59,759
sort of shown in that first picture at

52
00:01:57,840 --> 00:02:01,520
the very introduction you know it sort

53
00:01:59,759 --> 00:02:03,759
of looks its switch in the sense of like

54
00:02:01,520 --> 00:02:05,759
a network switch it's basically routing

55
00:02:03,759 --> 00:02:08,000
transactional packets between a bunch of

56
00:02:05,759 --> 00:02:09,039
different devices on this low-level

57
00:02:08,000 --> 00:02:11,280
fabric

58
00:02:09,039 --> 00:02:13,200
so once again returning to this somewhat

59
00:02:11,280 --> 00:02:15,920
ancient diagram showing a memory

60
00:02:13,200 --> 00:02:17,760
controller hub and an i o controller hub

61
00:02:15,920 --> 00:02:19,920
i said i like this because it shows

62
00:02:17,760 --> 00:02:22,400
these internal devices which are

63
00:02:19,920 --> 00:02:24,720
internal pcie devices so the two things

64
00:02:22,400 --> 00:02:26,720
we care about are the dram controller

65
00:02:24,720 --> 00:02:28,160
which now you can see says bus zero

66
00:02:26,720 --> 00:02:30,560
device zero

67
00:02:28,160 --> 00:02:33,760
and the lpc device which says bus zero

68
00:02:30,560 --> 00:02:37,280
device 31 function zero and so we have

69
00:02:33,760 --> 00:02:39,440
this pcie pci configuration window in

70
00:02:37,280 --> 00:02:41,920
the i o space so that meant sort of the

71
00:02:39,440 --> 00:02:44,239
port i o was a way that you could access

72
00:02:41,920 --> 00:02:46,720
all of these various pci devices

73
00:02:44,239 --> 00:02:49,200
and essentially the you know intra

74
00:02:46,720 --> 00:02:51,920
device fabric the backbone of this

75
00:02:49,200 --> 00:02:55,760
entire thing is actual pci

76
00:02:51,920 --> 00:02:57,840
pci or pcie and then that dmi link is

77
00:02:55,760 --> 00:03:00,239
very similar to pci it's not exactly the

78
00:02:57,840 --> 00:03:02,080
same it's not pci standards compliant or

79
00:03:00,239 --> 00:03:03,760
anything like that it's intel's own

80
00:03:02,080 --> 00:03:06,239
thing but at the end of the day its

81
00:03:03,760 --> 00:03:09,519
primary job whether it be from the cpu

82
00:03:06,239 --> 00:03:11,200
to the pch or the gmch to the ich it's

83
00:03:09,519 --> 00:03:13,360
really just kind of trying to route

84
00:03:11,200 --> 00:03:15,599
things through this you know pci

85
00:03:13,360 --> 00:03:17,440
backbone taking a little bit closer look

86
00:03:15,599 --> 00:03:20,480
at this diagram we can see things like

87
00:03:17,440 --> 00:03:22,800
the host pci express bridge so this

88
00:03:20,480 --> 00:03:25,680
would be a means of you know bridging

89
00:03:22,800 --> 00:03:27,920
between pci and pci express then you

90
00:03:25,680 --> 00:03:30,640
have things like internal regis internal

91
00:03:27,920 --> 00:03:32,640
graphics device stuff like that so this

92
00:03:30,640 --> 00:03:35,280
you know you could connect a external

93
00:03:32,640 --> 00:03:36,799
graphics card to pci on this and then

94
00:03:35,280 --> 00:03:39,120
that graphics card would be considered

95
00:03:36,799 --> 00:03:40,879
to be on a different bus interacting

96
00:03:39,120 --> 00:03:45,680
with the rest of the system through this

97
00:03:40,879 --> 00:03:45,680
particular pci to pcie bridge


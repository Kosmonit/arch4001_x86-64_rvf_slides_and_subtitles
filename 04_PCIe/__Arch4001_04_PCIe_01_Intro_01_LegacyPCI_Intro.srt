1
00:00:00,240 --> 00:00:04,240
now it's time to move on to learning

2
00:00:01,599 --> 00:00:06,000
about pcie configuration address space

3
00:00:04,240 --> 00:00:07,759
that's something that's accessed through

4
00:00:06,000 --> 00:00:10,000
port i o at the very beginning of the

5
00:00:07,759 --> 00:00:12,320
system boot and then later on memory

6
00:00:10,000 --> 00:00:15,040
mapped i o once the port i o is used to

7
00:00:12,320 --> 00:00:16,720
set up the memory mapped io

8
00:00:15,040 --> 00:00:19,359
so we're not going to really get into

9
00:00:16,720 --> 00:00:21,760
the super low level hardware details on

10
00:00:19,359 --> 00:00:23,840
this because they're not relevant for us

11
00:00:21,760 --> 00:00:25,519
all of the pcie access that we care

12
00:00:23,840 --> 00:00:26,640
about is done through the configuration

13
00:00:25,519 --> 00:00:29,199
address space that we're going to learn

14
00:00:26,640 --> 00:00:30,800
about and it's basically an abstraction

15
00:00:29,199 --> 00:00:32,320
and therefore we're only going to care

16
00:00:30,800 --> 00:00:33,920
about the abstraction that the bios

17
00:00:32,320 --> 00:00:35,360
actually sees in this class

18
00:00:33,920 --> 00:00:37,120
there's a lot of interesting things to

19
00:00:35,360 --> 00:00:39,440
learn down at the physical layers of the

20
00:00:37,120 --> 00:00:41,440
protocol about how you can do spoofing

21
00:00:39,440 --> 00:00:42,960
to cause you know dma requests to look

22
00:00:41,440 --> 00:00:45,039
like they come from different devices

23
00:00:42,960 --> 00:00:46,399
and so forth but that's not the purpose

24
00:00:45,039 --> 00:00:47,760
of this class hopefully in a future

25
00:00:46,399 --> 00:00:49,600
class

26
00:00:47,760 --> 00:00:51,120
so a little bit about the evolution of

27
00:00:49,600 --> 00:00:54,000
the architecture

28
00:00:51,120 --> 00:00:57,120
it started out as pci and then the next

29
00:00:54,000 --> 00:01:00,399
version was called pci x for extended

30
00:00:57,120 --> 00:01:02,079
and then eventually pcie for express now

31
00:01:00,399 --> 00:01:03,920
pci express is what you're going to be

32
00:01:02,079 --> 00:01:06,560
dealing with almost exclusively on all

33
00:01:03,920 --> 00:01:08,880
modern systems but there is backwards

34
00:01:06,560 --> 00:01:11,200
compatibility and you can introduce very

35
00:01:08,880 --> 00:01:12,479
old legacy systems if the hardware is

36
00:01:11,200 --> 00:01:14,720
set up that way

37
00:01:12,479 --> 00:01:18,400
but the interesting point here is that

38
00:01:14,720 --> 00:01:20,479
pci and pci x were a parallel connection

39
00:01:18,400 --> 00:01:22,400
and then once they move to pcie it's a

40
00:01:20,479 --> 00:01:24,799
serial connection using a sort of

41
00:01:22,400 --> 00:01:27,040
switching mechanism and therefore pcie

42
00:01:24,799 --> 00:01:29,680
looks very much like a typical network

43
00:01:27,040 --> 00:01:31,840
that you would see inter-computer but

44
00:01:29,680 --> 00:01:33,840
it's the intra-computer network that's

45
00:01:31,840 --> 00:01:36,240
used to connect a bunch of internal

46
00:01:33,840 --> 00:01:38,240
chunks of hardware in the cpu pch and

47
00:01:36,240 --> 00:01:40,240
also external peripheral cards

48
00:01:38,240 --> 00:01:41,920
thunderbolt cards and stuff like that

49
00:01:40,240 --> 00:01:44,960
now at the very beginning we're going to

50
00:01:41,920 --> 00:01:47,680
talk about normal pci or legacy pci or

51
00:01:44,960 --> 00:01:49,439
sometimes we call it compatible pci and

52
00:01:47,680 --> 00:01:51,119
that's just because the most important

53
00:01:49,439 --> 00:01:52,720
thing we care about that configuration

54
00:01:51,119 --> 00:01:56,640
address space was introduced by the

55
00:01:52,720 --> 00:01:58,799
original pci later on we'll move to pcie

56
00:01:56,640 --> 00:02:00,560
and there's only one sort of extension

57
00:01:58,799 --> 00:02:02,079
that we care about where they basically

58
00:02:00,560 --> 00:02:04,479
make the configuration address space a

59
00:02:02,079 --> 00:02:06,479
little bit bigger so the original pci

60
00:02:04,479 --> 00:02:08,640
stands for peripheral component

61
00:02:06,479 --> 00:02:12,400
interconnect you may also see me refer

62
00:02:08,640 --> 00:02:14,879
to it as legacy pci or compatible pci

63
00:02:12,400 --> 00:02:17,599
and it was specified and standardized

64
00:02:14,879 --> 00:02:19,520
around 1993 by intel as the name

65
00:02:17,599 --> 00:02:21,360
suggests the key point was to

66
00:02:19,520 --> 00:02:23,440
standardize how you connect to

67
00:02:21,360 --> 00:02:25,200
peripherals but what's interesting about

68
00:02:23,440 --> 00:02:27,360
it is the fact that because intel came

69
00:02:25,200 --> 00:02:31,440
up with it they integrated it into their

70
00:02:27,360 --> 00:02:34,000
cpus pchs mchs very deeply and so it

71
00:02:31,440 --> 00:02:37,200
effectively forms a sort of backbone

72
00:02:34,000 --> 00:02:39,280
fabric inside of cpus and pchs whereby

73
00:02:37,200 --> 00:02:41,360
the main processor can get access to

74
00:02:39,280 --> 00:02:43,120
other hardware components within the

75
00:02:41,360 --> 00:02:45,760
actual chip itself

76
00:02:43,120 --> 00:02:48,239
so the core interfaces of pci are

77
00:02:45,760 --> 00:02:51,519
actually processor independent so you

78
00:02:48,239 --> 00:02:53,440
could have a pci system on x86 or arm or

79
00:02:51,519 --> 00:02:55,200
power or any other thing but as i said

80
00:02:53,440 --> 00:02:56,239
for our purposes we really care about

81
00:02:55,200 --> 00:02:58,480
the fact that it's just sort of

82
00:02:56,239 --> 00:03:00,800
abstracted away and mostly it's all

83
00:02:58,480 --> 00:03:03,040
about configuration address space access

84
00:03:00,800 --> 00:03:04,640
that's what the bios and cpu use to

85
00:03:03,040 --> 00:03:06,560
access the various configuration

86
00:03:04,640 --> 00:03:08,319
registers of chunks of hardware and

87
00:03:06,560 --> 00:03:10,159
while modern systems are mostly going to

88
00:03:08,319 --> 00:03:11,760
be using pci express like i mentioned

89
00:03:10,159 --> 00:03:13,440
before they do of course support

90
00:03:11,760 --> 00:03:16,159
backwards compatibility for older

91
00:03:13,440 --> 00:03:18,400
hardware and nowadays there's a special

92
00:03:16,159 --> 00:03:21,440
interest group the pci special interest

93
00:03:18,400 --> 00:03:23,519
group which maintains the standards as

94
00:03:21,440 --> 00:03:25,920
pci continues to evolve for the most

95
00:03:23,519 --> 00:03:27,760
part just to make it faster to allow for

96
00:03:25,920 --> 00:03:29,440
higher throughput when interacting with

97
00:03:27,760 --> 00:03:31,920
peripherals now let's talk about the

98
00:03:29,440 --> 00:03:34,000
topology of pci because this is going to

99
00:03:31,920 --> 00:03:37,840
be more or less the same between the

100
00:03:34,000 --> 00:03:39,519
legacy or compatible pci and pci express

101
00:03:37,840 --> 00:03:42,720
so the first thing is that you can have

102
00:03:39,519 --> 00:03:46,000
up to 256 buses

103
00:03:42,720 --> 00:03:47,200
attached to each bus can be up to 32

104
00:03:46,000 --> 00:03:49,440
devices

105
00:03:47,200 --> 00:03:51,440
so let's add some more buses and let's

106
00:03:49,440 --> 00:03:53,599
add some more devices

107
00:03:51,440 --> 00:03:55,920
and for a given device you can actually

108
00:03:53,599 --> 00:03:58,720
have multiple what they call functions

109
00:03:55,920 --> 00:04:01,519
and so this would be if for instance a

110
00:03:58,720 --> 00:04:03,519
particular card supported wi-fi and

111
00:04:01,519 --> 00:04:05,680
bluetooth at the same time those would

112
00:04:03,519 --> 00:04:07,280
be two independent functions which would

113
00:04:05,680 --> 00:04:09,519
may or may not have two independent

114
00:04:07,280 --> 00:04:11,760
processors on the particular device or

115
00:04:09,519 --> 00:04:13,920
two cores in the same processor so you

116
00:04:11,760 --> 00:04:15,840
can think of functions as attached to

117
00:04:13,920 --> 00:04:17,680
devices or you can think of them as

118
00:04:15,840 --> 00:04:19,600
being actually internal to the device

119
00:04:17,680 --> 00:04:21,919
they're two different functions but it

120
00:04:19,600 --> 00:04:23,759
could just be something like a gpu that

121
00:04:21,919 --> 00:04:26,080
has you know only one purpose really

122
00:04:23,759 --> 00:04:27,680
being a gpu but it could still expose

123
00:04:26,080 --> 00:04:30,160
multiple different functionalities

124
00:04:27,680 --> 00:04:32,880
through different pci functions and then

125
00:04:30,160 --> 00:04:35,280
ultimately these multiple pci buses are

126
00:04:32,880 --> 00:04:37,360
connected via bridges

127
00:04:35,280 --> 00:04:39,520
these are how one device on one bus

128
00:04:37,360 --> 00:04:41,280
talks to other devices on other buses

129
00:04:39,520 --> 00:04:43,919
for instance the cpu which will

130
00:04:41,280 --> 00:04:46,080
frequently be up on bus 0 talks to

131
00:04:43,919 --> 00:04:48,160
something like the nominal wi-fi

132
00:04:46,080 --> 00:04:51,840
bluetooth card

133
00:04:48,160 --> 00:04:54,240
so importantly on intel systems bus 0 is

134
00:04:51,840 --> 00:04:55,600
always reserved for intel themselves and

135
00:04:54,240 --> 00:04:57,199
so that's where you're going to find a

136
00:04:55,600 --> 00:04:58,880
lot of the interesting bits that we care

137
00:04:57,199 --> 00:05:00,800
about and that we've made oblique

138
00:04:58,880 --> 00:05:02,639
references to previously in the class

139
00:05:00,800 --> 00:05:04,080
before you fully understood it well in

140
00:05:02,639 --> 00:05:06,320
this section you'll start to fully

141
00:05:04,080 --> 00:05:08,400
understand why things like the dram

142
00:05:06,320 --> 00:05:10,800
controller are on bus 0 device 0

143
00:05:08,400 --> 00:05:14,000
function 0 and why things like the lpc

144
00:05:10,800 --> 00:05:16,320
device are on bus 0 device 31 function

145
00:05:14,000 --> 00:05:16,320
0.


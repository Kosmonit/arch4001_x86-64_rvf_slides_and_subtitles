1
00:00:00,080 --> 00:00:03,840
okay here's an example of what messing

2
00:00:02,240 --> 00:00:07,440
with the bars could look like on the

3
00:00:03,840 --> 00:00:08,800
dell opt flex 7010 so if we go into read

4
00:00:07,440 --> 00:00:10,480
write everything you could do this from

5
00:00:08,800 --> 00:00:12,639
chipset too but i'm just doing it from

6
00:00:10,480 --> 00:00:14,320
reardot everything you go in you look at

7
00:00:12,639 --> 00:00:16,480
buzz zero device zero function zero and

8
00:00:14,320 --> 00:00:18,000
you go to offset 10 and it doesn't look

9
00:00:16,480 --> 00:00:20,720
like there's any bars there nothing's

10
00:00:18,000 --> 00:00:23,279
filled in so you move down to the vga

11
00:00:20,720 --> 00:00:25,039
controller and here we do some see

12
00:00:23,279 --> 00:00:27,840
something at offset 10 and we see

13
00:00:25,039 --> 00:00:30,480
something also at offset 18. so there's

14
00:00:27,840 --> 00:00:33,120
this kind of gap between them so if we

15
00:00:30,480 --> 00:00:35,200
parse to this bottom bits of this we

16
00:00:33,120 --> 00:00:37,360
would go back to this and we'd say if

17
00:00:35,200 --> 00:00:40,399
the least significant nibble is 4 then

18
00:00:37,360 --> 00:00:42,960
that would be 0 1 0 0

19
00:00:40,399 --> 00:00:44,559
and so 1 0 here which is that thing

20
00:00:42,960 --> 00:00:46,399
where it says it can be located in the

21
00:00:44,559 --> 00:00:48,719
64-bit address space

22
00:00:46,399 --> 00:00:50,559
so it's basically this is a 64-bit bar

23
00:00:48,719 --> 00:00:52,800
of course the upper bits are all just

24
00:00:50,559 --> 00:00:54,640
zero so it's still in the 32-bit address

25
00:00:52,800 --> 00:00:57,920
space but they're saying you know this

26
00:00:54,640 --> 00:01:03,039
is a 64-bit bar so 64-bit bar there

27
00:00:57,920 --> 00:01:05,360
64-bit bar here because 1 1 0 0

28
00:01:03,039 --> 00:01:07,920
is c and that still has the 64-bit

29
00:01:05,360 --> 00:01:10,320
designator and then interestingly down

30
00:01:07,920 --> 00:01:12,320
here we have what looks like a port i o

31
00:01:10,320 --> 00:01:14,799
bar because the least significant bit is

32
00:01:12,320 --> 00:01:17,520
one and when the least significant bit

33
00:01:14,799 --> 00:01:19,360
is one that indicates it's an i o space

34
00:01:17,520 --> 00:01:21,280
so basically just the bottom two bits

35
00:01:19,360 --> 00:01:23,280
are unused but otherwise this is saying

36
00:01:21,280 --> 00:01:25,520
you know this is in the port i o range

37
00:01:23,280 --> 00:01:28,000
at f zero zero zero

38
00:01:25,520 --> 00:01:30,159
well i don't want to mess up my video so

39
00:01:28,000 --> 00:01:32,400
previously i had said if you're

40
00:01:30,159 --> 00:01:35,040
rdp'ed in as i am currently on this

41
00:01:32,400 --> 00:01:36,720
system you could mess with your usb

42
00:01:35,040 --> 00:01:39,040
safely and then just you know restart

43
00:01:36,720 --> 00:01:41,119
the system but as i was digging around

44
00:01:39,040 --> 00:01:43,040
here i noticed that the serial device

45
00:01:41,119 --> 00:01:45,439
actually is a two for one in that it has

46
00:01:43,040 --> 00:01:48,399
a bar in the i o address space least

47
00:01:45,439 --> 00:01:51,119
significant bit is one and it has a bar

48
00:01:48,399 --> 00:01:54,240
in the memory space the significant bit

49
00:01:51,119 --> 00:01:57,520
is zero and also it's not four so it's

50
00:01:54,240 --> 00:01:59,280
zero zero for the saying it's a 32-bit

51
00:01:57,520 --> 00:02:01,360
address space thing

52
00:01:59,280 --> 00:02:03,840
so let's go ahead and mess with these

53
00:02:01,360 --> 00:02:05,840
bars so i said grab the

54
00:02:03,840 --> 00:02:09,280
existing value out of there

55
00:02:05,840 --> 00:02:12,800
and so that is f0 f0e1

56
00:02:09,280 --> 00:02:14,480
then i'm going to edit it oops i want to

57
00:02:12,800 --> 00:02:16,160
right click and edit it i know there's a

58
00:02:14,480 --> 00:02:17,920
way to like just edit it directly but i

59
00:02:16,160 --> 00:02:19,680
think it has something to do with rdp

60
00:02:17,920 --> 00:02:21,360
and the fact that i'm on mac

61
00:02:19,680 --> 00:02:22,800
it's not just doing the nice easy

62
00:02:21,360 --> 00:02:24,319
editing so instead i'm going to do it

63
00:02:22,800 --> 00:02:28,319
the hard way you right click and you hit

64
00:02:24,319 --> 00:02:30,480
edit and i'm going to put in all f's

65
00:02:28,319 --> 00:02:32,160
for this i'm going to hit done

66
00:02:30,480 --> 00:02:34,000
rewrite everything is going to read back

67
00:02:32,160 --> 00:02:37,920
the value and then it sees that it's set

68
00:02:34,000 --> 00:02:40,080
to 0 0 zero f f nine now theoretically

69
00:02:37,920 --> 00:02:41,840
you would you know just invert that and

70
00:02:40,080 --> 00:02:43,840
stuff but because the i o address space

71
00:02:41,840 --> 00:02:46,800
is only you know sixty five thousand so

72
00:02:43,840 --> 00:02:48,080
it can only be you know zero to f f f so

73
00:02:46,800 --> 00:02:51,840
we're only going to care about these

74
00:02:48,080 --> 00:02:54,400
bottom uh four nibbles so what is that

75
00:02:51,840 --> 00:02:55,599
so we got back one two three f's and a

76
00:02:54,400 --> 00:02:58,400
nine

77
00:02:55,599 --> 00:02:59,599
what is nine in binary it is one zero

78
00:02:58,400 --> 00:03:02,080
zero one

79
00:02:59,599 --> 00:03:06,560
so if we invert all of these we would

80
00:03:02,080 --> 00:03:09,360
get you know 0 1 1 0 which is 6

81
00:03:06,560 --> 00:03:13,120
and the most significant these f's turn

82
00:03:09,360 --> 00:03:15,519
into 0. so 1 2 3. so 0 0 6. that's the

83
00:03:13,120 --> 00:03:18,800
two's complement of f sorry that's the

84
00:03:15,519 --> 00:03:21,360
ones complement of f fff9 and to get the

85
00:03:18,800 --> 00:03:23,760
two's complement we add one so seven

86
00:03:21,360 --> 00:03:27,599
this is telling us that this is an i o

87
00:03:23,760 --> 00:03:29,599
address range and it has a size of seven

88
00:03:27,599 --> 00:03:35,360
all right let's look at the next one

89
00:03:29,599 --> 00:03:38,959
grab the value f 7 c 3 a 1 2 3.

90
00:03:35,360 --> 00:03:41,280
let's write all ones into there

91
00:03:38,959 --> 00:03:42,080
like so

92
00:03:41,280 --> 00:03:42,959
do

93
00:03:42,080 --> 00:03:44,840
f

94
00:03:42,959 --> 00:03:47,360
f

95
00:03:44,840 --> 00:03:49,599
f hit done

96
00:03:47,360 --> 00:03:51,680
it writes it in and rewrite everything

97
00:03:49,599 --> 00:03:53,920
reads it right back to refresh this

98
00:03:51,680 --> 00:03:56,000
you've got the refreshing going on and

99
00:03:53,920 --> 00:03:59,760
so it says basically

100
00:03:56,000 --> 00:04:02,720
five f's and three zeros so the two's

101
00:03:59,760 --> 00:04:05,200
complement of that is easy

102
00:04:02,720 --> 00:04:07,200
three four five one two three

103
00:04:05,200 --> 00:04:08,319
two's complement of that would be five

104
00:04:07,200 --> 00:04:09,439
zeros

105
00:04:08,319 --> 00:04:10,799
and

106
00:04:09,439 --> 00:04:13,439
three f's

107
00:04:10,799 --> 00:04:16,079
which when you add one to that

108
00:04:13,439 --> 00:04:17,680
you get hex one thousand so this is

109
00:04:16,079 --> 00:04:21,359
basically telling us this is a memory

110
00:04:17,680 --> 00:04:23,199
bar and the size is hex one thousand

111
00:04:21,359 --> 00:04:24,880
so that's the basics you know i said to

112
00:04:23,199 --> 00:04:26,960
put the values back in and maybe things

113
00:04:24,880 --> 00:04:29,520
would start working you know they hadn't

114
00:04:26,960 --> 00:04:31,040
in my experience but maybe they would so

115
00:04:29,520 --> 00:04:33,040
you can go ahead and put the you know

116
00:04:31,040 --> 00:04:34,720
original values back in but at this

117
00:04:33,040 --> 00:04:36,800
point if there were any drivers behind

118
00:04:34,720 --> 00:04:39,040
the scenes the fact that drivers

119
00:04:36,800 --> 00:04:41,600
accessing it behind the scenes the fact

120
00:04:39,040 --> 00:04:43,040
that we are you know in human level of

121
00:04:41,600 --> 00:04:45,199
timing

122
00:04:43,040 --> 00:04:47,440
just completely smashing these bass

123
00:04:45,199 --> 00:04:48,880
addresses and breaking the decoding

124
00:04:47,440 --> 00:04:52,160
means that the drivers would almost

125
00:04:48,880 --> 00:04:52,160
certainly have broken by now


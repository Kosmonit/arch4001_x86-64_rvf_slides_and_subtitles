1
00:00:00,240 --> 00:00:05,920
okay so what is a pci option rom well

2
00:00:03,600 --> 00:00:09,440
it's basically a blob of arbitrary

3
00:00:05,920 --> 00:00:12,000
machine code so x86 code that the bios

4
00:00:09,440 --> 00:00:14,559
just reads in and runs so yeah you can

5
00:00:12,000 --> 00:00:17,039
see how that could be a security problem

6
00:00:14,559 --> 00:00:19,439
so not every pci device will necessarily

7
00:00:17,039 --> 00:00:21,119
have an option rom it typically has to

8
00:00:19,439 --> 00:00:23,279
do with whether or not the device needs

9
00:00:21,119 --> 00:00:24,960
to do something at boot time and whether

10
00:00:23,279 --> 00:00:26,960
or not the device follows some sort of

11
00:00:24,960 --> 00:00:29,039
standard so you can imagine that things

12
00:00:26,960 --> 00:00:31,439
like graphic cards need to display

13
00:00:29,039 --> 00:00:33,680
graphics at boot time network cards need

14
00:00:31,439 --> 00:00:35,520
to support network boot and things like

15
00:00:33,680 --> 00:00:37,040
hardware raid controllers need to be

16
00:00:35,520 --> 00:00:38,320
available so that you can actually boot

17
00:00:37,040 --> 00:00:39,840
off of the hard drives that they're

18
00:00:38,320 --> 00:00:42,000
managing

19
00:00:39,840 --> 00:00:44,160
so if option roms are just some

20
00:00:42,000 --> 00:00:46,320
arbitrary machine code and if they're

21
00:00:44,160 --> 00:00:48,320
executed by the bios you can see that

22
00:00:46,320 --> 00:00:49,600
they have a very high privilege level

23
00:00:48,320 --> 00:00:51,520
indeed

24
00:00:49,600 --> 00:00:54,320
now returning to this picture from

25
00:00:51,520 --> 00:00:56,800
before i didn't plan this but if we

26
00:00:54,320 --> 00:00:59,199
actually zoom in on the particular nic

27
00:00:56,800 --> 00:01:00,719
card that i was using as my diagram we

28
00:00:59,199 --> 00:01:01,920
have to ask the question

29
00:01:00,719 --> 00:01:04,640
what's that

30
00:01:01,920 --> 00:01:06,479
that looks like a non-volatile storage

31
00:01:04,640 --> 00:01:08,400
chip a spy flash chip

32
00:01:06,479 --> 00:01:10,400
like this kind of thing that we've been

33
00:01:08,400 --> 00:01:13,040
seeing throughout and we previously were

34
00:01:10,400 --> 00:01:15,840
using this spy flash chip to refer to

35
00:01:13,040 --> 00:01:17,920
the main board bios but such a chip

36
00:01:15,840 --> 00:01:19,600
could exist on your pci peripheral and

37
00:01:17,920 --> 00:01:22,720
it's basically just hanging around

38
00:01:19,600 --> 00:01:23,600
saying run me you know bios run me run

39
00:01:22,720 --> 00:01:25,520
me

40
00:01:23,600 --> 00:01:28,000
and it just wants arbitrary code to be

41
00:01:25,520 --> 00:01:29,600
executed in the context of a bios so

42
00:01:28,000 --> 00:01:31,439
yeah arbitrary code in the context of

43
00:01:29,600 --> 00:01:34,000
the bios is a powerful and privileged

44
00:01:31,439 --> 00:01:36,000
thing they who run first run best on

45
00:01:34,000 --> 00:01:37,759
most systems if it's allowed to run it's

46
00:01:36,000 --> 00:01:40,000
going to be running with effectively

47
00:01:37,759 --> 00:01:42,159
ring zero privileges full and arbitrary

48
00:01:40,000 --> 00:01:43,920
control over the system and so it can do

49
00:01:42,159 --> 00:01:46,799
a lot of damage there's no effective

50
00:01:43,920 --> 00:01:48,799
difference between pci and pcie in terms

51
00:01:46,799 --> 00:01:50,399
of how they behave with respect to

52
00:01:48,799 --> 00:01:52,880
option roms

53
00:01:50,399 --> 00:01:55,119
and in both cases they're managed by a

54
00:01:52,880 --> 00:01:57,119
separate bar base address register

55
00:01:55,119 --> 00:01:59,119
called the expansion rom bar

56
00:01:57,119 --> 00:02:01,600
now these are frequently called

57
00:01:59,119 --> 00:02:03,280
expansion roms or option roms and the

58
00:02:01,600 --> 00:02:05,360
original notion was that it's optional

59
00:02:03,280 --> 00:02:07,119
that a particular hardware piece didn't

60
00:02:05,360 --> 00:02:09,119
actually need one of these flash chips

61
00:02:07,119 --> 00:02:10,239
attached to them but they could use it

62
00:02:09,119 --> 00:02:12,400
if they wanted

63
00:02:10,239 --> 00:02:13,760
and the expansion portion refers to the

64
00:02:12,400 --> 00:02:16,000
fact that this is kind of like an

65
00:02:13,760 --> 00:02:18,319
expansion of functionality so different

66
00:02:16,000 --> 00:02:21,040
chunks of the pci documentation refer to

67
00:02:18,319 --> 00:02:22,800
it as either i personally see the use of

68
00:02:21,040 --> 00:02:24,319
option rom more frequently than

69
00:02:22,800 --> 00:02:26,000
expansion rom but i'm going to have to

70
00:02:24,319 --> 00:02:28,160
use both of them interchangeably

71
00:02:26,000 --> 00:02:30,319
throughout this class so if we return to

72
00:02:28,160 --> 00:02:32,720
the pci standardized header we'll see

73
00:02:30,319 --> 00:02:34,800
that at offset hex 30 there is something

74
00:02:32,720 --> 00:02:36,400
called the expansion rom base address

75
00:02:34,800 --> 00:02:37,920
and this is a base address in the same

76
00:02:36,400 --> 00:02:39,920
form as the bars that we learned about

77
00:02:37,920 --> 00:02:43,519
before and it's managed much the same

78
00:02:39,920 --> 00:02:45,680
way but basically the bios will set this

79
00:02:43,519 --> 00:02:47,440
up to allow for a memory mapped i o and

80
00:02:45,680 --> 00:02:49,599
then it will actually read the contents

81
00:02:47,440 --> 00:02:52,480
of that memory mapped i o and copy it to

82
00:02:49,599 --> 00:02:54,319
ram and then execute it from ram instead

83
00:02:52,480 --> 00:02:55,760
so this is the format of the expansion

84
00:02:54,319 --> 00:02:57,760
rom base address and it's slightly

85
00:02:55,760 --> 00:02:59,760
different from the other normal base

86
00:02:57,760 --> 00:03:02,000
addresses that we saw before

87
00:02:59,760 --> 00:03:03,680
that the least significant bit is the

88
00:03:02,000 --> 00:03:05,680
enable bit which is just saying if this

89
00:03:03,680 --> 00:03:07,840
is used you can see here that it's

90
00:03:05,680 --> 00:03:09,440
actually reserving 11 bits rather than

91
00:03:07,840 --> 00:03:11,440
you know the four bits in the case of

92
00:03:09,440 --> 00:03:14,000
normal bars or two bits in the case of

93
00:03:11,440 --> 00:03:15,920
port io bars and so then the upper 21

94
00:03:14,000 --> 00:03:18,400
bits are used to specify the actual

95
00:03:15,920 --> 00:03:21,360
memory mapped base and so this is read

96
00:03:18,400 --> 00:03:23,680
and written just like normal bars

97
00:03:21,360 --> 00:03:27,360
and also again there's that command

98
00:03:23,680 --> 00:03:29,920
register in the pci header and it is

99
00:03:27,360 --> 00:03:31,920
only when this thing is set to memory

100
00:03:29,920 --> 00:03:34,319
spaces enabled and when the least

101
00:03:31,920 --> 00:03:37,360
significant bit is enabled that the

102
00:03:34,319 --> 00:03:39,760
option rom will actually be enabled

103
00:03:37,360 --> 00:03:42,560
so as with normal bars the bios will

104
00:03:39,760 --> 00:03:44,480
write all ones into these 21 bits

105
00:03:42,560 --> 00:03:46,080
and then if it reads back something

106
00:03:44,480 --> 00:03:47,840
other than all ones that means the

107
00:03:46,080 --> 00:03:49,519
hardware is saying yes i support an

108
00:03:47,840 --> 00:03:51,840
option rom and yes i'd like you to map

109
00:03:49,519 --> 00:03:53,439
it into memory for me and here are the

110
00:03:51,840 --> 00:03:55,599
bits that are available for the address

111
00:03:53,439 --> 00:03:57,280
to specify the size so you can think of

112
00:03:55,599 --> 00:03:59,200
those again as the don't care bits or

113
00:03:57,280 --> 00:04:01,120
you can just run a two's complement on

114
00:03:59,200 --> 00:04:04,319
the return value so in this case it

115
00:04:01,120 --> 00:04:06,560
returned fffe0000

116
00:04:04,319 --> 00:04:08,480
so you flip the bits and add one and

117
00:04:06,560 --> 00:04:10,239
it's basically saying it's this size

118
00:04:08,480 --> 00:04:12,000
worth of space

119
00:04:10,239 --> 00:04:13,680
now in terms of the actual base address

120
00:04:12,000 --> 00:04:15,760
where this can then be mapped like when

121
00:04:13,680 --> 00:04:17,680
you put in a real base address here it's

122
00:04:15,760 --> 00:04:20,560
of course going to have to be 128

123
00:04:17,680 --> 00:04:23,120
kilobyte aligned because these 11 bits

124
00:04:20,560 --> 00:04:24,960
are all expected to be zero so you know

125
00:04:23,120 --> 00:04:27,440
it has to start with you know zero zero

126
00:04:24,960 --> 00:04:29,120
zero two zero zero four zero zero et

127
00:04:27,440 --> 00:04:31,120
cetera

128
00:04:29,120 --> 00:04:33,840
so then the bios just puts some

129
00:04:31,120 --> 00:04:35,680
particular location into this address

130
00:04:33,840 --> 00:04:37,440
again the bios is responsible for

131
00:04:35,680 --> 00:04:39,199
maintaining the memory map and it has to

132
00:04:37,440 --> 00:04:41,280
keep track of you know what's where all

133
00:04:39,199 --> 00:04:42,880
over the system so it puts an address in

134
00:04:41,280 --> 00:04:44,800
there that it knows no one else is using

135
00:04:42,880 --> 00:04:46,240
the physical address space for and then

136
00:04:44,800 --> 00:04:47,840
it enables it

137
00:04:46,240 --> 00:04:50,160
so we're returning once again to this

138
00:04:47,840 --> 00:04:53,919
picture and we expand out that memory

139
00:04:50,160 --> 00:04:56,320
mapped i o space we have the pci nic but

140
00:04:53,919 --> 00:04:57,680
now we know that we can add the flash

141
00:04:56,320 --> 00:05:00,320
chip to it

142
00:04:57,680 --> 00:05:02,080
and basically if miss frizzle pokes some

143
00:05:00,320 --> 00:05:04,479
of these registers specifically the

144
00:05:02,080 --> 00:05:08,560
expansion rom base address register in

145
00:05:04,479 --> 00:05:10,560
the 256 bytes of memory mapped io

146
00:05:08,560 --> 00:05:13,120
or port i o i suppose

147
00:05:10,560 --> 00:05:14,320
then the orom base address register can

148
00:05:13,120 --> 00:05:16,320
be filled in

149
00:05:14,320 --> 00:05:18,720
with a particular physical address that

150
00:05:16,320 --> 00:05:21,360
will be used for the memory mapped i o

151
00:05:18,720 --> 00:05:23,520
of this bar only memory maps i o no port

152
00:05:21,360 --> 00:05:25,759
io access to option roms

153
00:05:23,520 --> 00:05:27,919
and so what that does is that if you

154
00:05:25,759 --> 00:05:30,240
know there was some sort of read or

155
00:05:27,919 --> 00:05:31,919
write from this memory mapped io region

156
00:05:30,240 --> 00:05:34,720
it's actually reading and writing from

157
00:05:31,919 --> 00:05:36,080
the flash chip on the peripheral device

158
00:05:34,720 --> 00:05:38,240
so what's typically going to happen is

159
00:05:36,080 --> 00:05:41,360
then the bios is going to copy that

160
00:05:38,240 --> 00:05:42,960
information down into ram using a mem

161
00:05:41,360 --> 00:05:45,600
copy effectively

162
00:05:42,960 --> 00:05:46,810
and then once there's a copy in ram

163
00:05:45,600 --> 00:05:49,879
basically

164
00:05:46,810 --> 00:05:49,879
[Music]

165
00:05:51,440 --> 00:05:56,240
and it's going to call into that

166
00:05:53,680 --> 00:05:58,080
particular ram copy of the option wrong

167
00:05:56,240 --> 00:05:59,759
in order to execute the arbitrary code

168
00:05:58,080 --> 00:06:01,759
that potentially an attacker has

169
00:05:59,759 --> 00:06:04,000
installed into this external peripheral

170
00:06:01,759 --> 00:06:05,919
device in the context of the bios

171
00:06:04,000 --> 00:06:08,240
allowing it to infect anything else

172
00:06:05,919 --> 00:06:09,840
later on in the system so that's not a

173
00:06:08,240 --> 00:06:12,000
good time all right once there is a

174
00:06:09,840 --> 00:06:13,680
memory mapped copy the bios is not

175
00:06:12,000 --> 00:06:15,280
actually supposed to jump into it unless

176
00:06:13,680 --> 00:06:17,120
it does a little bit of sanity checking

177
00:06:15,280 --> 00:06:19,680
of data structures that are expected to

178
00:06:17,120 --> 00:06:21,759
be at the beginning specifically the

179
00:06:19,680 --> 00:06:24,800
zeroth byte is expected to be five five

180
00:06:21,759 --> 00:06:26,720
the one byte is expected to be a a

181
00:06:24,800 --> 00:06:28,960
and so if that's not the case then this

182
00:06:26,720 --> 00:06:30,800
means it's not treated like a normal

183
00:06:28,960 --> 00:06:32,000
proper valid option rom and they

184
00:06:30,800 --> 00:06:34,319
shouldn't be jumping into the code

185
00:06:32,000 --> 00:06:36,880
anywhere but if it is the case then at

186
00:06:34,319 --> 00:06:38,560
offset 3 there is an entry point that

187
00:06:36,880 --> 00:06:40,720
says where specifically in this memory

188
00:06:38,560 --> 00:06:43,120
mapped region it should jump in order to

189
00:06:40,720 --> 00:06:45,280
execute the code so overall you should

190
00:06:43,120 --> 00:06:47,520
think of option roms like they're

191
00:06:45,280 --> 00:06:49,599
effectively a device driver for a

192
00:06:47,520 --> 00:06:51,599
particular device that needs to execute

193
00:06:49,599 --> 00:06:54,000
in the context of a bios it contains

194
00:06:51,599 --> 00:06:56,400
arbitrary code because any given bios

195
00:06:54,000 --> 00:06:58,960
when confronted with the myriad of

196
00:06:56,400 --> 00:07:01,360
possible pci peripherals may not have a

197
00:06:58,960 --> 00:07:03,759
driver built into it and therefore it's

198
00:07:01,360 --> 00:07:05,840
expected to basically grab a driver off

199
00:07:03,759 --> 00:07:07,759
of the device and execute that driver

200
00:07:05,840 --> 00:07:09,440
which will subsequently you know

201
00:07:07,759 --> 00:07:10,800
initialize all of the rest of the

202
00:07:09,440 --> 00:07:12,560
registers on that particular piece of

203
00:07:10,800 --> 00:07:14,720
hardware and make it available for

204
00:07:12,560 --> 00:07:16,960
running and being a useful device so

205
00:07:14,720 --> 00:07:18,639
that's it for optionram attacks and now

206
00:07:16,960 --> 00:07:20,400
you can see a whole bunch of other

207
00:07:18,639 --> 00:07:22,160
research that you can dig into and

208
00:07:20,400 --> 00:07:24,240
understand what exactly people did with

209
00:07:22,160 --> 00:07:26,080
optionramps but it really comes down to

210
00:07:24,240 --> 00:07:28,639
you know how did you actually infect it

211
00:07:26,080 --> 00:07:30,880
in the first place and what mechanisms

212
00:07:28,639 --> 00:07:32,960
if any does the operating system have to

213
00:07:30,880 --> 00:07:34,479
sort of constrain or restrict the

214
00:07:32,960 --> 00:07:36,240
introduction of malicious code via

215
00:07:34,479 --> 00:07:38,479
option roms there's multiple different

216
00:07:36,240 --> 00:07:40,400
approaches to that and things like uefi

217
00:07:38,479 --> 00:07:42,160
secure boot systems are supposed to use

218
00:07:40,400 --> 00:07:44,479
things like digital signatures over the

219
00:07:42,160 --> 00:07:46,479
option roms so that attacker can't just

220
00:07:44,479 --> 00:07:48,960
arbitrarily update that code unless they

221
00:07:46,479 --> 00:07:50,800
can also forge a digital signature but

222
00:07:48,960 --> 00:07:52,879
unfortunately a lot of systems don't

223
00:07:50,800 --> 00:07:54,960
actually have signed option roms and

224
00:07:52,879 --> 00:07:56,879
bios is because the customers expect

225
00:07:54,960 --> 00:07:58,800
their devices to work typically we'll

226
00:07:56,879 --> 00:08:00,639
either you know allow for disabling of

227
00:07:58,800 --> 00:08:02,000
those checks or not do the check at all

228
00:08:00,639 --> 00:08:04,319
in the first place

229
00:08:02,000 --> 00:08:06,560
other systems like max systems after

230
00:08:04,319 --> 00:08:08,400
corey kallenberg made his changes do

231
00:08:06,560 --> 00:08:10,160
things like jailing the option roms so

232
00:08:08,400 --> 00:08:12,319
that they're not effectively ring zero

233
00:08:10,160 --> 00:08:14,639
so they're significantly de-privileged

234
00:08:12,319 --> 00:08:16,160
or you can have user opt-in mechanisms

235
00:08:14,639 --> 00:08:18,000
like firmware password that basically

236
00:08:16,160 --> 00:08:19,919
say if this is set just don't run any

237
00:08:18,000 --> 00:08:22,800
option roms i know that those could be

238
00:08:19,919 --> 00:08:22,800
used to attack me


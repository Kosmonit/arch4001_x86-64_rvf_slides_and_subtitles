1
00:00:00,240 --> 00:00:05,440
okay when it comes to addressing a

2
00:00:02,879 --> 00:00:07,600
particular area of the pci configuration

3
00:00:05,440 --> 00:00:10,559
address space we're going to use a term

4
00:00:07,600 --> 00:00:13,280
called bdfo so that stands for bus

5
00:00:10,559 --> 00:00:15,519
device function and offset within the

6
00:00:13,280 --> 00:00:18,160
configuration address space you'll often

7
00:00:15,519 --> 00:00:20,240
see this written as b colon d colon f

8
00:00:18,160 --> 00:00:22,560
and then maybe a comma and an offset but

9
00:00:20,240 --> 00:00:26,960
you can also see it as b slash d slash f

10
00:00:22,560 --> 00:00:29,359
comma offset b colon d period f offset

11
00:00:26,960 --> 00:00:31,199
that's usually more used on unix c type

12
00:00:29,359 --> 00:00:32,719
systems

13
00:00:31,199 --> 00:00:34,480
now as i've already said a couple of

14
00:00:32,719 --> 00:00:36,320
times you can get to the configuration

15
00:00:34,480 --> 00:00:38,480
address space with both port i o and

16
00:00:36,320 --> 00:00:40,399
memory mapped i o and for the extended

17
00:00:38,480 --> 00:00:42,960
address space you must use memory mapped

18
00:00:40,399 --> 00:00:45,039
io we're going to focus on port io for

19
00:00:42,960 --> 00:00:47,920
now because again that's what we saw in

20
00:00:45,039 --> 00:00:49,600
the initial bios you know within the few

21
00:00:47,920 --> 00:00:51,600
assembly instructions you started seeing

22
00:00:49,600 --> 00:00:53,680
port io accessing this config address

23
00:00:51,600 --> 00:00:55,120
space and once we cover all of that

24
00:00:53,680 --> 00:00:57,520
we'll come back towards the end of the

25
00:00:55,120 --> 00:00:59,440
pcie section to talk about how the

26
00:00:57,520 --> 00:01:01,520
memory map diode works because you need

27
00:00:59,440 --> 00:01:03,680
to use port io to set up the memory map

28
00:01:01,520 --> 00:01:06,320
diode

29
00:01:03,680 --> 00:01:09,280
now access via port i o is going to be

30
00:01:06,320 --> 00:01:12,240
in address data style access so that was

31
00:01:09,280 --> 00:01:14,159
the pair of ports and specifically those

32
00:01:12,240 --> 00:01:17,759
pair of ports are called config address

33
00:01:14,159 --> 00:01:19,520
and config data cf-8 and cfc

34
00:01:17,759 --> 00:01:22,560
interestingly despite the fact that it's

35
00:01:19,520 --> 00:01:24,799
always cf8 and cfc on intel systems they

36
00:01:22,560 --> 00:01:26,720
aren't actually listed in the fixed i o

37
00:01:24,799 --> 00:01:28,400
address space registers data sheet

38
00:01:26,720 --> 00:01:30,400
location

39
00:01:28,400 --> 00:01:32,799
now so let's imagine that someone is

40
00:01:30,400 --> 00:01:34,159
using an out assembly instruction and

41
00:01:32,799 --> 00:01:36,479
they're gonna you know have the

42
00:01:34,159 --> 00:01:39,439
instruction decoded memory controller

43
00:01:36,479 --> 00:01:42,079
and let's say they're outing to cf eight

44
00:01:39,439 --> 00:01:44,720
that's going to select some particular

45
00:01:42,079 --> 00:01:45,600
address inside of the config address

46
00:01:44,720 --> 00:01:47,439
space

47
00:01:45,600 --> 00:01:50,000
so for instance could be you know bus

48
00:01:47,439 --> 00:01:51,119
device function zero zero zero offset

49
00:01:50,000 --> 00:01:54,079
four

50
00:01:51,119 --> 00:01:56,640
then if another out assembly instruction

51
00:01:54,079 --> 00:01:58,960
is targeted at cfc

52
00:01:56,640 --> 00:02:00,960
that's going to be writing out some data

53
00:01:58,960 --> 00:02:03,520
specifically to whatever bus device

54
00:02:00,960 --> 00:02:05,360
function offset was selected by the

55
00:02:03,520 --> 00:02:07,520
write to cf-8

56
00:02:05,360 --> 00:02:10,080
so that writes the data actually into

57
00:02:07,520 --> 00:02:12,400
the register stored behind the scenes

58
00:02:10,080 --> 00:02:13,920
likewise an assembly instruction of in

59
00:02:12,400 --> 00:02:16,560
after it's already been properly

60
00:02:13,920 --> 00:02:20,080
configured in the cf-8 register would

61
00:02:16,560 --> 00:02:23,040
allow reading in the data via a in of

62
00:02:20,080 --> 00:02:24,560
cfc from that particular offset and that

63
00:02:23,040 --> 00:02:26,239
gives you back the data that was stored

64
00:02:24,560 --> 00:02:29,280
in that particular register

65
00:02:26,239 --> 00:02:31,360
now when it comes to config data or cfc

66
00:02:29,280 --> 00:02:33,920
interpretation of that port is super

67
00:02:31,360 --> 00:02:36,800
easy because it's basically just 32 bits

68
00:02:33,920 --> 00:02:39,680
of data to send or receive

69
00:02:36,800 --> 00:02:42,160
on the other hand config address cf-8

70
00:02:39,680 --> 00:02:44,000
has this particular parsing that you

71
00:02:42,160 --> 00:02:44,800
have to understand

72
00:02:44,000 --> 00:02:46,640
so

73
00:02:44,800 --> 00:02:48,879
let's you know this is a bit more tricky

74
00:02:46,640 --> 00:02:50,720
let's focus on you know from left to

75
00:02:48,879 --> 00:02:52,959
right what do these bits do

76
00:02:50,720 --> 00:02:55,599
starting with bit 31

77
00:02:52,959 --> 00:02:57,440
if bit 31 is set then that means that

78
00:02:55,599 --> 00:03:00,080
this is actually enabled and this is

79
00:02:57,440 --> 00:03:02,560
going to cause port io to write to the

80
00:03:00,080 --> 00:03:04,239
pci configuration address space it's

81
00:03:02,560 --> 00:03:05,840
basically telling the you know hardware

82
00:03:04,239 --> 00:03:08,159
behind the scenes hey turn this into a

83
00:03:05,840 --> 00:03:10,319
pcie transaction

84
00:03:08,159 --> 00:03:13,200
next are some number of reserved bits

85
00:03:10,319 --> 00:03:15,440
which must always return zero

86
00:03:13,200 --> 00:03:20,080
then we have the bus number and we said

87
00:03:15,440 --> 00:03:22,640
there can be up to 256 buses in pci and

88
00:03:20,080 --> 00:03:26,080
that's why you have eight bits used for

89
00:03:22,640 --> 00:03:28,159
the bus number so two to the eight 256

90
00:03:26,080 --> 00:03:29,360
that specifies the bus that you want to

91
00:03:28,159 --> 00:03:31,840
access

92
00:03:29,360 --> 00:03:34,879
then device we said there can be 32

93
00:03:31,840 --> 00:03:37,920
devices which is 2 to the 5 so 5 bits

94
00:03:34,879 --> 00:03:40,319
bit 11 12 13 14 15

95
00:03:37,920 --> 00:03:41,840
5 bits for the device number of what you

96
00:03:40,319 --> 00:03:43,760
want to access

97
00:03:41,840 --> 00:03:46,159
and then three bits for the function

98
00:03:43,760 --> 00:03:48,879
number two to the three is eight eight

99
00:03:46,159 --> 00:03:52,640
functions 256 so you gotta have enough

100
00:03:48,879 --> 00:03:56,080
eight bits to index into 256 buses five

101
00:03:52,640 --> 00:03:59,200
bits to index into 32 devices three bits

102
00:03:56,080 --> 00:04:01,120
to index into eight functions

103
00:03:59,200 --> 00:04:04,400
and then after that that's the bdf here

104
00:04:01,120 --> 00:04:06,720
comes the o the offset then into the pci

105
00:04:04,400 --> 00:04:09,519
config address space and you can see the

106
00:04:06,720 --> 00:04:11,439
least significant two bits must be zero

107
00:04:09,519 --> 00:04:13,120
which means this must be a four byte

108
00:04:11,439 --> 00:04:16,799
aligned address you can only have you

109
00:04:13,120 --> 00:04:18,959
know one zero zero zero one zero zero so

110
00:04:16,799 --> 00:04:20,799
it's going to be you know four or eight

111
00:04:18,959 --> 00:04:23,840
etcetera so based on the data that's

112
00:04:20,799 --> 00:04:26,160
written into cf8 register the hardware

113
00:04:23,840 --> 00:04:28,479
will parse the particular 32-bit value

114
00:04:26,160 --> 00:04:30,479
according to this interpretation and use

115
00:04:28,479 --> 00:04:32,080
that to route its pci transactions

116
00:04:30,479 --> 00:04:34,000
accordingly

117
00:04:32,080 --> 00:04:36,160
so now let's go back for a second and

118
00:04:34,000 --> 00:04:38,479
drill down a little bit more on the dell

119
00:04:36,160 --> 00:04:41,199
optiplex example so we saw before there

120
00:04:38,479 --> 00:04:43,440
was some port io pokes and address data

121
00:04:41,199 --> 00:04:46,479
style but let's just break it down to

122
00:04:43,440 --> 00:04:48,800
just the very first instance of writing

123
00:04:46,479 --> 00:04:49,600
to the pci config address space that we

124
00:04:48,800 --> 00:04:51,919
saw

125
00:04:49,600 --> 00:04:53,840
so those were the assembly instructions

126
00:04:51,919 --> 00:04:56,720
now let's do a deep dive and understand

127
00:04:53,840 --> 00:04:58,720
you know what exactly is going on here

128
00:04:56,720 --> 00:05:02,160
so starting with these three assembly

129
00:04:58,720 --> 00:05:04,720
instructions cf-8 into the dx register

130
00:05:02,160 --> 00:05:07,440
that is the output port

131
00:05:04,720 --> 00:05:10,000
some particular constant into the eax

132
00:05:07,440 --> 00:05:12,639
register that is the four bytes that are

133
00:05:10,000 --> 00:05:14,320
going to be written to cf eight and so

134
00:05:12,639 --> 00:05:16,080
we just talked about a second ago how is

135
00:05:14,320 --> 00:05:19,360
that interpreted so i want you to go

136
00:05:16,080 --> 00:05:21,680
ahead and clear your mind

137
00:05:19,360 --> 00:05:24,639
so how do we interpret that constant

138
00:05:21,680 --> 00:05:27,360
well here's our little table and if we

139
00:05:24,639 --> 00:05:30,160
wrote out this constant value with

140
00:05:27,360 --> 00:05:33,039
nibilized granularity of hex values

141
00:05:30,160 --> 00:05:35,759
then we changed it into binary

142
00:05:33,039 --> 00:05:38,000
grouped four at a time as normal now we

143
00:05:35,759 --> 00:05:40,960
regroup it according to this grouping up

144
00:05:38,000 --> 00:05:42,800
here so eight bits for an offset three

145
00:05:40,960 --> 00:05:45,680
bits for a function five bits for a

146
00:05:42,800 --> 00:05:48,080
device eight bits for a bus number so

147
00:05:45,680 --> 00:05:50,880
with that grouping what we come up with

148
00:05:48,080 --> 00:05:53,680
is that this is accessing b

149
00:05:50,880 --> 00:05:54,800
d f

150
00:05:53,680 --> 00:05:56,880
o

151
00:05:54,800 --> 00:05:58,000
of

152
00:05:56,880 --> 00:05:59,120
0

153
00:05:58,000 --> 00:06:00,160
31

154
00:05:59,120 --> 00:06:01,520
0

155
00:06:00,160 --> 00:06:05,680
and f

156
00:06:01,520 --> 00:06:09,199
0. so hex f 0. so bus 0 device 31

157
00:06:05,680 --> 00:06:11,840
function 0 offset f0

158
00:06:09,199 --> 00:06:14,800
all right so boom miss frizzle throws

159
00:06:11,840 --> 00:06:18,240
down some out assembly instructions

160
00:06:14,800 --> 00:06:20,479
and that particular bus 0 device 31

161
00:06:18,240 --> 00:06:23,520
function 0 offset f0

162
00:06:20,479 --> 00:06:26,319
is written into cf8 so now on the next

163
00:06:23,520 --> 00:06:27,840
read or write to cfc it is going to be

164
00:06:26,319 --> 00:06:29,520
pointing at this particular location

165
00:06:27,840 --> 00:06:32,000
this particular offset in the

166
00:06:29,520 --> 00:06:33,360
configuration address space

167
00:06:32,000 --> 00:06:34,479
so what are the next assembly

168
00:06:33,360 --> 00:06:37,039
instructions

169
00:06:34,479 --> 00:06:39,199
now we have moving of fc to dl so the

170
00:06:37,039 --> 00:06:42,160
least significant bit late significant

171
00:06:39,199 --> 00:06:45,600
byte of the dx register so that turns it

172
00:06:42,160 --> 00:06:48,000
into cfc because it already had cf8 and

173
00:06:45,600 --> 00:06:49,360
then some random constant again written

174
00:06:48,000 --> 00:06:51,599
in there so we don't know what this

175
00:06:49,360 --> 00:06:53,840
constant is but whenever it's written to

176
00:06:51,599 --> 00:06:55,440
cfc it's going to be placed into the

177
00:06:53,840 --> 00:07:00,080
configuration address space where

178
00:06:55,440 --> 00:07:01,680
specified 0 31 0 f0

179
00:07:00,080 --> 00:07:05,599
all right so

180
00:07:01,680 --> 00:07:08,080
handed on down written into cfc and

181
00:07:05,599 --> 00:07:10,720
forwarded into a particular register

182
00:07:08,080 --> 00:07:15,360
whatever register happens to be at bus 0

183
00:07:10,720 --> 00:07:17,280
device 31 function 0 offset f0

184
00:07:15,360 --> 00:07:19,199
and what that's actually going to cause

185
00:07:17,280 --> 00:07:21,360
is some sort of memory mapped io that

186
00:07:19,199 --> 00:07:23,199
we're going to talk about later to be

187
00:07:21,360 --> 00:07:25,759
mapped at this particular address that

188
00:07:23,199 --> 00:07:27,680
was written in there fed1c000

189
00:07:25,759 --> 00:07:29,360
so that gives you a sense that probably

190
00:07:27,680 --> 00:07:31,520
you could have put other addresses there

191
00:07:29,360 --> 00:07:33,360
and that would lead to memory mapped i o

192
00:07:31,520 --> 00:07:35,520
happening at some other address but

193
00:07:33,360 --> 00:07:38,639
we'll come back to that later

194
00:07:35,520 --> 00:07:40,800
so what is pcie device zero thirty one

195
00:07:38,639 --> 00:07:42,800
zero well you actually already saw it

196
00:07:40,800 --> 00:07:45,360
quite early in the chipset section you

197
00:07:42,800 --> 00:07:46,720
just probably didn't notice so we were

198
00:07:45,360 --> 00:07:48,800
talking about the data sheets and what

199
00:07:46,720 --> 00:07:51,280
the particular device ids were but what

200
00:07:48,800 --> 00:07:55,039
it was saying over here is that this lpc

201
00:07:51,280 --> 00:07:57,360
device is device 31 function zero and

202
00:07:55,039 --> 00:07:59,120
intel always reserves plus zero for

203
00:07:57,360 --> 00:08:01,520
their own usage on their internal

204
00:07:59,120 --> 00:08:01,520
hardware

205
00:08:02,960 --> 00:08:06,160
so what is offset f0 well i'd like to

206
00:08:04,800 --> 00:08:07,919
tell you but instead we're going to come

207
00:08:06,160 --> 00:08:09,520
back to it later when we can drill down

208
00:08:07,919 --> 00:08:10,560
into it more but it's actually quite

209
00:08:09,520 --> 00:08:12,240
important

210
00:08:10,560 --> 00:08:14,400
but what's more important for right now

211
00:08:12,240 --> 00:08:17,520
is that you fully understand and

212
00:08:14,400 --> 00:08:19,919
internalize this notion of parsing these

213
00:08:17,520 --> 00:08:21,680
32 bits in this particular way so that

214
00:08:19,919 --> 00:08:24,240
if you want to go off and look at some

215
00:08:21,680 --> 00:08:26,319
assembly for the bios you can understand

216
00:08:24,240 --> 00:08:28,080
what exactly it's talking to so that you

217
00:08:26,319 --> 00:08:30,080
can look up in the data sheets what

218
00:08:28,080 --> 00:08:32,880
exactly the particular bus device

219
00:08:30,080 --> 00:08:34,719
function offset that it's accessing is

220
00:08:32,880 --> 00:08:35,839
so that's an extremely important thing

221
00:08:34,719 --> 00:08:37,680
when you're trying to understand what

222
00:08:35,839 --> 00:08:39,760
the code is doing you have to be able to

223
00:08:37,680 --> 00:08:41,760
interpret these 32-bit things and then

224
00:08:39,760 --> 00:08:45,760
go read the fun manuals so let's go do

225
00:08:41,760 --> 00:08:45,760
some exercises to reinforce this


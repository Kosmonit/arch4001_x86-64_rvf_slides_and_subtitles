1
00:00:00,320 --> 00:00:04,960
let's talk about the three address

2
00:00:02,080 --> 00:00:06,960
spaces that are provided by pci

3
00:00:04,960 --> 00:00:08,960
the first is required and it's the

4
00:00:06,960 --> 00:00:10,080
configuration address space which i said

5
00:00:08,960 --> 00:00:12,320
is the main thing we're going to care

6
00:00:10,080 --> 00:00:16,000
about and what we're trying to get to

7
00:00:12,320 --> 00:00:17,840
the next is a optional pci memory mapped

8
00:00:16,000 --> 00:00:20,160
io address space

9
00:00:17,840 --> 00:00:21,920
and then there is a optional port io

10
00:00:20,160 --> 00:00:24,320
address space and we'll see as we

11
00:00:21,920 --> 00:00:25,920
progress how exactly these are set up

12
00:00:24,320 --> 00:00:28,560
but it's really up to the vendor to

13
00:00:25,920 --> 00:00:31,840
decide whether or not they need these

14
00:00:28,560 --> 00:00:34,559
so we had these diagrams before cpus

15
00:00:31,840 --> 00:00:37,520
pchs and then a peripheral connected via

16
00:00:34,559 --> 00:00:39,600
pcie such as a network interface card

17
00:00:37,520 --> 00:00:41,200
and we saw that these were in some

18
00:00:39,600 --> 00:00:43,840
memory mapped i o space that was

19
00:00:41,200 --> 00:00:45,520
stealing some physical address range and

20
00:00:43,840 --> 00:00:47,039
therefore you couldn't get to the ram at

21
00:00:45,520 --> 00:00:49,039
that address range you would just talk

22
00:00:47,039 --> 00:00:51,120
to some particular peripheral

23
00:00:49,039 --> 00:00:53,440
so let's extend this a little bit so we

24
00:00:51,120 --> 00:00:56,000
can put some more information in there

25
00:00:53,440 --> 00:00:58,000
and what i have to say first is that

26
00:00:56,000 --> 00:01:00,559
with the configuration address space

27
00:00:58,000 --> 00:01:03,600
there is going to be some number of bus

28
00:01:00,559 --> 00:01:05,600
device functions worth of configuration

29
00:01:03,600 --> 00:01:09,040
address space and so we learned a second

30
00:01:05,600 --> 00:01:11,920
ago that there's up to 256 buses 32

31
00:01:09,040 --> 00:01:15,119
devices and eight functions available

32
00:01:11,920 --> 00:01:17,119
for indexing in the pci architecture

33
00:01:15,119 --> 00:01:20,560
so there will be bus zero device zero

34
00:01:17,119 --> 00:01:22,400
function zero and that will have 256

35
00:01:20,560 --> 00:01:25,680
bytes of this configuration address

36
00:01:22,400 --> 00:01:27,680
space so 256 bytes accessible via memory

37
00:01:25,680 --> 00:01:29,200
map diode or port i o i'm kind of going

38
00:01:27,680 --> 00:01:31,040
to be showing it as if it's memory

39
00:01:29,200 --> 00:01:34,400
mapped i o here but it can also be port

40
00:01:31,040 --> 00:01:37,200
i o and so that's just one of the many

41
00:01:34,400 --> 00:01:38,479
devices that are accessible 256 bytes at

42
00:01:37,200 --> 00:01:40,560
a time

43
00:01:38,479 --> 00:01:42,320
so that's bus zero device zero function

44
00:01:40,560 --> 00:01:43,840
zero and then you get bus zero device

45
00:01:42,320 --> 00:01:46,479
zero function one

46
00:01:43,840 --> 00:01:48,399
function two etc

47
00:01:46,479 --> 00:01:50,640
device one

48
00:01:48,399 --> 00:01:52,240
function zero all the way up through bus

49
00:01:50,640 --> 00:01:54,960
255

50
00:01:52,240 --> 00:01:57,439
device 31 and function 7.

51
00:01:54,960 --> 00:02:00,000
so potentially all of these things can

52
00:01:57,439 --> 00:02:01,360
be mapped into memory mapped i o space

53
00:02:00,000 --> 00:02:02,960
or

54
00:02:01,360 --> 00:02:05,280
port i o space although they don't

55
00:02:02,960 --> 00:02:06,880
necessarily all have to be if you if the

56
00:02:05,280 --> 00:02:08,720
system designers want to make sure that

57
00:02:06,880 --> 00:02:10,399
they don't steal so much of the physical

58
00:02:08,720 --> 00:02:13,040
address space so we'll see a little bit

59
00:02:10,399 --> 00:02:15,120
about the configurability of that later

60
00:02:13,040 --> 00:02:16,720
now we're going to introduce miss

61
00:02:15,120 --> 00:02:18,959
frizzle for those of you who don't know

62
00:02:16,720 --> 00:02:22,319
miss frizzle she is a children's

63
00:02:18,959 --> 00:02:24,879
educator a passionate advocate of stem

64
00:02:22,319 --> 00:02:26,560
education she's from a children's show

65
00:02:24,879 --> 00:02:28,400
called the magic school bus where

66
00:02:26,560 --> 00:02:30,480
basically they would have a magic school

67
00:02:28,400 --> 00:02:32,720
bus that would shrink and grow and have

68
00:02:30,480 --> 00:02:34,080
a variety of other magical properties so

69
00:02:32,720 --> 00:02:36,720
it could you know go in the human

70
00:02:34,080 --> 00:02:38,480
bloodstream or go to outer space but you

71
00:02:36,720 --> 00:02:40,560
know she's a wild and wacky character

72
00:02:38,480 --> 00:02:42,000
and i don't know exactly how i ended up

73
00:02:40,560 --> 00:02:44,560
finding this particular picture i think

74
00:02:42,000 --> 00:02:46,239
i was probably searching for bus driver

75
00:02:44,560 --> 00:02:47,680
and then her picture came up and i

76
00:02:46,239 --> 00:02:50,080
decided that you know she would be a

77
00:02:47,680 --> 00:02:52,239
perfect person to guide us through these

78
00:02:50,080 --> 00:02:55,599
lands and many others so let's say we

79
00:02:52,239 --> 00:02:56,959
have a device driver a bus driver or

80
00:02:55,599 --> 00:02:58,720
just a device driver and we're going to

81
00:02:56,959 --> 00:03:01,360
say this is the device driver for this

82
00:02:58,720 --> 00:03:04,319
particular network interface card right

83
00:03:01,360 --> 00:03:06,720
so some particular card from broadcom or

84
00:03:04,319 --> 00:03:08,319
3com or intel you know there's a bunch

85
00:03:06,720 --> 00:03:10,480
of different hardware makers who make

86
00:03:08,319 --> 00:03:12,480
nics and each of them is going to

87
00:03:10,480 --> 00:03:14,080
potentially have to have a device driver

88
00:03:12,480 --> 00:03:15,840
because there's not necessarily a

89
00:03:14,080 --> 00:03:17,519
standard that all network interface

90
00:03:15,840 --> 00:03:19,440
cards follow

91
00:03:17,519 --> 00:03:21,519
so that device driver is going to need

92
00:03:19,440 --> 00:03:24,000
to know how to talk to this device over

93
00:03:21,519 --> 00:03:26,480
pcie in order to configure and control

94
00:03:24,000 --> 00:03:28,000
it set it up for proper operation and in

95
00:03:26,480 --> 00:03:30,159
the case of network cards know how to

96
00:03:28,000 --> 00:03:31,120
send packets to it and get packets back

97
00:03:30,159 --> 00:03:34,000
from it

98
00:03:31,120 --> 00:03:35,519
so let's take this 256 we're going to

99
00:03:34,000 --> 00:03:38,799
say you know let's say that was you know

100
00:03:35,519 --> 00:03:43,599
bus 6 function 0 device

101
00:03:38,799 --> 00:03:46,000
bus 0 bus 6 device 0 function 0 256

102
00:03:43,599 --> 00:03:48,239
bytes of configuration address space so

103
00:03:46,000 --> 00:03:50,319
miss frizzle gets the bouncing ball

104
00:03:48,239 --> 00:03:52,159
sends some sort of instruction could be

105
00:03:50,319 --> 00:03:54,400
in this case a memory mapped io could be

106
00:03:52,159 --> 00:03:55,439
a port io sends an instruction that's

107
00:03:54,400 --> 00:03:57,599
going to interface with this

108
00:03:55,439 --> 00:03:59,439
configuration address space instruction

109
00:03:57,599 --> 00:04:01,680
decoder down to memory controller we saw

110
00:03:59,439 --> 00:04:04,000
before for memory mapped i o versus

111
00:04:01,680 --> 00:04:05,519
normal ram access instruction decoder is

112
00:04:04,000 --> 00:04:07,680
going to make a choice where does it map

113
00:04:05,519 --> 00:04:10,480
to how is it routed in this case it's

114
00:04:07,680 --> 00:04:13,519
going to be routed down to the pch and

115
00:04:10,480 --> 00:04:14,480
over to the pcie connected

116
00:04:13,519 --> 00:04:16,160
device

117
00:04:14,480 --> 00:04:18,000
and then when it reaches that device

118
00:04:16,160 --> 00:04:20,079
again as we said before the device

119
00:04:18,000 --> 00:04:22,240
interprets it however it feels like and

120
00:04:20,079 --> 00:04:24,639
so let's say that this particular access

121
00:04:22,240 --> 00:04:25,759
goes and pokes some registers on the

122
00:04:24,639 --> 00:04:27,600
internal

123
00:04:25,759 --> 00:04:28,639
configuration internal processor of that

124
00:04:27,600 --> 00:04:30,880
device

125
00:04:28,639 --> 00:04:33,840
upon poking those registers that can

126
00:04:30,880 --> 00:04:35,840
cause the device to set up a another

127
00:04:33,840 --> 00:04:38,880
chunk of memory mapped i o so this would

128
00:04:35,840 --> 00:04:41,919
be a to be determined number of bytes of

129
00:04:38,880 --> 00:04:43,919
nic memory map i o space so this is sort

130
00:04:41,919 --> 00:04:45,440
of the second configuration and sorry

131
00:04:43,919 --> 00:04:47,040
this is the second address space that's

132
00:04:45,440 --> 00:04:48,960
available there's the config address

133
00:04:47,040 --> 00:04:50,880
space then we said optionally there

134
00:04:48,960 --> 00:04:53,120
could be a memory mapped i o space this

135
00:04:50,880 --> 00:04:56,160
is required this is optional

136
00:04:53,120 --> 00:04:58,000
and then optionally the you know access

137
00:04:56,160 --> 00:05:01,039
to the registers internally could have

138
00:04:58,000 --> 00:05:03,199
also mapped a to be determined number of

139
00:05:01,039 --> 00:05:04,160
bytes available in a port io address

140
00:05:03,199 --> 00:05:05,759
space

141
00:05:04,160 --> 00:05:08,080
and so these are the three address

142
00:05:05,759 --> 00:05:10,400
spaces that a pci device could have i

143
00:05:08,080 --> 00:05:12,240
guess i showed this as a pcie network

144
00:05:10,400 --> 00:05:14,880
card let's you know pretend that e is

145
00:05:12,240 --> 00:05:17,280
not there and just say it's a pci device

146
00:05:14,880 --> 00:05:19,919
so these are the you know things that

147
00:05:17,280 --> 00:05:22,400
then the device driver can interface

148
00:05:19,919 --> 00:05:24,400
with in order to for instance feed you

149
00:05:22,400 --> 00:05:25,759
know packets into the device in order to

150
00:05:24,400 --> 00:05:28,080
make it you know send and receive

151
00:05:25,759 --> 00:05:30,000
packets they could interact with port io

152
00:05:28,080 --> 00:05:31,919
space if one exists they can interact

153
00:05:30,000 --> 00:05:34,320
with a memory mapped i o space if one

154
00:05:31,919 --> 00:05:36,400
exists or maybe the particular device

155
00:05:34,320 --> 00:05:38,160
just needs config space in this case for

156
00:05:36,400 --> 00:05:40,479
a nic card they probably need some sort

157
00:05:38,160 --> 00:05:42,320
of memory map space so that they can

158
00:05:40,479 --> 00:05:44,240
hand off packets back and forth between

159
00:05:42,320 --> 00:05:46,720
the main processor and the neck

160
00:05:44,240 --> 00:05:47,840
so count them one

161
00:05:46,720 --> 00:05:51,960
two

162
00:05:47,840 --> 00:05:51,960
three address spaces

163
00:05:53,199 --> 00:05:56,400
yes people internal to apple i know i

164
00:05:54,880 --> 00:05:58,880
already used that joke before but i like

165
00:05:56,400 --> 00:06:00,960
the joke so i'm gonna go with the count

166
00:05:58,880 --> 00:06:02,880
so let's go ahead and jump in and

167
00:06:00,960 --> 00:06:05,199
understand the first of those address

168
00:06:02,880 --> 00:06:08,240
spaces the configuration address space

169
00:06:05,199 --> 00:06:09,840
time for a lab to go and see those 256

170
00:06:08,240 --> 00:06:11,440
bytes which we said are the main things

171
00:06:09,840 --> 00:06:14,639
we really really care about in the

172
00:06:11,440 --> 00:06:14,639
context of pci


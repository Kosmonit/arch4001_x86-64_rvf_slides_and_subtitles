1
00:00:00,080 --> 00:00:04,560
so i just want to make a quick callback

2
00:00:02,000 --> 00:00:06,720
to our optional secret bios decoder ring

3
00:00:04,560 --> 00:00:09,200
lab from before where we talked about

4
00:00:06,720 --> 00:00:11,360
how physical addresses in the high four

5
00:00:09,200 --> 00:00:13,280
gigabyte range were controlled by some

6
00:00:11,360 --> 00:00:15,440
particular bits and you know we could

7
00:00:13,280 --> 00:00:17,680
actually map and control some of those

8
00:00:15,440 --> 00:00:18,880
things and so now we are at the point

9
00:00:17,680 --> 00:00:20,160
where you can actually understand it

10
00:00:18,880 --> 00:00:21,920
better i said before you'll understand

11
00:00:20,160 --> 00:00:23,680
it better later in the class and now you

12
00:00:21,920 --> 00:00:26,000
can understand it better so what

13
00:00:23,680 --> 00:00:27,920
specifically are the new bits that we

14
00:00:26,000 --> 00:00:28,960
can understand about these slides from

15
00:00:27,920 --> 00:00:31,279
before

16
00:00:28,960 --> 00:00:33,920
well we can now understand that it was

17
00:00:31,279 --> 00:00:37,640
talking about pci configuration address

18
00:00:33,920 --> 00:00:37,640
space bdf0310d8

19
00:00:38,160 --> 00:00:42,719
in the context of this biostatic code

20
00:00:40,320 --> 00:00:45,120
enable register so we talked about how

21
00:00:42,719 --> 00:00:48,719
the original reason why it existed in

22
00:00:45,120 --> 00:00:51,120
device 31 the lpc device function 0

23
00:00:48,719 --> 00:00:53,680
was because of the legacy reasons of the

24
00:00:51,120 --> 00:00:56,399
firmware hub which was an lpc device

25
00:00:53,680 --> 00:00:58,000
pre-spy flash usage but it still just

26
00:00:56,399 --> 00:01:00,160
continued to exist there for a while

27
00:00:58,000 --> 00:01:02,000
until intel eventually added a spy

28
00:01:00,160 --> 00:01:02,879
hardware on the 100 series chipsets and

29
00:01:02,000 --> 00:01:05,119
newer

30
00:01:02,879 --> 00:01:07,920
but so we look at it and we say okay lpc

31
00:01:05,119 --> 00:01:10,799
device the implicit bus zero that is

32
00:01:07,920 --> 00:01:14,240
always used for intel internal pci

33
00:01:10,799 --> 00:01:17,439
devices so bus 0 device 31 function 0

34
00:01:14,240 --> 00:01:20,159
and then offset d8 and d9 so there were

35
00:01:17,439 --> 00:01:22,080
16 bits and specifically we were

36
00:01:20,159 --> 00:01:24,080
concerned with this bit 14 which we

37
00:01:22,080 --> 00:01:26,240
could control because it was read write

38
00:01:24,080 --> 00:01:27,759
and bit 15 we couldn't control and

39
00:01:26,240 --> 00:01:29,600
that's the one that actually always

40
00:01:27,759 --> 00:01:31,439
forces the reset vector to be mapped

41
00:01:29,600 --> 00:01:33,200
from the high addresses down to the spy

42
00:01:31,439 --> 00:01:35,680
flash chip and then likewise for the

43
00:01:33,200 --> 00:01:37,680
data sheets for the 100 series chip sets

44
00:01:35,680 --> 00:01:40,640
we see that it was actually bus 0 device

45
00:01:37,680 --> 00:01:42,880
31 function 5 and that corresponded to

46
00:01:40,640 --> 00:01:45,840
the spy device if you look at the data

47
00:01:42,880 --> 00:01:48,159
sheets and it was also still at offset

48
00:01:45,840 --> 00:01:50,240
d8 so it's sort of like intel just

49
00:01:48,159 --> 00:01:52,720
picked up this information from the lpc

50
00:01:50,240 --> 00:01:55,280
device and plopped it down into the spy

51
00:01:52,720 --> 00:01:57,920
device so there you go just one of many

52
00:01:55,280 --> 00:02:00,159
registers in the pci configuration

53
00:01:57,920 --> 00:02:02,479
address space for the lpc device or the

54
00:02:00,159 --> 00:02:04,399
spy device all of them just waiting for

55
00:02:02,479 --> 00:02:08,320
you to read and explore and see what

56
00:02:04,399 --> 00:02:08,320
other sort of options exist out there


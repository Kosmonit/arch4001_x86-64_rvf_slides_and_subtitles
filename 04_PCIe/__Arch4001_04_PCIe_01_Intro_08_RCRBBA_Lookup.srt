1
00:00:00,080 --> 00:00:06,000
so let's return to this example code

2
00:00:02,240 --> 00:00:07,919
from the optiplex 7010 bios and try to

3
00:00:06,000 --> 00:00:08,880
fully understand what exactly was going

4
00:00:07,919 --> 00:00:10,639
on

5
00:00:08,880 --> 00:00:14,160
now we understood that there's a

6
00:00:10,639 --> 00:00:16,480
particular format to the cf-8 register

7
00:00:14,160 --> 00:00:18,160
and so values placed in it will be

8
00:00:16,480 --> 00:00:22,720
parsed according to

9
00:00:18,160 --> 00:00:25,039
a 8 byte 8-bit offset a 3-bit

10
00:00:22,720 --> 00:00:28,480
function a 5-bit

11
00:00:25,039 --> 00:00:31,359
device and an 8-bit bus

12
00:00:28,480 --> 00:00:35,360
so basically we decoded that before to

13
00:00:31,359 --> 00:00:37,360
bus device function offset 0 31 0 f0 and

14
00:00:35,360 --> 00:00:38,879
so how would we figure out what this is

15
00:00:37,360 --> 00:00:40,480
well we would of course read the fund

16
00:00:38,879 --> 00:00:42,960
manuals

17
00:00:40,480 --> 00:00:45,840
so specifically the pch data sheet and

18
00:00:42,960 --> 00:00:48,480
we had found that this bus zero which we

19
00:00:45,840 --> 00:00:51,680
said intel reserves for its own use bus

20
00:00:48,480 --> 00:00:54,160
zero device 31 function zero was the lpc

21
00:00:51,680 --> 00:00:55,440
interface

22
00:00:54,160 --> 00:00:57,920
now if you happen to be one of the

23
00:00:55,440 --> 00:00:59,520
students who had a 7010 in front of you

24
00:00:57,920 --> 00:01:02,559
then you could just use the typical

25
00:00:59,520 --> 00:01:04,239
device id lookup looking at offset 2 in

26
00:01:02,559 --> 00:01:05,920
the configuration address space where

27
00:01:04,239 --> 00:01:07,760
you find the device id

28
00:01:05,920 --> 00:01:09,520
but i didn't actually have a one of

29
00:01:07,760 --> 00:01:12,080
these yet when i was first making these

30
00:01:09,520 --> 00:01:14,320
slides so i just googled it and googling

31
00:01:12,080 --> 00:01:17,119
it you can find this link which tells

32
00:01:14,320 --> 00:01:18,560
you that this is a 7 series pch so

33
00:01:17,119 --> 00:01:20,080
that's the data sheet which you're going

34
00:01:18,560 --> 00:01:22,479
to want to look at

35
00:01:20,080 --> 00:01:25,200
once you open up the 7 series data sheet

36
00:01:22,479 --> 00:01:27,040
you're going to find this on the sidebar

37
00:01:25,200 --> 00:01:29,119
which is the lpc interface you can see

38
00:01:27,040 --> 00:01:31,360
that there's many different devices here

39
00:01:29,119 --> 00:01:34,240
specified so you can see device 30

40
00:01:31,360 --> 00:01:37,840
function 0 device 31 function 0 device

41
00:01:34,240 --> 00:01:40,400
31 function 2 5 etc

42
00:01:37,840 --> 00:01:43,200
so when you see that what you're going

43
00:01:40,400 --> 00:01:46,000
to see is a whole bunch of pci

44
00:01:43,200 --> 00:01:48,960
configuration address space specified

45
00:01:46,000 --> 00:01:52,000
so right offset 0 is the vendor id which

46
00:01:48,960 --> 00:01:54,159
will always be 8086 on intel hardware

47
00:01:52,000 --> 00:01:56,880
offset 2 is the device id which we used

48
00:01:54,159 --> 00:02:00,240
for identification so it's just a whole

49
00:01:56,880 --> 00:02:02,159
bunch of you know 256 bytes of pci

50
00:02:00,240 --> 00:02:04,880
configuration address space which is

51
00:02:02,159 --> 00:02:06,880
here to tell you how intel has chosen to

52
00:02:04,880 --> 00:02:09,119
place information in here now we

53
00:02:06,880 --> 00:02:10,479
actually saw this before at the very

54
00:02:09,119 --> 00:02:12,160
beginning when i was just kind of

55
00:02:10,479 --> 00:02:14,160
throwing labs at you even though you

56
00:02:12,160 --> 00:02:15,680
couldn't possibly understand them we

57
00:02:14,160 --> 00:02:17,920
said you know what if i told you that

58
00:02:15,680 --> 00:02:20,560
you could partially control memory to

59
00:02:17,920 --> 00:02:24,640
flash mapping and in this lab

60
00:02:20,560 --> 00:02:27,040
it was this offset d8 and d9 inside of

61
00:02:24,640 --> 00:02:29,280
the lpc interface which was the bios

62
00:02:27,040 --> 00:02:30,959
decode enable that was that thing that

63
00:02:29,280 --> 00:02:33,840
basically allowed you to

64
00:02:30,959 --> 00:02:37,280
unmap certain portions of the

65
00:02:33,840 --> 00:02:39,440
physical ram sorry physical memory to

66
00:02:37,280 --> 00:02:41,680
flash access

67
00:02:39,440 --> 00:02:45,360
and basically you couldn't unmap the

68
00:02:41,680 --> 00:02:47,040
very end you couldn't unmap ffff etc but

69
00:02:45,360 --> 00:02:49,200
you could unmap something that was

70
00:02:47,040 --> 00:02:50,720
further down so back then what we were

71
00:02:49,200 --> 00:02:52,879
seeing here is we were looking at the

72
00:02:50,720 --> 00:02:54,879
configuration address space of the lpc

73
00:02:52,879 --> 00:02:57,280
device didn't know what we were doing or

74
00:02:54,879 --> 00:02:58,800
why we were doing but basically that is

75
00:02:57,280 --> 00:03:00,800
what was happening then

76
00:02:58,800 --> 00:03:02,560
now this is actually where we're trying

77
00:03:00,800 --> 00:03:04,800
to get in this class this is the bios

78
00:03:02,560 --> 00:03:06,560
control register this has to do with

79
00:03:04,800 --> 00:03:08,800
access controls we said we're on our way

80
00:03:06,560 --> 00:03:10,480
to flash access controls and we said we

81
00:03:08,800 --> 00:03:13,040
needed you to learn about all this stuff

82
00:03:10,480 --> 00:03:15,920
like pcie configuration address space

83
00:03:13,040 --> 00:03:17,519
port io memory mapped io so you can see

84
00:03:15,920 --> 00:03:19,599
that here this is that's where we're

85
00:03:17,519 --> 00:03:20,879
trying to get to one of the places we're

86
00:03:19,599 --> 00:03:22,159
trying to get to

87
00:03:20,879 --> 00:03:24,080
and you know that's why we need to

88
00:03:22,159 --> 00:03:26,799
understand this but for now let's

89
00:03:24,080 --> 00:03:28,400
continue to just explain what what was

90
00:03:26,799 --> 00:03:30,000
happening in those first few assembly

91
00:03:28,400 --> 00:03:32,239
instructions and why

92
00:03:30,000 --> 00:03:35,840
the dell bios might have been accessing

93
00:03:32,239 --> 00:03:37,920
this offset f0 inside the lpc

94
00:03:35,840 --> 00:03:40,159
configuration address space so that has

95
00:03:37,920 --> 00:03:43,120
something called the root complex base

96
00:03:40,159 --> 00:03:44,720
address so let's look at that

97
00:03:43,120 --> 00:03:46,799
if we go into the data sheet a little

98
00:03:44,720 --> 00:03:47,920
further root complex base address

99
00:03:46,799 --> 00:03:50,080
register

100
00:03:47,920 --> 00:03:52,959
and we know that a value was written

101
00:03:50,080 --> 00:03:57,599
there let's see it was uh fed one sees

102
00:03:52,959 --> 00:03:59,680
this is okay so fed one c zero zero and

103
00:03:57,599 --> 00:04:02,239
then one is what was actually written

104
00:03:59,680 --> 00:04:04,400
there so the least significant bit 1

105
00:04:02,239 --> 00:04:07,360
seems to be an enable bit

106
00:04:04,400 --> 00:04:08,480
and the upper bits seem to be a base

107
00:04:07,360 --> 00:04:11,200
address

108
00:04:08,480 --> 00:04:13,200
so i'm going to call that the so it says

109
00:04:11,200 --> 00:04:15,200
the enable is

110
00:04:13,200 --> 00:04:17,600
the range specified in the ba base

111
00:04:15,200 --> 00:04:19,680
address is to be claimed by the root

112
00:04:17,600 --> 00:04:21,359
complex register block so

113
00:04:19,680 --> 00:04:24,240
i'm going to call this space address the

114
00:04:21,359 --> 00:04:25,040
root complex register block base address

115
00:04:24,240 --> 00:04:26,720
or

116
00:04:25,040 --> 00:04:28,880
rickerbar

117
00:04:26,720 --> 00:04:31,680
now rickerbaugh in this class is going

118
00:04:28,880 --> 00:04:34,240
to be represented by this nice can of rc

119
00:04:31,680 --> 00:04:35,919
cola just need some you know things to

120
00:04:34,240 --> 00:04:37,840
visually represent this because you know

121
00:04:35,919 --> 00:04:39,520
we get into a lot of acronyms and

122
00:04:37,840 --> 00:04:41,040
abbreviations and some are more

123
00:04:39,520 --> 00:04:43,040
important than others if they're

124
00:04:41,040 --> 00:04:45,040
important they're going to get an icon

125
00:04:43,040 --> 00:04:47,600
so this particular base address in this

126
00:04:45,040 --> 00:04:49,280
particular register is rickerbaugh and

127
00:04:47,600 --> 00:04:52,160
i'm going to introduce another

128
00:04:49,280 --> 00:04:53,919
association i'm going to call this rcba

129
00:04:52,160 --> 00:04:56,800
register you know i want you to think

130
00:04:53,919 --> 00:05:00,800
about mirror universe spock because

131
00:04:56,800 --> 00:05:03,440
basically cba abc cba abc backwards

132
00:05:00,800 --> 00:05:04,960
forwards mirror universe yes we just we

133
00:05:03,440 --> 00:05:06,800
need anything and you know i'm grasping

134
00:05:04,960 --> 00:05:09,520
at straws here because you know it's all

135
00:05:06,800 --> 00:05:12,320
dry material so

136
00:05:09,520 --> 00:05:14,479
our cba which is mary universe spock

137
00:05:12,320 --> 00:05:16,720
holds rickerbaugh

138
00:05:14,479 --> 00:05:22,080
and you know he finds this association

139
00:05:16,720 --> 00:05:24,479
most refreshing so rcba abc cba abc cba

140
00:05:22,080 --> 00:05:26,240
mirror universe spock holds rickerbaugh

141
00:05:24,479 --> 00:05:27,840
and i know that sounds more klingon than

142
00:05:26,240 --> 00:05:30,400
vulcan but you know we're just going to

143
00:05:27,840 --> 00:05:30,400
go with it

144
00:05:30,560 --> 00:05:36,080
so that brings up the question of what

145
00:05:33,199 --> 00:05:38,400
exactly is a root complex register block

146
00:05:36,080 --> 00:05:41,440
well it turns out that a root complex

147
00:05:38,400 --> 00:05:44,639
is a pci thing that has to do with the

148
00:05:41,440 --> 00:05:47,520
access to memory for pci devices as well

149
00:05:44,639 --> 00:05:50,320
as the cpu so i really like this nice

150
00:05:47,520 --> 00:05:52,880
clean diagram from wikipedia it shows

151
00:05:50,320 --> 00:05:55,759
pci devices it shows that there are

152
00:05:52,880 --> 00:05:59,520
switches inside of the pci network it

153
00:05:55,759 --> 00:06:00,319
shows things like pcie to pci bridges to

154
00:05:59,520 --> 00:06:02,160
you know

155
00:06:00,319 --> 00:06:04,080
access between different buses if

156
00:06:02,160 --> 00:06:06,639
there's some legacy devices on some

157
00:06:04,080 --> 00:06:08,880
other bus and it shows how both the cpu

158
00:06:06,639 --> 00:06:10,479
and the devices are mediated by the root

159
00:06:08,880 --> 00:06:12,800
complex when they're trying to get to

160
00:06:10,479 --> 00:06:14,720
memory i think this kind of shows a

161
00:06:12,800 --> 00:06:17,360
little bit more intuitively how

162
00:06:14,720 --> 00:06:19,520
something like dma might work right so

163
00:06:17,360 --> 00:06:22,479
dma is the concept of direct memory

164
00:06:19,520 --> 00:06:25,280
access and we know that certain pc pcie

165
00:06:22,479 --> 00:06:27,520
peripherals can directly access memory

166
00:06:25,280 --> 00:06:29,360
without having to go to the cpu the cpu

167
00:06:27,520 --> 00:06:31,360
fetching it and then it coming back down

168
00:06:29,360 --> 00:06:32,639
so with this notion of a root complex it

169
00:06:31,360 --> 00:06:34,720
should hopefully make a little more

170
00:06:32,639 --> 00:06:36,960
sense why there's some hardware that can

171
00:06:34,720 --> 00:06:38,080
just say oh you're trying to get there

172
00:06:36,960 --> 00:06:40,160
well i'm just going to route you to

173
00:06:38,080 --> 00:06:42,000
memory and send the memory back to you

174
00:06:40,160 --> 00:06:44,560
again that's for things like for

175
00:06:42,000 --> 00:06:47,039
instance a nic card which needs to

176
00:06:44,560 --> 00:06:49,440
read and write packets so the cpu might

177
00:06:47,039 --> 00:06:51,520
put some packets in here and then the

178
00:06:49,440 --> 00:06:52,560
nic card is just reading and writing

179
00:06:51,520 --> 00:06:54,160
them so

180
00:06:52,560 --> 00:06:56,000
reads them in order to send them out of

181
00:06:54,160 --> 00:06:58,800
the network and writes them to put a

182
00:06:56,000 --> 00:07:01,680
buffer of new incoming packets there for

183
00:06:58,800 --> 00:07:05,199
the cpu to check at its leisure

184
00:07:01,680 --> 00:07:07,120
but so this root complex register block

185
00:07:05,199 --> 00:07:08,800
back in the old data sheets they showed

186
00:07:07,120 --> 00:07:10,479
it a little bit nicer don't have a

187
00:07:08,800 --> 00:07:13,039
really good way to see it in newer data

188
00:07:10,479 --> 00:07:15,360
sheets but you know back in the you know

189
00:07:13,039 --> 00:07:18,319
northbridge southbridge world you had

190
00:07:15,360 --> 00:07:20,639
the graphics and memory controller hub

191
00:07:18,319 --> 00:07:22,400
and so if we said on this previous slide

192
00:07:20,639 --> 00:07:24,800
that the root complex is the thing that

193
00:07:22,400 --> 00:07:26,400
mediates access to memory and we said

194
00:07:24,800 --> 00:07:28,080
that back in the day the memory

195
00:07:26,400 --> 00:07:30,160
controller hubs had direct access to

196
00:07:28,080 --> 00:07:33,440
memory and the cpu didn't until it was

197
00:07:30,160 --> 00:07:35,199
merged up during the great pch merger

198
00:07:33,440 --> 00:07:38,000
well then it makes sense that there

199
00:07:35,199 --> 00:07:39,599
would be root complexes inside of the

200
00:07:38,000 --> 00:07:41,840
memory controller hub

201
00:07:39,599 --> 00:07:44,400
and indeed it turns out that there were

202
00:07:41,840 --> 00:07:46,240
multiple there were two root complex

203
00:07:44,400 --> 00:07:48,319
register blocks that could be specified

204
00:07:46,240 --> 00:07:50,400
in the memory controller hub

205
00:07:48,319 --> 00:07:52,960
one for the egress port meaning the

206
00:07:50,400 --> 00:07:55,280
access to main memory and then one for

207
00:07:52,960 --> 00:07:58,479
dmi which is that connection that

208
00:07:55,280 --> 00:08:01,280
interface down to the ich back then so

209
00:07:58,479 --> 00:08:03,919
nowadays on pch-based systems the root

210
00:08:01,280 --> 00:08:05,840
complex would be in the cpu but like i

211
00:08:03,919 --> 00:08:08,720
said there's no good way to visualize

212
00:08:05,840 --> 00:08:09,520
that with intel's documentation anyways

213
00:08:08,720 --> 00:08:12,319
so

214
00:08:09,520 --> 00:08:14,560
what exactly is this rickerbar and

215
00:08:12,319 --> 00:08:16,479
mirror universe spock up to and you know

216
00:08:14,560 --> 00:08:19,120
why would dell be setting this

217
00:08:16,479 --> 00:08:20,240
particular value so we had device 31

218
00:08:19,120 --> 00:08:25,680
function

219
00:08:20,240 --> 00:08:29,120
0 and offset f0 so offset f0 is rcba cba

220
00:08:25,680 --> 00:08:30,879
abc cba abc mirror universe spock which

221
00:08:29,120 --> 00:08:33,519
holds rickerbar

222
00:08:30,879 --> 00:08:36,719
and so what dell was doing is they were

223
00:08:33,519 --> 00:08:39,039
writing a rickerbar value of you know

224
00:08:36,719 --> 00:08:40,640
fed1c 0.

225
00:08:39,039 --> 00:08:43,200
and so that rickerbar is the root

226
00:08:40,640 --> 00:08:46,160
complex register b

227
00:08:43,200 --> 00:08:49,200
block base address so we want to see

228
00:08:46,160 --> 00:08:52,320
what this root complex register block is

229
00:08:49,200 --> 00:08:54,480
so it's some chunk of memory mapped io

230
00:08:52,320 --> 00:08:56,720
and dell had written some particular

231
00:08:54,480 --> 00:09:00,720
base address to say i want this memory

232
00:08:56,720 --> 00:09:03,360
mapped i o range to be at fed1c000

233
00:09:00,720 --> 00:09:05,440
so what's inside of the root complex

234
00:09:03,360 --> 00:09:08,160
register block a whole bunch of

235
00:09:05,440 --> 00:09:10,320
registers yay you know it's just you

236
00:09:08,160 --> 00:09:11,760
know registers within registers within

237
00:09:10,320 --> 00:09:13,680
registers

238
00:09:11,760 --> 00:09:15,519
so there's a whole bunch of stuff there

239
00:09:13,680 --> 00:09:18,000
and most of it we don't actually care

240
00:09:15,519 --> 00:09:21,839
about but later on we said that we're

241
00:09:18,000 --> 00:09:24,240
trying to get to the spy the flash

242
00:09:21,839 --> 00:09:26,000
access control mechanism so later on in

243
00:09:24,240 --> 00:09:27,760
the spy section we're going to you know

244
00:09:26,000 --> 00:09:29,440
start at this diagram and we're going to

245
00:09:27,760 --> 00:09:32,160
see that inside the root complex

246
00:09:29,440 --> 00:09:33,920
register block for some older machines

247
00:09:32,160 --> 00:09:37,760
not the newest machines but some older

248
00:09:33,920 --> 00:09:39,519
machines at offset hex 3800 there is

249
00:09:37,760 --> 00:09:42,320
going to be some particular memory

250
00:09:39,519 --> 00:09:44,640
mapped i o registers that have to do

251
00:09:42,320 --> 00:09:46,560
with reading and writing the flash chip

252
00:09:44,640 --> 00:09:48,800
so because of that i want you to go

253
00:09:46,560 --> 00:09:52,000
ahead and look up your rickerbot right

254
00:09:48,800 --> 00:09:54,959
now and so if you go to your data sheet

255
00:09:52,000 --> 00:09:59,200
and you find device 31 function 0 offset

256
00:09:54,959 --> 00:10:00,640
f0 doesn't actually have the rcba

257
00:09:59,200 --> 00:10:02,560
then that's fine that means you're

258
00:10:00,640 --> 00:10:04,560
probably running one of these newer

259
00:10:02,560 --> 00:10:07,760
systems you know a sixth or seventh

260
00:10:04,560 --> 00:10:09,920
generation core or a greater than 100

261
00:10:07,760 --> 00:10:11,600
series pch so that's fine you don't need

262
00:10:09,920 --> 00:10:14,480
to write it down but for everyone else

263
00:10:11,600 --> 00:10:16,320
if you go to offset f-zero and you find

264
00:10:14,480 --> 00:10:19,360
your rickerbot write it down because

265
00:10:16,320 --> 00:10:19,360
you'll need it later


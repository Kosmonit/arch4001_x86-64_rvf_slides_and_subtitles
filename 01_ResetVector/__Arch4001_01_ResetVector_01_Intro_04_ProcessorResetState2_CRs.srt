1
00:00:00,240 --> 00:00:04,799
so what else does simics have to teach

2
00:00:02,080 --> 00:00:07,759
us about the initial state after reset

3
00:00:04,799 --> 00:00:10,960
well it tells us that cr4 is mostly all

4
00:00:07,759 --> 00:00:13,519
zeros cr3 is zero and it says paging is

5
00:00:10,960 --> 00:00:17,119
disabled which is what we would expect

6
00:00:13,519 --> 00:00:18,800
and cr0 is mostly all zeros as well

7
00:00:17,119 --> 00:00:21,439
so let's dig into each of those a little

8
00:00:18,800 --> 00:00:22,320
bit more so cr2 three and four are all

9
00:00:21,439 --> 00:00:25,439
zero

10
00:00:22,320 --> 00:00:27,199
cr0 has some particular fixed value that

11
00:00:25,439 --> 00:00:29,599
it's likely been set to for quite a

12
00:00:27,199 --> 00:00:32,000
while furthermore here are the bits that

13
00:00:29,599 --> 00:00:34,559
we have seen in architecture 2001 and

14
00:00:32,000 --> 00:00:36,239
the pe in particular we said was going

15
00:00:34,559 --> 00:00:39,360
to be the one that we would see in 2001

16
00:00:36,239 --> 00:00:43,120
and 4001. so the initial state here is 0

17
00:00:39,360 --> 00:00:45,840
0 0 and that 0 right there is the

18
00:00:43,120 --> 00:00:47,600
protection enable bit so protected mode

19
00:00:45,840 --> 00:00:49,039
is not enabled that means we must be in

20
00:00:47,600 --> 00:00:51,760
real mode

21
00:00:49,039 --> 00:00:54,239
and so what is real how do you define

22
00:00:51,760 --> 00:00:58,160
rio if you're talking about what you can

23
00:00:54,239 --> 00:01:00,239
set for instance cr0.pe equals zero then

24
00:00:58,160 --> 00:01:02,640
real is merely electrical signals

25
00:01:00,239 --> 00:01:04,879
interpreted by your cpu

26
00:01:02,640 --> 00:01:06,799
morpheus

27
00:01:04,879 --> 00:01:10,720
and the rest of the values are filled in

28
00:01:06,799 --> 00:01:13,680
here and we can see up at bit 31 paging

29
00:01:10,720 --> 00:01:15,119
is also disabled as we would expect

30
00:01:13,680 --> 00:01:17,439
but what about some of these other

31
00:01:15,119 --> 00:01:19,920
things that are actually set to one so

32
00:01:17,439 --> 00:01:22,479
here we have the et bit i'm not going to

33
00:01:19,920 --> 00:01:24,799
do the et phone home it's the extension

34
00:01:22,479 --> 00:01:26,320
type bit that indicated originally

35
00:01:24,799 --> 00:01:29,040
whether or not the particular hardware

36
00:01:26,320 --> 00:01:31,040
supported some math coprocessor

37
00:01:29,040 --> 00:01:33,280
instructions and now everything supports

38
00:01:31,040 --> 00:01:35,280
it so it's always set to one

39
00:01:33,280 --> 00:01:38,079
then furthermore there's the cache

40
00:01:35,280 --> 00:01:40,720
disable and not write through bits so

41
00:01:38,079 --> 00:01:42,720
these both have to do with caching and

42
00:01:40,720 --> 00:01:44,880
basically it has to do with your initial

43
00:01:42,720 --> 00:01:47,040
state you don't want the cache to be

44
00:01:44,880 --> 00:01:50,399
used you want to be cleaned up and you

45
00:01:47,040 --> 00:01:52,399
know executing directly from the flash

46
00:01:50,399 --> 00:01:54,079
now i want to go look up in the manual

47
00:01:52,399 --> 00:01:56,159
you know what exactly does it mean when

48
00:01:54,079 --> 00:01:59,280
cache disable is one and non-write

49
00:01:56,159 --> 00:02:01,600
through is one and as i was reading it i

50
00:01:59,280 --> 00:02:03,600
read this little bit here saying that

51
00:02:01,600 --> 00:02:05,119
read hits access the cache and i was

52
00:02:03,600 --> 00:02:07,360
like well that's counter-intuitive

53
00:02:05,119 --> 00:02:08,800
that's weird like i didn't expect that

54
00:02:07,360 --> 00:02:10,959
you know the reset state should be

55
00:02:08,800 --> 00:02:12,319
reading from the cache that seems like

56
00:02:10,959 --> 00:02:13,440
an attack

57
00:02:12,319 --> 00:02:16,480
whoa

58
00:02:13,440 --> 00:02:18,640
can i execute stuff from the cache after

59
00:02:16,480 --> 00:02:20,000
reset to have attack control data

60
00:02:18,640 --> 00:02:23,520
executed

61
00:02:20,000 --> 00:02:25,520
and the answer is no so sad keanu and i

62
00:02:23,520 --> 00:02:27,760
feel like every time i come across this

63
00:02:25,520 --> 00:02:30,640
little bit you know however many years

64
00:02:27,760 --> 00:02:32,319
ago six seven eight years ago whenever i

65
00:02:30,640 --> 00:02:34,000
first came across this

66
00:02:32,319 --> 00:02:35,599
i you know said to john butterworth hey

67
00:02:34,000 --> 00:02:37,920
like check that out you know that seems

68
00:02:35,599 --> 00:02:39,360
like that could potentially be an attack

69
00:02:37,920 --> 00:02:40,879
and then you know you read through the

70
00:02:39,360 --> 00:02:43,519
manuals a little bit more and you find

71
00:02:40,879 --> 00:02:45,519
things like this initialization overview

72
00:02:43,519 --> 00:02:48,640
upon reset it also invalidates the

73
00:02:45,519 --> 00:02:50,640
internal caches or because all internal

74
00:02:48,640 --> 00:02:52,640
cache lines are invalid following reset

75
00:02:50,640 --> 00:02:54,800
initialization it is not necessary to

76
00:02:52,640 --> 00:02:56,640
invalidate the cache before enabling

77
00:02:54,800 --> 00:03:00,800
caching so

78
00:02:56,640 --> 00:03:02,400
not woe it's a no sad keyanu

79
00:03:00,800 --> 00:03:04,640
so the key thing though is that you

80
00:03:02,400 --> 00:03:06,879
shouldn't behave that virtual machines

81
00:03:04,640 --> 00:03:08,480
behave like real physical ones because

82
00:03:06,879 --> 00:03:10,480
it's actually been shown that there are

83
00:03:08,480 --> 00:03:12,080
those sort of attacks in the past where

84
00:03:10,480 --> 00:03:14,239
someone was able to write into the

85
00:03:12,080 --> 00:03:16,159
virtual bios and then it would actually

86
00:03:14,239 --> 00:03:17,920
persist across reset because the

87
00:03:16,159 --> 00:03:20,239
virtualization system didn't actually

88
00:03:17,920 --> 00:03:22,239
clear it so never take it for granted

89
00:03:20,239 --> 00:03:24,239
that something that doesn't work on a

90
00:03:22,239 --> 00:03:27,040
physical machine won't work on a virtual

91
00:03:24,239 --> 00:03:27,040
machine


1
00:00:03,840 --> 00:00:07,440
welcome

2
00:00:05,120 --> 00:00:10,240
to the desert

3
00:00:07,440 --> 00:00:10,240
of the real

4
00:00:12,960 --> 00:00:16,960
welcome

5
00:00:14,080 --> 00:00:19,520
to the desert of the reel

6
00:00:16,960 --> 00:00:21,760
so you are here x86 reset vector we're

7
00:00:19,520 --> 00:00:23,119
going to learn about real mode and we've

8
00:00:21,760 --> 00:00:25,840
already learned about protected mode in

9
00:00:23,119 --> 00:00:28,320
architecture 2001. so what is a reset

10
00:00:25,840 --> 00:00:29,840
vector well according to wikipedia in

11
00:00:28,320 --> 00:00:31,519
computing a reset vector as the default

12
00:00:29,840 --> 00:00:33,360
location a central processing unit will

13
00:00:31,519 --> 00:00:35,840
go to to find the first instruction it

14
00:00:33,360 --> 00:00:38,719
will execute after a reset so after a

15
00:00:35,840 --> 00:00:42,399
reset on x86 hardware the address that

16
00:00:38,719 --> 00:00:44,800
it goes to is ffff so seven fs and a

17
00:00:42,399 --> 00:00:48,399
zero which is basically four gigabytes

18
00:00:44,800 --> 00:00:50,879
minus 16 bytes and when it resets it's

19
00:00:48,399 --> 00:00:52,399
going to default to start in real

20
00:00:50,879 --> 00:00:55,199
address mode

21
00:00:52,399 --> 00:00:56,960
so real address mode aka real mode can

22
00:00:55,199 --> 00:00:59,199
be found you can find more information

23
00:00:56,960 --> 00:01:00,239
about this in the following fun manual

24
00:00:59,199 --> 00:01:02,079
sections

25
00:01:00,239 --> 00:01:04,640
system architecture overview processor

26
00:01:02,079 --> 00:01:06,479
management initialization and 8086

27
00:01:04,640 --> 00:01:08,320
emulation so we're actually not really

28
00:01:06,479 --> 00:01:09,920
going to cover some of the stuff that's

29
00:01:08,320 --> 00:01:12,880
in that section so i recommend you go

30
00:01:09,920 --> 00:01:14,320
check that out later specifically what

31
00:01:12,880 --> 00:01:16,479
we're not going to cover there is the

32
00:01:14,320 --> 00:01:18,320
thing called virtual 8086 mode that's

33
00:01:16,479 --> 00:01:20,080
covered in that section and so if we

34
00:01:18,320 --> 00:01:21,680
look at this diagram we can actually see

35
00:01:20,080 --> 00:01:24,960
that there's a whole bunch of edges

36
00:01:21,680 --> 00:01:27,360
labeled reset reset there reset there

37
00:01:24,960 --> 00:01:29,520
reset here reset there

38
00:01:27,360 --> 00:01:32,960
and so the basic point is out of all of

39
00:01:29,520 --> 00:01:36,320
these operating modes if a reset occurs

40
00:01:32,960 --> 00:01:38,000
all roads all resets lead to real mode

41
00:01:36,320 --> 00:01:41,200
so when the processor restarts it's

42
00:01:38,000 --> 00:01:43,360
going to be in real mode so what is real

43
00:01:41,200 --> 00:01:44,960
mode real address mode aka real mode i'm

44
00:01:43,360 --> 00:01:47,280
an american i don't like to say extra

45
00:01:44,960 --> 00:01:48,880
syllables if i don't have to so real

46
00:01:47,280 --> 00:01:51,360
mode called real address mode in the

47
00:01:48,880 --> 00:01:54,159
manuals but real mode in that diagram is

48
00:01:51,360 --> 00:01:57,280
the original execution mode of an 8086

49
00:01:54,159 --> 00:02:00,079
or 8088 intel processor it was actually

50
00:01:57,280 --> 00:02:03,360
just the only mode of the processor

51
00:02:00,079 --> 00:02:05,600
until the 8286 came along and they added

52
00:02:03,360 --> 00:02:07,439
protected mode now it still exists today

53
00:02:05,600 --> 00:02:10,160
for backwards compatibility and it still

54
00:02:07,439 --> 00:02:11,760
exists because the processor upon reset

55
00:02:10,160 --> 00:02:14,160
starts in real mode

56
00:02:11,760 --> 00:02:16,000
now modern bios is although they start

57
00:02:14,160 --> 00:02:18,319
in real mode try to get out of it as

58
00:02:16,000 --> 00:02:20,959
soon as humanly possible

59
00:02:18,319 --> 00:02:23,040
so rather than continue to talk about

60
00:02:20,959 --> 00:02:24,959
what real mode is and isn't we're going

61
00:02:23,040 --> 00:02:27,440
to just jump right in with your first

62
00:02:24,959 --> 00:02:28,800
simmix lab in order to have you poke

63
00:02:27,440 --> 00:02:30,720
around a little bit and then i'll come

64
00:02:28,800 --> 00:02:33,720
back and explain more about what you're

65
00:02:30,720 --> 00:02:33,720
seeing


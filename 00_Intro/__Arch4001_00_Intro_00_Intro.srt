1
00:00:01,040 --> 00:00:05,920
welcome to architecture 4001 first let's

2
00:00:03,679 --> 00:00:07,600
give credit where credit is due a big

3
00:00:05,920 --> 00:00:09,200
thanks goes to john butterworth who

4
00:00:07,600 --> 00:00:11,120
developed the original version of this

5
00:00:09,200 --> 00:00:12,160
class back when he was on my team at

6
00:00:11,120 --> 00:00:15,360
mitre

7
00:00:12,160 --> 00:00:18,560
so this was licensed as creative commons

8
00:00:15,360 --> 00:00:20,880
with attribution and share alike and so

9
00:00:18,560 --> 00:00:22,160
the original sharealike clause said that

10
00:00:20,880 --> 00:00:23,760
this class is derived from john

11
00:00:22,160 --> 00:00:25,599
butterworth and xenocova's advanced

12
00:00:23,760 --> 00:00:28,320
intel x86 bios

13
00:00:25,599 --> 00:00:30,439
smm class posted at blah blah blah open

14
00:00:28,320 --> 00:00:32,800
security training info

15
00:00:30,439 --> 00:00:34,480
introbios.html so again big thanks to

16
00:00:32,800 --> 00:00:37,120
john for making the original class which

17
00:00:34,480 --> 00:00:39,920
i've modified quite a bit since then

18
00:00:37,120 --> 00:00:42,160
so back in architecture 2001 we saw this

19
00:00:39,920 --> 00:00:43,600
diagram from the amd manuals which

20
00:00:42,160 --> 00:00:45,039
talked about the different processor

21
00:00:43,600 --> 00:00:46,879
execution modes

22
00:00:45,039 --> 00:00:48,879
and so while we learned about things

23
00:00:46,879 --> 00:00:52,079
like protected mode

24
00:00:48,879 --> 00:00:55,520
compatibility mode and 64-bit mode full

25
00:00:52,079 --> 00:00:57,600
long mode in architecture 2001 in this

26
00:00:55,520 --> 00:01:00,079
class we're going to learn about things

27
00:00:57,600 --> 00:01:02,160
like real mode which is where the

28
00:01:00,079 --> 00:01:04,080
firmware starts executing from when the

29
00:01:02,160 --> 00:01:05,600
processor is reset

30
00:01:04,080 --> 00:01:07,600
we're also going to learn about system

31
00:01:05,600 --> 00:01:09,840
management mode which is the thing where

32
00:01:07,600 --> 00:01:12,000
code is put into a special isolated

33
00:01:09,840 --> 00:01:14,159
region and executed when the system

34
00:01:12,000 --> 00:01:16,000
needs to do things like power management

35
00:01:14,159 --> 00:01:18,000
it's generally set up by firmware and

36
00:01:16,000 --> 00:01:20,400
then is left to run autonomously in the

37
00:01:18,000 --> 00:01:22,159
background secretly in a place that you

38
00:01:20,400 --> 00:01:23,759
can't really see

39
00:01:22,159 --> 00:01:26,640
so what are the course goals for this

40
00:01:23,759 --> 00:01:28,320
class well we want to teach you how to

41
00:01:26,640 --> 00:01:29,920
understand where the firmware is stored

42
00:01:28,320 --> 00:01:32,240
how it's read and written by the

43
00:01:29,920 --> 00:01:33,920
processor and how it's right protected

44
00:01:32,240 --> 00:01:36,320
so that malware can't introduce

45
00:01:33,920 --> 00:01:37,920
malicious software into the deepest part

46
00:01:36,320 --> 00:01:40,000
of the firmware we're also going to

47
00:01:37,920 --> 00:01:42,240
learn about system management mode how

48
00:01:40,000 --> 00:01:45,200
it's isolated why we generally say that

49
00:01:42,240 --> 00:01:47,360
smm is the most powerful and privileged

50
00:01:45,200 --> 00:01:49,759
code on the system and why that won't

51
00:01:47,360 --> 00:01:51,600
necessarily always be the case

52
00:01:49,759 --> 00:01:53,920
we'll also learn about how smm is

53
00:01:51,600 --> 00:01:56,320
protected to again stop malware from

54
00:01:53,920 --> 00:01:58,159
injecting itself into there

55
00:01:56,320 --> 00:02:00,479
now some other course goals is that we

56
00:01:58,159 --> 00:02:02,479
want you to understand how to inspect

57
00:02:00,479 --> 00:02:03,680
the configuration of your own system so

58
00:02:02,479 --> 00:02:06,079
that you can understand whether the

59
00:02:03,680 --> 00:02:08,160
vendor has properly locked it down

60
00:02:06,079 --> 00:02:09,920
because the reality is that there's no

61
00:02:08,160 --> 00:02:11,520
reason to actually go out and find

62
00:02:09,920 --> 00:02:13,680
memory corruption exploitable

63
00:02:11,520 --> 00:02:15,760
vulnerabilities if the vendor has just

64
00:02:13,680 --> 00:02:16,959
went ahead and left the thing completely

65
00:02:15,760 --> 00:02:18,959
unlocked

66
00:02:16,959 --> 00:02:20,959
now hopefully you won't see that as much

67
00:02:18,959 --> 00:02:23,599
anymore certainly not on enterprise

68
00:02:20,959 --> 00:02:26,239
grade systems but you will find that if

69
00:02:23,599 --> 00:02:27,760
you're running some lesser system from a

70
00:02:26,239 --> 00:02:30,000
company that doesn't really care about

71
00:02:27,760 --> 00:02:32,400
security then you almost certainly will

72
00:02:30,000 --> 00:02:33,760
find that the system is unlocked and

73
00:02:32,400 --> 00:02:36,560
sometimes they think of that as a

74
00:02:33,760 --> 00:02:38,560
feature i think of it as a bug another

75
00:02:36,560 --> 00:02:40,560
goal of this class is to prepare you for

76
00:02:38,560 --> 00:02:43,120
future classes on things like reverse

77
00:02:40,560 --> 00:02:45,040
engineering and exploits and as always

78
00:02:43,120 --> 00:02:47,120
we want to teach you how to read the fun

79
00:02:45,040 --> 00:02:49,120
manual so that you can go beyond what

80
00:02:47,120 --> 00:02:50,720
you've learned in this class because

81
00:02:49,120 --> 00:02:52,400
it's going to be necessary to read a

82
00:02:50,720 --> 00:02:54,959
whole bunch of stuff beyond what you

83
00:02:52,400 --> 00:02:56,959
learn in this class

84
00:02:54,959 --> 00:02:59,440
we're also going to provide you with a

85
00:02:56,959 --> 00:03:01,680
introduction to firmware in a mostly

86
00:02:59,440 --> 00:03:03,280
firmware agnostic way so the information

87
00:03:01,680 --> 00:03:06,239
you learn in this class is going to be

88
00:03:03,280 --> 00:03:08,400
relevant to something like uefi or core

89
00:03:06,239 --> 00:03:10,400
boot or legacy bios

90
00:03:08,400 --> 00:03:13,440
and just like with things like x86

91
00:03:10,400 --> 00:03:15,760
assembly this is again even lesser known

92
00:03:13,440 --> 00:03:18,720
information and therefore this knowledge

93
00:03:15,760 --> 00:03:21,280
is power and remember kids knowledge is

94
00:03:18,720 --> 00:03:23,440
power which you can rest from me

95
00:03:21,280 --> 00:03:24,400
because the accumulation of power is a

96
00:03:23,440 --> 00:03:27,040
game

97
00:03:24,400 --> 00:03:28,560
a game of which thanos has yet to tire

98
00:03:27,040 --> 00:03:30,319
and this is why i love the marvel

99
00:03:28,560 --> 00:03:32,720
cinematic universe because if i used

100
00:03:30,319 --> 00:03:35,040
this deep cut from some random comic

101
00:03:32,720 --> 00:03:36,480
book there's absolutely no way anyone

102
00:03:35,040 --> 00:03:38,400
except the super nerds would have

103
00:03:36,480 --> 00:03:41,360
understood me back in the day but now

104
00:03:38,400 --> 00:03:43,360
you know exactly what i'm talking about

105
00:03:41,360 --> 00:03:45,599
and to the point that this is rare and

106
00:03:43,360 --> 00:03:47,440
valuable knowledge i found this random

107
00:03:45,599 --> 00:03:49,920
screenshot while i was looking for

108
00:03:47,440 --> 00:03:52,080
something else from a previous intel

109
00:03:49,920 --> 00:03:54,480
presentation and i think this nicely

110
00:03:52,080 --> 00:03:56,239
summarizes the typical inverted pyramid

111
00:03:54,480 --> 00:03:58,879
that is associated with low level

112
00:03:56,239 --> 00:04:00,959
knowledge so lots of people know things

113
00:03:58,879 --> 00:04:02,720
up at the high layers but down at the

114
00:04:00,959 --> 00:04:04,640
lowest layers not that many people know

115
00:04:02,720 --> 00:04:06,640
it and so by taking this class you're

116
00:04:04,640 --> 00:04:09,120
acquiring a rarified resource of

117
00:04:06,640 --> 00:04:11,040
knowledge that will ultimately be

118
00:04:09,120 --> 00:04:12,640
valuable

119
00:04:11,040 --> 00:04:15,840
so let's talk about the outline for this

120
00:04:12,640 --> 00:04:18,560
class so we want to learn a lot of

121
00:04:15,840 --> 00:04:20,560
different stuff and here it is

122
00:04:18,560 --> 00:04:22,320
but specifically we're going to start

123
00:04:20,560 --> 00:04:24,560
here covering things like the reset

124
00:04:22,320 --> 00:04:27,199
vector protected mode real mode you know

125
00:04:24,560 --> 00:04:29,600
the processor execution modes

126
00:04:27,199 --> 00:04:31,840
and where we want to get from there is

127
00:04:29,600 --> 00:04:33,919
down to here to the flash right

128
00:04:31,840 --> 00:04:36,320
protection and the attacks that could

129
00:04:33,919 --> 00:04:38,400
potentially bypass the right protection

130
00:04:36,320 --> 00:04:40,560
but to understand that you need to

131
00:04:38,400 --> 00:04:42,720
understand flash writing and to

132
00:04:40,560 --> 00:04:45,440
understand that you need to understand

133
00:04:42,720 --> 00:04:48,400
what's called the spy bar spy base

134
00:04:45,440 --> 00:04:49,919
address registers memory mapped i o so

135
00:04:48,400 --> 00:04:52,000
some information about where some

136
00:04:49,919 --> 00:04:52,960
particular memory mapped i o registers

137
00:04:52,000 --> 00:04:54,720
exist

138
00:04:52,960 --> 00:04:57,440
well to understand that you have to

139
00:04:54,720 --> 00:04:59,919
understand things like pcie lpc

140
00:04:57,440 --> 00:05:02,080
controller configuration address space

141
00:04:59,919 --> 00:05:06,160
and also potentially the dram controller

142
00:05:02,080 --> 00:05:07,680
or spi controller config dress space

143
00:05:06,160 --> 00:05:10,080
to understand those you need to

144
00:05:07,680 --> 00:05:10,800
understand chipsets and memory mapped i

145
00:05:10,080 --> 00:05:12,720
o

146
00:05:10,800 --> 00:05:14,560
you also have to understand of course

147
00:05:12,720 --> 00:05:16,639
things like pci configuration address

148
00:05:14,560 --> 00:05:18,560
space but while we're there on that

149
00:05:16,639 --> 00:05:21,039
topic we may as well learn about another

150
00:05:18,560 --> 00:05:22,240
different type of attack pci option rom

151
00:05:21,039 --> 00:05:24,080
attacks

152
00:05:22,240 --> 00:05:26,240
and then furthermore you have to learn

153
00:05:24,080 --> 00:05:28,400
about things like port io memory mapped

154
00:05:26,240 --> 00:05:31,039
io to really understand how pci config

155
00:05:28,400 --> 00:05:33,919
address space is accessed and how the

156
00:05:31,039 --> 00:05:36,560
system sets up the overall memory map so

157
00:05:33,919 --> 00:05:38,160
all of this information is necessary to

158
00:05:36,560 --> 00:05:40,880
make our way to the flash right

159
00:05:38,160 --> 00:05:42,880
protection and the attacks they're upon

160
00:05:40,880 --> 00:05:44,880
but we also in this class want to learn

161
00:05:42,880 --> 00:05:47,039
about system management mode right

162
00:05:44,880 --> 00:05:48,720
protection and the attacks on that which

163
00:05:47,039 --> 00:05:50,720
requires us learning about things like

164
00:05:48,720 --> 00:05:52,479
system management ram which also

165
00:05:50,720 --> 00:05:55,280
requires the knowledge of the memory

166
00:05:52,479 --> 00:05:56,880
mapped configuration and it requires us

167
00:05:55,280 --> 00:05:58,080
to learn about system management mode

168
00:05:56,880 --> 00:05:59,680
itself

169
00:05:58,080 --> 00:06:01,680
and then after you learn all of that

170
00:05:59,680 --> 00:06:03,039
stuff and all of those different write

171
00:06:01,680 --> 00:06:05,520
protection things and all the different

172
00:06:03,039 --> 00:06:08,400
attacks then we also have to learn about

173
00:06:05,520 --> 00:06:10,800
acpi s3 which is typically referred to

174
00:06:08,400 --> 00:06:13,280
as sleep it's a lower lower power state

175
00:06:10,800 --> 00:06:16,080
of the processor and how that can

176
00:06:13,280 --> 00:06:18,400
actually undercut the defenses against

177
00:06:16,080 --> 00:06:20,000
these attacks so you've got some right

178
00:06:18,400 --> 00:06:21,360
protection mechanisms you've got some

179
00:06:20,000 --> 00:06:23,199
attacks against them you've got some

180
00:06:21,360 --> 00:06:26,400
counter measures defenses against the

181
00:06:23,199 --> 00:06:28,720
attacks but then things like acpi s3

182
00:06:26,400 --> 00:06:30,800
sleep can come along and cause even

183
00:06:28,720 --> 00:06:32,639
further attacks so that's what we're

184
00:06:30,800 --> 00:06:34,400
going to be covering in this class it's

185
00:06:32,639 --> 00:06:36,240
a whole lot of information that's why i

186
00:06:34,400 --> 00:06:37,840
made this nice little map you should

187
00:06:36,240 --> 00:06:39,680
think of it like

188
00:06:37,840 --> 00:06:41,120
when you're playing a video game and

189
00:06:39,680 --> 00:06:43,199
there's you know the fog of war

190
00:06:41,120 --> 00:06:45,120
something like starcraft warcraft diablo

191
00:06:43,199 --> 00:06:46,800
whatever you've got the fog of war it's

192
00:06:45,120 --> 00:06:48,960
all you know black space and you don't

193
00:06:46,800 --> 00:06:50,800
know exactly you know what's where and

194
00:06:48,960 --> 00:06:53,599
anything like that well i'm going to

195
00:06:50,800 --> 00:06:55,039
guide you through this particular path

196
00:06:53,599 --> 00:06:56,800
in order to understand this particular

197
00:06:55,039 --> 00:06:59,360
information but we're really just you

198
00:06:56,800 --> 00:07:01,759
know skimming by cities and villages and

199
00:06:59,360 --> 00:07:04,240
i expect you to go back after class and

200
00:07:01,759 --> 00:07:06,240
go get to know the locals go read the

201
00:07:04,240 --> 00:07:09,360
fun manual and understand this

202
00:07:06,240 --> 00:07:09,360
information better


1
00:00:00,160 --> 00:00:03,679
okay so this is what our threat tree

2
00:00:01,760 --> 00:00:06,000
looks like so far what else do we have

3
00:00:03,679 --> 00:00:08,000
as ways to potentially attack the

4
00:00:06,000 --> 00:00:10,160
protected range registers let's assume

5
00:00:08,000 --> 00:00:12,320
that the bios maker set flock down so

6
00:00:10,160 --> 00:00:14,000
now the attacker needs a new way around

7
00:00:12,320 --> 00:00:15,679
well the next thing is that they could

8
00:00:14,000 --> 00:00:18,240
potentially exploit protected range

9
00:00:15,679 --> 00:00:20,640
register misconfiguration

10
00:00:18,240 --> 00:00:22,640
and the defense for that is that the

11
00:00:20,640 --> 00:00:24,480
biospender needs to very carefully

12
00:00:22,640 --> 00:00:26,560
analyze their range coverage and make

13
00:00:24,480 --> 00:00:29,119
sure that it is actually correct

14
00:00:26,560 --> 00:00:31,679
so an example of this would be that

15
00:00:29,119 --> 00:00:33,920
in the thunderstrike 2 work we had

16
00:00:31,679 --> 00:00:35,760
looked at some apple systems trying to

17
00:00:33,920 --> 00:00:37,200
understand what applied and what didn't

18
00:00:35,760 --> 00:00:39,520
and we looked to try to understand

19
00:00:37,200 --> 00:00:41,760
whether speed racer which was a

20
00:00:39,520 --> 00:00:43,360
race condition vulnerability applied and

21
00:00:41,760 --> 00:00:45,120
it didn't exactly apply because the

22
00:00:43,360 --> 00:00:47,200
thing that it bypassed was just straight

23
00:00:45,120 --> 00:00:49,360
up not there but there were protected

24
00:00:47,200 --> 00:00:51,440
range registers and there were two gaps

25
00:00:49,360 --> 00:00:54,079
in the protected range register regions

26
00:00:51,440 --> 00:00:56,719
one of which covered the efi variables

27
00:00:54,079 --> 00:00:59,760
and the other just covered who knows

28
00:00:56,719 --> 00:01:01,280
what and uefa tool called it padding but

29
00:00:59,760 --> 00:01:02,239
you know we really can't say what that

30
00:01:01,280 --> 00:01:04,080
was

31
00:01:02,239 --> 00:01:06,560
so the interesting thing was that you

32
00:01:04,080 --> 00:01:08,560
know if we smashed all of this there was

33
00:01:06,560 --> 00:01:10,799
no problem there apple actually designs

34
00:01:08,560 --> 00:01:12,400
their systems to be able to reset all of

35
00:01:10,799 --> 00:01:14,320
the efi variables

36
00:01:12,400 --> 00:01:16,080
they actually have a command shortcut

37
00:01:14,320 --> 00:01:18,240
that you can you know hold as you're

38
00:01:16,080 --> 00:01:21,680
booting the system and it'll zap the

39
00:01:18,240 --> 00:01:24,320
nvram zap the pram back in the powerpc

40
00:01:21,680 --> 00:01:26,479
days back in the open firmware days

41
00:01:24,320 --> 00:01:27,759
so they're designed against defense

42
00:01:26,479 --> 00:01:29,680
against that so they don't really care

43
00:01:27,759 --> 00:01:31,680
if that goes away now most biospenders

44
00:01:29,680 --> 00:01:33,600
if you zap their variables they're

45
00:01:31,680 --> 00:01:36,079
almost certainly going to break but not

46
00:01:33,600 --> 00:01:37,840
apple but zapping this

47
00:01:36,079 --> 00:01:39,439
actually led to just straight up

48
00:01:37,840 --> 00:01:40,400
breaking the system the system would not

49
00:01:39,439 --> 00:01:42,320
boot anymore

50
00:01:40,400 --> 00:01:44,399
and so that became you know a sort of

51
00:01:42,320 --> 00:01:46,000
permanent dos attack which again we said

52
00:01:44,399 --> 00:01:47,680
is going to be extremely difficult to

53
00:01:46,000 --> 00:01:48,799
recover from if someone gets hit from

54
00:01:47,680 --> 00:01:50,960
that

55
00:01:48,799 --> 00:01:53,040
so this essentially is a research

56
00:01:50,960 --> 00:01:55,600
opportunity for those of you out there

57
00:01:53,040 --> 00:01:57,119
who want to go off and do work in this

58
00:01:55,600 --> 00:01:59,520
area so

59
00:01:57,119 --> 00:02:01,600
you can analyze whether or not a vendor

60
00:01:59,520 --> 00:02:03,040
has protected range register set that's

61
00:02:01,600 --> 00:02:05,920
easy you know we'll see that with

62
00:02:03,040 --> 00:02:07,920
chipset but analyzing whether or not the

63
00:02:05,920 --> 00:02:09,440
range is correct is an entirely

64
00:02:07,920 --> 00:02:11,760
different matter you have to understand

65
00:02:09,440 --> 00:02:13,760
what sort of code and data is covered or

66
00:02:11,760 --> 00:02:15,520
not covered so i think there's plenty of

67
00:02:13,760 --> 00:02:16,959
opportunities for research in that

68
00:02:15,520 --> 00:02:18,959
particular area

69
00:02:16,959 --> 00:02:20,560
well let's assume that they had done

70
00:02:18,959 --> 00:02:22,319
their due diligence and carefully

71
00:02:20,560 --> 00:02:24,800
analyzed and covered the correct ranges

72
00:02:22,319 --> 00:02:26,640
what else could an attacker do

73
00:02:24,800 --> 00:02:28,400
well they could use sleep wake

74
00:02:26,640 --> 00:02:30,800
vulnerabilities but we're not going to

75
00:02:28,400 --> 00:02:33,360
talk about that until later in the class

76
00:02:30,800 --> 00:02:35,120
but i'll just say as a call forward

77
00:02:33,360 --> 00:02:37,040
sleep wake vulnerabilities can actually

78
00:02:35,120 --> 00:02:39,120
just straight up unset the protected

79
00:02:37,040 --> 00:02:41,519
range registers they just magically go

80
00:02:39,120 --> 00:02:43,680
away so that's pretty interesting

81
00:02:41,519 --> 00:02:45,280
what else could an attacker do if they

82
00:02:43,680 --> 00:02:46,480
wanted to bypass protected range

83
00:02:45,280 --> 00:02:48,959
registers

84
00:02:46,480 --> 00:02:50,800
well they could exploit a normal memory

85
00:02:48,959 --> 00:02:53,040
corruption vulnerability in the bios

86
00:02:50,800 --> 00:02:55,280
before the lock is set before flock down

87
00:02:53,040 --> 00:02:57,440
is set so basically as the code is

88
00:02:55,280 --> 00:02:59,200
running in the bios at early boot time

89
00:02:57,440 --> 00:03:01,360
there has to be some period of time

90
00:02:59,200 --> 00:03:04,159
between reset when the locks come

91
00:03:01,360 --> 00:03:06,560
unlocked and the code executing and

92
00:03:04,159 --> 00:03:08,319
locking things down so in that time

93
00:03:06,560 --> 00:03:10,080
window if there is any attacker

94
00:03:08,319 --> 00:03:12,560
controlled inputs introduced into the

95
00:03:10,080 --> 00:03:15,120
system that can cause a typical memory

96
00:03:12,560 --> 00:03:16,400
corruption vulnerability well then if

97
00:03:15,120 --> 00:03:18,319
the attacker gets arbitrary code

98
00:03:16,400 --> 00:03:20,239
execution before this gets set then of

99
00:03:18,319 --> 00:03:22,480
course they can you know just ensure

100
00:03:20,239 --> 00:03:24,080
that that never gets set whatsoever and

101
00:03:22,480 --> 00:03:26,239
rewrite the bios

102
00:03:24,080 --> 00:03:28,319
so the defense against that is you know

103
00:03:26,239 --> 00:03:30,000
just audit all attack surfaces exposed

104
00:03:28,319 --> 00:03:32,319
before the lock is set and remove all

105
00:03:30,000 --> 00:03:33,440
the vulnerabilities sounds easy right

106
00:03:32,319 --> 00:03:36,000
cory

107
00:03:33,440 --> 00:03:38,000
and the mitigation for that is the

108
00:03:36,000 --> 00:03:40,159
typical exploit mitigations things like

109
00:03:38,000 --> 00:03:42,080
stack cookies non-executable heap and

110
00:03:40,159 --> 00:03:43,840
stack and so forth

111
00:03:42,080 --> 00:03:45,680
so this is what our threat tree looks

112
00:03:43,840 --> 00:03:48,239
like specifically for protected range

113
00:03:45,680 --> 00:03:50,319
registers as a defensive mechanism so in

114
00:03:48,239 --> 00:03:52,400
summary i believe that prrs and

115
00:03:50,319 --> 00:03:54,080
flockdown is the most important strategy

116
00:03:52,400 --> 00:03:56,319
for locking down the bios it's really

117
00:03:54,080 --> 00:03:58,000
the most powerful one that can't just be

118
00:03:56,319 --> 00:04:00,400
worked around and doesn't have a extra

119
00:03:58,000 --> 00:04:02,080
large attack surface on it like the next

120
00:04:00,400 --> 00:04:04,080
one that we're going to learn about

121
00:04:02,080 --> 00:04:05,760
because without protective range

122
00:04:04,080 --> 00:04:08,319
registers you would have to rely on this

123
00:04:05,760 --> 00:04:10,000
next one which depends on the system

124
00:04:08,319 --> 00:04:11,920
management mode and system management

125
00:04:10,000 --> 00:04:13,200
mode has a very large attack surface

126
00:04:11,920 --> 00:04:15,680
it's important to know that protected

127
00:04:13,200 --> 00:04:18,079
range registers are more strict than

128
00:04:15,680 --> 00:04:20,000
other things so things like flash flash

129
00:04:18,079 --> 00:04:22,639
master permissions from the optional

130
00:04:20,000 --> 00:04:24,960
material about the flash descriptor the

131
00:04:22,639 --> 00:04:26,720
protected range trumps those so even if

132
00:04:24,960 --> 00:04:28,479
the flash master says that something is

133
00:04:26,720 --> 00:04:30,479
allowed to read or write somewhere if

134
00:04:28,479 --> 00:04:32,560
the protected range says no then it

135
00:04:30,479 --> 00:04:34,160
means they're not allowed to

136
00:04:32,560 --> 00:04:36,960
but there's an interesting little side

137
00:04:34,160 --> 00:04:39,280
effect to the idea if the vendor uses

138
00:04:36,960 --> 00:04:40,880
protect range registers it's going to

139
00:04:39,280 --> 00:04:43,520
actually make them

140
00:04:40,880 --> 00:04:45,680
have to do firmware updates bios updates

141
00:04:43,520 --> 00:04:48,080
after reset because they set these

142
00:04:45,680 --> 00:04:49,600
registers and then they lock them down

143
00:04:48,080 --> 00:04:51,919
and then the register doesn't come

144
00:04:49,600 --> 00:04:53,680
unlocked until the system resets so

145
00:04:51,919 --> 00:04:56,400
otherwise how are you going to do a bios

146
00:04:53,680 --> 00:04:59,040
update you have to wait until you reset

147
00:04:56,400 --> 00:05:01,759
and then the existing bios has to take a

148
00:04:59,040 --> 00:05:03,840
bios update payload and apply that and

149
00:05:01,759 --> 00:05:06,479
write it down to the flash chip before

150
00:05:03,840 --> 00:05:08,880
the flash becomes locked again so that

151
00:05:06,479 --> 00:05:10,960
makes the flash update process itself

152
00:05:08,880 --> 00:05:13,680
become an interesting attack surface in

153
00:05:10,960 --> 00:05:14,800
the context of the window of time in

154
00:05:13,680 --> 00:05:17,199
which

155
00:05:14,800 --> 00:05:20,000
the flash is unlocked so let's go back

156
00:05:17,199 --> 00:05:21,759
to our threat diagram here for a second

157
00:05:20,000 --> 00:05:23,360
and so i said oh just you know exploit

158
00:05:21,759 --> 00:05:25,520
the bios before a lock is set and you

159
00:05:23,360 --> 00:05:27,360
might have thought to yourself oh that's

160
00:05:25,520 --> 00:05:29,120
dumb like why would they be consuming

161
00:05:27,360 --> 00:05:31,600
any attacker-controlled data before the

162
00:05:29,120 --> 00:05:33,039
lock is set well one example of why they

163
00:05:31,600 --> 00:05:34,880
would have to consume

164
00:05:33,039 --> 00:05:36,960
attacker-controlled data would be the

165
00:05:34,880 --> 00:05:39,600
bios update itself because it must be

166
00:05:36,960 --> 00:05:41,840
processed before the lock is set and it

167
00:05:39,600 --> 00:05:44,320
can be extremely complicated as you know

168
00:05:41,840 --> 00:05:46,240
my colleague corey showed when he has

169
00:05:44,320 --> 00:05:48,960
presented multiple

170
00:05:46,240 --> 00:05:51,120
attacks using the bios update process in

171
00:05:48,960 --> 00:05:53,360
order to exploit a bios and consequently

172
00:05:51,120 --> 00:05:55,600
take over now at the end of the day only

173
00:05:53,360 --> 00:05:57,600
a bios vendor themselves can really

174
00:05:55,600 --> 00:05:59,919
reliably determine whether or not it's

175
00:05:57,600 --> 00:06:01,039
safe to cover everything or not cover

176
00:05:59,919 --> 00:06:02,800
everything with protected range

177
00:06:01,039 --> 00:06:04,720
registers that's why i said you know

178
00:06:02,800 --> 00:06:07,440
this is a interesting area potentially

179
00:06:04,720 --> 00:06:09,199
for research and in general when you

180
00:06:07,440 --> 00:06:10,880
start looking at protected ranges you

181
00:06:09,199 --> 00:06:13,360
will see quickly that there's going to

182
00:06:10,880 --> 00:06:15,120
be gaps and there'll be gaps like i

183
00:06:13,360 --> 00:06:18,319
showed in that picture for the apple

184
00:06:15,120 --> 00:06:20,400
systems typically around efi and vram

185
00:06:18,319 --> 00:06:22,479
variables so there's a typically an

186
00:06:20,400 --> 00:06:25,600
expectation that those efi nvram

187
00:06:22,479 --> 00:06:27,280
variables are writable at runtime and so

188
00:06:25,600 --> 00:06:28,960
there's an expectation that for instance

189
00:06:27,280 --> 00:06:31,199
you would update them to say hey i want

190
00:06:28,960 --> 00:06:33,440
to boot something else and that's would

191
00:06:31,199 --> 00:06:34,880
be a boot choice that you would make

192
00:06:33,440 --> 00:06:36,560
from within an operating system

193
00:06:34,880 --> 00:06:38,479
potentially rather than you know just

194
00:06:36,560 --> 00:06:39,520
sitting in a bios configuration screen

195
00:06:38,479 --> 00:06:41,520
somewhere

196
00:06:39,520 --> 00:06:43,919
so anything that needs to be run time

197
00:06:41,520 --> 00:06:46,560
writable by definition cannot be covered

198
00:06:43,919 --> 00:06:48,800
by protected range registers beyond that

199
00:06:46,560 --> 00:06:50,639
bios is unfortunately just keep getting

200
00:06:48,800 --> 00:06:52,720
bigger over time and that means the

201
00:06:50,639 --> 00:06:55,199
range that needs to be covered over time

202
00:06:52,720 --> 00:06:57,680
needs to expand and who knows they might

203
00:06:55,199 --> 00:07:00,319
you know leave a protected range that

204
00:06:57,680 --> 00:07:02,240
doesn't cover the full range once the

205
00:07:00,319 --> 00:07:05,599
bios expands and you know grows by four

206
00:07:02,240 --> 00:07:05,599
kilobytes for instance


1
00:00:00,44 --> 00:00:02,11
Congratulations,

2
00:00:02,11 --> 00:00:03,02
you did it

3
00:00:03,03 --> 00:00:05,17
You survived getting nitty gritty

4
00:00:05,23 --> 00:00:07,26
So let me be the first to welcome you to

5
00:00:07,26 --> 00:00:08,13
the gritty gang

6
00:00:08,16 --> 00:00:10,35
Traditionally upon induction to the gritty gang,

7
00:00:10,35 --> 00:00:12,67
it's recommended that you get a tattoo right there on

8
00:00:12,67 --> 00:00:16,05
your shoulder and then people will ask you throughout the

9
00:00:16,05 --> 00:00:17,2
rest of your life,

10
00:00:17,21 --> 00:00:17,59
oh,

11
00:00:17,59 --> 00:00:20,12
you must be a really big philadelphia flyers fan

12
00:00:20,29 --> 00:00:22,11
And you say no,

13
00:00:22,12 --> 00:00:22,96
not really

14
00:00:23,16 --> 00:00:26,76
It's just that I attended osd to Architecture 4001 and

15
00:00:26,76 --> 00:00:30,55
survived parsing the flash descriptor region for a Dell optiplex

16
00:00:30,55 --> 00:00:32,66
70 10 and they'll say,

17
00:00:32,67 --> 00:00:33,16
whoa,

18
00:00:33,94 --> 00:00:35,05
that's pretty gritty


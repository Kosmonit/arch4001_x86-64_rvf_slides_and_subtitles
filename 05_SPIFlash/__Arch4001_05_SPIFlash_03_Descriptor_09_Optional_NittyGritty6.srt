1
00:00:00,160 --> 00:00:05,600
so we said the oem section is 256 bytes

2
00:00:03,679 --> 00:00:09,200
at the end of the flash descriptor flash

3
00:00:05,600 --> 00:00:11,840
descriptor is fff for the last byte so

4
00:00:09,200 --> 00:00:13,759
this is the data we said the vendor can

5
00:00:11,840 --> 00:00:15,920
use that however they want but you can

6
00:00:13,759 --> 00:00:17,920
clearly see that dell chose not to put

7
00:00:15,920 --> 00:00:19,199
anything in particular in there

8
00:00:17,920 --> 00:00:21,039
but it's actually an interesting

9
00:00:19,199 --> 00:00:23,279
question so what if a vendor did put

10
00:00:21,039 --> 00:00:25,359
something in there so i consider this a

11
00:00:23,279 --> 00:00:28,320
little bit of a research opportunity if

12
00:00:25,359 --> 00:00:32,000
you ever find a flash dump that has

13
00:00:28,320 --> 00:00:34,079
something non-fs or non-zeros in the oem

14
00:00:32,000 --> 00:00:35,840
section well reverse engineer things

15
00:00:34,079 --> 00:00:37,120
look into it figure out what's going on

16
00:00:35,840 --> 00:00:40,120
there there might be something

17
00:00:37,120 --> 00:00:40,120
interesting


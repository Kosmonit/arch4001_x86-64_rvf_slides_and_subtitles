1
00:00:00,080 --> 00:00:06,319
all right on to the flop map flip map

2
00:00:02,800 --> 00:00:08,480
one the descriptor upper map which

3
00:00:06,319 --> 00:00:12,080
points at information down here most

4
00:00:08,480 --> 00:00:12,960
notably the management engine vscc table

5
00:00:12,080 --> 00:00:16,960
so

6
00:00:12,960 --> 00:00:19,039
at fdbar plus efc that's hard coded

7
00:00:16,960 --> 00:00:22,160
always efc and it's not actually going

8
00:00:19,039 --> 00:00:25,359
to be 10 plus efc here it's just efc

9
00:00:22,160 --> 00:00:27,599
there is this register flop map one so

10
00:00:25,359 --> 00:00:30,519
how do we interpret that once again bits

11
00:00:27,599 --> 00:00:34,480
24 through 12. so this means

12
00:00:30,519 --> 00:00:37,280
df0 is going to be where the data is

13
00:00:34,480 --> 00:00:41,120
and how much data is it well that's in

14
00:00:37,280 --> 00:00:44,559
the next field the vscc table length so

15
00:00:41,120 --> 00:00:46,960
that is 2c d word so a total of

16
00:00:44,559 --> 00:00:50,079
b zero bytes and you can see that right

17
00:00:46,960 --> 00:00:51,760
there amidst all of the other f's

18
00:00:50,079 --> 00:00:52,879
so how do we interpret all of those

19
00:00:51,760 --> 00:00:55,440
bytes

20
00:00:52,879 --> 00:00:59,120
well that's management engine vendor

21
00:00:55,440 --> 00:01:02,960
specific component capabilities table

22
00:00:59,120 --> 00:01:05,119
and so the first thing jdec id 0

23
00:01:02,960 --> 00:01:06,880
so that is just a sort of industry

24
00:01:05,119 --> 00:01:09,439
standard id and you can see that it's

25
00:01:06,880 --> 00:01:12,080
broken up into a vendor id a device id 0

26
00:01:09,439 --> 00:01:14,240
and a device id 1. well conveniently

27
00:01:12,080 --> 00:01:16,080
uefi tool has already parsed that out

28
00:01:14,240 --> 00:01:18,880
for us and it's telling us that's a

29
00:01:16,080 --> 00:01:19,920
winbond w25x16

30
00:01:18,880 --> 00:01:22,320
chip

31
00:01:19,920 --> 00:01:24,960
then the next four bytes takes a bit of

32
00:01:22,320 --> 00:01:26,720
interpretation so big long register

33
00:01:24,960 --> 00:01:27,600
let's look at the least significant four

34
00:01:26,720 --> 00:01:29,920
bits

35
00:01:27,600 --> 00:01:32,079
all right this is five in the least

36
00:01:29,920 --> 00:01:33,920
significant four bits so that is zero

37
00:01:32,079 --> 00:01:36,799
one zero one

38
00:01:33,920 --> 00:01:39,360
and lowest least significant two bits

39
00:01:36,799 --> 00:01:42,960
zero one says that the upper block

40
00:01:39,360 --> 00:01:46,399
sector array size is four kilobytes

41
00:01:42,960 --> 00:01:49,600
then the upper right granularity is one

42
00:01:46,399 --> 00:01:51,520
byte and the upper right status required

43
00:01:49,600 --> 00:01:53,360
has to do with some little protocol so

44
00:01:51,520 --> 00:01:55,360
if it's zero there's no special protocol

45
00:01:53,360 --> 00:01:57,360
and if there's one then it says you have

46
00:01:55,360 --> 00:02:00,240
to write a zero to the spy flash status

47
00:01:57,360 --> 00:02:02,240
register prior to the write and erase to

48
00:02:00,240 --> 00:02:04,000
unlock the flash component blah blah we

49
00:02:02,240 --> 00:02:05,759
don't care it's zero right here so

50
00:02:04,000 --> 00:02:07,439
whatever now the only other point i'll

51
00:02:05,759 --> 00:02:09,440
make here is that it's going to refer to

52
00:02:07,439 --> 00:02:12,000
upper here and lower later on this has

53
00:02:09,440 --> 00:02:14,400
to do with those flash partitions

54
00:02:12,000 --> 00:02:16,879
so we saw the upper right status then on

55
00:02:14,400 --> 00:02:17,920
to the reserved that is zero so that's

56
00:02:16,879 --> 00:02:21,040
fine

57
00:02:17,920 --> 00:02:23,920
then the upper erase op code so up code

58
00:02:21,040 --> 00:02:26,080
20 when sent to the so hex 20 when sent

59
00:02:23,920 --> 00:02:28,480
to the spy flash chip for this

60
00:02:26,080 --> 00:02:31,200
particular chip for this particular id

61
00:02:28,480 --> 00:02:34,239
would be the erase op code

62
00:02:31,200 --> 00:02:36,959
then we have the lower block sector

63
00:02:34,239 --> 00:02:39,840
array size lower right granularity lower

64
00:02:36,959 --> 00:02:43,440
right status required etc and once again

65
00:02:39,840 --> 00:02:47,120
zero one so this is four kilobyte erase

66
00:02:43,440 --> 00:02:48,879
zero uh one so this is a 64 byte

67
00:02:47,120 --> 00:02:50,480
right granularity

68
00:02:48,879 --> 00:02:52,480
i think i said that incorrectly on the

69
00:02:50,480 --> 00:02:54,160
previous one and then zero saying it

70
00:02:52,480 --> 00:02:57,040
does not have any requirements to write

71
00:02:54,160 --> 00:03:00,400
to the status register prior to a write

72
00:02:57,040 --> 00:03:02,480
then again this is four bits of zeros so

73
00:03:00,400 --> 00:03:05,280
no lower right enable status blah blah

74
00:03:02,480 --> 00:03:07,040
blah and reserved and the lower eraseop

75
00:03:05,280 --> 00:03:08,640
code is 20.

76
00:03:07,040 --> 00:03:10,080
so then it's just going to be the same

77
00:03:08,640 --> 00:03:13,440
thing over and over again you've got

78
00:03:10,080 --> 00:03:16,840
another jdec id which points at this in

79
00:03:13,440 --> 00:03:19,599
uefi tool and then another

80
00:03:16,840 --> 00:03:21,040
vscc entry now if you scroll down

81
00:03:19,599 --> 00:03:23,120
through all these you see that they all

82
00:03:21,040 --> 00:03:25,360
look mostly the same and the same thing

83
00:03:23,120 --> 00:03:27,120
over here too they look mostly the same

84
00:03:25,360 --> 00:03:30,640
but there is occasionally these

85
00:03:27,120 --> 00:03:32,480
instances of 15 instead of 0 5 right so

86
00:03:30,640 --> 00:03:34,640
what's the actual difference there well

87
00:03:32,480 --> 00:03:36,319
that has to do with those particular

88
00:03:34,640 --> 00:03:38,799
flash chip parts that have those

89
00:03:36,319 --> 00:03:41,200
particular ids if they have this as

90
00:03:38,799 --> 00:03:43,760
their vendor specific capabilities

91
00:03:41,200 --> 00:03:46,080
then it is one meaning that you have to

92
00:03:43,760 --> 00:03:48,480
write some zero to a fly spy flash

93
00:03:46,080 --> 00:03:50,879
status register before you do a right

94
00:03:48,480 --> 00:03:53,200
and race so it's basically just saying

95
00:03:50,879 --> 00:03:56,159
there's some standard little dance that

96
00:03:53,200 --> 00:03:58,239
is done with certain spy flash chips and

97
00:03:56,159 --> 00:04:01,040
this is one of those chips

98
00:03:58,239 --> 00:04:02,959
and boom we are done looking through all

99
00:04:01,040 --> 00:04:04,879
of those data structures that was so

100
00:04:02,959 --> 00:04:06,480
much fun right and i'm sure you went

101
00:04:04,879 --> 00:04:09,439
through all of that you know very

102
00:04:06,480 --> 00:04:11,519
rigorously as you were doing the lab so

103
00:04:09,439 --> 00:04:13,280
what do we come away with from this we

104
00:04:11,519 --> 00:04:14,720
learned that there's two components one

105
00:04:13,280 --> 00:04:17,280
of which was eight megabytes one of

106
00:04:14,720 --> 00:04:19,199
which was four megabytes and we knew

107
00:04:17,280 --> 00:04:21,280
that the partition between those was on

108
00:04:19,199 --> 00:04:23,520
the eight megabyte boundary here and so

109
00:04:21,280 --> 00:04:25,919
the flash descriptor is hex 1000 as

110
00:04:23,520 --> 00:04:28,960
usual and then we have the gigabit from

111
00:04:25,919 --> 00:04:33,040
1000 to 4ff then we have the management

112
00:04:28,960 --> 00:04:35,120
engine from hex 5000 up to 5 and 5fs

113
00:04:33,040 --> 00:04:36,960
and then the bios region starting at 6

114
00:04:35,120 --> 00:04:39,759
megabytes going all the way up to 12

115
00:04:36,960 --> 00:04:39,759
megabytes


1
00:00:00,320 --> 00:00:04,880
and then there's one more known smi

2
00:00:02,480 --> 00:00:08,160
suppression attack the ascenter sandman

3
00:00:04,880 --> 00:00:10,960
attack and this uses intel txt in order

4
00:00:08,160 --> 00:00:13,360
to suppress smis so intel txt is

5
00:00:10,960 --> 00:00:15,360
supposed to be a defensive technology

6
00:00:13,360 --> 00:00:16,640
but in this particular case because the

7
00:00:15,360 --> 00:00:19,119
attacker wants something that can

8
00:00:16,640 --> 00:00:21,199
suppress smi's it does fit the bill

9
00:00:19,119 --> 00:00:23,600
there and the countermeasure against

10
00:00:21,199 --> 00:00:25,039
this would again be using intel bioscard

11
00:00:23,600 --> 00:00:27,039
which would say you know i don't care if

12
00:00:25,039 --> 00:00:29,279
you suppress smi's if you're not an

13
00:00:27,039 --> 00:00:31,920
authenticated code module running an smm

14
00:00:29,279 --> 00:00:34,880
you don't get to write to the bios and

15
00:00:31,920 --> 00:00:37,040
the other way is that txt has a notion

16
00:00:34,880 --> 00:00:39,040
of launch control policies that are

17
00:00:37,040 --> 00:00:41,760
basically restrictions to say like you

18
00:00:39,040 --> 00:00:43,200
know this txt code can run and this

19
00:00:41,760 --> 00:00:45,120
other code cannot

20
00:00:43,200 --> 00:00:47,760
or you know on many systems that don't

21
00:00:45,120 --> 00:00:49,840
use txt just straight up disabling txt

22
00:00:47,760 --> 00:00:51,520
would be a way to deal with this

23
00:00:49,840 --> 00:00:53,760
of course that would not be recommended

24
00:00:51,520 --> 00:00:56,480
these days because microsoft is moving

25
00:00:53,760 --> 00:00:59,039
in the direction of using txt to

26
00:00:56,480 --> 00:01:01,359
increase the security of the boot of

27
00:00:59,039 --> 00:01:03,120
their virtualization based system so you

28
00:01:01,359 --> 00:01:05,519
don't want to disable it might want to

29
00:01:03,120 --> 00:01:07,119
use a launch control policy

30
00:01:05,519 --> 00:01:09,200
but i don't know exactly how that's

31
00:01:07,119 --> 00:01:11,040
going to work with you know whether

32
00:01:09,200 --> 00:01:12,560
microsoft will allow you to use their

33
00:01:11,040 --> 00:01:14,799
secure launch thing while still being

34
00:01:12,560 --> 00:01:16,720
able to launch extra stuff i don't know

35
00:01:14,799 --> 00:01:18,720
haven't looked into it so i've been

36
00:01:16,720 --> 00:01:20,640
looking at txt for some other work

37
00:01:18,720 --> 00:01:23,040
copernicus 2 to create a more

38
00:01:20,640 --> 00:01:24,799
trustworthy bios measurement system and

39
00:01:23,040 --> 00:01:27,439
i noticed in the documentation that it

40
00:01:24,799 --> 00:01:29,280
said the launch code must re-enable sms

41
00:01:27,439 --> 00:01:31,439
which were disabled as part of the s

42
00:01:29,280 --> 00:01:34,000
enter process so that was interesting

43
00:01:31,439 --> 00:01:36,000
that sort of told me that txt is

44
00:01:34,000 --> 00:01:37,200
disabling sms

45
00:01:36,000 --> 00:01:39,040
but you know

46
00:01:37,200 --> 00:01:41,200
once i read a little further it said you

47
00:01:39,040 --> 00:01:43,840
know newer cpus may automatically

48
00:01:41,200 --> 00:01:46,079
re-enable sms on enter entry to the

49
00:01:43,840 --> 00:01:47,439
measured launch environment so then

50
00:01:46,079 --> 00:01:48,960
there was you know cut out a bunch of

51
00:01:47,439 --> 00:01:50,560
slides here and i dug around trying to

52
00:01:48,960 --> 00:01:52,560
find documentation about which things

53
00:01:50,560 --> 00:01:55,680
behave which way and eventually i gave

54
00:01:52,560 --> 00:01:58,799
up and had to ask intel and intel said

55
00:01:55,680 --> 00:02:00,960
well nekolim so first generation

56
00:01:58,799 --> 00:02:04,159
core series and newer are just going to

57
00:02:00,960 --> 00:02:06,159
re-enable sms automatically so that was

58
00:02:04,159 --> 00:02:08,800
not particularly helpful for my attack

59
00:02:06,159 --> 00:02:10,879
and i subsequently concluded well this

60
00:02:08,800 --> 00:02:12,640
sms suppression via txt probably only

61
00:02:10,879 --> 00:02:14,560
works on older systems that we don't

62
00:02:12,640 --> 00:02:17,440
really care about anymore

63
00:02:14,560 --> 00:02:19,599
now that said as i was recently you know

64
00:02:17,440 --> 00:02:21,200
looking around and trying to spin up a

65
00:02:19,599 --> 00:02:23,440
little bit more on how microsoft's

66
00:02:21,200 --> 00:02:25,040
secure launch worked they had this blog

67
00:02:23,440 --> 00:02:27,360
post which you know talked about how

68
00:02:25,040 --> 00:02:28,640
they use txt to increase the security

69
00:02:27,360 --> 00:02:30,480
and that's good but they have this

70
00:02:28,640 --> 00:02:32,959
picture here that showed this

71
00:02:30,480 --> 00:02:34,879
interesting sms allowed and then the

72
00:02:32,959 --> 00:02:38,080
dynamic root of trust for measurement

73
00:02:34,879 --> 00:02:40,640
event meaning txt launch so txt interest

74
00:02:38,080 --> 00:02:43,599
computing base is launched and the smis

75
00:02:40,640 --> 00:02:45,840
are like suppressed here and then smis

76
00:02:43,599 --> 00:02:47,599
are allowed thereafter

77
00:02:45,840 --> 00:02:49,440
so it seems like there's still you know

78
00:02:47,599 --> 00:02:51,120
some sort of smi suppression going on

79
00:02:49,440 --> 00:02:54,239
and the real question is just like is

80
00:02:51,120 --> 00:02:56,640
this automatic smi re-enablement is this

81
00:02:54,239 --> 00:02:58,560
maybe configurable so you know that

82
00:02:56,640 --> 00:03:01,200
bears further investigation

83
00:02:58,560 --> 00:03:03,200
and just in general this is a potential

84
00:03:01,200 --> 00:03:05,840
research opportunity that you know are

85
00:03:03,200 --> 00:03:08,319
there other ways to suppress sms

86
00:03:05,840 --> 00:03:10,239
now this is a not the best research

87
00:03:08,319 --> 00:03:11,840
opportunity because these days people

88
00:03:10,239 --> 00:03:14,159
should be using protected range

89
00:03:11,840 --> 00:03:16,879
registers in addition to bios lock

90
00:03:14,159 --> 00:03:19,519
enable but i'm sure you can find systems

91
00:03:16,879 --> 00:03:21,760
out there that are not using

92
00:03:19,519 --> 00:03:23,680
protected range registers properly and

93
00:03:21,760 --> 00:03:26,000
if they're only depending on this then

94
00:03:23,680 --> 00:03:28,319
if you can find some new smi suppression

95
00:03:26,000 --> 00:03:31,280
mechanism you may be able to break into

96
00:03:28,319 --> 00:03:31,280
the bios


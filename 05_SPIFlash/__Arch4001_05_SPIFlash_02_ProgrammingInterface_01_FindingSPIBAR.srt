1
00:00:00,24 --> 00:00:01,91
Okey doke you are here,

2
00:00:01,91 --> 00:00:04,42
it's time to finally learn about SPIBAR

3
00:00:04,42 --> 00:00:04,76
MMIO

4
00:00:05,34 --> 00:00:09,38
So the intel hardware has a little sub component inside

5
00:00:09,38 --> 00:00:09,68
of it

6
00:00:09,68 --> 00:00:12,53
That is going to provide a interface to the spi

7
00:00:12,53 --> 00:00:13,26
flash chip

8
00:00:13,74 --> 00:00:15,76
So that will be baked into something like the 

9
00:00:15,76 --> 00:00:17,14
PCH or ICH

10
00:00:17,15 --> 00:00:20,45
And then it will actually communicate via the spi protocol

11
00:00:20,45 --> 00:00:22,6
to the external spi flash chip,

12
00:00:22,61 --> 00:00:23,7
which as we said before,

13
00:00:23,7 --> 00:00:25,01
it can have multiple things in it

14
00:00:25,01 --> 00:00:28,13
Not just the bios on the simplest possible view of

15
00:00:28,13 --> 00:00:28,64
the world,

16
00:00:28,64 --> 00:00:31,4
you have some tool running on the CPU like flash

17
00:00:31,4 --> 00:00:31,97
RAM chips,

18
00:00:31,97 --> 00:00:35,72
zecora vendor specific bios update er and that takes a

19
00:00:35,72 --> 00:00:40,02
bios update and just magically somehow writes it to the

20
00:00:40,02 --> 00:00:41,09
spi flash chip

21
00:00:41,1 --> 00:00:43,31
And so we're going to look behind the magic in

22
00:00:43,31 --> 00:00:44,02
this section

23
00:00:44,84 --> 00:00:45,03
So,

24
00:00:45,03 --> 00:00:48,36
a slightly less magical version would be to recognize that

25
00:00:48,36 --> 00:00:51,87
there exists some pc I config address space and that's

26
00:00:51,87 --> 00:00:54,4
going to have some sort of pointer to some other

27
00:00:54,4 --> 00:00:57,03
registers which will refer to as the SPIBAR memory

28
00:00:57,03 --> 00:00:57,54
mapped I

29
00:00:57,54 --> 00:00:57,98
O

30
00:00:57,99 --> 00:01:01,64
And that will give you your magical access to the

31
00:01:01,64 --> 00:01:02,75
spy fash memory

32
00:01:03,24 --> 00:01:06,36
But the even less magical view of the world is

33
00:01:06,36 --> 00:01:09,03
to recognize that there is something like a PCH and

34
00:01:09,03 --> 00:01:10,96
I ch are built into the sock,

35
00:01:10,97 --> 00:01:12,54
there's some spy hardware,

36
00:01:12,54 --> 00:01:16,19
sub component and all of the accesses to these Spy

37
00:01:16,19 --> 00:01:17,4
bar memory mapped I

38
00:01:17,4 --> 00:01:22,01
O registers are really mapping to registers inside of the

39
00:01:22,01 --> 00:01:25,22
spy hardware and it's not fairy magic and wizard magic

40
00:01:25,22 --> 00:01:27,21
that gets you access to the spi Flash?

41
00:01:27,25 --> 00:01:28,38
It's Elf magic,

42
00:01:28,38 --> 00:01:30,13
specifically Keebler elves

43
00:01:30,34 --> 00:01:33,57
So intel is using Keebler elves inside of its by

44
00:01:33,57 --> 00:01:37,12
hardware in order to translate requests over to the spi

45
00:01:37,12 --> 00:01:37,86
flash ship

46
00:01:38,04 --> 00:01:39,36
So let's see how they do that

47
00:01:39,84 --> 00:01:43,35
There's two forms of accessing the SPI flash hardware sequencing

48
00:01:43,35 --> 00:01:45,21
and software sequencing hardware

49
00:01:45,21 --> 00:01:47,17
Sequencing is where you basically say,

50
00:01:47,17 --> 00:01:48,72
hey hardware do your thing,

51
00:01:48,75 --> 00:01:51,81
elves give me access and you don't care about how

52
00:01:51,81 --> 00:01:53,42
exactly that works behind the scenes

53
00:01:53,44 --> 00:01:55,96
So there's going to be some sort of memory mapped

54
00:01:55,96 --> 00:01:56,14
I

55
00:01:56,14 --> 00:01:56,55
O

56
00:01:56,74 --> 00:01:59,14
And you're going to send a command request saying like

57
00:01:59,14 --> 00:02:01,36
set the address that I want to read from two

58
00:02:01,37 --> 00:02:03,85
hex 1000 in the spi flash chip

59
00:02:04,04 --> 00:02:07,56
Set the type of action to read four bites and

60
00:02:07,56 --> 00:02:09,62
that is sent to the SPIBAR

61
00:02:09,62 --> 00:02:10,45
Memory mapped I

62
00:02:10,45 --> 00:02:10,93
O

63
00:02:11,17 --> 00:02:15,55
That gets translated down into the actual hardware control block

64
00:02:15,55 --> 00:02:17,0
for the spy access

65
00:02:17,01 --> 00:02:21,13
And the elves convert that into a Spy Flash command

66
00:02:21,44 --> 00:02:22,16
For instance,

67
00:02:22,16 --> 00:02:24,65
opcode three and size of four

68
00:02:24,94 --> 00:02:27,44
They pass it along and they send it over to

69
00:02:27,44 --> 00:02:30,19
the Spi flash chip and that's how you actually read

70
00:02:30,19 --> 00:02:31,86
four bytes from the spi flash chip

71
00:02:32,24 --> 00:02:33,17
On the other hand,

72
00:02:33,17 --> 00:02:34,84
if you're using software sequencing,

73
00:02:34,85 --> 00:02:37,34
you're basically saying I don't want to use the abstractions

74
00:02:37,34 --> 00:02:39,26
I know exactly how I want to talk to the

75
00:02:39,26 --> 00:02:41,66
sPI flash chip and I'm going to tell you how

76
00:02:42,04 --> 00:02:44,4
So here instead you specify,

77
00:02:44,4 --> 00:02:44,73
you know,

78
00:02:44,73 --> 00:02:48,72
I want this exact opcode for the spi flash chip

79
00:02:48,73 --> 00:02:50,43
and I know that that's going to work on my

80
00:02:50,43 --> 00:02:53,59
particular chip and you send that through this by bar

81
00:02:53,59 --> 00:02:55,69
Memory map tile registers through the software,

82
00:02:55,69 --> 00:02:57,1
sequencing portion of them

83
00:02:57,11 --> 00:03:00,43
And that will be handed down exactly as is and

84
00:03:00,43 --> 00:03:03,16
the elves will pass it along exactly as is

85
00:03:04,64 --> 00:03:04,96
Now

86
00:03:04,96 --> 00:03:08,0
We saw earlier in the class SPIBAR very briefly

87
00:03:08,0 --> 00:03:10,33
when we were talking about R C B A and

88
00:03:10,33 --> 00:03:11,06
Riker bah

89
00:03:11,07 --> 00:03:13,24
So we said R C B A C B A

90
00:03:13,24 --> 00:03:14,98
A B C C B A A B C

91
00:03:14,99 --> 00:03:16,19
Mere universe Spock

92
00:03:16,2 --> 00:03:19,72
R R C B A holds record baja,

93
00:03:19,73 --> 00:03:23,06
which is the base address for the root complex registered

94
00:03:23,06 --> 00:03:23,66
block

95
00:03:24,44 --> 00:03:28,99
So base address RC cola can in the manuals you

96
00:03:28,99 --> 00:03:33,12
will find this description which says the Spy host interface

97
00:03:33,12 --> 00:03:36,05
registers are memory mapped in the R C R B

98
00:03:36,05 --> 00:03:39,55
root complex register block with a base address which they

99
00:03:39,55 --> 00:03:42,47
call SPIBAR of 3800

100
00:03:42,48 --> 00:03:44,34
So root complex based address

101
00:03:44,35 --> 00:03:48,52
Root complex register block plus 3800 is the SPIBAR

102
00:03:48,53 --> 00:03:49,19
location

103
00:03:49,36 --> 00:03:51,81
And they hopefully tell you that the address of the

104
00:03:51,81 --> 00:03:53,99
R C R B can be found in the R

105
00:03:53,99 --> 00:03:56,09
C B A registered So yes,

106
00:03:56,09 --> 00:03:58,82
lots of our register acronyms,

107
00:03:58,82 --> 00:04:02,16
but all you have to remember is RC beholds Recoba

108
00:04:02,74 --> 00:04:04,45
So that's what we showed before

109
00:04:04,84 --> 00:04:10,1
Device 31 function zero offset F zero is R C

110
00:04:10,1 --> 00:04:10,57
B A

111
00:04:10,58 --> 00:04:14,1
And it's holding recur bought the base address for the

112
00:04:14,11 --> 00:04:15,93
root complex register block

113
00:04:15,98 --> 00:04:19,32
And what we said is hard coded on these particular

114
00:04:19,32 --> 00:04:19,86
chips,

115
00:04:19,97 --> 00:04:22,69
is that at hex 3800?

116
00:04:23,0 --> 00:04:25,56
That is where you're going to find the SPIBAR

117
00:04:25,72 --> 00:04:29,83
Spy base address registers which are used for accessing the

118
00:04:29,83 --> 00:04:32,15
spi flash chip directly Now

119
00:04:32,15 --> 00:04:34,35
This only applies to the I C H nine and

120
00:04:34,35 --> 00:04:37,09
newer and up to the P C H uh nine

121
00:04:37,09 --> 00:04:37,7
series

122
00:04:37,71 --> 00:04:39,42
If you're 100 series or newer,

123
00:04:39,42 --> 00:04:40,5
it's going to be something different

124
00:04:40,5 --> 00:04:41,66
We'll talk about in a second

125
00:04:42,94 --> 00:04:46,92
But actually this particular mechanism existed even back further beyond

126
00:04:46,92 --> 00:04:48,01
the I C H nine

127
00:04:48,02 --> 00:04:50,11
It's just that this offset was different

128
00:04:50,11 --> 00:04:51,11
It wasn't 3800

129
00:04:51,11 --> 00:04:53,64
It was somewhere else in the R C R B

130
00:04:53,65 --> 00:04:54,55
But you know,

131
00:04:54,56 --> 00:04:56,32
that's all way too old for us to care about

132
00:04:56,32 --> 00:04:56,87
at this point

133
00:04:56,87 --> 00:04:58,76
You can of course find that though if you go

134
00:04:58,76 --> 00:04:59,76
look at older manuals

135
00:05:00,04 --> 00:05:03,18
So let's see how we find the SPIBAR on

136
00:05:03,18 --> 00:05:04,36
a newer system

137
00:05:04,54 --> 00:05:07,07
So P C H 100 series and newer,

138
00:05:07,23 --> 00:05:11,23
there is a dedicated spi interface now which is device

139
00:05:11,23 --> 00:05:15,78
31 function five And at offset hex 10 in there

140
00:05:15,78 --> 00:05:18,66
there's a field called SPIBAR zero mm I O

141
00:05:18,94 --> 00:05:22,63
So offset 10 inside of PC configured dress space is

142
00:05:22,63 --> 00:05:25,56
very literally the typical normal location where you would find

143
00:05:25,56 --> 00:05:28,12
base address registers for a pc device

144
00:05:28,48 --> 00:05:31,67
So in this case the interpretation of it further along

145
00:05:31,67 --> 00:05:35,06
in the manual is that bits 12 and beyond are

146
00:05:35,06 --> 00:05:39,01
the meme bar field which says where in memory should

147
00:05:39,01 --> 00:05:40,98
this base address register be mapped?

148
00:05:41,18 --> 00:05:42,57
And other things like memes,

149
00:05:42,57 --> 00:05:45,47
eyes are just hardwired to zero to indicate four kilobytes

150
00:05:45,47 --> 00:05:45,99
of space

151
00:05:46,24 --> 00:05:49,57
So we really just care about that meme Barfield because

152
00:05:49,57 --> 00:05:51,9
basically it looks like this now instead,

153
00:05:52,23 --> 00:05:56,13
device 31 function five instead of function zero and offset

154
00:05:56,13 --> 00:05:59,17
10 is the bios SPIBAR zero field

155
00:05:59,18 --> 00:06:03,29
And inside of that is specifically the meme Barfield

156
00:06:03,3 --> 00:06:07,25
And the member field doesn't exactly correspond directly to Spy

157
00:06:07,25 --> 00:06:07,48
Bar

158
00:06:07,48 --> 00:06:08,53
But for our purposes,

159
00:06:08,53 --> 00:06:10,54
we're going to say it does just for consistency to

160
00:06:10,54 --> 00:06:10,96
say,

161
00:06:11,14 --> 00:06:13,48
SPIBAR is what we're going to refer to as

162
00:06:13,48 --> 00:06:15,44
the start of the memory mapped I

163
00:06:15,44 --> 00:06:15,57
O

164
00:06:15,57 --> 00:06:17,59
Region that contains the registers,

165
00:06:17,6 --> 00:06:20,02
registers are going to behave the same way between these

166
00:06:20,02 --> 00:06:20,63
two things

167
00:06:20,64 --> 00:06:22,97
It's just a different question of how you actually find

168
00:06:22,97 --> 00:06:23,26
them


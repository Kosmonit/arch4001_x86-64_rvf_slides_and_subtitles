1
00:00:00,44 --> 00:00:02,08
okay you are here,

2
00:00:02,08 --> 00:00:04,83
you are actually in a limbo between sections

3
00:00:04,84 --> 00:00:07,29
So I want to make a few general comments about

4
00:00:07,29 --> 00:00:11,1
how spi flash access works on intel systems because this

5
00:00:11,1 --> 00:00:13,57
content is particular to intel,

6
00:00:13,57 --> 00:00:15,16
it's not specific to spy

7
00:00:15,24 --> 00:00:18,3
So the reason that the attacker would be interested in

8
00:00:18,3 --> 00:00:20,49
infecting the biases because as we said at the very

9
00:00:20,49 --> 00:00:21,06
beginning,

10
00:00:21,23 --> 00:00:24,28
they who run first run best and the bios is

11
00:00:24,28 --> 00:00:26,89
code that lives on a flash chip soldered onto the

12
00:00:26,89 --> 00:00:27,46
motherboard

13
00:00:27,63 --> 00:00:31,57
It is not vulnerable to detection by defenders due to

14
00:00:31,57 --> 00:00:34,85
things like hard drive forensics or typical security tools

15
00:00:34,94 --> 00:00:37,5
Because the code is running at such high privilege,

16
00:00:37,5 --> 00:00:40,26
they can generally manipulate any sort of security tools that

17
00:00:40,26 --> 00:00:41,33
would attempt to catch them

18
00:00:41,41 --> 00:00:43,98
If a defender does actually managed to find it,

19
00:00:44,16 --> 00:00:46,77
then they'll actually have a very difficult time removing it

20
00:00:46,77 --> 00:00:50,71
because the attacker can prevent things like normal firmware updates

21
00:00:50,71 --> 00:00:51,99
from overriding themselves

22
00:00:52,07 --> 00:00:52,44
So,

23
00:00:52,44 --> 00:00:55,16
Spy is the serial peripheral interface and as the name

24
00:00:55,16 --> 00:00:59,75
suggests it's a serial hardware protocol for interfacing with peripherals

25
00:00:59,84 --> 00:01:01,02
in our context,

26
00:01:01,02 --> 00:01:04,66
we're mostly interested in it interfacing with a non volatile

27
00:01:04,66 --> 00:01:05,89
storage chip,

28
00:01:05,9 --> 00:01:07,56
like a spy flash chip,

29
00:01:07,57 --> 00:01:10,26
it can actually be used for just arbitrary communication between

30
00:01:10,26 --> 00:01:10,8
peripherals,

31
00:01:10,8 --> 00:01:14,49
but we care about accessing the flash on modern systems

32
00:01:14,49 --> 00:01:16,87
The spi flash chip is not only used to store

33
00:01:16,87 --> 00:01:18,27
the firmware for the bios proper,

34
00:01:18,27 --> 00:01:21,38
which runs from the reset vector of the CPU it's

35
00:01:21,38 --> 00:01:25,04
also used to store information for peripheral processors like the

36
00:01:25,04 --> 00:01:26,49
intel management engine,

37
00:01:26,78 --> 00:01:31,43
the intel integrated gigabit ethernet or embedded controllers on newer

38
00:01:31,43 --> 00:01:35,55
systems because the spi protocol uses 24 bit addressing,

39
00:01:35,61 --> 00:01:38,11
that means that you only have two to the 24

40
00:01:38,11 --> 00:01:41,03
bits to specify what the offset is inside of the

41
00:01:41,03 --> 00:01:42,0
spi flash chip

42
00:01:42,01 --> 00:01:45,16
And consequently that means you have a 16 megabyte limit

43
00:01:45,16 --> 00:01:46,46
for a typical spi flash chip

44
00:01:46,84 --> 00:01:49,73
But Intel does support up to two difference by flash

45
00:01:49,73 --> 00:01:53,27
chips allowing vendors to have up to 32 MB of

46
00:01:53,27 --> 00:01:53,86
storage

47
00:01:54,24 --> 00:01:57,14
While the sPI protocol can run up to for instance

48
00:01:57,14 --> 00:01:59,16
100 megahertz Intel systems

49
00:01:59,16 --> 00:02:01,81
And the hardware that interacts with their spi flash chips

50
00:02:01,85 --> 00:02:04,86
typically runs between 20 and 66 megahertz

51
00:02:04,87 --> 00:02:07,45
So you can see that's actually quite slow and that's

52
00:02:07,45 --> 00:02:10,81
why reading information and writing information to a spi flash

53
00:02:10,81 --> 00:02:12,97
chip is quite slow at the very beginning,

54
00:02:12,97 --> 00:02:13,31
very,

55
00:02:13,31 --> 00:02:14,5
very beginning of the class

56
00:02:14,5 --> 00:02:16,25
In the setting up your lab section,

57
00:02:16,25 --> 00:02:18,03
you are supposed to use chips that in order to

58
00:02:18,04 --> 00:02:20,51
dump out the full contents of the flash chip and

59
00:02:20,51 --> 00:02:23,4
that probably took a while because access to flash chips

60
00:02:23,4 --> 00:02:25,88
is slow because they're running at such slow speeds

61
00:02:25,96 --> 00:02:28,39
But regardless of those sort of hardware details,

62
00:02:28,39 --> 00:02:30,41
the fact of the matter is intel is going to

63
00:02:30,41 --> 00:02:33,52
abstract away all of the hardware details and just provide

64
00:02:33,52 --> 00:02:36,34
us with very nice memory mapped i O registers to

65
00:02:36,35 --> 00:02:39,26
peek and poke it and consequently will be able to

66
00:02:39,26 --> 00:02:42,02
interact with the spi flash storage without needing to know

67
00:02:42,02 --> 00:02:43,55
any of these low level details

68
00:02:43,84 --> 00:02:44,01
Now,

69
00:02:44,01 --> 00:02:47,28
the interesting thing about spy is that it's actually not

70
00:02:47,28 --> 00:02:50,68
a fully specified protocol is basically just the fact of

71
00:02:50,68 --> 00:02:51,06
things

72
00:02:51,06 --> 00:02:54,26
And Spi flash chips in particular don't have to conform

73
00:02:54,26 --> 00:02:56,13
to any particular requirements

74
00:02:56,23 --> 00:02:59,7
So consequently intel has specified in their data sheets that

75
00:02:59,71 --> 00:03:01,66
if you want to use the spi flash chip with

76
00:03:01,66 --> 00:03:02,63
an intel system,

77
00:03:02,65 --> 00:03:05,46
it needs to support the following commands as a minimum

78
00:03:05,47 --> 00:03:08,81
from their their particular hardware is going to actually know

79
00:03:08,81 --> 00:03:11,18
how to send those commands to the spi flash chip

80
00:03:11,19 --> 00:03:13,2
behind the scenes that we don't have to care about

81
00:03:13,2 --> 00:03:14,46
it when we want to read and write

82
00:03:14,84 --> 00:03:15,06
Now,

83
00:03:15,06 --> 00:03:18,27
intel hardware in particular can interact with SPI flash chips

84
00:03:18,27 --> 00:03:19,36
in a couple of ways

85
00:03:19,74 --> 00:03:23,02
So originally they always behaved in a particular way,

86
00:03:23,02 --> 00:03:25,02
which we're going to call non descriptor mode

87
00:03:25,38 --> 00:03:26,09
Eventually,

88
00:03:26,09 --> 00:03:28,6
in the I C H eight they added thing called

89
00:03:28,6 --> 00:03:29,41
Descriptor mode

90
00:03:29,42 --> 00:03:33,0
And this was basically a introduction of a data structure

91
00:03:33,0 --> 00:03:35,57
at the beginning of the spi flash chip called the

92
00:03:35,58 --> 00:03:38,69
flash descriptor and the hardware would go out and read

93
00:03:38,69 --> 00:03:41,59
this data structure in order to understand things about,

94
00:03:41,59 --> 00:03:41,79
you know,

95
00:03:41,79 --> 00:03:43,55
what the spi flash part was,

96
00:03:43,8 --> 00:03:46,55
Information about where different regions were so that it could

97
00:03:46,55 --> 00:03:49,84
lock things down for hardware access controls and things like

98
00:03:49,84 --> 00:03:50,15
that

99
00:03:50,54 --> 00:03:53,33
So basically Descriptor mode is what you're going to see

100
00:03:53,33 --> 00:03:54,4
on all modern system,

101
00:03:54,4 --> 00:03:56,69
starting with I C H eight and 10,

102
00:03:56,7 --> 00:03:59,31
8 through 10 and those supported multiple modes,

103
00:03:59,31 --> 00:04:02,2
but on all modern PCH based systems were only going

104
00:04:02,2 --> 00:04:03,32
to be seeing descriptor mode

105
00:04:03,32 --> 00:04:05,27
Non descriptor mode is not supported anymore,

106
00:04:05,48 --> 00:04:07,52
but I'm covering it very briefly here

107
00:04:07,52 --> 00:04:10,62
Just because of the fact that until adam chips actually

108
00:04:10,62 --> 00:04:11,7
support both modes

109
00:04:11,7 --> 00:04:13,39
And so if you go off and want to look

110
00:04:13,39 --> 00:04:15,38
at some embedded systems using atom chips,

111
00:04:15,39 --> 00:04:17,76
you should be aware that non descriptor mode is a

112
00:04:17,76 --> 00:04:18,07
thing,

113
00:04:18,07 --> 00:04:20,84
even though we almost exclusively focus on descriptor mode in

114
00:04:20,84 --> 00:04:21,46
this class

115
00:04:22,34 --> 00:04:24,5
So what does the scripture mod look like for practical

116
00:04:24,5 --> 00:04:25,17
purposes?

117
00:04:25,3 --> 00:04:25,64
Well,

118
00:04:25,64 --> 00:04:28,41
you've got a spi flash chip and that chip is

119
00:04:28,41 --> 00:04:30,96
going to be mapped into memory at the high four

120
00:04:30,96 --> 00:04:31,78
gigabyte range

121
00:04:31,79 --> 00:04:34,13
We already saw that at the very beginning of Class

122
00:04:34,25 --> 00:04:35,63
F f f F zero

123
00:04:35,63 --> 00:04:38,87
The reset vector corresponds to code at the end of

124
00:04:38,87 --> 00:04:39,86
the spi flash chip

125
00:04:40,24 --> 00:04:41,83
But in non descriptor mode,

126
00:04:41,84 --> 00:04:43,09
if you had a spy flash ship,

127
00:04:43,09 --> 00:04:44,9
that was less than 16 megabytes,

128
00:04:44,93 --> 00:04:48,27
you would actually see the same flash chip contents mapped

129
00:04:48,27 --> 00:04:52,6
multiple times up to the end of memory minus 16

130
00:04:52,6 --> 00:04:53,06
limit

131
00:04:53,16 --> 00:04:54,59
So if it was four megabytes,

132
00:04:54,59 --> 00:04:56,8
you would see the same thing mapped four times

133
00:04:56,94 --> 00:04:59,56
And the sort of key property of non descriptor mode

134
00:04:59,56 --> 00:05:02,93
is that it's just a straight mapping of spi flash

135
00:05:02,93 --> 00:05:04,33
contents to memory

136
00:05:04,33 --> 00:05:05,23
Map contents

137
00:05:05,4 --> 00:05:07,78
In contrast to this descriptor mode as I said,

138
00:05:07,78 --> 00:05:11,39
introduces a flash descriptor data structure that talks about how

139
00:05:11,39 --> 00:05:14,26
the different portions of the flash can be used because

140
00:05:14,26 --> 00:05:17,35
the flash chip is actually reused for multiple things,

141
00:05:17,36 --> 00:05:18,1
like we said,

142
00:05:18,31 --> 00:05:22,58
integrated intel gigabit ethernet management engine and embedded controller on

143
00:05:22,58 --> 00:05:23,15
newer things

144
00:05:23,44 --> 00:05:27,29
Those will coexist with the bios on the spi flash

145
00:05:27,29 --> 00:05:29,41
chip and they can potentially be reading and writing the

146
00:05:29,41 --> 00:05:31,09
spi flash chip independently

147
00:05:31,48 --> 00:05:33,4
The data structure at the beginning of the flash chip

148
00:05:33,41 --> 00:05:35,32
breaks it up into different regions,

149
00:05:35,32 --> 00:05:38,92
which can be used for things like the integrated intel

150
00:05:38,93 --> 00:05:40,06
gigabit ethernet,

151
00:05:40,07 --> 00:05:41,08
the management engine,

152
00:05:41,09 --> 00:05:42,66
or embedded controllers

153
00:05:43,04 --> 00:05:47,21
It also provides hardware support for restricting access between these

154
00:05:47,25 --> 00:05:48,41
so that for instance,

155
00:05:48,41 --> 00:05:50,59
the management engine can't write to the bios and the

156
00:05:50,59 --> 00:05:52,2
bios can't write to the management engine

157
00:05:52,21 --> 00:05:54,14
You can learn a lot more about that later on

158
00:05:54,14 --> 00:05:55,55
in some optional material

159
00:05:55,84 --> 00:05:59,87
Descriptor mode also introduces an interesting capability for what's called

160
00:05:59,87 --> 00:06:01,03
soft straps

161
00:06:01,34 --> 00:06:01,76
So,

162
00:06:01,76 --> 00:06:05,3
a hard strap is usually a place in the hardware

163
00:06:05,3 --> 00:06:07,75
where a particular pin is pulled up to a high

164
00:06:07,75 --> 00:06:10,94
voltage or down to a low voltage and that will

165
00:06:10,94 --> 00:06:13,32
actually change the behavior of the hardware

166
00:06:13,5 --> 00:06:17,16
Soft straps are configurable things that again change the behavior

167
00:06:17,16 --> 00:06:17,91
of the hardware,

168
00:06:17,93 --> 00:06:21,8
but instead of the hardware manufacturer having to hard code

169
00:06:21,8 --> 00:06:23,84
those things to a high voltage or low voltage to

170
00:06:23,84 --> 00:06:25,08
get the behavior they want,

171
00:06:25,14 --> 00:06:27,79
they can actually place data into the spi flash ship

172
00:06:27,79 --> 00:06:30,61
in the soft strap region and that will change the

173
00:06:30,61 --> 00:06:31,85
behavior of the hardware

174
00:06:31,94 --> 00:06:36,37
It allows for easier prototyping and configuration and trying things

175
00:06:36,37 --> 00:06:39,05
out when they're first developing the system

176
00:06:39,44 --> 00:06:39,66
Now,

177
00:06:39,66 --> 00:06:43,86
in contrast with the non descriptor mode in descriptor mode

178
00:06:43,86 --> 00:06:46,5
because there are different regions and because the bios is

179
00:06:46,5 --> 00:06:47,92
assigned one of those regions,

180
00:06:48,01 --> 00:06:50,96
it is only the bios region which ultimately gets mapped

181
00:06:50,96 --> 00:06:53,02
into memory at the high address range

182
00:06:53,06 --> 00:06:56,71
The rest of it is not directly visible or accessible

183
00:06:56,72 --> 00:06:59,01
Even if this thing was a small flash chip,

184
00:06:59,02 --> 00:07:00,74
you would still not be able to see these other

185
00:07:00,74 --> 00:07:03,77
regions like the management engine region mapped into memory,

186
00:07:03,78 --> 00:07:05,76
you would still be able to access it if you

187
00:07:05,76 --> 00:07:07,14
went directly to flash chip,

188
00:07:07,15 --> 00:07:09,36
it just isn't mapped to memory by default

189
00:07:09,94 --> 00:07:10,75
That said,

190
00:07:10,75 --> 00:07:13,95
if someone were to actually corrupt the data structure at

191
00:07:13,95 --> 00:07:16,6
the beginning of the spi flash ship the system

192
00:07:16,61 --> 00:07:19,94
If it supports both non descriptor and descriptor mode would

193
00:07:19,94 --> 00:07:23,17
revert to behaving as a non descriptor system and it

194
00:07:23,17 --> 00:07:25,78
would return to multi mapping things

195
00:07:25,79 --> 00:07:26,99
And in particular,

196
00:07:27,0 --> 00:07:29,2
if there were other regions like the management engine,

197
00:07:29,2 --> 00:07:32,65
they would become visible in the memory at that point

198
00:07:33,14 --> 00:07:35,31
you're not supposed to be able to corrupt that data

199
00:07:35,31 --> 00:07:38,86
structure at the very beginning because intel specifies the data

200
00:07:38,86 --> 00:07:42,05
structure itself should say something about protecting itself,

201
00:07:42,05 --> 00:07:44,41
so that normal hardware can't get in there,

202
00:07:44,41 --> 00:07:46,63
but a physical attacker should be able to read the

203
00:07:46,63 --> 00:07:48,05
entire flash chip as well

204
00:07:48,44 --> 00:07:51,01
But here's just an example that was taken from a

205
00:07:51,01 --> 00:07:54,66
older system that supported descriptor and non descriptor mode where

206
00:07:54,66 --> 00:07:57,71
the descriptor was corrupted and then you can actually see

207
00:07:57,71 --> 00:07:59,46
that at multiple different addresses

208
00:07:59,48 --> 00:08:02,79
We see the same contents and this particular content is

209
00:08:02,79 --> 00:08:06,86
the sort of offset zero contents which has a corrupted

210
00:08:06,87 --> 00:08:10,76
descriptor because the magic number is supposed to be zero

211
00:08:10,76 --> 00:08:13,46
ff zero A 55 A

212
00:08:13,54 --> 00:08:16,95
But it was corrupted to end with five B instead

213
00:08:17,08 --> 00:08:18,82
the last thing that I want to talk about is

214
00:08:18,82 --> 00:08:21,77
the concept of direct access versus register access to the

215
00:08:21,77 --> 00:08:22,72
spi flash chip

216
00:08:22,9 --> 00:08:25,6
We've already seen how the contents of the spi flash

217
00:08:25,6 --> 00:08:28,64
chip for the reset vector are mapped into physical memory

218
00:08:28,64 --> 00:08:32,16
at the high range at four gigabytes minus whatever the

219
00:08:32,16 --> 00:08:35,95
size of the bios region is on descriptor mode systems

220
00:08:36,01 --> 00:08:38,11
And when we're talking about direct access,

221
00:08:38,12 --> 00:08:40,92
it turns out that the fact that the flash chip

222
00:08:40,93 --> 00:08:43,7
could be shared between different peripheral processors,

223
00:08:43,7 --> 00:08:47,35
like the management engine and the main CPU direct access

224
00:08:47,36 --> 00:08:48,18
automatically

225
00:08:48,18 --> 00:08:50,59
In descriptor mode is restricted so that the bios can

226
00:08:50,59 --> 00:08:53,08
only get access to the bios region and the management

227
00:08:53,08 --> 00:08:55,55
engine can only get access to the management engine region

228
00:08:55,74 --> 00:08:57,39
When it comes to register access,

229
00:08:57,39 --> 00:09:00,17
which is via the memory mapped i O registers that

230
00:09:00,17 --> 00:09:01,84
we're going to be finding in the next section

231
00:09:01,96 --> 00:09:04,3
There is actually part of the data structure of the

232
00:09:04,3 --> 00:09:08,38
flash descriptor specifies different access controls for register access

233
00:09:08,44 --> 00:09:09,81
So for instance,

234
00:09:09,81 --> 00:09:12,2
the bios may be able to write to the management

235
00:09:12,2 --> 00:09:12,53
engine,

236
00:09:12,53 --> 00:09:13,12
for instance,

237
00:09:13,12 --> 00:09:15,26
to update the management engine firmware,

238
00:09:15,44 --> 00:09:17,85
but it could set access control so that the management

239
00:09:17,85 --> 00:09:19,46
engine can't right back to the bios


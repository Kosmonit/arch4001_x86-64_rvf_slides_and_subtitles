1
00:00:00,240 --> 00:00:05,279
welcome to the sega spy bus master

2
00:00:02,560 --> 00:00:07,040
system featuring the cpu bios the

3
00:00:05,279 --> 00:00:09,120
management engine and the gigabit

4
00:00:07,040 --> 00:00:10,639
ethernet all sharing the same flash but

5
00:00:09,120 --> 00:00:12,880
all independent hardware that could

6
00:00:10,639 --> 00:00:14,880
access it at any moment

7
00:00:12,880 --> 00:00:16,880
so how does all that work out well this

8
00:00:14,880 --> 00:00:18,640
is the section where we talk about the

9
00:00:16,880 --> 00:00:20,720
privilege and access control that is

10
00:00:18,640 --> 00:00:22,400
enforced between these different spy bus

11
00:00:20,720 --> 00:00:25,039
masters these different chunks of

12
00:00:22,400 --> 00:00:29,199
hardware that can access the spy bus

13
00:00:25,039 --> 00:00:31,840
so back in the map we had a hex 60 which

14
00:00:29,199 --> 00:00:34,000
is going to be the start of the master

15
00:00:31,840 --> 00:00:35,200
section of the flash descriptor and

16
00:00:34,000 --> 00:00:36,079
what's there

17
00:00:35,200 --> 00:00:38,079
well

18
00:00:36,079 --> 00:00:40,239
this requires a bit of interpretation

19
00:00:38,079 --> 00:00:42,960
and a bit of table reading

20
00:00:40,239 --> 00:00:47,039
so the first thing is flash master 1

21
00:00:42,960 --> 00:00:49,120
which corresponds to the cpu slash bios

22
00:00:47,039 --> 00:00:50,719
and this basically says the cpu has

23
00:00:49,120 --> 00:00:53,120
access to the spy flash and that's going

24
00:00:50,719 --> 00:00:55,600
to be called flash master one

25
00:00:53,120 --> 00:00:58,000
now this is going to specify different

26
00:00:55,600 --> 00:01:00,559
permissions about what this thing can

27
00:00:58,000 --> 00:01:03,359
access what it can't access

28
00:01:00,559 --> 00:01:06,320
so scrolling down we interpret the least

29
00:01:03,359 --> 00:01:07,920
significant 16 bits as the requester id

30
00:01:06,320 --> 00:01:09,439
which must be set to zero okay well

31
00:01:07,920 --> 00:01:12,240
that's easy so boom we just covered

32
00:01:09,439 --> 00:01:14,000
those 16 bits but now the rest of this

33
00:01:12,240 --> 00:01:17,040
is going to require some careful

34
00:01:14,000 --> 00:01:20,799
interpretation after those 16 bits we

35
00:01:17,040 --> 00:01:24,720
have the next five bits as b and then

36
00:01:20,799 --> 00:01:27,680
zero so b would be one zero one one and

37
00:01:24,720 --> 00:01:30,640
zero is zero so what does one zero one

38
00:01:27,680 --> 00:01:32,479
one correspond to well it means that can

39
00:01:30,640 --> 00:01:36,400
the bios

40
00:01:32,479 --> 00:01:38,560
read the flash descriptor region and

41
00:01:36,400 --> 00:01:40,960
this is set to one okay so the bios can

42
00:01:38,560 --> 00:01:44,000
read the spy flash descriptor region

43
00:01:40,960 --> 00:01:46,799
which you know is from zero to fff

44
00:01:44,000 --> 00:01:49,040
can it read the bios master region well

45
00:01:46,799 --> 00:01:51,439
it can because it's set to one but also

46
00:01:49,040 --> 00:01:54,960
this is just kind of a do not care bit

47
00:01:51,439 --> 00:01:58,719
because a particular spy bus master may

48
00:01:54,960 --> 00:02:01,439
always read its own region so the bio

49
00:01:58,719 --> 00:02:03,920
cpu spy bus master can always read and

50
00:02:01,439 --> 00:02:04,960
write the bios region the management

51
00:02:03,920 --> 00:02:06,640
engine

52
00:02:04,960 --> 00:02:07,840
management engines by bus master can

53
00:02:06,640 --> 00:02:10,479
always read and write the management

54
00:02:07,840 --> 00:02:12,640
engine and so forth so the self-access

55
00:02:10,479 --> 00:02:13,760
control is always sort of doesn't care

56
00:02:12,640 --> 00:02:15,280
it's always going to the hardware is

57
00:02:13,760 --> 00:02:16,879
always going to let it through

58
00:02:15,280 --> 00:02:19,120
but basically you can see that this is

59
00:02:16,879 --> 00:02:21,360
saying you know can the bios read flash

60
00:02:19,120 --> 00:02:23,440
can the bios read bios can the bios read

61
00:02:21,360 --> 00:02:25,520
me can the bios read gigabit ethernet

62
00:02:23,440 --> 00:02:27,680
can the bios read platform data region

63
00:02:25,520 --> 00:02:29,200
so there are however many bits there are

64
00:02:27,680 --> 00:02:31,280
for different regions and like i said

65
00:02:29,200 --> 00:02:32,560
you know intel still has more bits that

66
00:02:31,280 --> 00:02:34,160
are available

67
00:02:32,560 --> 00:02:36,319
and then this says you know these are

68
00:02:34,160 --> 00:02:37,760
all going to be the read accesses and

69
00:02:36,319 --> 00:02:40,800
then the next is going to be all the

70
00:02:37,760 --> 00:02:43,360
right accesses so can bios access flash

71
00:02:40,800 --> 00:02:45,200
descriptor yes can it access itself yes

72
00:02:43,360 --> 00:02:47,599
but we don't care what that says can the

73
00:02:45,200 --> 00:02:48,480
bios access the management engine for

74
00:02:47,599 --> 00:02:50,720
read

75
00:02:48,480 --> 00:02:53,120
and this is set to zero which means the

76
00:02:50,720 --> 00:02:54,800
hardware is actually going to restrict

77
00:02:53,120 --> 00:02:56,160
the bios from being able to read from

78
00:02:54,800 --> 00:02:58,560
that memory

79
00:02:56,160 --> 00:03:00,480
can the bios access the gigabit ethernet

80
00:02:58,560 --> 00:03:02,080
the answer is yes

81
00:03:00,480 --> 00:03:04,319
can it access the platform data region

82
00:03:02,080 --> 00:03:05,760
the answer is no but it's unused on this

83
00:03:04,319 --> 00:03:07,280
system so that doesn't really say

84
00:03:05,760 --> 00:03:09,840
anything

85
00:03:07,280 --> 00:03:13,120
okay so i said moving along

86
00:03:09,840 --> 00:03:14,720
what is this information this is a one

87
00:03:13,120 --> 00:03:16,720
zero one zero

88
00:03:14,720 --> 00:03:19,200
so can the bios write to the flash

89
00:03:16,720 --> 00:03:21,040
descriptor no it may not can it write to

90
00:03:19,200 --> 00:03:23,519
itself yes it may can it write to the

91
00:03:21,040 --> 00:03:25,280
management engine no it may not

92
00:03:23,519 --> 00:03:27,040
can it write to the gigabit ethernet yes

93
00:03:25,280 --> 00:03:28,799
it may can it write to the platform data

94
00:03:27,040 --> 00:03:30,400
who cares it's not there

95
00:03:28,799 --> 00:03:32,400
so what

96
00:03:30,400 --> 00:03:34,480
what uefi tool here is trying to tell

97
00:03:32,400 --> 00:03:36,319
you is it basically parsed this data

98
00:03:34,480 --> 00:03:38,400
from the flash master section of the

99
00:03:36,319 --> 00:03:41,280
flash descriptor and it's saying you

100
00:03:38,400 --> 00:03:43,519
know in summary the bios can read the

101
00:03:41,280 --> 00:03:46,000
descriptor the bios not the management

102
00:03:43,519 --> 00:03:48,640
engine and gigabit ethernet it can write

103
00:03:46,000 --> 00:03:50,799
not the descriptor yes to the bios node

104
00:03:48,640 --> 00:03:53,200
to the me yes to the gigabit ethernet

105
00:03:50,799 --> 00:03:54,640
and no to the platform data region all

106
00:03:53,200 --> 00:03:56,640
right i shouldn't have just leaned over

107
00:03:54,640 --> 00:03:58,959
there i'm sure that changed the audio

108
00:03:56,640 --> 00:04:00,959
sound of things but anyways these two

109
00:03:58,959 --> 00:04:04,000
bytes are talking about read permissions

110
00:04:00,959 --> 00:04:06,159
and write permissions for the cpu flash

111
00:04:04,000 --> 00:04:08,640
bus master

112
00:04:06,159 --> 00:04:10,879
and to the other regions on the flash

113
00:04:08,640 --> 00:04:10,879
chip

114
00:04:11,920 --> 00:04:16,639
now specifically intel recommends that

115
00:04:14,480 --> 00:04:18,639
oems configure the flash descriptor so

116
00:04:16,639 --> 00:04:20,880
that it's never writable by anyone

117
00:04:18,639 --> 00:04:23,680
anywhere after it leaves the factory and

118
00:04:20,880 --> 00:04:26,320
so this is good because if you make the

119
00:04:23,680 --> 00:04:28,000
flash descriptor non-writable by anyone

120
00:04:26,320 --> 00:04:29,919
then all of this information gets sort

121
00:04:28,000 --> 00:04:31,600
of locked in place so that means

122
00:04:29,919 --> 00:04:34,000
anything you said in terms of like you

123
00:04:31,600 --> 00:04:36,240
know the bios may not access the me or

124
00:04:34,000 --> 00:04:38,720
the me may not access the bios that

125
00:04:36,240 --> 00:04:40,880
stuff becomes sort of set in stone as

126
00:04:38,720 --> 00:04:43,040
far as software is concerned could still

127
00:04:40,880 --> 00:04:45,680
potentially be changed with hardware or

128
00:04:43,040 --> 00:04:46,720
some you know override mechanisms

129
00:04:45,680 --> 00:04:48,479
but you know these are the

130
00:04:46,720 --> 00:04:50,400
recommendations and as long as the

131
00:04:48,479 --> 00:04:52,240
vendors follow it or just use the

132
00:04:50,400 --> 00:04:53,120
defaults which you know their hardware

133
00:04:52,240 --> 00:04:54,560
tools

134
00:04:53,120 --> 00:04:56,720
provide defaults that i'll set it this

135
00:04:54,560 --> 00:04:59,919
way then it'll be you know set up to

136
00:04:56,720 --> 00:04:59,919
make these sort of restrictions

137
00:05:00,240 --> 00:05:05,120
additionally we talked about how you

138
00:05:02,479 --> 00:05:07,199
know the bios is not able to actually

139
00:05:05,120 --> 00:05:09,120
write to the management engine region

140
00:05:07,199 --> 00:05:11,759
and so that you know begs the question

141
00:05:09,120 --> 00:05:13,520
well how exactly is a management engine

142
00:05:11,759 --> 00:05:15,520
firmware update going to happen you know

143
00:05:13,520 --> 00:05:17,360
there has to be some way to do it so

144
00:05:15,520 --> 00:05:19,280
what is that way well we don't really

145
00:05:17,360 --> 00:05:21,680
cover that in this class but that's just

146
00:05:19,280 --> 00:05:25,840
a thing to keep you wondering and to go

147
00:05:21,680 --> 00:05:25,840
out and explore and read the fun manuals

148
00:05:26,479 --> 00:05:30,560
okay now moving on to the next bus

149
00:05:28,639 --> 00:05:31,759
master the next bus master is flash

150
00:05:30,560 --> 00:05:32,800
master 2

151
00:05:31,759 --> 00:05:36,560
me

152
00:05:32,800 --> 00:05:39,120
and the interpretation is as follows

153
00:05:36,560 --> 00:05:41,120
least significant 16 bits once again has

154
00:05:39,120 --> 00:05:43,199
some sort of requester id and this is

155
00:05:41,120 --> 00:05:45,440
actually set to zero so that's

156
00:05:43,199 --> 00:05:47,840
interesting the same requester id used

157
00:05:45,440 --> 00:05:49,759
for the cpu and the management engine

158
00:05:47,840 --> 00:05:51,440
usually things like requester ids are

159
00:05:49,759 --> 00:05:53,199
used by the hardware to try to

160
00:05:51,440 --> 00:05:55,360
distinguish you know who's talking on a

161
00:05:53,199 --> 00:05:56,720
particular bus this is kind of implying

162
00:05:55,360 --> 00:05:58,960
it's not able to make that

163
00:05:56,720 --> 00:06:00,880
distinguishment which is interesting

164
00:05:58,960 --> 00:06:03,759
but anyways let's you know focus on

165
00:06:00,880 --> 00:06:07,120
those excess control bits so this is d

166
00:06:03,759 --> 00:06:10,160
and c so what is d d1101

167
00:06:07,120 --> 00:06:12,639
so it's saying can the csme

168
00:06:10,160 --> 00:06:14,720
read from the flash descriptor yes it

169
00:06:12,639 --> 00:06:16,720
may can it read from the bios no it may

170
00:06:14,720 --> 00:06:18,800
not can it read from itself yes doesn't

171
00:06:16,720 --> 00:06:21,280
matter what this bit says can it read

172
00:06:18,800 --> 00:06:22,880
from the gigabit ethernet yes it may and

173
00:06:21,280 --> 00:06:25,840
can read from platform data it doesn't

174
00:06:22,880 --> 00:06:28,000
matter it's not used on the system

175
00:06:25,840 --> 00:06:30,160
can it write to the flash descriptor no

176
00:06:28,000 --> 00:06:32,560
again that's per the recommendations can

177
00:06:30,160 --> 00:06:34,319
it write to the bios no okay that's good

178
00:06:32,560 --> 00:06:36,639
that's sort of privileged separation of

179
00:06:34,319 --> 00:06:38,319
a sort gonna write to itself yes it may

180
00:06:36,639 --> 00:06:40,160
can it write to the gigabit ethernet yes

181
00:06:38,319 --> 00:06:41,280
it may and again this is sort of you

182
00:06:40,160 --> 00:06:43,120
know

183
00:06:41,280 --> 00:06:44,720
the particular vendor could set this up

184
00:06:43,120 --> 00:06:46,319
so that it's you know not writable it

185
00:06:44,720 --> 00:06:49,039
really just comes down to what kind of

186
00:06:46,319 --> 00:06:50,240
functionality is there one of the sort

187
00:06:49,039 --> 00:06:52,960
of early

188
00:06:50,240 --> 00:06:54,560
marketed functionalities of intel

189
00:06:52,960 --> 00:06:57,599
management engine or the marketing name

190
00:06:54,560 --> 00:06:59,520
was active platform technology sorry the

191
00:06:57,599 --> 00:07:01,199
marketing name was active management

192
00:06:59,520 --> 00:07:03,039
technology was the notion that the

193
00:07:01,199 --> 00:07:04,639
management engine could be alive in low

194
00:07:03,039 --> 00:07:06,800
power states it could still interact

195
00:07:04,639 --> 00:07:08,880
with the network in order to wake up the

196
00:07:06,800 --> 00:07:10,400
system you know do remote bios updates

197
00:07:08,880 --> 00:07:12,400
while the system's still sleeping stuff

198
00:07:10,400 --> 00:07:13,520
like that so you can imagine that if

199
00:07:12,400 --> 00:07:15,440
you're going to use that kind of

200
00:07:13,520 --> 00:07:17,199
functionality then the management engine

201
00:07:15,440 --> 00:07:19,120
needs access to the

202
00:07:17,199 --> 00:07:21,120
gigabit ethernet in order to interact

203
00:07:19,120 --> 00:07:23,039
with it somehow whether that necessarily

204
00:07:21,120 --> 00:07:25,199
means it actually needs to write to its

205
00:07:23,039 --> 00:07:27,280
flash is a different question

206
00:07:25,199 --> 00:07:29,120
and can the management engine actually

207
00:07:27,280 --> 00:07:30,960
write to the platform data region no it

208
00:07:29,120 --> 00:07:32,160
can't but it's not in use so it doesn't

209
00:07:30,960 --> 00:07:34,000
matter

210
00:07:32,160 --> 00:07:35,840
all right so the good news here is that

211
00:07:34,000 --> 00:07:37,919
you know the dell system is configured

212
00:07:35,840 --> 00:07:40,240
per intel's recommendation to lock it

213
00:07:37,919 --> 00:07:42,960
down so that neither the management

214
00:07:40,240 --> 00:07:45,280
engine nor the bios are able to write to

215
00:07:42,960 --> 00:07:47,199
the flash descriptor region

216
00:07:45,280 --> 00:07:48,960
that's good what about writing to the

217
00:07:47,199 --> 00:07:50,800
bios region it's not allowed to do that

218
00:07:48,960 --> 00:07:53,120
either that's good because that would

219
00:07:50,800 --> 00:07:54,800
theoretically mean that if the

220
00:07:53,120 --> 00:07:56,639
management engine was compromised it

221
00:07:54,800 --> 00:07:58,479
wouldn't be able to just automatically

222
00:07:56,639 --> 00:08:01,039
and arbitrarily privilege escalated into

223
00:07:58,479 --> 00:08:02,080
the bios by rewriting the bios contents

224
00:08:01,039 --> 00:08:03,840
of course you know there's other

225
00:08:02,080 --> 00:08:05,440
mechanisms that could potentially still

226
00:08:03,840 --> 00:08:07,199
allow that but you know from an

227
00:08:05,440 --> 00:08:09,919
architectural perspective this is trying

228
00:08:07,199 --> 00:08:13,840
to provide the you know separation from

229
00:08:09,919 --> 00:08:13,840
the management engine and the bios

230
00:08:14,240 --> 00:08:18,879
so then we move on to the gigabit

231
00:08:16,160 --> 00:08:20,000
ethernet and that flash master what can

232
00:08:18,879 --> 00:08:22,960
that do

233
00:08:20,000 --> 00:08:24,800
well now we actually see a requester id

234
00:08:22,960 --> 00:08:26,479
that is you know first of all it's

235
00:08:24,800 --> 00:08:28,479
non-zero so that's good now we actually

236
00:08:26,479 --> 00:08:30,319
have some distinction but second of all

237
00:08:28,479 --> 00:08:32,080
when you were reading your documentation

238
00:08:30,319 --> 00:08:35,279
you would have seen that documentation

239
00:08:32,080 --> 00:08:37,200
said this must be 218 but on the dial

240
00:08:35,279 --> 00:08:38,719
system you would have seen 118 well

241
00:08:37,200 --> 00:08:41,039
that's of course because this is not the

242
00:08:38,719 --> 00:08:43,200
right data sheet this is not a pch7

243
00:08:41,039 --> 00:08:46,080
series data sheet this is the ich10

244
00:08:43,200 --> 00:08:47,519
datasheet so whatever close enough

245
00:08:46,080 --> 00:08:49,120
pretty close there

246
00:08:47,519 --> 00:08:51,680
how do we interpret the rest of it well

247
00:08:49,120 --> 00:08:54,720
we've got eight so one zero zero zero so

248
00:08:51,680 --> 00:08:57,360
can the gigabit ethernet read from flash

249
00:08:54,720 --> 00:08:59,360
host or me region no it may not can it

250
00:08:57,360 --> 00:09:01,120
read from itself yes it can and this bit

251
00:08:59,360 --> 00:09:03,839
really doesn't matter

252
00:09:01,120 --> 00:09:05,519
can it write to the flash descriptor no

253
00:09:03,839 --> 00:09:07,519
it may not which follows the intel

254
00:09:05,519 --> 00:09:09,600
recommendation can it write to the bios

255
00:09:07,519 --> 00:09:11,760
or management engine no it may not so

256
00:09:09,600 --> 00:09:14,160
again you know this would mean if there

257
00:09:11,760 --> 00:09:16,080
was some sort of functionality for

258
00:09:14,160 --> 00:09:18,080
you know for instance

259
00:09:16,080 --> 00:09:20,160
arbitrary firmware code execution in the

260
00:09:18,080 --> 00:09:22,160
context of the intel

261
00:09:20,160 --> 00:09:23,920
gigabit ethernet perhaps there was you

262
00:09:22,160 --> 00:09:25,279
know some extra network boot

263
00:09:23,920 --> 00:09:27,440
functionality or something like that

264
00:09:25,279 --> 00:09:29,200
that ended up getting compromised well

265
00:09:27,440 --> 00:09:31,680
at least this sort of configuration

266
00:09:29,200 --> 00:09:33,600
would make it so that a compromised

267
00:09:31,680 --> 00:09:35,440
intel gigabit ethernet could not

268
00:09:33,600 --> 00:09:36,640
automatically privilege escalade by

269
00:09:35,440 --> 00:09:39,279
being able to

270
00:09:36,640 --> 00:09:42,560
write into the flash regions of the bios

271
00:09:39,279 --> 00:09:42,560
or the management engine


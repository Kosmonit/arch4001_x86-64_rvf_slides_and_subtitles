1
00:00:01,54 --> 00:00:01,92
Okay

2
00:00:01,92 --> 00:00:04,56
So what's the other second major protection mechanism?

3
00:00:05,14 --> 00:00:05,42
Well,

4
00:00:05,42 --> 00:00:08,8
this is a right protect mechanism that covers the entire

5
00:00:08,8 --> 00:00:13,13
range of the flash and it is overridden by a

6
00:00:13,13 --> 00:00:16,59
system management interrupt as opposed to a reset of the

7
00:00:16,59 --> 00:00:17,15
computer

8
00:00:17,23 --> 00:00:20,26
So what this allows is the protection of the entire

9
00:00:20,26 --> 00:00:24,16
flash ship with system management code having the option to

10
00:00:24,24 --> 00:00:26,96
decide to allow certain rights to proceed if you would

11
00:00:26,96 --> 00:00:27,36
like

12
00:00:28,14 --> 00:00:31,81
So this mechanism is mediated by once again something in

13
00:00:31,81 --> 00:00:33,43
the LPC device

14
00:00:33,44 --> 00:00:36,66
So bus zero device 31 function zero offset D

15
00:00:36,66 --> 00:00:37,16
C

16
00:00:37,17 --> 00:00:38,84
Gives us this register,

17
00:00:38,84 --> 00:00:43,67
the bios control register and specifically bit zero and one

18
00:00:43,67 --> 00:00:44,62
or what we care about

19
00:00:44,63 --> 00:00:47,3
So bit zero is called the bios right enable

20
00:00:47,31 --> 00:00:49,75
Bit and if it's zero then you're not allowed to

21
00:00:49,75 --> 00:00:51,89
write and if it's one then you are allowed to

22
00:00:51,89 --> 00:00:54,42
write but it's sort of briefly made mention of this

23
00:00:54,42 --> 00:00:57,49
previously in the description of how you would

24
00:00:57,49 --> 00:00:59,35
Right by the flash registers

25
00:01:00,04 --> 00:01:01,27
The bios lock enable,

26
00:01:01,27 --> 00:01:03,0
then behaves in an interesting way

27
00:01:03,33 --> 00:01:05,46
If you set the bios lock enable,

28
00:01:05,84 --> 00:01:09,92
then that means that someone setting the bios right and

29
00:01:09,92 --> 00:01:12,95
able to one will cause a system management interrupt

30
00:01:13,34 --> 00:01:16,82
So the processor will transition the system management mode and

31
00:01:16,82 --> 00:01:19,62
that code will have an opportunity to decide to either

32
00:01:19,62 --> 00:01:20,79
allow the bios right?

33
00:01:20,79 --> 00:01:23,35
Unable to stay one or it could reset it if

34
00:01:23,35 --> 00:01:24,97
it would like the idea here,

35
00:01:24,97 --> 00:01:28,37
is that system management code can decide whether or not

36
00:01:28,37 --> 00:01:29,04
to allow it,

37
00:01:29,04 --> 00:01:29,66
for instance,

38
00:01:29,66 --> 00:01:32,3
if it knows that a firmware update is in progress

39
00:01:32,54 --> 00:01:35,68
or if it knows that the operating system is writing

40
00:01:35,68 --> 00:01:37,7
into non volatile memory,

41
00:01:37,7 --> 00:01:38,45
for instance,

42
00:01:38,46 --> 00:01:39,56
NV RAM variables

43
00:01:39,74 --> 00:01:42,97
So this provides a little bit more flexible protection than

44
00:01:42,98 --> 00:01:45,81
the protective range registers which are just locked and set

45
00:01:45,82 --> 00:01:49,46
and can't be gotten around until you restart the system

46
00:01:49,84 --> 00:01:52,88
Same thing over on the 100 series chip set in

47
00:01:52,88 --> 00:01:56,17
that there's a right production able and a lock,

48
00:01:56,18 --> 00:01:57,51
but they changed the names

49
00:01:57,52 --> 00:02:00,15
So whereas bios control bios lock enable

50
00:02:00,15 --> 00:02:01,65
Change to B C

51
00:02:01,66 --> 00:02:04,97
So it's still bias control with the abbreviated be CNBC

52
00:02:04,98 --> 00:02:06,25
Ellie lock enable

53
00:02:06,64 --> 00:02:08,45
And then bias control bios right enable

54
00:02:08,45 --> 00:02:11,2
Change to write to protect disabled

55
00:02:11,74 --> 00:02:14,97
So this is kind of a strange and in my

56
00:02:14,97 --> 00:02:15,38
opinion,

57
00:02:15,38 --> 00:02:18,17
disingenuous uh change of name

58
00:02:18,18 --> 00:02:20,58
So previously it was like set this bit to enable

59
00:02:20,58 --> 00:02:22,3
writing and now it's like oh,

60
00:02:22,3 --> 00:02:26,11
set this bit to disable the right protection as if

61
00:02:26,11 --> 00:02:29,8
somehow there was always right protection by default and you're

62
00:02:29,8 --> 00:02:32,07
just disabling it here better than that,

63
00:02:32,07 --> 00:02:33,17
it behaves the same way

64
00:02:33,17 --> 00:02:35,81
Once lock enable is set writing this bit to one

65
00:02:35,81 --> 00:02:38,98
will cause a system management interrupt and system management mode

66
00:02:38,98 --> 00:02:41,24
can decide whether or not it wants to allow this

67
00:02:41,24 --> 00:02:42,65
to continue to be one

68
00:02:43,44 --> 00:02:47,65
So jumping back to what we saw in the section

69
00:02:47,65 --> 00:02:49,36
on flash register programming,

70
00:02:49,46 --> 00:02:51,49
there was this little line but I just sort of

71
00:02:51,49 --> 00:02:53,91
briefly jumped over it so and unaddressed,

72
00:02:53,91 --> 00:02:54,92
right exercised,

73
00:02:54,92 --> 00:02:55,29
right,

74
00:02:55,3 --> 00:02:57,02
enter the data to write into the F D two

75
00:02:57,02 --> 00:02:57,75
registers,

76
00:02:58,01 --> 00:02:59,45
Set the cycle type two right

77
00:02:59,45 --> 00:03:01,4
And then you have to make sure that the bios

78
00:03:01,4 --> 00:03:02,76
right enable is equal to one

79
00:03:02,77 --> 00:03:04,84
So that needs to be set before you can say

80
00:03:04,84 --> 00:03:07,45
go and have the flash right actually happened

81
00:03:08,24 --> 00:03:10,62
So what does this look like behind the scenes?

82
00:03:10,91 --> 00:03:11,27
Well,

83
00:03:11,27 --> 00:03:13,83
if software running on the CPU sees that,

84
00:03:13,84 --> 00:03:17,48
if bios control bios right enable is equal to zero

85
00:03:17,49 --> 00:03:20,24
then it goes ahead and rights bios right enable equal

86
00:03:20,24 --> 00:03:20,7
to one

87
00:03:21,14 --> 00:03:23,99
That's going to hit the pc I memory mapped I

88
00:03:23,99 --> 00:03:25,1
O configuration address,

89
00:03:25,1 --> 00:03:25,63
space,

90
00:03:25,64 --> 00:03:26,38
offset,

91
00:03:26,38 --> 00:03:27,05
D C

92
00:03:27,05 --> 00:03:30,42
And then the specific bit for bios roundtable and behind

93
00:03:30,42 --> 00:03:31,04
the scenes,

94
00:03:31,04 --> 00:03:34,34
the happy little elves down in the LPC hardware are

95
00:03:34,34 --> 00:03:36,49
going to use some conditional logic

96
00:03:36,5 --> 00:03:38,92
They're going to say if someone is writing to bios

97
00:03:38,92 --> 00:03:40,26
right enable equal to one

98
00:03:40,64 --> 00:03:45,08
And if bios control bios lock enable is currently set

99
00:03:45,31 --> 00:03:48,36
then they're going to fire off a system management interrupt

100
00:03:48,36 --> 00:03:48,76
signal

101
00:03:49,34 --> 00:03:51,7
Now we're going to learn about system management mode later

102
00:03:51,7 --> 00:03:52,09
on,

103
00:03:52,15 --> 00:03:55,61
but basically this processor is going to stop whatever it

104
00:03:55,61 --> 00:03:58,95
was doing transition into system management mode and then there's

105
00:03:58,95 --> 00:04:00,55
going to be some code over here

106
00:04:00,55 --> 00:04:01,78
The sme handler,

107
00:04:01,78 --> 00:04:05,1
system management interrupt handler and it's going to decide like

108
00:04:05,1 --> 00:04:07,8
does it want to allow the software running in the

109
00:04:07,8 --> 00:04:10,46
CPU To write bios right enable equaled one

110
00:04:11,14 --> 00:04:14,2
In this case it says no and then it rewrites

111
00:04:14,2 --> 00:04:15,36
that back to zero

112
00:04:15,84 --> 00:04:19,11
So to you running over on the CPU,

113
00:04:19,12 --> 00:04:22,04
it would look like you wrote a one and then

114
00:04:22,04 --> 00:04:25,02
when you try to read it back immediately afterwards,

115
00:04:25,02 --> 00:04:27,5
if you just very next few assembly instructions,

116
00:04:27,5 --> 00:04:28,76
you tried to read it back,

117
00:04:28,77 --> 00:04:30,58
you would see that it was still set to zero

118
00:04:30,58 --> 00:04:33,89
So it would look like some locked register where you're

119
00:04:33,89 --> 00:04:36,82
right of one was just discarded but it wasn't actually

120
00:04:36,82 --> 00:04:37,47
discarded,

121
00:04:37,48 --> 00:04:39,12
it was behind the scenes,

122
00:04:39,37 --> 00:04:40,86
there was a right that occurred,

123
00:04:40,86 --> 00:04:42,56
there was a interrupt that happened,

124
00:04:42,56 --> 00:04:45,38
there was an interrupt handled and then the interrupt handler

125
00:04:45,38 --> 00:04:47,8
decided whether or not it was going to allow that

126
00:04:48,08 --> 00:04:51,33
this is going to be an important distinction between lockable

127
00:04:51,33 --> 00:04:55,43
registers that just don't ever get set versus lockable registers

128
00:04:55,43 --> 00:04:57,56
that do get set and then get reset

129
00:04:57,84 --> 00:04:59,69
Uh It's going to be an important distinction for an

130
00:04:59,69 --> 00:05:00,56
attack later on

131
00:05:01,34 --> 00:05:05,37
So this is kind of an implementation of the useless

132
00:05:05,37 --> 00:05:05,84
box

133
00:05:05,85 --> 00:05:06,14
Right?

134
00:05:06,15 --> 00:05:08,02
So the thing where you turn it on and it

135
00:05:08,02 --> 00:05:08,96
turns itself off,

136
00:05:09,34 --> 00:05:10,6
so system measurement mode,

137
00:05:10,6 --> 00:05:11,66
if it sees bios right,

138
00:05:11,66 --> 00:05:12,25
enable,

139
00:05:12,28 --> 00:05:14,16
it can just turn it off immediately


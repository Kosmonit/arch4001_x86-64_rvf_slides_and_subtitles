1
00:00:00,399 --> 00:00:04,240
okay so we learned about protected range

2
00:00:02,720 --> 00:00:05,680
registers now let's learn about

3
00:00:04,240 --> 00:00:08,240
attacking them how an attacker would try

4
00:00:05,680 --> 00:00:10,080
to bypass them so i'm almost certainly

5
00:00:08,240 --> 00:00:12,960
going to regret this i'm going to build

6
00:00:10,080 --> 00:00:15,040
up the threat tree inline in the

7
00:00:12,960 --> 00:00:17,199
animations of this thing it looks way

8
00:00:15,040 --> 00:00:19,359
better but it's probably going to lead

9
00:00:17,199 --> 00:00:22,560
to extremely difficult video editing

10
00:00:19,359 --> 00:00:25,359
later anyways tucker goal is to write

11
00:00:22,560 --> 00:00:27,760
persistent malware into the spy flash

12
00:00:25,359 --> 00:00:30,320
the defender will set protected range

13
00:00:27,760 --> 00:00:31,760
registers in order to try to stop them

14
00:00:30,320 --> 00:00:34,079
what's the attacker going to do in

15
00:00:31,760 --> 00:00:35,920
response to that well the attacker can

16
00:00:34,079 --> 00:00:38,079
actually just go in and change the

17
00:00:35,920 --> 00:00:40,719
protected range registers

18
00:00:38,079 --> 00:00:42,960
and the defense for that is to lock the

19
00:00:40,719 --> 00:00:45,920
protected range registers with the flock

20
00:00:42,960 --> 00:00:48,160
down bit flash lockdown bit

21
00:00:45,920 --> 00:00:50,239
so if you go back and you look a little

22
00:00:48,160 --> 00:00:52,000
more carefully at this protected range

23
00:00:50,239 --> 00:00:53,680
register zero and of course i do

24
00:00:52,000 --> 00:00:55,840
recommend that you always look very

25
00:00:53,680 --> 00:00:57,920
carefully at the manuals whatever

26
00:00:55,840 --> 00:00:59,840
documentation is available to you you

27
00:00:57,920 --> 00:01:02,239
will see that it actually says of this

28
00:00:59,840 --> 00:01:04,400
register that it is read write and it

29
00:01:02,239 --> 00:01:07,200
doesn't actually say directly that it is

30
00:01:04,400 --> 00:01:09,840
lockable there's no lock bits right here

31
00:01:07,200 --> 00:01:13,119
so who protects the protector

32
00:01:09,840 --> 00:01:14,799
well this dude does flock down so if you

33
00:01:13,119 --> 00:01:16,720
read the thing it says the register

34
00:01:14,799 --> 00:01:19,759
cannot be written when the flock down

35
00:01:16,720 --> 00:01:23,040
bit is set to one so what's flock down

36
00:01:19,759 --> 00:01:25,360
flock down at spy bar plus four in the

37
00:01:23,040 --> 00:01:29,439
hardware sequencing flash status

38
00:01:25,360 --> 00:01:31,840
register is bit 15 and it says that it's

39
00:01:29,439 --> 00:01:34,079
readable writable and lockable

40
00:01:31,840 --> 00:01:36,079
and so typically when an intel register

41
00:01:34,079 --> 00:01:38,880
says it's lockable that will very often

42
00:01:36,079 --> 00:01:41,360
mean that if you set this thing then it

43
00:01:38,880 --> 00:01:43,759
can't be unset until the system resets

44
00:01:41,360 --> 00:01:46,000
but we'll go into this a whole lot more

45
00:01:43,759 --> 00:01:48,560
when we start talking about power on

46
00:01:46,000 --> 00:01:50,960
resets and sleep attacks

47
00:01:48,560 --> 00:01:52,799
so anyways flock down is a thing that

48
00:01:50,960 --> 00:01:54,399
when you set this to one then a whole

49
00:01:52,799 --> 00:01:56,079
bunch of other registers all of a sudden

50
00:01:54,399 --> 00:01:57,439
become non-writable

51
00:01:56,079 --> 00:01:59,040
so it says when you set this to one

52
00:01:57,439 --> 00:02:00,560
those flash program registers that are

53
00:01:59,040 --> 00:02:02,079
locked down by this bit cannot be

54
00:02:00,560 --> 00:02:03,759
written it doesn't actually tell you

55
00:02:02,079 --> 00:02:05,520
what they are you have to go scrape

56
00:02:03,759 --> 00:02:07,600
around in the documentation looking for

57
00:02:05,520 --> 00:02:09,119
things that reference flock down

58
00:02:07,600 --> 00:02:10,959
and then it says once this is set can

59
00:02:09,119 --> 00:02:12,800
only be cleared by hardware reset due to

60
00:02:10,959 --> 00:02:15,599
a global reset or host partition reset

61
00:02:12,800 --> 00:02:17,680
an intel me enabled system and so that's

62
00:02:15,599 --> 00:02:19,440
the little conditionality that we're

63
00:02:17,680 --> 00:02:22,319
going to learn a lot more about later on

64
00:02:19,440 --> 00:02:23,920
when we talk about power states so like

65
00:02:22,319 --> 00:02:25,440
i said you got a scrape around in the

66
00:02:23,920 --> 00:02:27,440
data sheets to figure out what is

67
00:02:25,440 --> 00:02:29,120
actually locked by flock down so what i

68
00:02:27,440 --> 00:02:31,440
came up with and you know who knows

69
00:02:29,120 --> 00:02:33,280
there could be more stuff but i came up

70
00:02:31,440 --> 00:02:35,519
with the protected range registers all

71
00:02:33,280 --> 00:02:38,560
five of them came up with the flash

72
00:02:35,519 --> 00:02:41,680
region access permissions register frap

73
00:02:38,560 --> 00:02:43,760
which i used to represent by frappuccino

74
00:02:41,680 --> 00:02:45,840
but that has a couple other things

75
00:02:43,760 --> 00:02:48,400
inside of it that have to do with bios

76
00:02:45,840 --> 00:02:50,239
master write access granting and read

77
00:02:48,400 --> 00:02:52,080
access granting which basically has to

78
00:02:50,239 --> 00:02:54,720
do with this notion in the optional

79
00:02:52,080 --> 00:02:57,440
material we talked about how uh you know

80
00:02:54,720 --> 00:02:59,760
different spy bus masters can or cannot

81
00:02:57,440 --> 00:03:01,599
access each other's flash well there's

82
00:02:59,760 --> 00:03:03,920
these registers that basically say well

83
00:03:01,599 --> 00:03:05,920
i would like to actually grant the

84
00:03:03,920 --> 00:03:07,519
management engine access to my flash you

85
00:03:05,920 --> 00:03:09,519
know i want the management engine to be

86
00:03:07,519 --> 00:03:12,400
able to write to my bios flash or i want

87
00:03:09,519 --> 00:03:14,879
the intel gigabit ethernet read engine

88
00:03:12,400 --> 00:03:16,400
to be able to read from my bios flash so

89
00:03:14,879 --> 00:03:18,319
those particular bits we don't super

90
00:03:16,400 --> 00:03:20,560
much care about them hopefully they're

91
00:03:18,319 --> 00:03:22,800
just set to zero so the bios is not

92
00:03:20,560 --> 00:03:25,120
giving out any extra permissions

93
00:03:22,800 --> 00:03:27,680
but if however they're set they will be

94
00:03:25,120 --> 00:03:29,680
locked down when flockdown is set

95
00:03:27,680 --> 00:03:32,080
then we mostly had seen things like

96
00:03:29,680 --> 00:03:34,239
hardware sequencing but the software

97
00:03:32,080 --> 00:03:36,400
sequencing register has within it a few

98
00:03:34,239 --> 00:03:38,879
bits that can be locked down and these

99
00:03:36,400 --> 00:03:40,959
have to do with the spy cycle frequency

100
00:03:38,879 --> 00:03:43,280
and so changing those could potentially

101
00:03:40,959 --> 00:03:44,959
lead to errors when talking to the spy

102
00:03:43,280 --> 00:03:46,720
flash chip so that's a good thing to

103
00:03:44,959 --> 00:03:48,959
lock down and then there's some other

104
00:03:46,720 --> 00:03:51,360
stuff about different op codes that can

105
00:03:48,959 --> 00:03:53,200
or can't be sent to the

106
00:03:51,360 --> 00:03:55,120
sent to this flash chip when you're in

107
00:03:53,200 --> 00:03:56,720
software sequencing mode

108
00:03:55,120 --> 00:03:58,480
flipping the page looking at the 100

109
00:03:56,720 --> 00:04:00,720
series chipset you know what i came up

110
00:03:58,480 --> 00:04:02,879
with is the protected range registers

111
00:04:00,720 --> 00:04:05,360
and this particular register name which

112
00:04:02,879 --> 00:04:07,840
still inside of it has this bios master

113
00:04:05,360 --> 00:04:09,760
write access grant and bios master read

114
00:04:07,840 --> 00:04:12,400
access grant so again those have to do

115
00:04:09,760 --> 00:04:15,200
with the bios granting other spy flash

116
00:04:12,400 --> 00:04:17,519
masters access read or write access to

117
00:04:15,200 --> 00:04:19,040
its region of the spy flash chip

118
00:04:17,519 --> 00:04:20,880
and for what it's worth as you're

119
00:04:19,040 --> 00:04:22,560
searching around in the manuals looking

120
00:04:20,880 --> 00:04:25,280
for flockdown you will come across the

121
00:04:22,560 --> 00:04:27,360
fact that the gigabit ethernet engine

122
00:04:25,280 --> 00:04:30,000
also has its own separate flock down

123
00:04:27,360 --> 00:04:31,280
that can be used to lock registers but

124
00:04:30,000 --> 00:04:34,320
we don't really care about that that

125
00:04:31,280 --> 00:04:34,320
much in this class


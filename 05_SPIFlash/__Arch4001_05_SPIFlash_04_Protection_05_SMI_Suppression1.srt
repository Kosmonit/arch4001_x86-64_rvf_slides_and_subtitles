1
00:00:00,080 --> 00:00:04,720
okay so we just learned about bios lock

2
00:00:01,920 --> 00:00:06,480
enable so that is one mechanism that a

3
00:00:04,720 --> 00:00:08,160
defender can use to try to defend

4
00:00:06,480 --> 00:00:10,719
against an attacker writing into the spy

5
00:00:08,160 --> 00:00:13,440
flash so what can attackers then do to

6
00:00:10,719 --> 00:00:14,639
defeat bios lock enable well the sleep

7
00:00:13,440 --> 00:00:16,480
wake vulnerabilities that we're going to

8
00:00:14,639 --> 00:00:18,400
talk about later in the class which we

9
00:00:16,480 --> 00:00:19,760
also said applied to protected range

10
00:00:18,400 --> 00:00:21,680
registers

11
00:00:19,760 --> 00:00:22,800
apply to this as well but we'll cover

12
00:00:21,680 --> 00:00:25,039
that later

13
00:00:22,800 --> 00:00:27,519
what else well once again you as an

14
00:00:25,039 --> 00:00:29,920
attacker could try to exploit the bios

15
00:00:27,519 --> 00:00:31,840
before the bios lock enable is set or

16
00:00:29,920 --> 00:00:34,480
any time before smm is locked down

17
00:00:31,840 --> 00:00:36,160
really because if you can get into smm

18
00:00:34,480 --> 00:00:38,480
then you can just go ahead and change

19
00:00:36,160 --> 00:00:41,040
the code that is otherwise rewriting

20
00:00:38,480 --> 00:00:43,520
bios write enable equal to zero and once

21
00:00:41,040 --> 00:00:46,079
again you know the defense is just find

22
00:00:43,520 --> 00:00:48,239
all the bugs right find all the bugs and

23
00:00:46,079 --> 00:00:49,840
mitigate all the bugs so what else could

24
00:00:48,239 --> 00:00:52,320
an attacker do well they could

25
00:00:49,840 --> 00:00:54,879
compromise smm at run time so this is

26
00:00:52,320 --> 00:00:57,039
about attacking the bios before smm is

27
00:00:54,879 --> 00:00:59,520
locked down and this is about attacking

28
00:00:57,039 --> 00:01:02,559
smm just any time that the system is

29
00:00:59,520 --> 00:01:04,320
running but that is a full threat tree

30
00:01:02,559 --> 00:01:06,960
unto itself understanding you know the

31
00:01:04,320 --> 00:01:09,760
various attack surfaces of smm so we're

32
00:01:06,960 --> 00:01:12,479
gonna skip that until later as well

33
00:01:09,760 --> 00:01:15,759
so what else what else well then there

34
00:01:12,479 --> 00:01:17,920
is a thing called suppressing of smis so

35
00:01:15,759 --> 00:01:20,960
we saw that bios write enable being

36
00:01:17,920 --> 00:01:23,360
rewritten to zero depends on the system

37
00:01:20,960 --> 00:01:25,920
management interrupt firing and then the

38
00:01:23,360 --> 00:01:28,159
code in system management mode rewriting

39
00:01:25,920 --> 00:01:30,000
the bios write enable equal to zero so

40
00:01:28,159 --> 00:01:32,240
if the system management interrupt never

41
00:01:30,000 --> 00:01:34,400
fires then there will never be an

42
00:01:32,240 --> 00:01:36,159
opportunity to write it back to zero

43
00:01:34,400 --> 00:01:38,400
so the first attack of that form that

44
00:01:36,159 --> 00:01:40,799
we'll consider was named charizard by

45
00:01:38,400 --> 00:01:42,960
sam cornwell and that was the

46
00:01:40,799 --> 00:01:44,960
recognition that there is a

47
00:01:42,960 --> 00:01:47,360
system management interrupt enable

48
00:01:44,960 --> 00:01:49,280
global smi enable field which if set to

49
00:01:47,360 --> 00:01:51,840
zero will suppress all system management

50
00:01:49,280 --> 00:01:53,520
interrupts on the system but there was a

51
00:01:51,840 --> 00:01:55,759
lock bit that could be set so that you

52
00:01:53,520 --> 00:01:57,040
can't actually set that to zero so what

53
00:01:55,759 --> 00:01:59,280
does that look like

54
00:01:57,040 --> 00:02:01,200
well here's from the slides and in the

55
00:01:59,280 --> 00:02:03,280
slides it said that at the time that we

56
00:02:01,200 --> 00:02:05,759
were exploring the internal miter

57
00:02:03,280 --> 00:02:07,600
systems we found that 40 percent of them

58
00:02:05,759 --> 00:02:10,239
did not set the system management

59
00:02:07,600 --> 00:02:12,800
interrupt lock and consequently

60
00:02:10,239 --> 00:02:14,879
at some register smi enable which is

61
00:02:12,800 --> 00:02:16,239
pmbase 30 which we haven't learned about

62
00:02:14,879 --> 00:02:17,840
yet but we'll learn about in the system

63
00:02:16,239 --> 00:02:19,680
management section

64
00:02:17,840 --> 00:02:22,160
some register had a global system

65
00:02:19,680 --> 00:02:25,120
management enable and if it's set to

66
00:02:22,160 --> 00:02:27,760
zero then no sms straight up will be

67
00:02:25,120 --> 00:02:30,319
generated by the pch and if it's set to

68
00:02:27,760 --> 00:02:32,480
one then it is allowed to enable system

69
00:02:30,319 --> 00:02:33,680
management interrupts and it says there

70
00:02:32,480 --> 00:02:35,840
that when the system management

71
00:02:33,680 --> 00:02:37,760
interrupt lock is set this bit cannot be

72
00:02:35,840 --> 00:02:39,840
changed so you couldn't change it from a

73
00:02:37,760 --> 00:02:41,920
one to a zero which is what an attacker

74
00:02:39,840 --> 00:02:43,920
would want to do so we said at the time

75
00:02:41,920 --> 00:02:46,480
that about forty percent of our systems

76
00:02:43,920 --> 00:02:49,360
you know on our internal network were

77
00:02:46,480 --> 00:02:51,519
all having this lock nut set

78
00:02:49,360 --> 00:02:53,599
so here it is just restated in the 5

79
00:02:51,519 --> 00:02:55,680
series chipset and that is basically the

80
00:02:53,599 --> 00:02:58,800
exact same thing as was shown in that

81
00:02:55,680 --> 00:03:01,920
presentation and then the smi lock bit

82
00:02:58,800 --> 00:03:04,560
is found you guessed it in the lpc you

83
00:03:01,920 --> 00:03:06,480
know this is why we said the lpc

84
00:03:04,560 --> 00:03:08,959
pci configuration address space is

85
00:03:06,480 --> 00:03:10,879
really really important in this class so

86
00:03:08,959 --> 00:03:14,959
general power management configuration

87
00:03:10,879 --> 00:03:17,360
lpc bus 0 device 31 function 0 offset a0

88
00:03:14,959 --> 00:03:18,959
there's a register and at bit4 is the

89
00:03:17,360 --> 00:03:20,800
smi lock

90
00:03:18,959 --> 00:03:22,800
so that is what's going to stop an

91
00:03:20,800 --> 00:03:24,400
attacker from being able to disable all

92
00:03:22,800 --> 00:03:25,920
the system management interrupts and

93
00:03:24,400 --> 00:03:28,560
consequently be able to write to the

94
00:03:25,920 --> 00:03:31,440
bios if bios lock enable is the only

95
00:03:28,560 --> 00:03:33,840
thing protecting the system

96
00:03:31,440 --> 00:03:36,319
on the 100 series chipset it's more or

97
00:03:33,840 --> 00:03:39,760
less the same we once again have a smi

98
00:03:36,319 --> 00:03:42,400
enable and there is still the global smi

99
00:03:39,760 --> 00:03:44,560
enable bit in order to say if it's zero

100
00:03:42,400 --> 00:03:46,959
then you're not allowed to have any sms

101
00:03:44,560 --> 00:03:48,720
whatsoever and if it's one then you are

102
00:03:46,959 --> 00:03:51,840
and once more it says that it is locked

103
00:03:48,720 --> 00:03:53,840
by smi lock bit

104
00:03:51,840 --> 00:03:55,760
the smi lock bit is then found in a

105
00:03:53,840 --> 00:03:57,280
slightly different location on the 100

106
00:03:55,760 --> 00:04:00,000
series chipset you can see it says

107
00:03:57,280 --> 00:04:02,159
device 31 function 2. so instead of

108
00:04:00,000 --> 00:04:04,720
function 0 it's function 2 but it is

109
00:04:02,159 --> 00:04:07,280
still at offset a0 and it does still

110
00:04:04,720 --> 00:04:09,760
behave the same way so once this is set

111
00:04:07,280 --> 00:04:11,200
then writes to the global smi enable bit

112
00:04:09,760 --> 00:04:13,519
will have no effect

113
00:04:11,200 --> 00:04:15,840
so the primary defense for that is to

114
00:04:13,519 --> 00:04:19,840
set the smi lock bit but there are other

115
00:04:15,840 --> 00:04:22,000
defenses such as setting smm bwp bios

116
00:04:19,840 --> 00:04:23,600
write protect so what is that so we saw

117
00:04:22,000 --> 00:04:25,840
this diagram back when we were looking

118
00:04:23,600 --> 00:04:28,000
at the bios write enable and bios lock

119
00:04:25,840 --> 00:04:30,880
enable but up here at bit5 there is

120
00:04:28,000 --> 00:04:32,720
actually this other thing smm bwp smm

121
00:04:30,880 --> 00:04:34,400
bios write protect and i don't think

122
00:04:32,720 --> 00:04:37,759
that disable should be there because

123
00:04:34,400 --> 00:04:41,120
when it's won it is enabled not disabled

124
00:04:37,759 --> 00:04:43,759
anyways so when bit 5 is set to 1 the

125
00:04:41,120 --> 00:04:45,440
bios region of smm is sorry the bias

126
00:04:43,759 --> 00:04:47,520
region smm protection is enabled the

127
00:04:45,440 --> 00:04:49,919
bios region is not writable unless all

128
00:04:47,520 --> 00:04:52,400
processors are in smm

129
00:04:49,919 --> 00:04:54,880
so this notion of you know you can

130
00:04:52,400 --> 00:04:57,520
suppress the smis and then you can write

131
00:04:54,880 --> 00:05:00,000
to the bios if this bios write protect

132
00:04:57,520 --> 00:05:02,080
bit is set then that would mean well it

133
00:05:00,000 --> 00:05:04,560
doesn't matter if you suppress the sms

134
00:05:02,080 --> 00:05:06,960
because you and your attacker code are

135
00:05:04,560 --> 00:05:08,639
probably not in smm if you were then you

136
00:05:06,960 --> 00:05:10,000
wouldn't need to do any of these games

137
00:05:08,639 --> 00:05:11,360
so you're just running in the kernel

138
00:05:10,000 --> 00:05:14,560
somewhere you're trying to write to the

139
00:05:11,360 --> 00:05:16,400
bios but if this bit was set then the

140
00:05:14,560 --> 00:05:19,039
hardware will still not let you write to

141
00:05:16,400 --> 00:05:21,280
the bios because you are not in smm it

142
00:05:19,039 --> 00:05:23,440
needs to have all the processors in smm

143
00:05:21,280 --> 00:05:25,440
before it can write so this bit didn't

144
00:05:23,440 --> 00:05:27,440
even exist on you know some of the older

145
00:05:25,440 --> 00:05:29,759
systems and that's a good reason to

146
00:05:27,440 --> 00:05:32,000
always read the manuals to see what new

147
00:05:29,759 --> 00:05:34,400
protections come into existence

148
00:05:32,000 --> 00:05:36,720
and so you know this is a very powerful

149
00:05:34,400 --> 00:05:38,720
defense against an attacker who's only

150
00:05:36,720 --> 00:05:41,759
in kernel and is trying to prayerfully

151
00:05:38,720 --> 00:05:43,360
escalate to bios on the 100 series data

152
00:05:41,759 --> 00:05:46,400
sheet instead

153
00:05:43,360 --> 00:05:49,520
it is still in the lpc device so

154
00:05:46,400 --> 00:05:51,840
device 31 function zero but now they've

155
00:05:49,520 --> 00:05:56,560
changed the name to

156
00:05:51,840 --> 00:05:59,120
enable in smm sts so probably status

157
00:05:56,560 --> 00:06:01,440
but unfortunately this description

158
00:05:59,120 --> 00:06:04,560
leaves a lot to be desired

159
00:06:01,440 --> 00:06:09,440
it says things like you know if this bit

160
00:06:04,560 --> 00:06:10,639
5 is set then wpd must be 1 and in smm

161
00:06:09,440 --> 00:06:13,120
sts

162
00:06:10,639 --> 00:06:17,919
which is at address fed 3

163
00:06:13,120 --> 00:06:19,919
0 8 8 0 bit 0 must be 1. well that thing

164
00:06:17,919 --> 00:06:22,400
is not documented anywhere that i can

165
00:06:19,919 --> 00:06:25,280
find hopefully it's documented in the

166
00:06:22,400 --> 00:06:27,440
private documentation but you know this

167
00:06:25,280 --> 00:06:29,440
this doesn't really help someone who was

168
00:06:27,440 --> 00:06:31,280
uh you know trying to actually lock down

169
00:06:29,440 --> 00:06:33,840
their system

170
00:06:31,280 --> 00:06:35,360
okay then there is one more way that

171
00:06:33,840 --> 00:06:37,440
potentially could be used to defend

172
00:06:35,360 --> 00:06:39,680
against this and that is technology

173
00:06:37,440 --> 00:06:43,039
called intel bioscarred

174
00:06:39,680 --> 00:06:44,560
so intel bioscard is mostly you know

175
00:06:43,039 --> 00:06:46,560
private documentation so there's not

176
00:06:44,560 --> 00:06:48,720
really much i can point you at here i

177
00:06:46,560 --> 00:06:50,400
can just show you the marketing picture

178
00:06:48,720 --> 00:06:53,120
and according to the marketing picture

179
00:06:50,400 --> 00:06:56,960
the basic idea is that updates to the

180
00:06:53,120 --> 00:06:59,680
bios can only occur uh via something

181
00:06:56,960 --> 00:07:02,400
called an authenticated code module so

182
00:06:59,680 --> 00:07:05,280
an authenticated code module is a blob

183
00:07:02,400 --> 00:07:07,199
of code that is signed by intel

184
00:07:05,280 --> 00:07:08,720
the you know other third-party vendors

185
00:07:07,199 --> 00:07:10,960
are not allowed to change it or alter it

186
00:07:08,720 --> 00:07:13,520
in any way but there's this notion that

187
00:07:10,960 --> 00:07:15,919
they could assign a you know bios update

188
00:07:13,520 --> 00:07:17,199
package and with that signature they

189
00:07:15,919 --> 00:07:19,840
could feed the bios update and the

190
00:07:17,199 --> 00:07:22,560
signature to the bioscarred agent it

191
00:07:19,840 --> 00:07:24,479
would run an smm and it and only it

192
00:07:22,560 --> 00:07:27,280
would be allowed to write the update to

193
00:07:24,479 --> 00:07:29,360
the spy flash so it's kind of a even

194
00:07:27,280 --> 00:07:31,919
more privileged version of it's not good

195
00:07:29,360 --> 00:07:34,319
enough to just be an smm you also

196
00:07:31,919 --> 00:07:36,479
specifically have to be you know this

197
00:07:34,319 --> 00:07:38,479
authenticated code module running by

198
00:07:36,479 --> 00:07:40,319
intel and presumably behind the scenes

199
00:07:38,479 --> 00:07:41,919
there's you know some sort of special

200
00:07:40,319 --> 00:07:44,000
bits that are twiddled that can only be

201
00:07:41,919 --> 00:07:45,919
twiddled by an authenticated code module

202
00:07:44,000 --> 00:07:47,840
that makes it so that only their smm

203
00:07:45,919 --> 00:07:51,240
code can actually

204
00:07:47,840 --> 00:07:51,240
write to the bios


1
00:00:00,719 --> 00:00:04,080
all right let's go through each of the

2
00:00:02,240 --> 00:00:05,759
chunks of the flash descriptor one at a

3
00:00:04,080 --> 00:00:07,759
time we already saw signature it's

4
00:00:05,759 --> 00:00:09,200
pretty simple it's just a magic number

5
00:00:07,759 --> 00:00:12,160
that the hardware reads to know that

6
00:00:09,200 --> 00:00:14,400
it's operating in flash descriptor mode

7
00:00:12,160 --> 00:00:16,480
next is the descriptor map and that

8
00:00:14,400 --> 00:00:18,560
basically provides the pointers to each

9
00:00:16,480 --> 00:00:20,240
of these subsequent sections so it tells

10
00:00:18,560 --> 00:00:23,039
you where the component is the region

11
00:00:20,240 --> 00:00:24,880
the master and so forth it also contains

12
00:00:23,039 --> 00:00:27,039
some information about how many spy

13
00:00:24,880 --> 00:00:29,359
flash chips are used in this particular

14
00:00:27,039 --> 00:00:31,599
setup the components section contains

15
00:00:29,359 --> 00:00:33,360
more information about the particulars

16
00:00:31,599 --> 00:00:35,440
of the spy flash chips that are used on

17
00:00:33,360 --> 00:00:37,440
this particular hardware for instance

18
00:00:35,440 --> 00:00:39,840
things around their clock frequencies

19
00:00:37,440 --> 00:00:41,760
that they run and if there's any invalid

20
00:00:39,840 --> 00:00:44,480
instructions which should never be sent

21
00:00:41,760 --> 00:00:45,920
to this particular flash chip the region

22
00:00:44,480 --> 00:00:47,760
section is then one of the most

23
00:00:45,920 --> 00:00:49,680
interesting parts because this is the

24
00:00:47,760 --> 00:00:51,360
thing that says where does the flash

25
00:00:49,680 --> 00:00:53,600
descriptor start well of course it's

26
00:00:51,360 --> 00:00:55,680
going to start at offset you know 0 or

27
00:00:53,600 --> 00:00:58,160
10. where does the bios region start

28
00:00:55,680 --> 00:01:00,640
where does the csme region start etc so

29
00:00:58,160 --> 00:01:02,719
this provides pointers up and out of the

30
00:01:00,640 --> 00:01:04,879
flash descriptor so that the hardware

31
00:01:02,719 --> 00:01:06,640
knows where the other regions are and

32
00:01:04,879 --> 00:01:09,360
for newer hardware even though they may

33
00:01:06,640 --> 00:01:11,280
support things like the platform data

34
00:01:09,360 --> 00:01:12,799
region or the embedded controller region

35
00:01:11,280 --> 00:01:15,280
if they're not actually using it on that

36
00:01:12,799 --> 00:01:17,280
particular system then the expectation

37
00:01:15,280 --> 00:01:19,840
is that these registers will be filled

38
00:01:17,280 --> 00:01:21,680
in with base address of one ff and a

39
00:01:19,840 --> 00:01:23,920
limit of zero and that basically tells

40
00:01:21,680 --> 00:01:26,159
the pch you know this is not actually

41
00:01:23,920 --> 00:01:29,759
used on this system so once again it is

42
00:01:26,159 --> 00:01:33,040
the fl reg 0 through 5 which is on the

43
00:01:29,759 --> 00:01:35,200
flash descriptor and the f reg 0 through

44
00:01:33,040 --> 00:01:37,280
5 which is the memory mapped copy found

45
00:01:35,200 --> 00:01:40,240
somewhere in the spy bar f reg for

46
00:01:37,280 --> 00:01:42,399
memory in the spy bar fl reg for the

47
00:01:40,240 --> 00:01:44,799
thing on the flash descriptor

48
00:01:42,399 --> 00:01:47,280
the master region has to do with what

49
00:01:44,799 --> 00:01:49,520
spy bus masters meaning things that are

50
00:01:47,280 --> 00:01:52,720
allowed to cause transactions on the spy

51
00:01:49,520 --> 00:01:55,680
bus what spy bus masters are allowed to

52
00:01:52,720 --> 00:01:57,200
write to other regions of other devices

53
00:01:55,680 --> 00:01:59,759
so this has to do with for instance

54
00:01:57,200 --> 00:02:01,680
whether the cpu slash bios is allowed to

55
00:01:59,759 --> 00:02:03,920
write to the management engine because

56
00:02:01,680 --> 00:02:05,920
that's one particular spy bus master the

57
00:02:03,920 --> 00:02:07,520
management engine is a different spy bus

58
00:02:05,920 --> 00:02:09,280
master the gigabit ethernet a different

59
00:02:07,520 --> 00:02:11,200
one embedded controller at if one and so

60
00:02:09,280 --> 00:02:13,440
forth well there's no so forth those are

61
00:02:11,200 --> 00:02:15,760
the only ones that exist at this time so

62
00:02:13,440 --> 00:02:18,000
anyways the master region describes

63
00:02:15,760 --> 00:02:20,000
permissions about which bus masters are

64
00:02:18,000 --> 00:02:22,080
allowed to write to other bus master

65
00:02:20,000 --> 00:02:24,720
sections so this is actually the first

66
00:02:22,080 --> 00:02:27,200
bit of flash right access control that

67
00:02:24,720 --> 00:02:28,879
we can see and specifically it has to do

68
00:02:27,200 --> 00:02:31,280
not with you know software coming from

69
00:02:28,879 --> 00:02:32,720
within the cpu like making sure that you

70
00:02:31,280 --> 00:02:34,560
know only kernel could write to it or

71
00:02:32,720 --> 00:02:36,800
something like that no this has to do

72
00:02:34,560 --> 00:02:39,200
with these other processors these other

73
00:02:36,800 --> 00:02:41,200
spy bus masters and whether or not they

74
00:02:39,200 --> 00:02:43,360
can write two different regions the soft

75
00:02:41,200 --> 00:02:45,519
straps like we said are things that

76
00:02:43,360 --> 00:02:47,920
allow for reconfiguration of hardware

77
00:02:45,519 --> 00:02:50,160
functionality without having to actually

78
00:02:47,920 --> 00:02:52,400
reconfigure the hardware and how it's

79
00:02:50,160 --> 00:02:54,879
laid out on the pcb and so there was the

80
00:02:52,400 --> 00:02:57,280
mch straps in the ich straps when it was

81
00:02:54,879 --> 00:03:00,640
an orthopage southbridge configuration

82
00:02:57,280 --> 00:03:03,360
and pch straps now on pch systems now

83
00:03:00,640 --> 00:03:06,159
even back in the ich10 world where

84
00:03:03,360 --> 00:03:08,319
everything else was described the soft

85
00:03:06,159 --> 00:03:10,080
straps for the ich 10 were not actually

86
00:03:08,319 --> 00:03:11,840
publicly described and those were only

87
00:03:10,080 --> 00:03:14,159
available in the confidential spy

88
00:03:11,840 --> 00:03:16,159
programming guide then jumping up to the

89
00:03:14,159 --> 00:03:18,400
top of the flash descriptor at four

90
00:03:16,159 --> 00:03:20,959
kilobytes there's what's called the oem

91
00:03:18,400 --> 00:03:23,120
section and this is basically just 256

92
00:03:20,959 --> 00:03:25,760
bytes for an oem to use however they

93
00:03:23,120 --> 00:03:27,840
want the pch and the ich don't read and

94
00:03:25,760 --> 00:03:30,080
don't do anything with this

95
00:03:27,840 --> 00:03:32,239
then there's the descriptor upper map

96
00:03:30,080 --> 00:03:35,440
and this describes where you can find

97
00:03:32,239 --> 00:03:37,360
the management engine vscc table that is

98
00:03:35,440 --> 00:03:40,239
the vendor-specific component

99
00:03:37,360 --> 00:03:42,560
capabilities which basically is telling

100
00:03:40,239 --> 00:03:44,239
something about how the spy flash chips

101
00:03:42,560 --> 00:03:46,239
can be used with the management engine

102
00:03:44,239 --> 00:03:48,480
then there's the management engine vscc

103
00:03:46,239 --> 00:03:50,959
table this contains things like the

104
00:03:48,480 --> 00:03:52,799
jetic id which is a standardized id that

105
00:03:50,959 --> 00:03:55,040
describes who the vendor was and what

106
00:03:52,799 --> 00:03:58,000
the particular device is so that'll tell

107
00:03:55,040 --> 00:04:00,560
you for instance this is a win bond 64

108
00:03:58,000 --> 00:04:02,799
megabit chip that kind of thing

109
00:04:00,560 --> 00:04:04,959
but it also describes attributes that

110
00:04:02,799 --> 00:04:06,480
can be used for spy partitions and so

111
00:04:04,959 --> 00:04:08,959
there's a notion of an upper and lower

112
00:04:06,480 --> 00:04:11,519
spy partition the idea is that there

113
00:04:08,959 --> 00:04:13,200
might be different properties on a given

114
00:04:11,519 --> 00:04:14,560
spy flash chip or they might be

115
00:04:13,200 --> 00:04:16,320
different properties between two

116
00:04:14,560 --> 00:04:18,560
different spy flash chips and

117
00:04:16,320 --> 00:04:21,040
consequently you might need to

118
00:04:18,560 --> 00:04:23,199
accommodate the different behaviors

119
00:04:21,040 --> 00:04:25,840
so this is a visualization from the 5

120
00:04:23,199 --> 00:04:27,600
series pch it basically says there can

121
00:04:25,840 --> 00:04:29,440
be an upper flash partition and a lower

122
00:04:27,600 --> 00:04:30,960
flash partition and there's some

123
00:04:29,440 --> 00:04:33,199
boundary and it basically says you know

124
00:04:30,960 --> 00:04:35,120
there might be different you know erase

125
00:04:33,199 --> 00:04:37,520
granularities or something like that so

126
00:04:35,120 --> 00:04:40,639
maybe in a race up here a race is 4k and

127
00:04:37,520 --> 00:04:42,320
down here it races 64k because it all

128
00:04:40,639 --> 00:04:44,639
depends on the particulars of the spy

129
00:04:42,320 --> 00:04:47,840
flash chips a single spy flash chip

130
00:04:44,639 --> 00:04:49,520
could support reconfiguration because

131
00:04:47,840 --> 00:04:50,800
some properties of how it's ultimately

132
00:04:49,520 --> 00:04:52,800
going to be used suggest that you know

133
00:04:50,800 --> 00:04:54,800
you want to do bulk erases in one region

134
00:04:52,800 --> 00:04:56,400
and smaller races in another region

135
00:04:54,800 --> 00:04:57,840
maybe you want to even be able to erase

136
00:04:56,400 --> 00:04:59,919
you know one bite at a time instead of

137
00:04:57,840 --> 00:05:02,080
four kilobytes and some different chips

138
00:04:59,919 --> 00:05:04,400
support that but for the most part you

139
00:05:02,080 --> 00:05:06,639
should pretty much just think of it like

140
00:05:04,400 --> 00:05:08,400
most of the time the upper and lower is

141
00:05:06,639 --> 00:05:10,160
going to be about upper and lower flash

142
00:05:08,400 --> 00:05:12,400
chips

143
00:05:10,160 --> 00:05:14,080
all right now with all of that said i'm

144
00:05:12,400 --> 00:05:17,039
going to have a lab for you where you

145
00:05:14,080 --> 00:05:20,479
read the fun ich 10 data sheet

146
00:05:17,039 --> 00:05:23,280
so i want you to take the optiplex 7010

147
00:05:20,479 --> 00:05:25,280
flash image that is provided to you and

148
00:05:23,280 --> 00:05:27,840
i want you to go back and read the ich

149
00:05:25,280 --> 00:05:30,320
10 data sheet now that is not actually

150
00:05:27,840 --> 00:05:31,840
the correct exact data sheet now we

151
00:05:30,320 --> 00:05:34,880
actually saw in a previous section that

152
00:05:31,840 --> 00:05:38,720
the optiplex 7010 is using a 7 series

153
00:05:34,880 --> 00:05:41,440
pch not a 10 series ich so it's not

154
00:05:38,720 --> 00:05:43,759
going to be exactly exactly correct but

155
00:05:41,440 --> 00:05:46,240
other than the soft straps pretty much

156
00:05:43,759 --> 00:05:47,440
everything exactly maps up one to one so

157
00:05:46,240 --> 00:05:49,360
i'm going to have a bunch of questions

158
00:05:47,440 --> 00:05:50,960
that you can go read on the website and

159
00:05:49,360 --> 00:05:53,120
i want you to answer all of those

160
00:05:50,960 --> 00:05:54,639
questions you're basically going to hand

161
00:05:53,120 --> 00:05:56,960
parse your way through

162
00:05:54,639 --> 00:05:59,120
reading the data sheet parsing the data

163
00:05:56,960 --> 00:06:01,039
that you see and try to answer these

164
00:05:59,120 --> 00:06:03,199
questions and then afterwards i'll have

165
00:06:01,039 --> 00:06:04,800
some videos where i hand parse along

166
00:06:03,199 --> 00:06:07,039
with you and show you exactly the

167
00:06:04,800 --> 00:06:08,720
correct interpretation so you can see if

168
00:06:07,039 --> 00:06:11,120
you got anything wrong why you got it

169
00:06:08,720 --> 00:06:11,120
wrong


1
00:00:00,160 --> 00:00:04,880
okay so let's assume that all of those

2
00:00:02,159 --> 00:00:06,720
smi suppression vulnerabilities were you

3
00:00:04,880 --> 00:00:08,240
know closed off by using the appropriate

4
00:00:06,720 --> 00:00:10,400
defensive technology

5
00:00:08,240 --> 00:00:12,799
what else is there well there's one more

6
00:00:10,400 --> 00:00:14,559
thing an attack called speed racer and

7
00:00:12,799 --> 00:00:17,440
this was an actual hardware race

8
00:00:14,559 --> 00:00:19,760
condition in intel hardware and the

9
00:00:17,440 --> 00:00:22,880
defense for this would be to set the smm

10
00:00:19,760 --> 00:00:26,400
bwp which was again the you must be in

11
00:00:22,880 --> 00:00:28,320
smm in order to write to the bios so how

12
00:00:26,400 --> 00:00:31,039
did that work well

13
00:00:28,320 --> 00:00:33,360
if you're on a pch and later chipset and

14
00:00:31,039 --> 00:00:34,719
if it's at smm bwp

15
00:00:33,360 --> 00:00:36,559
then you are not vulnerable right so

16
00:00:34,719 --> 00:00:39,200
there you go that's the defensive path

17
00:00:36,559 --> 00:00:41,120
if it didn't set smm bwp and it used

18
00:00:39,200 --> 00:00:43,840
protected range registers

19
00:00:41,120 --> 00:00:46,320
then are you vulnerable well if no prrs

20
00:00:43,840 --> 00:00:47,840
then yes so again i said that you know

21
00:00:46,320 --> 00:00:49,600
there's these two main mechanism

22
00:00:47,840 --> 00:00:51,600
protected range registers and the bios

23
00:00:49,600 --> 00:00:53,280
lock enable if you're not using

24
00:00:51,600 --> 00:00:54,879
protected range registers and you only

25
00:00:53,280 --> 00:00:56,800
have bios lock enable then you would be

26
00:00:54,879 --> 00:00:58,800
vulnerable if you are using protected

27
00:00:56,800 --> 00:01:00,640
range registers this is listed as maybe

28
00:00:58,800 --> 00:01:01,920
vulnerable because if you go and look at

29
00:01:00,640 --> 00:01:04,159
this research

30
00:01:01,920 --> 00:01:06,880
basically you know rafal and cory went

31
00:01:04,159 --> 00:01:09,600
off and found vulnerabilities in nvram

32
00:01:06,880 --> 00:01:11,520
variables and so nvram is typically not

33
00:01:09,600 --> 00:01:14,080
going to be protectable by a protected

34
00:01:11,520 --> 00:01:15,840
range registers so if an attacker could

35
00:01:14,080 --> 00:01:17,520
steal right to an nvram variable they

36
00:01:15,840 --> 00:01:19,680
could potentially exploit the bios so

37
00:01:17,520 --> 00:01:21,280
that goes back to that notion of you

38
00:01:19,680 --> 00:01:23,040
know exploiting the bios is one of the

39
00:01:21,280 --> 00:01:25,520
different ways that you could attack the

40
00:01:23,040 --> 00:01:26,720
system in order to bypass bios lock

41
00:01:25,520 --> 00:01:30,000
enable

42
00:01:26,720 --> 00:01:31,840
so how does speed racer work well

43
00:01:30,000 --> 00:01:33,920
introduce speed racer and we are now

44
00:01:31,840 --> 00:01:37,680
going to do a race condition between

45
00:01:33,920 --> 00:01:40,000
multiple cpus or multiple cores and so

46
00:01:37,680 --> 00:01:42,720
this core down here is going to set up

47
00:01:40,000 --> 00:01:44,560
all of the spy bar memory mapped i o

48
00:01:42,720 --> 00:01:46,479
registers so they've got an address in

49
00:01:44,560 --> 00:01:49,040
there they've got a write ready and all

50
00:01:46,479 --> 00:01:50,880
they need to do is just hit go and then

51
00:01:49,040 --> 00:01:52,399
a transaction will occur

52
00:01:50,880 --> 00:01:54,320
but you need to set the bios right

53
00:01:52,399 --> 00:01:57,280
enable before you hit go otherwise it's

54
00:01:54,320 --> 00:01:59,280
not going to work so the first cpu is

55
00:01:57,280 --> 00:02:01,040
going to set bios write enable

56
00:01:59,280 --> 00:02:03,360
and down in the hardware we know we've

57
00:02:01,040 --> 00:02:05,439
got our lpc hardware spy hardware i'm

58
00:02:03,360 --> 00:02:08,080
just mixing them together here we said

59
00:02:05,439 --> 00:02:09,920
bios write enable when set to 1 goes

60
00:02:08,080 --> 00:02:11,599
down to the lpc hardware they've got

61
00:02:09,920 --> 00:02:14,319
their conditional logic that says well i

62
00:02:11,599 --> 00:02:17,760
see that bios control bios lock enable

63
00:02:14,319 --> 00:02:19,760
is set so i'm going to fire off an smi

64
00:02:17,760 --> 00:02:21,520
now the smi will fire

65
00:02:19,760 --> 00:02:22,400
and this particular processor will go

66
00:02:21,520 --> 00:02:24,800
into

67
00:02:22,400 --> 00:02:27,360
system management mode but the thing is

68
00:02:24,800 --> 00:02:30,160
that not all processors will necessarily

69
00:02:27,360 --> 00:02:32,319
enter smm exactly at the same time so

70
00:02:30,160 --> 00:02:34,720
this processor could still be active for

71
00:02:32,319 --> 00:02:37,599
a very short period of time before it

72
00:02:34,720 --> 00:02:39,680
too is forced into smm so this thing may

73
00:02:37,599 --> 00:02:42,080
be forced in smm and this thing may be

74
00:02:39,680 --> 00:02:45,280
ready to run its rewrite of bios write

75
00:02:42,080 --> 00:02:47,680
enable to zero but before that code gets

76
00:02:45,280 --> 00:02:50,560
a chance to rewrite this this code

77
00:02:47,680 --> 00:02:53,920
successfully wins the race it's not an

78
00:02:50,560 --> 00:02:56,239
smm and it issues the command f go and

79
00:02:53,920 --> 00:02:58,879
then all of a sudden its spy right

80
00:02:56,239 --> 00:03:01,360
transaction is allowed to proceed

81
00:02:58,879 --> 00:03:02,879
through to the flash chip so in this way

82
00:03:01,360 --> 00:03:04,640
you know this is the

83
00:03:02,879 --> 00:03:06,800
sort of you know problem with this

84
00:03:04,640 --> 00:03:08,879
little you know useless box of setting

85
00:03:06,800 --> 00:03:10,879
and unsetting setting and unsetting

86
00:03:08,879 --> 00:03:12,879
this did get written to one

87
00:03:10,879 --> 00:03:14,640
and then this race condition can come

88
00:03:12,879 --> 00:03:17,040
through and write to the flash chip

89
00:03:14,640 --> 00:03:19,200
before it can be you know quickly reset

90
00:03:17,040 --> 00:03:20,879
to zero so fundamentally a race

91
00:03:19,200 --> 00:03:23,200
condition in the hardware

92
00:03:20,879 --> 00:03:26,000
and why does the bios write enable smm

93
00:03:23,200 --> 00:03:29,120
bwp why does that bit help protect you

94
00:03:26,000 --> 00:03:32,159
because even if this thing won the race

95
00:03:29,120 --> 00:03:33,760
if the smm bwp was set it would this

96
00:03:32,159 --> 00:03:35,920
hardware would recognize you know this

97
00:03:33,760 --> 00:03:38,159
cpu is not an smm right now i should not

98
00:03:35,920 --> 00:03:40,080
allow it to write to the flash chip

99
00:03:38,159 --> 00:03:42,720
so that's why that defense works for us

100
00:03:40,080 --> 00:03:42,720
here


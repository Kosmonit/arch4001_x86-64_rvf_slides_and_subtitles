1
00:00:00,399 --> 00:00:06,560
next up in the flash map portion of the

2
00:00:03,760 --> 00:00:10,000
flash descriptor was the pch soft straps

3
00:00:06,560 --> 00:00:12,559
which it told us was that offset hex 100

4
00:00:10,000 --> 00:00:14,719
and that's that right there

5
00:00:12,559 --> 00:00:15,440
and furthermore it tells us that there

6
00:00:14,719 --> 00:00:18,880
are

7
00:00:15,440 --> 00:00:20,880
18 so hex 12 18

8
00:00:18,880 --> 00:00:23,119
possible soft straps inside of this

9
00:00:20,880 --> 00:00:25,680
thing each which is d word sized so four

10
00:00:23,119 --> 00:00:27,279
bite sized but unfortunately there's not

11
00:00:25,680 --> 00:00:29,039
really anything i can tell you about

12
00:00:27,279 --> 00:00:30,640
that given the fact that these are

13
00:00:29,039 --> 00:00:33,600
described in the confidential

14
00:00:30,640 --> 00:00:36,480
documentation of intel so unless you go

15
00:00:33,600 --> 00:00:38,399
sign an intel nda or you go find some

16
00:00:36,480 --> 00:00:41,680
leak documentation there's nothing you

17
00:00:38,399 --> 00:00:41,680
can really know about that


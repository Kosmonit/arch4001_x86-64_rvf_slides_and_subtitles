1
00:00:00,080 --> 00:00:03,679
all right now it's time for you to go

2
00:00:01,680 --> 00:00:06,000
off and find which data sheets are

3
00:00:03,679 --> 00:00:08,800
applicable to you so that's our goal

4
00:00:06,000 --> 00:00:10,639
identify the cpa cpu and pch as

5
00:00:08,800 --> 00:00:12,480
applicable to find which data sheets

6
00:00:10,639 --> 00:00:14,080
matter and we have to find these data

7
00:00:12,480 --> 00:00:16,000
sheets because we're going to be looking

8
00:00:14,080 --> 00:00:17,520
at some memory mapped i o registers

9
00:00:16,000 --> 00:00:20,080
again we'll learn more about memory

10
00:00:17,520 --> 00:00:21,680
mapped io next in the class but we need

11
00:00:20,080 --> 00:00:22,960
to look at some registers in order to

12
00:00:21,680 --> 00:00:24,080
understand whether the system is

13
00:00:22,960 --> 00:00:25,599
vulnerable

14
00:00:24,080 --> 00:00:27,199
so some of these registers have stayed

15
00:00:25,599 --> 00:00:28,960
the same over time and other ones have

16
00:00:27,199 --> 00:00:30,800
moved around so that means you have to

17
00:00:28,960 --> 00:00:32,960
have a bit of flexibility in order to

18
00:00:30,800 --> 00:00:36,880
find things in the data sheets

19
00:00:32,960 --> 00:00:38,879
so first thing let's find an lpc device

20
00:00:36,880 --> 00:00:41,360
in the data sheets well we said we don't

21
00:00:38,879 --> 00:00:43,600
care about this particular architecture

22
00:00:41,360 --> 00:00:46,800
you could go back and look at our older

23
00:00:43,600 --> 00:00:47,600
ost1 class if you want to look more into

24
00:00:46,800 --> 00:00:50,960
this

25
00:00:47,600 --> 00:00:51,680
10 plus year old system

26
00:00:50,960 --> 00:00:54,320
so

27
00:00:51,680 --> 00:00:56,480
instead if you happen to be running a

28
00:00:54,320 --> 00:01:00,640
cpu with a pch

29
00:00:56,480 --> 00:01:03,840
if you have a 5 through 9 series pch

30
00:01:00,640 --> 00:01:06,159
we want to find the lpc device id in

31
00:01:03,840 --> 00:01:07,200
what is called the pch specification

32
00:01:06,159 --> 00:01:10,640
update

33
00:01:07,200 --> 00:01:12,240
so you're going to be looking at a

34
00:01:10,640 --> 00:01:13,119
data sheet that's named something like

35
00:01:12,240 --> 00:01:16,240
this

36
00:01:13,119 --> 00:01:17,759
so the 5 and 3400 chipset specification

37
00:01:16,240 --> 00:01:19,840
update

38
00:01:17,759 --> 00:01:21,280
so in that datasheet there would be an

39
00:01:19,840 --> 00:01:23,520
element saying you know these are all

40
00:01:21,280 --> 00:01:24,720
the possible device ids so you're not

41
00:01:23,520 --> 00:01:26,960
going to like go look through every

42
00:01:24,720 --> 00:01:28,320
single data sheet to find device id

43
00:01:26,960 --> 00:01:30,240
you're going to find a device id with

44
00:01:28,320 --> 00:01:32,079
the tools that we'll cover in a second

45
00:01:30,240 --> 00:01:33,680
and i'm just saying here the way you

46
00:01:32,079 --> 00:01:36,000
would normally look it up is you'd go

47
00:01:33,680 --> 00:01:37,759
back to something like this and find it

48
00:01:36,000 --> 00:01:39,439
i'm going to give you a nicer table on

49
00:01:37,759 --> 00:01:41,520
the website to make it a little bit

50
00:01:39,439 --> 00:01:43,280
easier once you use the table then you

51
00:01:41,520 --> 00:01:44,720
have to go back and you know open the

52
00:01:43,280 --> 00:01:46,560
relevant data sheets depending on

53
00:01:44,720 --> 00:01:48,720
whether the table tells you you have

54
00:01:46,560 --> 00:01:50,640
five through nine series pch

55
00:01:48,720 --> 00:01:52,479
this is usually one of the hardest

56
00:01:50,640 --> 00:01:54,560
portions of the class when when i teach

57
00:01:52,479 --> 00:01:57,040
it live is just getting everyone to

58
00:01:54,560 --> 00:01:57,759
figure out what the right data sheet is

59
00:01:57,040 --> 00:01:59,280
so

60
00:01:57,759 --> 00:02:02,240
if you looked something up and you just

61
00:01:59,280 --> 00:02:03,759
happened to get 3b08 then my table would

62
00:02:02,240 --> 00:02:05,280
tell you hey you've got a five series

63
00:02:03,759 --> 00:02:07,280
data sheet and then you'd go off and

64
00:02:05,280 --> 00:02:09,759
grab this data sheet and you'd confirm

65
00:02:07,280 --> 00:02:12,800
oh yeah the specification update says

66
00:02:09,759 --> 00:02:15,520
that i've got this thing so spec updates

67
00:02:12,800 --> 00:02:18,080
say the ids on five through nine series

68
00:02:15,520 --> 00:02:20,239
data sheets

69
00:02:18,080 --> 00:02:22,400
on the other hand if you have a 100

70
00:02:20,239 --> 00:02:24,959
series pch or newer

71
00:02:22,400 --> 00:02:27,520
instead you would look at the pch data

72
00:02:24,959 --> 00:02:30,319
sheet specifically volume 1 of the pch

73
00:02:27,520 --> 00:02:32,879
data sheet where there is a device and

74
00:02:30,319 --> 00:02:35,280
revision id table thing and so again

75
00:02:32,879 --> 00:02:38,160
just a bunch of list of a bunch of ids

76
00:02:35,280 --> 00:02:40,480
for device 31 function 0 which is our

77
00:02:38,160 --> 00:02:41,920
lpc device

78
00:02:40,480 --> 00:02:44,239
now if you were to look at one of these

79
00:02:41,920 --> 00:02:45,760
you would actually see that the lpc can

80
00:02:44,239 --> 00:02:48,319
be called

81
00:02:45,760 --> 00:02:51,760
the it can actually be called the e-spy

82
00:02:48,319 --> 00:02:54,000
or lpc device in the 100 200 and 300

83
00:02:51,760 --> 00:02:56,080
series and then starting in 400 series

84
00:02:54,000 --> 00:02:58,239
and newers it's only called the espy

85
00:02:56,080 --> 00:03:00,560
espy is basically a newer technology

86
00:02:58,239 --> 00:03:02,879
from intel that allows them to tunnel a

87
00:03:00,560 --> 00:03:05,280
bunch of different protocols through a

88
00:03:02,879 --> 00:03:07,200
sort of packetized data structure

89
00:03:05,280 --> 00:03:11,599
mechanism instead of

90
00:03:07,200 --> 00:03:11,599
running the old lpc lines for instance

91
00:03:12,879 --> 00:03:18,239
all right if instead my table on the

92
00:03:15,120 --> 00:03:20,879
website says that you have a four

93
00:03:18,239 --> 00:03:22,800
through ninth series core cpu what it's

94
00:03:20,879 --> 00:03:23,920
basically saying is that your lpc device

95
00:03:22,800 --> 00:03:26,319
has you know

96
00:03:23,920 --> 00:03:28,560
kind of been merged up into your cpu

97
00:03:26,319 --> 00:03:31,599
data sheets and specifically there will

98
00:03:28,560 --> 00:03:34,560
be what's called a cpu io data sheet

99
00:03:31,599 --> 00:03:37,360
so on a fifth generation core series

100
00:03:34,560 --> 00:03:40,239
thing there's a i o data sheet so four

101
00:03:37,360 --> 00:03:42,480
through nine series core cpus there will

102
00:03:40,239 --> 00:03:44,319
be an i o data sheet to

103
00:03:42,480 --> 00:03:47,120
prove that the lpc device has the

104
00:03:44,319 --> 00:03:50,799
particular device id that you look up

105
00:03:47,120 --> 00:03:53,439
and finally the case 4 is if you're on

106
00:03:50,799 --> 00:03:55,280
10th series core cpus or later if you

107
00:03:53,439 --> 00:03:58,239
have a very new machine

108
00:03:55,280 --> 00:04:00,959
then you it goes back to the pch data

109
00:03:58,239 --> 00:04:02,640
sheet the on package pch data sheet so

110
00:04:00,959 --> 00:04:05,200
there's going to be pch data sheets and

111
00:04:02,640 --> 00:04:06,720
on package pch database sheets and that

112
00:04:05,200 --> 00:04:07,840
again has to do with the fact that you

113
00:04:06,720 --> 00:04:11,040
know it's

114
00:04:07,840 --> 00:04:13,439
you know it's on the same package

115
00:04:11,040 --> 00:04:15,680
and so again just look up in the table

116
00:04:13,439 --> 00:04:17,759
on the website if it says that you're on

117
00:04:15,680 --> 00:04:20,079
a 10 series newer then you would

118
00:04:17,759 --> 00:04:23,520
actually go back to the 300 series

119
00:04:20,079 --> 00:04:25,520
chipset pc on package pch datasheet

120
00:04:23,520 --> 00:04:27,759
volume one

121
00:04:25,520 --> 00:04:31,280
and so you know this just makes me like

122
00:04:27,759 --> 00:04:33,440
why intel why please stop moving it

123
00:04:31,280 --> 00:04:36,080
around everywhere makes it very

124
00:04:33,440 --> 00:04:38,479
difficult to deal with

125
00:04:36,080 --> 00:04:40,560
so how do you actually get the device id

126
00:04:38,479 --> 00:04:42,479
in order to go and look it up in my

127
00:04:40,560 --> 00:04:44,560
table and find the right date find the

128
00:04:42,479 --> 00:04:46,720
right data sheets well there's a bunch

129
00:04:44,560 --> 00:04:48,720
of different ways the two most

130
00:04:46,720 --> 00:04:51,680
recommended ways are going to be to use

131
00:04:48,720 --> 00:04:53,759
either rewrite everything or chipset but

132
00:04:51,680 --> 00:04:56,320
i show a few others here and really just

133
00:04:53,759 --> 00:04:58,639
focus on what ways are mentioned on the

134
00:04:56,320 --> 00:05:00,400
website these videos slides may get out

135
00:04:58,639 --> 00:05:02,800
of date but i just want to kind of

136
00:05:00,400 --> 00:05:05,039
overview it for you once here quick

137
00:05:02,800 --> 00:05:07,199
so basically in read write everything

138
00:05:05,039 --> 00:05:09,680
you would select a pci device and then

139
00:05:07,199 --> 00:05:12,639
you'd say bus zero device one that's

140
00:05:09,680 --> 00:05:16,000
device 31 function 0 and then you would

141
00:05:12,639 --> 00:05:18,240
go to offset 2 displaying as 16 bits at

142
00:05:16,000 --> 00:05:20,479
a time so that value right there is the

143
00:05:18,240 --> 00:05:21,759
value you would look up and then you

144
00:05:20,479 --> 00:05:22,960
would basically put that into the

145
00:05:21,759 --> 00:05:25,199
website you'd search for it on the

146
00:05:22,960 --> 00:05:26,560
website if you don't find it you might

147
00:05:25,199 --> 00:05:28,639
have to look for you know what's the

148
00:05:26,560 --> 00:05:31,360
closest number because sometimes i list

149
00:05:28,639 --> 00:05:33,600
ranges when intel reserves a range but

150
00:05:31,360 --> 00:05:35,440
they don't say a specific thing is used

151
00:05:33,600 --> 00:05:36,560
in specific datasheet versions that i

152
00:05:35,440 --> 00:05:38,960
have

153
00:05:36,560 --> 00:05:41,520
so yeah you basically you know pci and

154
00:05:38,960 --> 00:05:43,840
then 16 bit values at a time device one

155
00:05:41,520 --> 00:05:46,560
f offset two and that's going to be

156
00:05:43,840 --> 00:05:49,520
thing oh good i've got animated stuff

157
00:05:46,560 --> 00:05:52,160
pci 16 bits at a time device one f and

158
00:05:49,520 --> 00:05:54,960
offset two that's going to be the value

159
00:05:52,160 --> 00:05:56,960
for your lpc device id no matter what

160
00:05:54,960 --> 00:06:00,880
hardware you're using that will always

161
00:05:56,960 --> 00:06:00,880
be where the device id is stored

162
00:06:02,160 --> 00:06:07,039
now if you wanted to find this on linux

163
00:06:03,759 --> 00:06:11,039
for instance you could use lspci saying

164
00:06:07,039 --> 00:06:13,039
bus 0 device 1f off function 0 and then

165
00:06:11,039 --> 00:06:17,520
it would give you something like this

166
00:06:13,039 --> 00:06:22,560
and the 8086 is offset 0 and the 8c 4e

167
00:06:17,520 --> 00:06:22,560
is offset 2. so you want the offset too

168
00:06:22,800 --> 00:06:28,800
and you could do the same thing on mac

169
00:06:24,479 --> 00:06:30,639
os using ioreg and looking for lpcb

170
00:06:28,800 --> 00:06:32,960
and it would basically give you the same

171
00:06:30,639 --> 00:06:35,759
kind of thing it would say pci 8086

172
00:06:32,960 --> 00:06:39,120
that's just intel's id at offset 0 and

173
00:06:35,759 --> 00:06:41,360
then a particular device id a313 in this

174
00:06:39,120 --> 00:06:41,360
case

175
00:06:42,800 --> 00:06:46,720
and then the way that's preferred is to

176
00:06:44,639 --> 00:06:48,479
use chipset whatever operating system

177
00:06:46,720 --> 00:06:50,160
you're on whether you're booted off of a

178
00:06:48,479 --> 00:06:51,919
usb whether you're in windows whether in

179
00:06:50,160 --> 00:06:55,680
your linux

180
00:06:51,919 --> 00:06:59,120
run sudo python chipset util pci read

181
00:06:55,680 --> 00:07:01,199
zero so that's bust zero device one f

182
00:06:59,120 --> 00:07:02,880
that's 31

183
00:07:01,199 --> 00:07:06,080
function zero

184
00:07:02,880 --> 00:07:09,520
offset two showing two bytes and here

185
00:07:06,080 --> 00:07:11,360
it'll show you 8c 4e

186
00:07:09,520 --> 00:07:12,800
and it actually shows you up there and

187
00:07:11,360 --> 00:07:14,960
down there

188
00:07:12,800 --> 00:07:16,639
now if i hadn't made this table for you

189
00:07:14,960 --> 00:07:20,160
on the website what you would typically

190
00:07:16,639 --> 00:07:22,800
do is you'd go to a website like pci ids

191
00:07:20,160 --> 00:07:25,280
and you would just go to this big text

192
00:07:22,800 --> 00:07:27,199
file full of pci ids you'd search for

193
00:07:25,280 --> 00:07:29,680
your particular id and it would tell you

194
00:07:27,199 --> 00:07:31,840
something like oh this is a q87 express

195
00:07:29,680 --> 00:07:35,599
lpc controller and that would be your

196
00:07:31,840 --> 00:07:37,039
hint that this is an 8 series pch

197
00:07:35,599 --> 00:07:39,840
and then from there you would go look at

198
00:07:37,039 --> 00:07:41,680
the 8 series pch data sheets but again

199
00:07:39,840 --> 00:07:44,960
i've you know provided on the website

200
00:07:41,680 --> 00:07:47,199
the translation from these values to the

201
00:07:44,960 --> 00:07:49,440
relevant data sheets

202
00:07:47,199 --> 00:07:53,039
and here again you won't necessarily

203
00:07:49,440 --> 00:07:55,680
find your exact device id on my table or

204
00:07:53,039 --> 00:07:57,440
even in these pci ids thing because

205
00:07:55,680 --> 00:07:59,120
intel reserves particular ranges and

206
00:07:57,440 --> 00:08:01,840
they don't necessarily always say that

207
00:07:59,120 --> 00:08:04,240
they're in use so for my you know last

208
00:08:01,840 --> 00:08:07,280
generation t2 mac

209
00:08:04,240 --> 00:08:10,160
it says a313 and that just straight up

210
00:08:07,280 --> 00:08:13,039
does not exist in the

211
00:08:10,160 --> 00:08:14,960
pcie device ids you may be able to find

212
00:08:13,039 --> 00:08:17,520
it in some you know open source pro

213
00:08:14,960 --> 00:08:19,599
projects etc but i i can tell you it's

214
00:08:17,520 --> 00:08:21,680
not actually listed in the data sheets

215
00:08:19,599 --> 00:08:23,599
for the relevant thing so in that case

216
00:08:21,680 --> 00:08:26,879
you look for you know what's the closest

217
00:08:23,599 --> 00:08:28,479
thing you know canon lake pch

218
00:08:26,879 --> 00:08:31,199
and then you would you know look up the

219
00:08:28,479 --> 00:08:33,120
canon like pch data sheets

220
00:08:31,199 --> 00:08:34,959
and if you did that then you would find

221
00:08:33,120 --> 00:08:36,640
something like this in the 300 series

222
00:08:34,959 --> 00:08:39,680
data sheets it actually says intel

223
00:08:36,640 --> 00:08:40,880
reserves device id a300 through three

224
00:08:39,680 --> 00:08:43,839
one f

225
00:08:40,880 --> 00:08:45,760
and consequently the a313 is within that

226
00:08:43,839 --> 00:08:47,519
range so you know okay yes this is the

227
00:08:45,760 --> 00:08:49,440
right data sheet but again as you can

228
00:08:47,519 --> 00:08:51,519
see it doesn't actually list this as a

229
00:08:49,440 --> 00:08:52,959
specific thing that's in use and that's

230
00:08:51,519 --> 00:08:54,720
why this is one of the most difficult

231
00:08:52,959 --> 00:08:56,320
portions of the class when taught in

232
00:08:54,720 --> 00:08:58,480
person because i have to run around and

233
00:08:56,320 --> 00:09:00,160
try to help everyone figure these out

234
00:08:58,480 --> 00:09:02,240
all right now it's time for you to go to

235
00:09:00,160 --> 00:09:04,080
the website you know so first use one of

236
00:09:02,240 --> 00:09:06,000
these tools the tools will be listed how

237
00:09:04,080 --> 00:09:07,279
to use them on the website

238
00:09:06,000 --> 00:09:10,000
and it'll tell you how to find your

239
00:09:07,279 --> 00:09:12,640
device id then you use the device id to

240
00:09:10,000 --> 00:09:14,720
do the mega manual lookup and that'll

241
00:09:12,640 --> 00:09:18,320
tell you which data sheet is relevant

242
00:09:14,720 --> 00:09:18,320
for your lpc device


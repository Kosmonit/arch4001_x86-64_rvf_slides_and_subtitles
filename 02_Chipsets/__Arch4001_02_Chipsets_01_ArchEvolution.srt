1
00:00:00,240 --> 00:00:04,400
all right moving along on our path

2
00:00:02,240 --> 00:00:07,040
towards eventual flash right protection

3
00:00:04,400 --> 00:00:09,360
we need to learn about chipsets these

4
00:00:07,040 --> 00:00:11,120
are chips that have some of the

5
00:00:09,360 --> 00:00:13,920
chunks of hardware that we care about as

6
00:00:11,120 --> 00:00:17,199
we make our way towards flash writing

7
00:00:13,920 --> 00:00:18,880
so in architecture 2001 we said that you

8
00:00:17,199 --> 00:00:21,439
know when you want to know where ports

9
00:00:18,880 --> 00:00:23,680
come from for port io one does not

10
00:00:21,439 --> 00:00:25,359
simply read the intel software

11
00:00:23,680 --> 00:00:28,000
development manuals

12
00:00:25,359 --> 00:00:30,240
one reads the chipset documentation

13
00:00:28,000 --> 00:00:33,360
and so we had said that a previous

14
00:00:30,240 --> 00:00:35,200
architecture was that you had cpus mchs

15
00:00:33,360 --> 00:00:38,960
which are memory controller hubs and

16
00:00:35,200 --> 00:00:40,800
ich's i o controller hubs

17
00:00:38,960 --> 00:00:43,040
that architecture generally has moved

18
00:00:40,800 --> 00:00:44,399
towards the cpu and a platform

19
00:00:43,040 --> 00:00:45,600
controller hub

20
00:00:44,399 --> 00:00:47,440
and then

21
00:00:45,600 --> 00:00:49,760
over time things continue to look more

22
00:00:47,440 --> 00:00:52,079
and more like a system on a chip

23
00:00:49,760 --> 00:00:54,399
which may have some extra data sheet

24
00:00:52,079 --> 00:00:57,760
associated with it so ports are

25
00:00:54,399 --> 00:00:59,760
generally in ich data sheets where

26
00:00:57,760 --> 00:01:02,559
systems have this architecture

27
00:00:59,760 --> 00:01:04,720
pch data sheets where they have this one

28
00:01:02,559 --> 00:01:07,760
and for things that look more sockish

29
00:01:04,720 --> 00:01:09,680
some generations of cpus have an i o

30
00:01:07,760 --> 00:01:12,240
datasheet although it is then you know

31
00:01:09,680 --> 00:01:13,280
started to move back into pch data

32
00:01:12,240 --> 00:01:15,439
sheets

33
00:01:13,280 --> 00:01:17,280
so when we revisit port io later in the

34
00:01:15,439 --> 00:01:19,360
class this will be an example of where

35
00:01:17,280 --> 00:01:21,680
we're going to be looking

36
00:01:19,360 --> 00:01:23,840
now this is out of scope for this class

37
00:01:21,680 --> 00:01:25,360
we're not going to consider things that

38
00:01:23,840 --> 00:01:28,000
are the sole because that's over a

39
00:01:25,360 --> 00:01:29,920
decade old and we want to stay on more

40
00:01:28,000 --> 00:01:31,280
recent and relevant things

41
00:01:29,920 --> 00:01:33,200
things that you're more likely to be

42
00:01:31,280 --> 00:01:34,640
running on your particular system as you

43
00:01:33,200 --> 00:01:36,240
take this class

44
00:01:34,640 --> 00:01:38,000
but it still helps to understand a

45
00:01:36,240 --> 00:01:40,960
little bit of the history of how we got

46
00:01:38,000 --> 00:01:43,680
here and so to that end here was the

47
00:01:40,960 --> 00:01:45,759
older mch based things memory controller

48
00:01:43,680 --> 00:01:47,759
hubs those were actually called a

49
00:01:45,759 --> 00:01:49,840
chipset in and of themselves even though

50
00:01:47,759 --> 00:01:52,479
it was not a chipset it was a single

51
00:01:49,840 --> 00:01:54,320
chip so sometimes chipset refers to just

52
00:01:52,479 --> 00:01:56,799
kind of whatever the thing is that's

53
00:01:54,320 --> 00:01:58,719
occupying the space below the processor

54
00:01:56,799 --> 00:02:01,200
and it could be a single chip rather

55
00:01:58,719 --> 00:02:02,880
than a chipset certainly in the pch case

56
00:02:01,200 --> 00:02:05,119
as we'll learn about still called the

57
00:02:02,880 --> 00:02:07,680
chipset but yet it's a single chip

58
00:02:05,119 --> 00:02:09,520
but back then it was an actual chip set

59
00:02:07,680 --> 00:02:10,879
even though this was called the chipset

60
00:02:09,520 --> 00:02:13,040
so you know

61
00:02:10,879 --> 00:02:14,800
just take the take the language with a

62
00:02:13,040 --> 00:02:16,319
grain of salt that it's not always going

63
00:02:14,800 --> 00:02:18,000
to be intuitive

64
00:02:16,319 --> 00:02:20,239
so back then you had the memory

65
00:02:18,000 --> 00:02:22,319
controller hub which had direct access

66
00:02:20,239 --> 00:02:23,680
to the memory so the processor did not

67
00:02:22,319 --> 00:02:25,440
actually have direct access to the

68
00:02:23,680 --> 00:02:27,599
memory had to go through the front side

69
00:02:25,440 --> 00:02:30,160
bus to get to this chipset or memory

70
00:02:27,599 --> 00:02:32,560
controller dram controller in order to

71
00:02:30,160 --> 00:02:34,400
get to memory then you had the i o

72
00:02:32,560 --> 00:02:36,959
controller hub which had access to a

73
00:02:34,400 --> 00:02:38,879
bunch of the slower low speed

74
00:02:36,959 --> 00:02:40,720
input output interfaces as well as some

75
00:02:38,879 --> 00:02:43,360
of the pci interfaces

76
00:02:40,720 --> 00:02:45,200
in this sort of architecture this was

77
00:02:43,360 --> 00:02:47,280
unofficially typically called a

78
00:02:45,200 --> 00:02:49,120
northbridge south bridge sort of

79
00:02:47,280 --> 00:02:50,800
configuration so sometimes you'll hear

80
00:02:49,120 --> 00:02:52,800
about the north bridge that would refer

81
00:02:50,800 --> 00:02:55,599
to an mch and a south bridge would be an

82
00:02:52,800 --> 00:02:57,280
ich and so ultimately it was the job of

83
00:02:55,599 --> 00:02:59,440
the bios when it starts running on the

84
00:02:57,280 --> 00:03:01,440
processor to fiddle with bits on these

85
00:02:59,440 --> 00:03:03,120
various chips and configure them into

86
00:03:01,440 --> 00:03:05,280
the way that the processor wants to

87
00:03:03,120 --> 00:03:08,000
execute things like initializing access

88
00:03:05,280 --> 00:03:08,879
to memory discovering pci devices and so

89
00:03:08,000 --> 00:03:11,120
forth

90
00:03:08,879 --> 00:03:14,080
now this architecture changed over time

91
00:03:11,120 --> 00:03:17,200
because the interface between the mch

92
00:03:14,080 --> 00:03:19,440
and the ich was a bit of a bottleneck so

93
00:03:17,200 --> 00:03:22,080
i found this delightful picture of m ms

94
00:03:19,440 --> 00:03:24,239
which perfectly illustrates a bottleneck

95
00:03:22,080 --> 00:03:26,400
so that was turning into a bottleneck

96
00:03:24,239 --> 00:03:29,280
and so what intel did was they took some

97
00:03:26,400 --> 00:03:32,080
of the stuff from the ich and merged it

98
00:03:29,280 --> 00:03:34,000
in with the mch they took some of the

99
00:03:32,080 --> 00:03:35,599
stuff from the mch and they merged it in

100
00:03:34,000 --> 00:03:37,360
with the processor things like memory

101
00:03:35,599 --> 00:03:39,120
access where you would of course want

102
00:03:37,360 --> 00:03:41,280
your processor to have as quick of

103
00:03:39,120 --> 00:03:43,360
access as possible

104
00:03:41,280 --> 00:03:45,440
so bits from here merged into their bits

105
00:03:43,360 --> 00:03:47,040
from there merged into there and that

106
00:03:45,440 --> 00:03:49,120
leaves you with more of our modern

107
00:03:47,040 --> 00:03:51,599
architecture which is the use of the

108
00:03:49,120 --> 00:03:55,439
platform controller hub so you have a

109
00:03:51,599 --> 00:03:57,200
processor you now have this faster dmi2

110
00:03:55,439 --> 00:03:59,920
interface so a little bit less of a

111
00:03:57,200 --> 00:04:02,879
bottleneck here now and so intel did

112
00:03:59,920 --> 00:04:04,319
this around 2008 and amd did this around

113
00:04:02,879 --> 00:04:06,319
2003

114
00:04:04,319 --> 00:04:08,159
and so it starts sort of merging and

115
00:04:06,319 --> 00:04:10,799
things start looking more and more like

116
00:04:08,159 --> 00:04:12,640
a system on a chip as chips get combined

117
00:04:10,799 --> 00:04:14,319
into single packages

118
00:04:12,640 --> 00:04:16,160
but the stuff that we tend to care about

119
00:04:14,319 --> 00:04:19,120
in this class are things that were

120
00:04:16,160 --> 00:04:21,600
attached to the ics in that architecture

121
00:04:19,120 --> 00:04:23,680
and the pch in the modern architecture

122
00:04:21,600 --> 00:04:25,360
things like the spy flash chip that's

123
00:04:23,680 --> 00:04:27,440
where the bios is stored on modern

124
00:04:25,360 --> 00:04:29,360
systems firmware hub is where it used to

125
00:04:27,440 --> 00:04:32,000
be stored on older systems but we're

126
00:04:29,360 --> 00:04:34,160
mostly going to care about the spy flash

127
00:04:32,000 --> 00:04:36,400
and the lpc bus only because there's

128
00:04:34,160 --> 00:04:38,720
some legacy chunk of hardware in here

129
00:04:36,400 --> 00:04:40,560
that still has to do with spy flash

130
00:04:38,720 --> 00:04:43,520
access controls because of how the

131
00:04:40,560 --> 00:04:46,240
access controls used to work

132
00:04:43,520 --> 00:04:48,800
and if we dug even deeper into the

133
00:04:46,240 --> 00:04:50,320
particulars of the inside of a pch we

134
00:04:48,800 --> 00:04:51,680
would again see those bits that we're

135
00:04:50,320 --> 00:04:54,400
going to care about in this class things

136
00:04:51,680 --> 00:04:58,080
like the pci express interface

137
00:04:54,400 --> 00:05:00,479
the spy interface or the lpc

138
00:04:58,080 --> 00:05:02,560
firmware hub interface this is sort of

139
00:05:00,479 --> 00:05:04,080
the more common view that you would see

140
00:05:02,560 --> 00:05:06,160
in embedded systems where you start

141
00:05:04,080 --> 00:05:08,320
digging down into a particular chip and

142
00:05:06,160 --> 00:05:11,919
you see all the various lines that are

143
00:05:08,320 --> 00:05:14,160
available corresponding to pins on a cpu

144
00:05:11,919 --> 00:05:16,160
and just because the picture of m ms

145
00:05:14,160 --> 00:05:18,639
came with a second picture of m ms i

146
00:05:16,160 --> 00:05:21,680
have to point out that this dmi2

147
00:05:18,639 --> 00:05:24,320
interface allows for much more data to

148
00:05:21,680 --> 00:05:26,000
go across this interface

149
00:05:24,320 --> 00:05:27,840
so if you thought that those look like

150
00:05:26,000 --> 00:05:30,320
system on a chip well starting in the

151
00:05:27,840 --> 00:05:32,479
fourth generation cpus they really

152
00:05:30,320 --> 00:05:35,440
looked like a system on a chip but if

153
00:05:32,479 --> 00:05:37,039
you dug deeper into this you would see

154
00:05:35,440 --> 00:05:38,960
that although there are things like the

155
00:05:37,039 --> 00:05:41,199
bios that we still care about all

156
00:05:38,960 --> 00:05:43,199
attached to you know nominally attached

157
00:05:41,199 --> 00:05:45,440
to the processor when you look at some

158
00:05:43,199 --> 00:05:46,960
subsequent intel marketing information

159
00:05:45,440 --> 00:05:49,360
you'll actually see that what you're

160
00:05:46,960 --> 00:05:53,199
really looking at is a multi-chip

161
00:05:49,360 --> 00:05:55,199
package so if a cpu and a pch looks like

162
00:05:53,199 --> 00:05:56,720
this then you could have a multi-chip

163
00:05:55,199 --> 00:05:58,560
package where they just basically take

164
00:05:56,720 --> 00:06:00,720
the pch and they slap it on the same

165
00:05:58,560 --> 00:06:02,560
package with the cpu and that would be a

166
00:06:00,720 --> 00:06:04,800
u series and this would be the y series

167
00:06:02,560 --> 00:06:07,360
more for you know ultra portables and

168
00:06:04,800 --> 00:06:08,960
things like that so basically it's not

169
00:06:07,360 --> 00:06:10,880
really a sock at this point it's not a

170
00:06:08,960 --> 00:06:13,360
single system on a chip it's two chips

171
00:06:10,880 --> 00:06:15,680
on a package

172
00:06:13,360 --> 00:06:17,680
and indeed the eighth generation diagram

173
00:06:15,680 --> 00:06:20,319
shows that a little bit more explicitly

174
00:06:17,680 --> 00:06:22,400
processor and pch two chips on the same

175
00:06:20,319 --> 00:06:24,720
package and this is not like an overlay

176
00:06:22,400 --> 00:06:25,600
by me this is an actual screenshot from

177
00:06:24,720 --> 00:06:27,360
the

178
00:06:25,600 --> 00:06:28,880
data sheet showing that yes there's all

179
00:06:27,360 --> 00:06:32,319
these things there's flash embedded

180
00:06:28,880 --> 00:06:35,039
controllers flash tpm pci camera blah

181
00:06:32,319 --> 00:06:36,880
blah all of this attached to this

182
00:06:35,039 --> 00:06:38,319
multi-chip package

183
00:06:36,880 --> 00:06:40,240
but then when you get to the 10th

184
00:06:38,319 --> 00:06:42,160
generation core chips you'll see that

185
00:06:40,240 --> 00:06:44,639
the intel documentation refers to them

186
00:06:42,160 --> 00:06:46,800
as actually being on the same die that

187
00:06:44,639 --> 00:06:48,400
means that these are the same piece of

188
00:06:46,800 --> 00:06:50,400
silicon they're not two pieces of

189
00:06:48,400 --> 00:06:52,319
silicon put on the same package they're

190
00:06:50,400 --> 00:06:54,479
actually etched out of the same piece of

191
00:06:52,319 --> 00:06:56,479
silicon but they're actually still

192
00:06:54,479 --> 00:06:58,479
logically separated you know chunks of

193
00:06:56,479 --> 00:07:01,360
hardware chunks of design that were

194
00:06:58,479 --> 00:07:03,280
originally intended for a separate pch

195
00:07:01,360 --> 00:07:05,759
in which intel could still sell as a

196
00:07:03,280 --> 00:07:07,759
separate pch and manufacture on their

197
00:07:05,759 --> 00:07:09,599
own chip they're just all manufactured

198
00:07:07,759 --> 00:07:12,160
onto the same slice of silicon if it

199
00:07:09,599 --> 00:07:14,479
happens to be for one of the mobile form

200
00:07:12,160 --> 00:07:16,319
factors so it's getting right up to the

201
00:07:14,479 --> 00:07:17,599
edge of being a system on a chip except

202
00:07:16,319 --> 00:07:18,960
they're still separated there's still

203
00:07:17,599 --> 00:07:20,319
going to be a bottleneck there's still

204
00:07:18,960 --> 00:07:22,560
going to be something like a dmi

205
00:07:20,319 --> 00:07:25,120
interface between these two

206
00:07:22,560 --> 00:07:27,360
logically separated hardware units

207
00:07:25,120 --> 00:07:29,280
intel does actually make proper system

208
00:07:27,360 --> 00:07:31,680
on a chips these are typically the atom

209
00:07:29,280 --> 00:07:33,919
series processors more used in embedded

210
00:07:31,680 --> 00:07:35,840
systems rather than laptops or servers

211
00:07:33,919 --> 00:07:37,840
or anything like that and there you

212
00:07:35,840 --> 00:07:40,639
really do have everything all of the

213
00:07:37,840 --> 00:07:42,880
hardware units all in a single chip all

214
00:07:40,639 --> 00:07:44,960
connected through some internal bus in

215
00:07:42,880 --> 00:07:46,960
this class we mostly focus on the

216
00:07:44,960 --> 00:07:48,800
desktop or server grade

217
00:07:46,960 --> 00:07:49,919
processors which you are more likely to

218
00:07:48,800 --> 00:07:51,759
be running

219
00:07:49,919 --> 00:07:54,479
and just personally you know i haven't

220
00:07:51,759 --> 00:07:56,720
encountered atom processors that much i

221
00:07:54,479 --> 00:07:58,720
believe at some point they actually

222
00:07:56,720 --> 00:08:00,639
discontinued them you know sometime in

223
00:07:58,720 --> 00:08:03,039
the last few years although they're

224
00:08:00,639 --> 00:08:04,639
still you know that name has been

225
00:08:03,039 --> 00:08:06,560
retired but they're still you know

226
00:08:04,639 --> 00:08:09,360
utilizing that technology in certain

227
00:08:06,560 --> 00:08:09,360
situations


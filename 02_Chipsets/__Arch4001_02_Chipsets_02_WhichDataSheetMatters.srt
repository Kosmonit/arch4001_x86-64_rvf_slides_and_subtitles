1
00:00:00,320 --> 00:00:04,319
now the thing about intel is that they

2
00:00:02,000 --> 00:00:06,160
loves them some code names and

3
00:00:04,319 --> 00:00:07,839
frequently you have to actually learn

4
00:00:06,160 --> 00:00:10,559
the code name of the particular

5
00:00:07,839 --> 00:00:12,320
generation of cpu or pch that you're

6
00:00:10,559 --> 00:00:14,400
dealing with in order to look up the

7
00:00:12,320 --> 00:00:16,400
correct data sheets and so what we're

8
00:00:14,400 --> 00:00:18,480
concerned about in this section is

9
00:00:16,400 --> 00:00:20,880
finding the right data sheet for your

10
00:00:18,480 --> 00:00:23,039
particular system so that you can follow

11
00:00:20,880 --> 00:00:25,039
along and do the exercises on your

12
00:00:23,039 --> 00:00:26,480
system instead of necessarily a

13
00:00:25,039 --> 00:00:28,560
reference system

14
00:00:26,480 --> 00:00:31,679
so the code names were pretty simple for

15
00:00:28,560 --> 00:00:33,440
a while but then we have this era where

16
00:00:31,679 --> 00:00:35,280
i sort of think of it like intel

17
00:00:33,440 --> 00:00:37,520
wandering in the desert

18
00:00:35,280 --> 00:00:39,200
there was a number of years where they

19
00:00:37,520 --> 00:00:41,040
were having a lot of trouble keeping up

20
00:00:39,200 --> 00:00:44,000
with their tick tock schedule where they

21
00:00:41,040 --> 00:00:46,960
basically you know shrink the transistor

22
00:00:44,000 --> 00:00:48,879
process and then you know do new micro

23
00:00:46,960 --> 00:00:50,559
architectures and things like that so

24
00:00:48,879 --> 00:00:52,559
they had a bunch of problems shrinking

25
00:00:50,559 --> 00:00:55,039
things and so they had to keep you know

26
00:00:52,559 --> 00:00:57,120
selling new chips so things got very

27
00:00:55,039 --> 00:01:00,399
messy for a while here between you know

28
00:00:57,120 --> 00:01:02,480
2015 and 2020 or so but while this is a

29
00:01:00,399 --> 00:01:04,720
quick cut that i just made from you know

30
00:01:02,480 --> 00:01:06,080
some wikipedia pages and so forth i'm

31
00:01:04,720 --> 00:01:08,400
not even going to try to keep this

32
00:01:06,080 --> 00:01:10,320
accurate in the videos you should see

33
00:01:08,400 --> 00:01:12,479
the class website to see the more

34
00:01:10,320 --> 00:01:14,240
accurate version of this but what you're

35
00:01:12,479 --> 00:01:16,560
going to be doing is trying to find out

36
00:01:14,240 --> 00:01:18,240
what is the code name for the particular

37
00:01:16,560 --> 00:01:20,000
cpu generation that you're dealing with

38
00:01:18,240 --> 00:01:22,799
so you can look it up in the

39
00:01:20,000 --> 00:01:26,159
organized data sheets that i provide and

40
00:01:22,799 --> 00:01:28,799
what is the code name for the or the pch

41
00:01:26,159 --> 00:01:30,799
series for the particular pch that might

42
00:01:28,799 --> 00:01:32,560
be on your system or might be sort of

43
00:01:30,799 --> 00:01:33,840
integrated onto the same package as your

44
00:01:32,560 --> 00:01:35,600
cpu

45
00:01:33,840 --> 00:01:37,439
now there's going to be two main things

46
00:01:35,600 --> 00:01:39,680
that we care about in these data sheets

47
00:01:37,439 --> 00:01:41,840
and the thing is they move around and so

48
00:01:39,680 --> 00:01:43,840
i'm going to start with a very old view

49
00:01:41,840 --> 00:01:47,119
of a data sheet again this is back to

50
00:01:43,840 --> 00:01:50,880
the technically out of scope mchich or

51
00:01:47,119 --> 00:01:53,200
gmch graphics and memory controller hub

52
00:01:50,880 --> 00:01:55,119
so this is a better picture than exists

53
00:01:53,200 --> 00:01:56,560
in the newer data sheets so i want to

54
00:01:55,119 --> 00:01:58,719
focus on this

55
00:01:56,560 --> 00:02:01,759
the first thing is that from the cpu's

56
00:01:58,719 --> 00:02:05,119
perspective what it sees is some sort of

57
00:02:01,759 --> 00:02:07,920
pci configuration window so there's some

58
00:02:05,119 --> 00:02:10,080
sort of pci interface to these little

59
00:02:07,920 --> 00:02:12,239
chunks of hardware down in these other

60
00:02:10,080 --> 00:02:14,239
process down in these other chips so

61
00:02:12,239 --> 00:02:16,160
whether it's an old style northward

62
00:02:14,239 --> 00:02:19,360
southbridge configuration or a newer

63
00:02:16,160 --> 00:02:21,599
style pch configuration there's pci way

64
00:02:19,360 --> 00:02:23,520
to get access to the stuff we care about

65
00:02:21,599 --> 00:02:25,760
so what do we care about well the first

66
00:02:23,520 --> 00:02:28,160
thing that we care about is a thing

67
00:02:25,760 --> 00:02:30,800
called the dram controller and it's on

68
00:02:28,160 --> 00:02:32,319
pci device zero

69
00:02:30,800 --> 00:02:33,360
bus zero

70
00:02:32,319 --> 00:02:35,040
and so

71
00:02:33,360 --> 00:02:37,599
pci essentially acts as sort of the

72
00:02:35,040 --> 00:02:39,840
backbone of this system and it's going

73
00:02:37,599 --> 00:02:41,519
to be how the cpu interfaces with all

74
00:02:39,840 --> 00:02:44,000
these chunks of hardware

75
00:02:41,519 --> 00:02:46,400
and so this dram controller is the thing

76
00:02:44,000 --> 00:02:48,640
that's going to have stuff about the

77
00:02:46,400 --> 00:02:50,800
memory map configuration that's used for

78
00:02:48,640 --> 00:02:52,239
things like getting access to other pci

79
00:02:50,800 --> 00:02:54,080
devices

80
00:02:52,239 --> 00:02:56,640
configuring the setup of system

81
00:02:54,080 --> 00:02:58,560
management mode in some cases some

82
00:02:56,640 --> 00:03:00,000
access control bits that we care about

83
00:02:58,560 --> 00:03:01,760
so the first thing we care about in this

84
00:03:00,000 --> 00:03:03,120
class is how do we find the dram

85
00:03:01,760 --> 00:03:05,120
controller

86
00:03:03,120 --> 00:03:08,640
the second thing we care about is the

87
00:03:05,120 --> 00:03:10,879
lpc device and this is pci bus 0 device

88
00:03:08,640 --> 00:03:13,120
31 function 0. we'll learn more about

89
00:03:10,879 --> 00:03:15,440
buses and devices and functions in the

90
00:03:13,120 --> 00:03:17,519
pci section but you can see already just

91
00:03:15,440 --> 00:03:18,640
like in order to find these two devices

92
00:03:17,519 --> 00:03:20,159
we're going to need to know something

93
00:03:18,640 --> 00:03:23,440
about pci

94
00:03:20,159 --> 00:03:26,159
so we hit upon this lpc or low pin count

95
00:03:23,440 --> 00:03:28,239
interface device frequently but we

96
00:03:26,159 --> 00:03:30,959
actually don't ever care about it for

97
00:03:28,239 --> 00:03:32,799
lpc traffic proper the only reason we

98
00:03:30,959 --> 00:03:36,159
care about it is because on these older

99
00:03:32,799 --> 00:03:38,159
systems that had a firmware hub

100
00:03:36,159 --> 00:03:40,239
they would have the access control bits

101
00:03:38,159 --> 00:03:42,959
for the firmware hub in the lpc device

102
00:03:40,239 --> 00:03:45,120
because the firmware hub was off on lpc

103
00:03:42,959 --> 00:03:47,680
modern systems although they use a spy

104
00:03:45,120 --> 00:03:49,760
flash device still have this little

105
00:03:47,680 --> 00:03:52,560
hardware block that controls access

106
00:03:49,760 --> 00:03:54,560
control for the spy flash device

107
00:03:52,560 --> 00:03:56,159
so again we care about the lpc device

108
00:03:54,560 --> 00:03:58,080
because of access controls but we don't

109
00:03:56,159 --> 00:04:00,000
really care about it because of lpc

110
00:03:58,080 --> 00:04:02,319
traffic or anything like that that's not

111
00:04:00,000 --> 00:04:04,159
what's used anymore so dram controller

112
00:04:02,319 --> 00:04:07,040
and lpc device these are the two things

113
00:04:04,159 --> 00:04:08,879
we need to find in your data sheets

114
00:04:07,040 --> 00:04:10,319
now what it looked like back then when

115
00:04:08,879 --> 00:04:12,720
it was the oldest thing is you had the

116
00:04:10,319 --> 00:04:14,799
dram controller in the mch we said the

117
00:04:12,720 --> 00:04:17,440
cpu didn't have direct access to ram

118
00:04:14,799 --> 00:04:20,400
back then the memory controller did and

119
00:04:17,440 --> 00:04:22,880
the lpc device in the ich

120
00:04:20,400 --> 00:04:25,680
as the system moved to a pch

121
00:04:22,880 --> 00:04:28,240
architecture instead you had the dram

122
00:04:25,680 --> 00:04:31,360
controller merged up into the cpu and

123
00:04:28,240 --> 00:04:33,600
the lpc device in the pch

124
00:04:31,360 --> 00:04:36,320
on the newest systems it's not actually

125
00:04:33,600 --> 00:04:38,560
that the lpc device exists in the cpu

126
00:04:36,320 --> 00:04:41,280
itself here i'm only representing where

127
00:04:38,560 --> 00:04:42,800
it exists in the data sheets so i'm just

128
00:04:41,280 --> 00:04:44,720
saying here that you'll actually be

129
00:04:42,800 --> 00:04:47,919
looking at cpu data sheets rather than

130
00:04:44,720 --> 00:04:52,560
pch data sheets of a form in order to

131
00:04:47,919 --> 00:04:52,560
find the lpc device on some systems

